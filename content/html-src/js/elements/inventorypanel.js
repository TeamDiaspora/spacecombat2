var InventoryPanelTemplate;
var InventoryItemTemplate;
var PreviouslySelectedInventoryItem;
var InventoryData = {};

function OnInventoryDataUpdated(Data)
{
    InventoryData = Data;
}

function GetInventoryPanelSettings(Id)
{
    return InventoryData[Id];
}

function RefreshInventoryPanels()
{
    spacecombat.UpdateInventoryData(OnInventoryDataUpdated);

    const Elements = document.getElementsByClassName('sc2-inventory-panel');
    for (Element of Elements) {
        var OldScrollBox = document.getElementById(Element.id + "-scrollbox");
        var OldScrollPos;
        if (OldScrollBox) {
            OldScrollPos = OldScrollBox.scrollTop;
        }

        var OldItemId;
        if (PreviouslySelectedInventoryItem) {
            OldItemId = PreviouslySelectedInventoryItem.id;
            PreviouslySelectedInventoryItem = null;
        }

        Element.innerHTML = InventoryPanelTemplate(GetInventoryPanelSettings(Element.id));

        if (OldScrollPos) {
            var NewScrollBox = document.getElementById(Element.id + "-scrollbox");
            if (NewScrollBox) {
                NewScrollBox.scrollTop = OldScrollPos;
            }
        }

        if (OldItemId) {
            InventoryItemSelected(OldItemId);
        }
    }
}

function SetupInventoryPanels()
{
    var ItemTemplateHTML = document.getElementById("template_inventoryitem").innerHTML;
    Handlebars.registerPartial("InventoryItem", ItemTemplateHTML);
    var PanelTemplateScript = document.getElementById("template_inventorypanel");
    var PanelTemplateHTML = PanelTemplateScript.innerHTML;
    InventoryPanelTemplate = Handlebars.compile(PanelTemplateHTML);

    spacecombat.UpdateInventoryData(OnInventoryDataUpdated);

    const Elements = document.getElementsByClassName('sc2-inventory-panel');
    for (Element of Elements) {
        Element.innerHTML = InventoryPanelTemplate(GetInventoryPanelSettings(Element.id));
    }
};

function InventoryItemSelected(ItemID)
{
    if (PreviouslySelectedInventoryItem)
    {
        PreviouslySelectedInventoryItem.classList.remove("selected");
    }

    var InventoryItem = document.getElementById(ItemID);
    if (InventoryItem != PreviouslySelectedInventoryItem)
    {
        InventoryItem.classList.add("selected");
        PreviouslySelectedInventoryItem = InventoryItem;
    }
    else
    {
        PreviouslySelectedInventoryItem = null;
    }
};