local Shaft = Material("effects/ar2ground2");

--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )
	self.StartPos	= data:GetStart()
	self.Forward	= data:GetNormal()
	self.size		= data:GetScale()
	self.vel		= data:GetMagnitude()
	self.time		= CurTime()
	--TODO: Replace this with a missile model
	self.Model		= ClientsideModel("models/slyfo/how_apfsdsround.mdl", 7)
	self.emitter = ParticleEmitter( self.StartPos )	
	self.Model:SetModelScale(self.size * 2)
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
	self.EndPos = self.StartPos + (self.Forward * self.vel * (CurTime() - self.time))
	
	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	local tr = util.TraceLine(tracedata)
	
	if tr.Hit and tr.Entity ~= self.Model then
		self.EndPos = tr.HitPos
		self.Dead = true
	end
	
	self.StartPos = self.EndPos
	self.Model:SetPos(self.StartPos)
	self.Model:SetAngles(self.Forward:Angle())
	self:SetRenderBoundsWS( self.StartPos, self.StartPos + (self.Forward * -250 * self.size) )
	
	if self.Dead then
		self.Model:Remove()
		return false
	end

    local particle = self.emitter:Add( "particles/flamelet"..math.random(1,5), self.StartPos + (self.Forward * -10))
	if particle then
		particle:SetVelocity((self.Forward * -5) )
		particle:SetLifeTime( 0 )
		particle:SetDieTime( 0.1 )
		particle:SetStartAlpha( math.Rand( 230, 255 ) )
		particle:SetEndAlpha( 0 )
		particle:SetStartSize( 17 * self.size )
		particle:SetEndSize( 0 )
		particle:SetRoll( math.Rand(0, 360) )
		particle:SetRollDelta( math.Rand(-10, 10) )
		particle:SetColor( 255 , 255 , 255 ) 
	end

    local smoke = self.emitter:Add( "particles/smokey", self.StartPos )
	if smoke then
		smoke:SetPos(self.StartPos + self.Forward * -10)
		smoke:SetVelocity( self.Forward * -math.random( 50 ) )
		smoke:SetLifeTime( 0 )
		smoke:SetDieTime( 0.5 )
		smoke:SetStartAlpha( math.random( 30 ) + 30 )
		smoke:SetEndAlpha( 0 )
		smoke:SetStartSize( (math.random( 10 ) + 5) * self.size )
		smoke:SetEndSize( math.random( 15 ) + 35 )
		smoke:SetRoll( math.random( 359 ) + 1 )
		smoke:SetRollDelta( math.random( -2, 2 ) )
		smoke:SetColor( 175, 175, 175 )
	end
	
	self.time = CurTime()
	
	return true
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	render.SetMaterial(Shaft)
	render.DrawBeam(self.StartPos,self.StartPos + (self.Forward * -250 * self.size),20*self.size,1,0,Color(255,180,180,255))
end
