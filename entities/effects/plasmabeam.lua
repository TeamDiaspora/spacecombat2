local bMats = {}
bMats.Glow1 = GAMEMODE.MaterialFromVMT(
	"sc_blue_ball01",
	[["UnLitGeneric"
	{
		"$basetexture"		"sprites/bluelight1"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)


bMats.Glow2 = GAMEMODE.MaterialFromVMT(
	"sc_blue_ball02",
	[["UnLitGeneric"
	{
		"$basetexture"		"effects/blueflare1"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)
bMats.Glow3 = GAMEMODE.MaterialFromVMT(
	"sc_blue_ball03",
	[["UnLitGeneric"
	{
		"$basetexture"		"effects/blueflare1"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)
local lMats = {}

lMats.Glow2 = GAMEMODE.MaterialFromVMT(
	"sc_blue_beam02",
	[["UnLitGeneric"
	{
		"$basetexture"		"sprites/laserbeam"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)

lMats.Glow3 = GAMEMODE.MaterialFromVMT(
	"sc_blue_beam03",
	[["UnLitGeneric"
	{
		"$basetexture"		"sprites/laserbeam"
		"$nocull" 1
		"$additive" 1
		"$vertexalpha" 1
		"$vertexcolor" 1
	}]]
)


--[[---------------------------------------------------------
   Init( data table )
---------------------------------------------------------]]--
function EFFECT:Init( data )
	self.HitPos		= data:GetStart()
	self.StartPos	= self.HitPos
    self.ActualStart = self.HitPos
	self.Forward	= data:GetNormal()
	self.size		= data:GetScale()
	self.vel		= data:GetMagnitude()
	self.time		= CurTime()	
end

--[[---------------------------------------------------------
   THINK
---------------------------------------------------------]]--
function EFFECT:Think( )
    if not self.Dead then
	    self.EndPos = self.HitPos + (self.Forward * self.vel * (CurTime() - self.time))

        if self.ActualStart:Distance(self.EndPos) > (5000 * self.size) then
            self.StartPos = self.EndPos + (-self.Forward * 5000 * self.size)
        end
	
	    local tracedata = {}
	    tracedata.start = self.HitPos
	    tracedata.endpos = self.EndPos
	    local tr = util.TraceLine(tracedata)
	
	    if tr.Hit then
		    self.EndPos = tr.HitPos
		    self.Dead = true

            timer.Simple(10, function() if IsValid(self) then self.ForceDead = true end end)
	    end
	
	    self.HitPos = self.EndPos
    else
        self.StartPos = self.StartPos + (self.Forward * self.vel * (CurTime() - self.time))
    end
	
	self:SetRenderBoundsWS( self.StartPos, self.EndPos )
	
	if (self.Dead and (self.StartPos:Distance(self.EndPos) <= 300 or self.StartPos:Dot(self.EndPos) <= 0)) or self.ForceDead then
		return false
	end
	
	self.time = CurTime()
	
	return true
end

--[[---------------------------------------------------------
   Draw the effect
---------------------------------------------------------]]--
function EFFECT:Render( )
	local Start = math.random(750,1000) * self.size
	local End = math.random(750,1000) * self.size
	render.SetMaterial( bMats.Glow1 )		
   	render.DrawBeam( self.EndPos, self.StartPos, 300 * self.size, ((2*CurTime())-(0.0005*self.size)), 2*CurTime(), Color( 0, 0, 255, 255 ) )
   	
	render.SetMaterial( bMats.Glow2 )
   	render.DrawSprite(self.StartPos, Start , Start , Color(24, 108, 240, 255 )) 
	
	render.SetMaterial( bMats.Glow3 )
   	render.DrawSprite(self.StartPos, Start/2 , Start/2 , Color(255, 255, 255, 255 )) 
	
	render.SetMaterial( bMats.Glow2 )
   	render.DrawSprite(self.EndPos, End , End , Color(24, 108, 240, 255)) 
	
	render.SetMaterial( bMats.Glow3 )
   	render.DrawSprite(self.EndPos, End/2 , End/2 , Color(255, 255, 255, 255)) 
   	
	render.SetMaterial( lMats.Glow2 )	
   	render.DrawBeam( self.EndPos , self.StartPos , 188 * self.size, ((2*CurTime())-(0.001*self.size)), 2*CurTime(), Color( 24, 108, 240, 255) )
   	
	render.SetMaterial( lMats.Glow3 )	
   	render.DrawBeam( self.EndPos , self.StartPos, 70 * self.size, ((2*CurTime())-(0.001*self.size)), 2*CurTime(), Color( 255, 255, 255, 255  ) )
	
	render.SetMaterial( lMats.Glow3 )	
   	render.DrawBeam( self.EndPos , self.StartPos, 128 * self.size, ((2*CurTime())-(0.001*self.size)), 2*CurTime(), Color( 255, 255, 255, 155) )

	return false 
end			 

