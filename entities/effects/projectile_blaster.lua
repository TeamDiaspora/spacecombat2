/**********************************************
	Scalable Blaster Projectile Effect
	
	Author: Steeveeo
***********************************************/

local trailMat = Material("effects/ar2ground2")
local spriteFlareMat = Material("effects/flares/halo-flare_001")
local spriteCoreMat = Material("sprites/light_glow03")

function EFFECT:Init( data )
	self.StartPos	= data:GetStart()
	self.Forward	= data:GetNormal()
	self.Scale		= data:GetScale()
	self.Velocity	= data:GetMagnitude()
	self.LifeTime	= 0
	self.ModelLength = 19.5 * 7.5 * self.Scale
	self.Model		= ClientsideModel("models/cerus/weapons/projectiles/pc_proj.mdl", 9)
	
	self.TrailLength = 750 * self.Scale
	self.CurTrailLength = 0
	
	self.Emitter = ParticleEmitter(self.StartPos)
	
	local mat = Matrix()
	mat:Scale(Vector(7.5, 2, 3.5) * self.Scale)
	self.Model:EnableMatrix("RenderMultiply", mat)
end

function EFFECT:Think()
	self.EndPos = self.StartPos + (self.Forward * self.Velocity * (FrameTime()))
	
	--Grow trail (so it doesn't shoot out the back of the gun)
	self.CurTrailLength = math.Clamp(self.CurTrailLength + (self.Velocity * FrameTime()), 0, self.TrailLength)

	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	local tr = util.TraceLine(tracedata)
	
	if tr.Hit and tr.Entity ~= self.Model then
		self.EndPos = tr.HitPos
		self.Dead = true
	end
	
	self.StartPos = self.EndPos
	self.Model:SetPos(self.StartPos - (self.Forward * (self.ModelLength * 0.25)))
	self.Model:SetAngles(self.Forward:Angle())
	self:SetRenderBoundsWS(self.StartPos, self.StartPos + (self.Forward * -self.CurTrailLength))
	
	if self.Dead then
		self.Model:Remove()
		self.Emitter:Finish()
		return false
	end
	
	--Particles
	for i = 1, 2 do
		local velocity = ((self.Forward + (VectorRand() * 0.5)):GetNormalized() * math.Rand(200, 350)) + (self.Forward * (self.Velocity * math.Rand(-0.1, 0.75)))
		
		local p = self.Emitter:Add("effects/flares/light-rays_001", self.StartPos)
		p:SetDieTime(math.Rand(0.5, 1))
		p:SetVelocity(velocity)
		p:SetAirResistance(300)
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(math.random(5, 10) * self.Scale)
		p:SetEndSize(0)
	end
	
	return true
end

function EFFECT:Render()
	render.SetMaterial(trailMat)
	render.DrawBeam(self.StartPos, self.StartPos + (self.Forward * -self.CurTrailLength), 7.5 * self.Scale, 1, 0, Color(0, 144, 255, 255))
	
	render.SetMaterial(spriteFlareMat)
	render.DrawSprite(self.StartPos, self.Scale * 200, self.Scale * 200, Color(200, 200, 255, 255))
	
	render.SetMaterial(spriteCoreMat)
	render.DrawSprite(self.StartPos, self.Scale * 75, self.Scale * 75, Color(255, 255, 255, 255))
	
	return true
end
