--[[*********************************************
	Mineral Collector Beam Thingy
Spawns different particles based on what the beam
hits (i.e. crystal fragments on minerals, dust on
roids, etc)
	
	Author: Steeveeo
**********************************************]]--

local glowMat = Material("effects/flares/halo-flare_001")

local debrisModels = {
	"models/props_debris/concrete_chunk01a.mdl",
	"models/props_debris/concrete_chunk01b.mdl",
	"models/props_debris/concrete_chunk01c.mdl",
	"models/props_debris/concrete_chunk02a.mdl",
	"models/props_debris/concrete_chunk02b.mdl",
	"models/props_debris/concrete_chunk06c.mdl"
}

local maxChunks = 10
local chunkCullDist = 10

function EFFECT:Init( data )
	self.Scale 		= data:GetScale()
	self.ColLerp	= data:GetMagnitude()
	self.Entity		= data:GetEntity()
    self.StartOffset= self.Entity:WorldToLocal(data:GetStart())
	self.LocalAngle	= self.Entity:WorldToLocalAngles(data:GetNormal():Angle())
	self.EndOffset 	= self.Entity:GetRange()
	self.Color 		= Color(Lerp(self.ColLerp, 0, 255), Lerp(self.ColLerp, 255, 0), Lerp(self.ColLerp, 0, 255), 64)

	self.CurrentAngle = self.Entity:LocalToWorldAngles(self.LocalAngle):Forward()
	self.StartPos = self.Entity:GetPos() + (self.CurrentAngle * self.StartOffset)
	self.MaxRange = self.EndOffset
	
	self.Emitter = ParticleEmitter(self.Entity:LocalToWorld(self.StartPos))
	self.PlanarEmitter = ParticleEmitter(self.Entity:LocalToWorld(self.StartPos), true)
	self.NextEmit = CurTime()
	
	self.NextChunkEmit = CurTime()
	
	--Setup Energy Funnel Thingy
	self.Funnel = ClientsideModel("models/effects/portalfunnel.mdl", RENDERGROUP_TRANSLUCENT)
	self.Funnel:SetPos(self.Entity:LocalToWorld(self.StartOffset))
	self.Funnel:SetMaterial("models/props_combine/portalball001_sheet")
	
	local scale = Matrix()
	scale:Scale(Vector(0.075, 0.075, 0.25))
	self.Funnel:EnableMatrix("RenderMultiply", scale)
	
	self.Funnel:Spawn()
	
	self.Chunks = {}
end

function EFFECT:Shutdown( )
	if IsValid(self.Funnel) then
		self.Funnel:Remove()
	end
	
	for k,v in pairs(self.Chunks) do
		v:Remove()
	end
end

function EFFECT:Think( )
	if self.Emitter == nil then return false end
	if self.PlanarEmitter == nil then return false end

	if not IsValid(self.Entity) or not self.Entity:GetShooting() then
		self.Emitter:Finish()
		self.PlanarEmitter:Finish()
		self:Shutdown()
		return false
	end
	
	self.CurrentAngle = self.Entity:LocalToWorldAngles(self.LocalAngle):Forward()
	self.StartPos = self.Entity:LocalToWorld(self.StartOffset)
	self.EndPos = self.StartPos + (self.CurrentAngle * self.EndOffset)
	
	local tracedata = {}
	tracedata.start = self.StartPos
	tracedata.endpos = self.EndPos
	tracedata.filter = {self.Entity}
	local tr = util.TraceLine(tracedata)
	
	local hitEnt = nil
	
	if tr.Hit then
		self.EndPos = tr.HitPos
		self.LastHitRange = tr.HitPos:Distance(self.StartPos)
		
		hitEnt = tr.Entity
	else
		self.LastHitRange = self.Entity:GetRange()
	end
	
	--Update Beam Cone (doing this per-frame to deal with the odd things players can do to ents that breaks normal parenting).
	self.Funnel:SetAngles(self.Entity:LocalToWorldAngles(self.LocalAngle))
	self.Funnel:SetAngles(self.Funnel:LocalToWorldAngles(Angle(90, 0, 0)))
	self.Funnel:SetPos(self.StartPos)
	
	self:SetRenderBoundsWS( self.StartPos, self.EndPos )
	
	--Particles
	if CurTime() > self.NextEmit then	
		--Hit Smoke
		if tr.Hit then
			local velocity = -self.CurrentAngle * (self.LastHitRange * 2)
			
			local p = self.Emitter:Add("effects/shipsplosion/smoke_004add", self.EndPos)
			p:SetDieTime(math.Rand(0.5, 0.75))
			p:SetVelocity(velocity)
			p:SetStartAlpha(0)
			p:SetEndAlpha(64)
			p:SetStartSize(math.random(35, 65))
			p:SetEndSize(0)
			p:SetColor(255, 255, 255)
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-5, 5))
		end
		
		--Sucky Rings
		local velocity = -self.CurrentAngle * (self.Entity:GetRange() * 2)
		local ang = (self.EndPos - self.StartPos):Angle()
		local revAng = (self.StartPos - self.EndPos):Angle()
		local pos = self.StartPos + (self.CurrentAngle * self.EndOffset)
		for i = 1, 6 do
			local p = self.PlanarEmitter:Add("effects/shipsplosion/shockwave_001", pos)
			p:SetDieTime(math.Rand(0.5, 0.75))
			p:SetVelocity(velocity)
			p:SetStartAlpha(0)
			p:SetEndAlpha(255)
			p:SetStartSize(math.random(15, 65))
			p:SetEndSize(0)
			p:SetColor(255, 255, 255)
			
			if i % 2 == 0 then
				p:SetAngles(ang)
			else
				p:SetAngles(revAng)
			end
		end
		
		self.NextEmit = CurTime() + 0.05
	end
	
	--Mined Chunks
	if CurTime() > self.NextChunkEmit then
		if tr.Hit and IsValid(tr.Entity) then
			local isMineral = tr.Entity:GetClass() == "mining_mineral" or tr.Entity:GetClass() == "mining_mineral_parent"
			local isAsteroid = tr.Entity:GetClass() == "mining_asteroid"
			
			if isMineral or isAsteroid then
				if #self.Chunks < maxChunks then
					local objPos = self.EndPos + VectorRand() * 25
					
					if isMineral then
						local min = tr.Entity:OBBMins() * 0.9
						local max = tr.Entity:OBBMaxs() * 0.9
						objPos = tr.Entity:GetPos() + Vector(math.Rand(min.x, max.x), math.Rand(min.y, max.y), math.Rand(min.z, max.z))
					end
					
					local chunk = ClientsideModel(table.Random(debrisModels), RENDERGROUP_OPAQUE)
					chunk:SetPos(objPos)
					chunk:SetAngles(AngleRand())
					chunk:Spawn()
					
					--Hit Minerals
					if isMineral then
						local crysType = tr.Entity:GetSkin()
						chunk:SetMaterial("models/shiny")
						
						--Cold Blues
						if crysType == 1 then
							chunk:SetColor(Color(100, 100, 194))
						--Hot Reds
						elseif crysType == 2 then
							chunk:SetColor(Color(175, 0, 0))
						--Normal Greens
						else
							chunk:SetColor(Color(0, 220, 0))
						end
					else
						chunk:SetColor(Color(96, 96, 96))
					end
					
					chunk.DieTime = CurTime() + 3
					chunk.Velocity = math.random(15, 25)
					chunk.AngVelocity = AngleRand()
					
					local scale = Matrix()
					scale:Scale(VectorRand() * 0.25)
					chunk:EnableMatrix("RenderMultiply", scale)
					
					table.insert(self.Chunks, chunk)
				end
			end
			
			self.NextChunkEmit = CurTime() + math.Rand(0.1, 0.25)
		end
	end
	
	--Chunk Updates
	local dist = chunkCullDist * chunkCullDist
	for i,chunk in pairs(self.Chunks) do
		--Remove if timeout or near effect origin
		if CurTime() > chunk.DieTime or chunk:GetPos():DistToSqr(self.StartPos) < dist then
			table.remove(self.Chunks, i)
			chunk:Remove()
		else
			--Move towards Beam
			chunk.Velocity = chunk.Velocity + 50 * FrameTime()
			local vel = (self.StartPos - chunk:GetPos()):GetNormalized() * (chunk.Velocity * FrameTime())
			chunk:SetPos(chunk:GetPos() + vel)
			
			--Rotate
			chunk.AngVelocity = chunk.AngVelocity * (1 + FrameTime())
			chunk:SetAngles(chunk:LocalToWorldAngles(chunk.AngVelocity * FrameTime()))
		end
	end
	
	return true
end


function EFFECT:Render( )
	render.SetMaterial(glowMat)
   	render.DrawSprite(self.StartPos, math.random(150, 175), math.random(150, 175), Color(255, 255, 255, 255))
   	render.DrawSprite(self.StartPos, math.random(150, 175), math.random(150, 175), Color(255, 255, 255, 255))
end

