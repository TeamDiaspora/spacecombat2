--[[*********************************************
	Scalable Hull Damage particle (Heavy Smoke, Sparks, and Fire)

	Author: Steeveeo
**********************************************]]--

--Pruning thought process for particles so they can go away when the core dies,
--making room for big explosion effects
local function particleThink(p)
	if p.HadCore and not IsValid(p.Core) then
		p:SetLifeTime(1e30)
	else
		p:SetNextThink(CurTime())
	end
end

function EFFECT:Init( data )
	self.Core = data:GetEntity()
	self.HadCore = IsValid(self.Core)

    if not self.HadCore then
        return
    end

	self.Angle = self.Core:WorldToLocalAngles(data:GetNormal():Angle())
	self.Pos = self.Core:WorldToLocal(data:GetOrigin())
	self.Scale = data:GetScale()
	self.CutPercent = data:GetMagnitude() --Cut out if core is above this percent health
	self.LifeTime = 20
	self.Life = 0
	self.Emitter = ParticleEmitter(self.Core:LocalToWorld(self.Pos))

	self.NextEmit = CurTime()
	self.NextSpark = CurTime() + math.Rand(0.1, 0.25)
end

function EFFECT:Think( )
	if self.Emitter == nil then return false end --Garry why...

	if not IsValid(self.Core) then
		self.Emitter:Finish()
		return false
	end

	self.Life = self.Life + FrameTime()

	--Remove after LifeTime seconds if we're not tracking a core
	if self.Core:GetClass() ~= "ship_core" then
		self.LifeTime = self.LifeTime - FrameTime()
		if self.LifeTime <= 0 then
			self.Emitter:Finish()
			return false
		end
	--Remove only if Armor and Hull are above CutPercent percent (after living for at least 3 seconds)
	else
		if self.Life > 3 then
			local maxHP = self.Core:GetArmorMax() + self.Core:GetHullMax() --Using this method instead of Percent to deal with Hull Tanks
			local curHP = self.Core:GetArmorAmount() + self.Core:GetHullAmount()
			local percent = (curHP / maxHP) * 100

			if percent >= self.CutPercent then
				self.Emitter:Finish()
				return false
			end
		end
	end

	--[[ Currently spawns too many particles and isn't visible enough to be worth it
	if CurTime() > self.NextSpark then
		local velocity = (self.Core:LocalToWorldAngles(self.Angle):Forward() + (VectorRand() * 0.25)):GetNormalized() * (math.Rand(80, 100) * (1 + math.log(1 + self.Scale)))
		local pos = self.Core:LocalToWorld(self.Pos) + (VectorRand() * (3 * self.Scale))

		local p = self.Emitter:Add("sprites/light_ignorez", pos)
		p:SetDieTime(math.Rand(0.5, 0.75))
		p:SetVelocity(velocity)
		p:SetGravity(Vector(math.Rand(-300, 300), math.Rand(-300, 300), math.Rand(0, 300)) * (1 + math.log(self.Scale / 5)))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(0.75, 0.85) * self.Scale)
		p:SetEndSize(0)
		p:SetStartLength(math.random(2, 7) * self.Scale)
		p:SetEndLength(math.random(5, 7) * self.Scale)
		p:SetColor(255, 220, 200)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetThinkFunction(particleThink)
		p:SetNextThink(CurTime())
		p.Core = self.Core
		p.HadCore = self.HadCore

		self.NextSpark = CurTime() + math.Rand(0.005, 0.01)
	end
	]]--

	if CurTime() > self.NextEmit then
		local velocity = (self.Core:LocalToWorldAngles(self.Angle):Forward() + (VectorRand() * 0.25)):GetNormalized() * (math.Rand(40, 50) * (1 + math.log(1 + self.Scale)))

        --Smoke
		local p = self.Emitter:Add("effects/shipsplosion/smoke_001", self.Core:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(2, 3) * (1 + math.log(1 + self.Scale)))
		p:SetVelocity(velocity)
		p:SetGravity(Vector(10, 10, 0) * (1 + math.log(1 + self.Scale / 10)))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(5, 10) * self.Scale)
		p:SetEndSize(math.random(45, 65) * self.Scale)
		p:SetColor(32, 32, 32)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetThinkFunction(particleThink)
		p:SetNextThink(CurTime())
		p.Core = self.Core
		p.HadCore = self.HadCore

        --Fire
		for i = 1, 5 do
			local velocity = (self.Core:LocalToWorldAngles(self.Angle):Forward() + (VectorRand() * 0.25)):GetNormalized() * (math.Rand(40, 50) * (1 + math.log(1 + self.Scale)))

			local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Core:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(0.25, 0.5) * (1 + math.log(1 + self.Scale)))
			p:SetVelocity(velocity)
			p:SetStartAlpha(0)
			p:SetEndAlpha(255)
			p:SetStartSize(math.random(20, 25) * self.Scale)
			p:SetEndSize(0)
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.25, 0.25))
			p:SetThinkFunction(particleThink)
			p:SetNextThink(CurTime())
			p.Core = self.Core
			p.HadCore = self.HadCore
		end

		--Flare
		local p = self.Emitter:Add("sprites/light_ignorez", self.Core:LocalToWorld(self.Pos) + (VectorRand() * (3 * self.Scale)))
        p:SetDieTime(math.Rand(0.20, 0.25) * (1 + math.log(1 + self.Scale)))
        p:SetStartAlpha(0)
        p:SetEndAlpha(255)
        p:SetStartSize(50 * self.Scale)
        p:SetEndSize(0)
        p:SetRoll(math.Rand(0, 360))
        p:SetColor(255, 220, 200)
		p:SetThinkFunction(particleThink)
		p:SetNextThink(CurTime())
		p.Core = self.Core
		p.HadCore = self.HadCore

		self.NextEmit = CurTime() + 0.1
	end

	return true
end

function EFFECT:Render( )
	return false
end
