--[[*********************************************
	Medium Hull Explosion Effect
	
	Author: Steeveeo
**********************************************]]--

local sound_Explosions = {}

sound_Explosions[1] = {}
sound_Explosions[1].Near = Sound("shipsplosion/sc_explode_01_near.mp3")
sound_Explosions[1].Mid = Sound("shipsplosion/sc_explode_01_mid.mp3")
sound_Explosions[1].Far = Sound("shipsplosion/sc_explode_01_far.mp3")

sound_Explosions[2] = {}
sound_Explosions[2].Near = Sound("shipsplosion/sc_explode_02_near.mp3")
sound_Explosions[2].Mid = Sound("shipsplosion/sc_explode_02_mid.mp3")
sound_Explosions[2].Far = Sound("shipsplosion/sc_explode_02_far.mp3")

sound_Explosions[3] = {}
sound_Explosions[3].Near = Sound("shipsplosion/sc_explode_03_near.mp3")
sound_Explosions[3].Mid = Sound("shipsplosion/sc_explode_03_mid.mp3")
sound_Explosions[3].Far = Sound("shipsplosion/sc_explode_03_far.mp3")

sound_Explosions[4] = {}
sound_Explosions[4].Near = Sound("shipsplosion/sc_explode_04_near.mp3")
sound_Explosions[4].Mid = Sound("shipsplosion/sc_explode_04_mid.mp3")
sound_Explosions[4].Far = Sound("shipsplosion/sc_explode_04_far.mp3")

function EFFECT:Init( data )
	self.Normal = data:GetNormal()
	
	self.Pos = data:GetOrigin()
	self.Emitter = ParticleEmitter(self.Pos)
	
	self.Dist = LocalPlayer():GetPos():Distance(data:GetOrigin())
	self.FarDist = GetConVar("sc_shipDeathSoundRadius_Max"):GetFloat()
	self.MidDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_MidFactor"):GetFloat() * 0.5
	self.NearDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_NearFactor"):GetFloat() * 0.5
	
	--Fire Quick
	for i=1, 15 do
		local velocity = (self.Normal + (VectorRand() * 0.75)):GetNormalized() * math.Rand(300, 2000)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Rand(0.25, 0.75))
		p:SetVelocity(velocity)
		p:SetAirResistance(25)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(300, 350))
		p:SetEndSize(math.random(350, 400))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
	end
	
	--Fire Main
	for i=1, 20 do
		local velocity = (self.Normal + (VectorRand() * 0.75)):GetNormalized() * math.Rand(150, 1000)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Rand(2, 3))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(100, 225))
		p:SetEndSize(math.random(250, 400))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
	end
	
	--Fire Highlights
	for i=1, 15 do
		local velocity = (self.Normal + (VectorRand() * 0.75)):GetNormalized() * math.Rand(150, 1000)
		
		local p = self.Emitter:Add("particles/flamelet" .. math.random(1,5), self.Pos)
		p:SetDieTime(math.Rand(2, 3))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(100, 225))
		p:SetEndSize(math.random(250, 400))
		p:SetRoll(math.random(-360, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(96, 96, 255)
	end
	
	--Flare
	for i=1, 5 do
		local p = self.Emitter:Add("effects/flares/light-rays_001_ignorez", self.Pos)
		p:SetDieTime(math.Rand(0.5, 0.75))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(1250)
		p:SetEndSize(1250)
		p:SetColor(255, 220, 200)
	end
	
	--Play Sound
	math.randomseed(CurTime())
	local sound = sound_Explosions[math.random(1, #sound_Explosions)]
	if self.Dist < self.NearDist then
		LocalPlayer():EmitSound(sound.Near, 511, 100, 1)
	elseif self.Dist < self.MidDist then
		LocalPlayer():EmitSound(sound.Mid, 511, 100, 1)
	elseif self.Dist < self.FarDist then
		LocalPlayer():EmitSound(sound.Far, 511, 100, 1)
	end
	
	--Shake Screen
	if GetConVar("sc_doScreenShakeFX"):GetBool() then
		--util.ScreenShake(self.Pos, 3, 255, 0.75, 4096)
	end
	
	self.Emitter:Finish()
end

function EFFECT:Think( )
	return false
end

function EFFECT:Render( )
	return false
end
