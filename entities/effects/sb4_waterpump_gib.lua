--[[*********************************************
	Waterpump Gib Effect
	(for when players get 'sploded by water pumps)
	
	Author: Steeveeo
**********************************************]]--

local Sounds = {
	"physics/body/body_medium_break3.wav",
	"physics/flesh/flesh_bloody_break.wav"
}

for k,v in pairs(Sounds) do
	util.PrecacheSound(v)
end

local BloodSplats = {
	"effects/blood",
	"effects/blood2"
}

function EFFECT:Init( data )
	self.Normal = data:GetNormal()
	self.Ent = data:GetEntity()
	self.Pos = data:GetOrigin()
	self.Emitter = ParticleEmitter(self.Pos)
	
	--Blood
	for i=1, 75 do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * (math.Rand(0, 500))
		
		local p = self.Emitter:Add(table.Random(BloodSplats), self.Pos)
		p:SetDieTime(math.Rand(1, 3))
		p:SetAirResistance(150)
		p:SetVelocity(velocity)
		p:SetGravity(Vector(0, 0, -30))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(5, 15))
		p:SetEndSize(math.random(35, 50))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.5, 0.5))
		p:SetColor(96, 0, 0)
	end
	
	--Play Sound
	math.randomseed(CurTime())
	self.Ent:EmitSound(table.Random(Sounds), 75)
end

function EFFECT:Think( )
  	self.Emitter:Finish()
	return false
end


function EFFECT:Render( )
	
	
	return false
					 
end
