/**********************************************
	Dynamic Asteroid Crack Effect

	Author: Steeveeo
***********************************************/

local debrisModels = {
    "models/mandrac/asteroid/rock4.mdl",
    "models/mandrac/asteroid/rock2.mdl"
}

local biggestBoundingRadius = 1037.02 --Bounding radius of the largest asteroid model, used to calculate scale

function EFFECT:Init( data )

	//Particle Settings
	self.Pos = data:GetOrigin()
	self.Roid = data:GetEntity()
	self.DieTime = CurTime() + 16
	self.Emitter = ParticleEmitter(self.Pos)
	self.Debris = {}

	self.Scale = 1
	if IsValid(self.Roid) then
		self.Scale = self.Roid:BoundingRadius() / biggestBoundingRadius
	end

	--Dust Plumes
	for i=1, 50 do
		local velocity = VectorRand() * math.Rand(50, 75) * self.Scale

		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.Rand(1, 4), self.Pos)
		p:SetDieTime(math.Rand(0.5, 3))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartLength(math.Rand(30, 50) * self.Scale)
		p:SetEndLength(p:GetStartLength() + (math.Rand(1700, 2000) * self.Scale))
		p:SetStartSize(math.Rand(200, 250) * self.Scale)
		p:SetEndSize(p:GetStartSize() + (math.Rand(150, 175) * self.Scale))
		p:SetRoll(math.Rand(0, 360))
		p:SetColor(40, 36, 32)
	end

	--Dust Cloud
	for i=1, 20 do
		local velocity = VectorRand() * math.Rand(250, 1250) * self.Scale

		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.Rand(1, 4), self.Pos)
		p:SetDieTime(math.Rand(5, 15))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(128)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(200, 350) * self.Scale)
		p:SetEndSize(p:GetStartSize() + (math.random(250, 400) * self.Scale))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(40, 36, 32)
	end

	--Asteroid Debris
	if IsValid(self.Roid) then
		local boxMin = self.Roid:OBBMins() * 0.75
		local boxMax = self.Roid:OBBMaxs() * 0.75
		for i=1, math.Max(7, 15 * self.Scale) do
			local prop = ents.CreateClientProp(table.Random(debrisModels))
			prop:SetRenderMode(RENDERMODE_TRANSALPHA)

			local pos = Vector(math.Rand(boxMin.x, boxMax.x), math.Rand(boxMin.y, boxMax.y), math.Rand(boxMin.z, boxMax.z))

			prop:SetModelScale(math.Rand(2, 5) * self.Scale)
			prop:SetPos(self.Roid:LocalToWorld(pos))
			prop:SetAngles(AngleRand())
			prop.Velocity = (prop:GetPos() - self.Pos):GetNormalized() * math.Rand(1, 35) * self.Scale
			prop.AngVelocity = Angle(math.Rand(-2, 2), math.Rand(-2, 2), math.Rand(-2, 2))
			prop.LifeTime = 15
			prop.StartTime = CurTime()

			table.insert(self.Debris, prop)
		end
	end

	--Play Sound
	math.randomseed(SysTime())
	local pitch = 120 - (35 * self.Scale)
	sound.Play("sb3_mining/mining_asteroidcrack.mp3", self.Pos, 511, pitch)
	sound.Play("sb3_mining/mining_asteroidcrack.mp3", self.Pos, 511, pitch)
end

function EFFECT:Think( )
  	if CurTime() > self.DieTime then
		self.Emitter:Finish()

		for k,v in pairs(self.Debris) do
			v:Remove()
		end

		return false
	else
		--Move and fade roids
		for k,v in pairs(self.Debris) do
			v:SetPos(v:GetPos() + (v.Velocity * FrameTime()))
			v:SetAngles(v:GetAngles() + (v.AngVelocity * FrameTime()))

			--Fade Out
			if CurTime() > v.StartTime + v.LifeTime - 3 then
				local alpha = math.Clamp(v:GetColor().a - ((255 / 3) * FrameTime()), 0, 255)
				v:SetColor(Color(255, 255, 255, alpha))
			end
		end

		return true
	end
end


function EFFECT:Render( )
	return false
end
