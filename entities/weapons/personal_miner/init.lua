AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

SWEP.Weight = 8
SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false

SWEP.Receiver = nil
SWEP.Triggered = false
SWEP.Pointing = false
SWEP.Ent = nil

function SWEP:Initialize()
	self.Triggered = false
	self.Pointing = false
	self.MOwner = self.Owner
end

function SWEP:Reload()
	
end

function SWEP:PrimaryAttack()
    if CLIENT then return end
	if IsValid(self.Receiver) then
		local Dis = self.Owner:GetPos():Distance(self.Receiver:GetPos())
		
		if self.Receiver.EnergySupply then
			if Dis > 2000 then
				self:LaserDisable(2)
			else
				self.Triggered = not self.Triggered
			
				if self.Triggered then
					self.Owner:EmitSound( "sb3_mining/mining_laser/mining_laser_windup.wav" )
				
					self.Weapon:SetNWBool("Charging",true)
			
					--Fire Delay
					timer.Create("PersonalLaser_FireDelay" .. self.Weapon:EntIndex(), 1.5, 1, function()
						self.Pointing = true
						self.Weapon:SetNWBool("Active",true)
						self.Weapon:SetNWBool("Charging",false)
					end)
				else
					self:LaserDisable()
				end
			end
		else
			self:LaserDisable(1)
		end
	else
		self.Owner:PrintMessage( HUD_PRINTTALK, "No Link!" )
	end
end

function SWEP:SecondaryAttack()
	local trace = self.Owner:GetEyeTrace()
	if IsValid(trace.Entity) and (trace.Entity:GetClass() == "personal_ore_receiver") then
		local Receiver = trace.Entity
		local Present = false
		
		for k,Miner in pairs(Receiver.Miners) do
			if Miner == self.Weapon then
				Present = true
				self.Receiver = nil
				Receiver:RemoveMiner(k)
				self.Owner:PrintMessage( HUD_PRINTTALK, "Unlinked Successfully.")
				return false
			end
		end
		
		if #Receiver.Miners <= 4 and not Present then
			self.Receiver = Receiver
			Receiver:AddMiner(self.Weapon)
			
			self.Owner:PrintMessage( HUD_PRINTTALK, "Linked Sucessfully.")
		else
			self.Owner:PrintMessage( HUD_PRINTTALK, "Cannot exceed 4 Miners per Receiver!")
		end
		return true
	end
end

function SWEP:LaserDisable(Con)
	timer.Destroy("PersonalLaser_FireDelay" .. self.Weapon:EntIndex())
	
	self.Pointing = false
	self.Ent = nil

	self.Weapon:SetNWBool("Active",false)
	self.Weapon:SetNWBool("Charging",false)
	
	self.Owner:EmitSound( "sb3_mining/mining_laser/mining_laser_off.wav" )
	
	if Con == 1 then
		self.Owner:PrintMessage( HUD_PRINTTALK, "Not enough Energy!")
	elseif Con == 2 then
		self.Owner:PrintMessage( HUD_PRINTTALK, "Too far away!")
	end
end

function SWEP:Think()
	if IsValid(self.Receiver) then
		if self.Pointing then
			local pos = self.Owner:GetShootPos()
			local ang = self.Owner:GetAimVector()
			local tracedata = {}
			tracedata.start = pos
			tracedata.endpos = pos+(ang*150)
			tracedata.filter = self.Owner
			local trace = util.TraceLine(tracedata)
			
			self.Ent = trace.Entity
		end
	end
end

function SWEP:OnRemove()
	if IsValid(self.Receiver) then
		for k,Miner in pairs(self.Receiver.Miners) do
			if Miner == self.Weapon then
				self.Receiver:RemoveMiner(k)
			end
		end
	end
end
