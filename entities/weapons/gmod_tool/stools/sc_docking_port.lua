TOOL.Category		= "Ship Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Docking Port Spawner"
TOOL.Command		= nil
TOOL.ConfigName		= ""

TOOL.ClientConVar["model"] = "models/props_phx/construct/metal_wire1x1.mdl"

--Language setup
if CLIENT then
	language.Add("Tool.sc_docking_port.name", "Space Combat 2 Docking Port Tool")
	language.Add("Tool.sc_docking_port.desc", "Creates a Docking Port.")
	language.Add("Tool.sc_docking_port.0", "Left-Click to spawn.")
	language.Add("Tool.sc_docking_port.left", "Create Docking Port")
	language.Add("undone_sc_docking_port", "Undone Docking Port")
end

if SERVER then
	CreateConVar('sbox_maxsc_docking_port', 20)
end

--Undo/cleanup registration
cleanup.Register("sc_docking_port")

--Tool functionality
function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end

	local ply = self:GetOwner()
	local DockPort = ents.Create("sc_docking_port")
	local Model = self:GetClientInfo("model")
	local Ang = ply:EyeAngles()

	Ang.yaw = Ang.yaw + 180 -- Rotate it 180 degrees in my flavour
	Ang.roll = 0
	Ang.pitch = 0


	DockPort:SetPos(trace.HitPos)

	-- Once the model has been set use it's OBB to move the can of beans out of the ground
	timer.Simple(0, function()
		if IsValid(DockPort) then
			DockPort:SetPos(trace.HitPos - trace.HitNormal * DockPort:OBBMins().z)
		end
	end)

	DockPort:SetAngles(Ang)
	DockPort:SetModel(Model)
	DockPort:Spawn()
	DockPort:Activate()
	DockPort.Owner = ply

	local PhysObj = DockPort:GetPhysicsObject()
	if not IsValid(PhysObj) then
		SC.Error("[SC2] - ERROR: Docking Port spawned without a valid physics object! What?!\n", 5)
		return
	end
	PhysObj:EnableMotion(false)

	undo.Create("sc_docking_port")
	undo.AddEntity(DockPort)
	undo.SetPlayer(ply)
	undo.Finish()

	ply:AddCleanup("sc_docking_port", DockPort)

	return true
end

function TOOL.BuildCPanel(CPanel)
	CPanel:AddControl("PropSelect", {Label = "Docking Port Model", ConVar = "sc_docking_port_model", Height = 0, Models = list.Get("SBEP_DockingClampModels")})
end