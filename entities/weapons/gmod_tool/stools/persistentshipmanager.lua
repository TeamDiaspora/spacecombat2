TOOL.Tab = "Space Combat 2"
TOOL.Category = "Admin"
TOOL.Name = "#Persistent Ship Manager"

TOOL.Information = {
    {
        name = "left"
    },
    {
        name = "right"
    }
}

TOOL.ClientConVar["enablecompression"] = "1"
TOOL.ClientConVar["filename"] = "sc_shipwright_test.json"
TOOL.ClientConVar["hullid"] = "sc_ship_dupe.json"

if CLIENT then
    language.Add("Tool.persistentshipmanager.name", "Persistent Ship Manager")
    language.Add("Tool.persistentshipmanager.desc", "Magical admin persistent ship management tool.")
    language.Add("Tool.persistentshipmanager.left", "Spawn ship")
    language.Add("Tool.persistentshipmanager.right", "Update ship")
end

--
-- PASTE
--
function TOOL:LeftClick(Trace)
    if CLIENT then return true end
    if not self:GetOwner():IsAdmin() then return true end

    -- Get the persistent ship info
    local AsJSON = file.Read("persistentships/" .. self:GetClientInfo("filename"))
    if not AsJSON then return false end

    if self:GetClientInfo("enablecompression") == "1" then
        AsJSON = util.Base64Decode(AsJSON)
        if not AsJSON then return false end

        AsJSON = util.Decompress(AsJSON)
        if not AsJSON then return false end
    end

    local ShipInfo = util.JSONToTable(AsJSON)
    if not ShipInfo then return false end

    -- Get the ship hull
    local HullID = ShipInfo.Info.HullID or self:GetClientInfo("hullid")
    local HullJSON = file.Read("blueprinter/" .. HullID)
    if not HullJSON then return false end

    if self:GetClientInfo("enablecompression") == "1" then
        HullJSON = util.Base64Decode(HullJSON)
        if not HullJSON then return false end

        HullJSON = util.Decompress(HullJSON)
        if not HullJSON then return false end
    end

    local HullInfo = util.JSONToTable(HullJSON)
    if not HullInfo then return false end

    -- We want to spawn it flush on thr ground. So get the point that we hit
    -- and take away the mins.z of the bounding box of the dupe.
    local SpawnCenter = Trace.HitPos
    SpawnCenter.z = SpawnCenter.z - HullInfo.Mins.z

    ShipInfo.PersistentData[tonumber(HullInfo.ShipInfo.ShipCoreID)] = ShipInfo.Info.ShipCoreData

    local Ents = Blueprinter_SpawnDupe(SpawnCenter, angle_zero, self:GetOwner(), HullInfo, ShipInfo.PersistentData)

    --
    -- Create one undo for the whole creation
    --
    undo.Create("Duplicator")

    for k, ent in pairs(Ents) do
        undo.AddEntity(ent)
    end

    for k, ent in pairs(Ents) do
        self:GetOwner():AddCleanup("duplicates", ent)
    end

    undo.SetPlayer(self:GetOwner())
    undo.Finish()

    return true
end

function TOOL:RightClick(Trace)
    if CLIENT then return true end
    if not self:GetOwner():IsAdmin() then return true end
    if not IsValid(Trace.Entity) then return true end

    local ShipCore = Trace.Entity.IsCoreEnt and Trace.Entity or Trace.Entity:GetProtector()
    if not IsValid(ShipCore) then return true end

    local HullID = ShipCore.PersistentHullID or self:GetClientInfo("hullid")

    if not ShipCore:IsPersistent() then
        Blueprinter_SaveDupe(ShipCore, Trace.HitPos, HullID, self:GetClientInfo("enablecompression") == "1")
    end

    local Ents = {}
    local Constraints = {}
    local PersistentData = {}
    duplicator.GetAllConstrainedEntitiesAndConstraints(ShipCore, Ents, Constraints)

    for k, v in pairs(Ents) do
        if v.SavePersistentInfo and v ~= ShipCore then
            PersistentData[v.PersistentEntityID or k] = v:SavePersistentInfo()
        end
    end

    local AsJSON = util.TableToJSON(
        {
            Info = {
                HullID = HullID,
                ShipCoreData = ShipCore:SavePersistentInfo(),
            },
            PersistentData = PersistentData
        }
    )

    if self:GetClientInfo("enablecompression") == "1" then
        AsJSON = util.Compress(AsJSON)
        AsJSON = util.Base64Encode(AsJSON, true)
    end

    file.CreateDir("persistentships")
    file.Write("persistentships/" .. self:GetClientInfo("filename"), AsJSON)

    return true
end