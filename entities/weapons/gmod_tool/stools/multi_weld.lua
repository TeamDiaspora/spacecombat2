TOOL.Category		= "Constraints"
TOOL.Name		    = "Multi-weld"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.ClientConVar["selradius"] = 2048
TOOL.Information = {
	"left",
	"right",
	{
		name  = "shift_left",
		icon2  = "gui/e.png",
		icon = "gui/lmb.png",

	},
	"reload"
}

if ( CLIENT ) then
    language.Add( "Tool.multi_weld.name", "Multi-Weld Tool" )
    language.Add( "Tool.multi_weld.desc", "Weld multiple props to one prop." )
    language.Add( "Tool.multi_weld.left", "Selects an entity to Weld" )
    language.Add( "Tool.multi_weld.shift_left", "Selects all entities in a radius (default 2048 units)." )
    language.Add( "Tool.multi_weld.right", "Weld all selected entities to this prop." )
    language.Add( "Tool.multi_weld.reload", "Clear Targets." )
end

TOOL.enttbl = {}

function TOOL:LeftClick( trace )
	local ply = self:GetOwner()
	if CLIENT then return true end
	if IsValid(trace.Entity) and trace.Entity:IsPlayer() then return end

	local ent = trace.Entity
	if self:GetOwner():KeyDown(IN_USE) then
		for k,v in pairs(ents.FindInSphere(trace.HitPos, self:GetClientNumber("selradius", 2048))) do
			if IsValid(v) and util.IsValidPhysicsObject(v, 0) and not v:IsPlayer() and not v:IsWeapon() and v:GetClass() ~= "predicted_viewmodel" and self:IsPropOwner(ply, v) and v:CPPICanTool(ply) then
				local eid = v:EntIndex()
				if not (self.enttbl[eid]) then
					local col = v:GetColor()
					self.enttbl[eid] = col
					v:SetColor(Color(0,255,0,255))
				else
					local col = self.enttbl[eid]
					v:SetColor(col)
					self.enttbl[eid] = nil
				end
			end
		end
	else
        if not util.IsValidPhysicsObject(ent, trace.PhysicsBone) then return false end
	    if ent:IsWorld() then return false end

		if ent:CPPICanTool(ply) then
			local eid = ent:EntIndex()
			if not self.enttbl[eid] then
				local col = ent:GetColor()
				self.enttbl[eid] = col
				ent:SetColor(Color(0,255,0,255))
			else
				local col = self.enttbl[eid]
				ent:SetColor(col)
				self.enttbl[eid] = nil
			end
		end
	end

	return true
end


function TOOL:RightClick( trace )
	if CLIENT then return true end
	if table.Count(self.enttbl) < 1 then return end
	if IsValid(trace.Entity) and trace.Entity:IsPlayer() then return end
	if not util.IsValidPhysicsObject(trace.Entity, trace.PhysicsBone) then return false end
	if trace.Entity:IsWorld() then return false end

	local ent = trace.Entity
	for k,v in pairs(self.enttbl) do
		local prop = ents.GetByIndex(k)
		if IsValid(prop) then
			prop:SetColor(v)
			constraint.Weld(prop,ent,0,0)
			self.enttbl[k] = nil
		end
	end
	self.enttbl = {}
	return true
end

function TOOL:SendMessage( message )
	self:GetOwner():PrintMessage( HUD_PRINTCENTER, message )
end

function TOOL:Reload()
	if CLIENT then return false end
	if table.Count(self.enttbl) < 1 then return end
	for k,v in pairs(self.enttbl) do
		local prop = ents.GetByIndex(k)
		if (prop:IsValid()) then
			prop:SetColor(v)
			self.enttbl[k] = nil
		end
	end
	self.enttbl = {}
	return true
end

function TOOL:IsPropOwner(ply, ent)
	if ent:CPPIGetOwner() == ply then return true end
	return false
end

function TOOL.BuildCPanel(panel)
	CPanel = panel

	CPanel:AddControl("Slider", {
		Label = "Area Selection Radius",
		Type = "Int",
		Min = "1",
		Max = "32768",
		Command = "multi_weld_selradius"
	})
end