TOOL.Tab = "Space Combat 2"
TOOL.Category = "Admin"
TOOL.Name = "#Blueprinter"

TOOL.Information = {
    {
        name = "left"
    },
    {
        name = "right"
    }
}

TOOL.ClientConVar["enablecompression"] = "1"
TOOL.ClientConVar["filename"] = "sc_ship_dupe.json"

if CLIENT then
    language.Add("Tool.blueprinter.name", "Blueprinter")
    language.Add("Tool.blueprinter.desc", "Magical admin ship hull management tool.")
    language.Add("Tool.blueprinter.left", "Spawn ship hull")
    language.Add("Tool.blueprinter.right", "Copy ship hull")
end

cleanup.Register("duplicates")
local LocalPos = vector_origin
local LocalAng = angle_zero

Blueprinter_IsPasting = false
function Blueprinter_SpawnDupe(SpawnCenter, SpawnAngle, Owner, Dupe, PersistentData)
    Blueprinter_IsPasting = true

    -- Put back any data we removed when saving that might cause issues
    for ID, EntityInfo in pairs(Dupe.Entities) do
        -- Force the entity to be marked as persistent
        if PersistentData then
            EntityInfo.IsPersistentEntity = true
        end

        -- If an entity didn't have a class then spawn a prop
        if not EntityInfo.Class then
            EntityInfo.Class = "prop_dynamic_override"
        end

        -- Hacky fix for wire entities not working if their entity mods don't exist
        -- even though they're just empty tables 99% of the time
        if not EntityInfo.EntityMods and string.StartsWith(EntityInfo.Class, "gmod_wire_") then
            EntityInfo.EntityMods = {}
        end

        if EntityInfo.EntityMods then
            -- Fix up removed wire data
            if EntityInfo.EntityMods.WireDupeInfo and EntityInfo.EntityMods.WireDupeInfo.Wires then
                for Connection, Info in pairs(EntityInfo.EntityMods.WireDupeInfo.Wires) do
                    Info.Path = {}
                    Info.Material = "cable/cable2"
                    Info.Color = color_white
                    Info.StartPos = vector_origin
                    Info.SrcPos = vector_origin
                    Info.Width = 0
                end
            end

            -- re-add wire dupe info table just in case something isn't checking that it exists
            if not EntityInfo.EntityMods.WireDupeInfo then
                EntityInfo.EntityMods.WireDupeInfo = {
                    Wires = {}
                }
            end
        end
    end

    --
    -- Spawn them all at our chosen positions
    --
    LocalPos = SpawnCenter
    LocalAng = SpawnAngle
    duplicator.SetLocalPos(LocalPos)
    duplicator.SetLocalAng(LocalAng)
    DisablePropCreateEffect = true
    local Ents = duplicator.Paste(Owner or game.GetWorld(), Dupe.Entities, {})
    DisablePropCreateEffect = nil
    -- Find special entities
    local ShipCore
    local Gyropod

    for i, k in pairs(Ents) do
        -- Apply persistent data if it was included
        if PersistentData then
            k.IsPersistentEntity = true
            k.PersistentEntityID = tonumber(Dupe.Entities[i].PersistentEntityID)
            if PersistentData[k.PersistentEntityID] then
                k:LoadPersistentInfo(PersistentData[k.PersistentEntityID])
            end
        end

        -- Check for special entities
        if k.IsCoreEnt then
            ShipCore = k
        elseif k.IsSCGyropod then
            Gyropod = k
            Gyropod.ShouldAutoParent = false
        end

        -- Freeze any physics objects
        local PhysObj = k:GetPhysicsObject()

        if IsValid(PhysObj) then
            PhysObj:EnableMotion(false)
            PhysObj:Sleep()
        end

        -- Re-create physics object for prop dynamics, otherwise parenting breaks
        if k:GetClass() == "prop_dynamic" then
            k:PhysicsInitStatic(SOLID_VPHYSICS)
        end

        -- Ship parts should only transmit if the parent does
        if k ~= Gyropod then
            k:SetTransmitWithParent(true)
        end

        -- Prevent the entity from being interacted with
        k.SB_Ignore = true
        k.Untouchable = true
        k.Unconstrainable = true
        k.PhysgunDisabled = true
        k.CanTool = function(self, ply, trace, mode)
            if IsValid(ply) and ply:IsAdmin() and (mode == "blueprinter" or mode == "persistentshipmanager") then
                return true
            end

            return false
        end

        -- Ships cannot be unfrozen
        k:SetUnFreezable(false)

        if Owner == nil then
            -- Owner is nil, set the owner to world
            if NADMOD then --If we're using NADMOD PP, then use its function
                NADMOD.SetOwnerWorld(k)
            elseif CPPI then --If we're using SPP, then use its function
                k:SetNWString("Owner", "World")
            else --Well fuck it, lets just use our own!
                k.Owner = game.GetWorld()
            end
        else
            -- Otherwise set the owner to the player, who isn't nil
            k.Owner = Owner

            if k.CPPISetOwner then
                k:CPPISetOwner(Owner)
            end
        end
    end

    -- Now that all the special entities are found, loop over everything again and apply constraints
    for i, k in pairs(Ents) do
        if k ~= Gyropod then
            --reweld everything
            if util.IsValidPhysicsObject(k, 0) then
                constraint.Weld(k, Gyropod, 0, 0, 0, false, true)
            end

            --parent that shit up
            if not k:IsVehicle() then
                k:SetParent(Gyropod)
            end
        end
    end

    LocalPos = vector_origin
    duplicator.SetLocalPos(LocalPos)
    duplicator.SetLocalAng(LocalAng)

    Blueprinter_IsPasting = false

    hook.Call("AdvDupe_FinishPasting", nil, {
        {
            EntityList = Dupe.Entities,
            CreatedEntities = Ents,
            ConstraintList = {},
            CreatedConstraints = {},
            HitPos = SpawnCenter,
            Player = Owner
        }
    }, 1)

    return Ents, ShipCore, Gyropod
end

--
-- PASTE
--
function TOOL:LeftClick(trace)
    if CLIENT then return true end
    if not self:GetOwner():IsAdmin() then return true end

    --
    -- Get the copied dupe. We store it on the player so it will still exist if they die and respawn.
    --
    local AsJSON = file.Read("blueprinter/" .. self:GetClientInfo("filename"))
    if not AsJSON then return false end

    if self:GetClientInfo("enablecompression") == "1" then
        AsJSON = util.Base64Decode(AsJSON)
        if not AsJSON then return false end

        AsJSON = util.Decompress(AsJSON)
        if not AsJSON then return false end
    end

    local DupeInfo = util.JSONToTable(AsJSON)
    if not DupeInfo then return false end
    --
    -- We want to spawn it flush on thr ground. So get the point that we hit
    -- and take away the mins.z of the bounding box of the dupe.
    --
    local SpawnCenter = trace.HitPos
    SpawnCenter.z = SpawnCenter.z - DupeInfo.Mins.z

    local Ents = Blueprinter_SpawnDupe(SpawnCenter, angle_zero, self:GetOwner(), DupeInfo)

    --
    -- Create one undo for the whole creation
    --
    undo.Create("Duplicator")

    for k, ent in pairs(Ents) do
        undo.AddEntity(ent)
    end

    for k, ent in pairs(Ents) do
        self:GetOwner():AddCleanup("duplicates", ent)
    end

    undo.SetPlayer(self:GetOwner())
    undo.Finish()

    return true
end

local function FilterEntityTable(tab)
    local varType

    for k, v in pairs(tab) do
        varType = TypeID(v)

        if varType == 5 then
            tab[k] = FilterEntityTable(tab[k])
        elseif varType == 6 or varType == 9 then
            tab[k] = nil
        end
    end

    return tab
end

--
-- Copy
--
local function SaveEnt(data, ent)
    --
    -- Merge the entities actual table with the table we're saving
    -- this is terrible behaviour - but it's what we've always done.
    --
    if ent.PreEntityCopy then
        ent:PreEntityCopy()
    end

    local EntityClass = duplicator.FindEntityClass(ent:GetClass())
    local EntTable = ent:GetTable()

    -- Fix dumb entities that actually relied on the behavior of copying the entity table
    -- I'm looking at you, wiremod.
    if EntityClass then
        local VarType

        for iNumber, Key in pairs(EntityClass.Args) do
            -- Translate keys from old system
            if not (Key == "Pos" or Key == "Model" or Key == "Ang" or Key == "Angle" or Key == "ang" or Key == "angle" or Key == "pos" or Key == "position" or Key == "model") then
                VarType = TypeID(EntTable[Key])

                if VarType == 5 then
                    data[Key] = FilterEntityTable(table.Copy(EntTable[Key]))
                elseif not (VarType == 9 or VarType == 6) then
                    data[Key] = EntTable[Key]
                end
            end
        end
    end

    if ent.EntityMods then
        data.EntityMods = table.Copy(ent.EntityMods)
    end

    if ent.PostEntityCopy then
        ent:PostEntityCopy()
    end

    --
    -- Set so me generic variables that pretty much all entities
    -- would like to save.
    --
    data.Pos = ent:GetPos()
    data.Angle = ent:GetAngles()
    data.Class = ent:GetClass()
    data.Model = ent:GetModel()
    data.Skin = ent:GetSkin()
    data.Mins, data.Maxs = ent:GetCollisionBounds()
    --data.ColGroup = ent:GetCollisionGroup()
    data.Name = ent:GetName()
    data.Pos, data.Angle = WorldToLocal(data.Pos, data.Angle, LocalPos, LocalAng)
    data.ModelScale = ent:GetModelScale()

    if data.ModelScale == 1 then
        data.ModelScale = nil
    end

    if data.Name == "" then
        data.Name = nil
    end

    if data.Skin == 0 then
        data.Skin = nil
    end

    -- Force all physics props to be prop dynamic
    if data.Class == "prop_physics" then
        data.Class = "prop_dynamic_override"
    end

    -- Allow the entity to override the class
    -- (this is a hack for the jeep, since it's real class is different from the one it reports as)
    if ent.ClassOverride then
        data.Class = ent.ClassOverride
    end

    if data.EntityMods then
        if data.EntityMods.colour then
            if data.EntityMods.colour.Color then
                local C = data.EntityMods.colour.Color
                if C.r == 255 and C.g == 255 and C.b == 255 and C.a == 255 then
                    data.EntityMods.colour.Color = nil
                end
            end

            if data.EntityMods.colour.RenderFX == 0 then
                data.EntityMods.colour.RenderFX = nil
            end

            if data.EntityMods.colour.RenderMode == 0 then
                data.EntityMods.colour.RenderMode = nil
            end

            if table.Count(data.EntityMods.colour) == 0 then
                data.EntityMods.colour = nil
            end
        end

        if data.EntityMods.WireDupeInfo then
            -- Check for any wire connections, we want to make them all invisible
            local WireDupeInfo = data.EntityMods.WireDupeInfo
            if WireDupeInfo.Wires then
                if table.IsEmpty(WireDupeInfo.Wires) then
                    WireDupeInfo.Wires = nil
                else
                    for Connection, Info in pairs(WireDupeInfo.Wires) do
                        Info.Path = nil
                        Info.Material = nil
                        Info.Color = nil
                        Info.StartPos = nil
                        Info.SrcPos = nil
                        Info.Width = nil
                    end
                end
            end

            -- If there isn't anything in here just remove the whole table
            if table.IsEmpty(WireDupeInfo) then
                data.EntityMods.WireDupeInfo = nil
            end
        end

        -- Remove empty material overrides
        if data.EntityMods.material and data.EntityMods.material.MaterialOverride == "" then
            data.EntityMods.material = nil
        end

        -- Remove the entire EntityMods table if it's empty
        if table.IsEmpty(data.EntityMods) then
            data.EntityMods = nil
        end
    end

    --
    -- Store networks vars/DT vars (assigned using SetupDataTables)
    --
    if ent.GetNetworkVars then
        data.DT = ent:GetNetworkVars()
    end

    -- Save the persistent entity id
    data.PersistentEntityID = ent.PersistentEntityID or tostring(ent:EntIndex())

    -- Make this function on your SENT if you want to modify the
    -- returned table specifically for your entity.
    if ent.OnEntityCopyTableFinish then
        ent:OnEntityCopyTableFinish(data)
    end


    --
    -- Exclude this crap
    --
    for k, v in pairs(data) do
        if isfunction(v) then
            data[k] = nil
        end
    end

    if data.Class == "prop_dynamic_override" then
        data.Class = nil
    end

    data.OnDieFunctions = nil
    data.AutomaticFrameAdvance = nil
    data.BaseClass = nil
    data.PhysicsObjects = nil
end

local function CopyEntTable(Ent)
    local output = {}
    SaveEnt(output, Ent)

    return output
end

local function Copy(Ent, AddToTable)
    local Ents = {}
    local Constraints = {}
    duplicator.GetAllConstrainedEntitiesAndConstraints(Ent, Ents, Constraints)
    local EntTables = {}
    local ShipInfo

    if AddToTable ~= nil then
        EntTables = AddToTable.Entities or {}
    end

    for k, v in pairs(Ents) do
        if v.IsCoreEnt then
            ShipInfo = {
                ShipCoreID = tostring(v:EntIndex()),
                Class = v:GetShipClass(),
                SubClass = v:GetShipSubClass(),
                SigRad = v:GetSigRad(),
                MaxCapacitor = v:GetCapMax(),
                MaxShield = v:GetShieldMax(),
                MaxArmor = v:GetArmorMax(),
                NPCShipTags = {
                    ["Default"] = true
                },
                PowerLevel = 0
            }
        end
        EntTables[k] = CopyEntTable(v)
    end

    local mins, maxs = duplicator.WorkoutSize(EntTables)

    -- Now that the mins and maxs have been determined, let's get rid of that data so it isn't saved
    -- It isn't used for anything in space combat
    for EntId, EntityInfo in pairs(EntTables) do
        EntityInfo.Mins = nil
        EntityInfo.Maxs = nil
    end

    return {
        Entities = EntTables,
        Mins = mins,
        Maxs = maxs,
        ShipInfo = ShipInfo
    }
end

function Blueprinter_SaveDupe(Entity, Pos, FileName, EnableCompression)
    -- Set the position to our local position (so we can paste relative to our `hold`)
    LocalPos = Pos
    duplicator.SetLocalPos(LocalPos)
    duplicator.SetLocalAng(LocalAng)
    local Dupe = Copy(Entity)
    local AsJSON = util.TableToJSON(Dupe)

    if EnableCompression then
        AsJSON = util.Compress(AsJSON)
        AsJSON = util.Base64Encode(AsJSON, true)
    end

    file.CreateDir("blueprinter")
    file.Write("blueprinter/" .. FileName, AsJSON)

    LocalPos = vector_origin
    duplicator.SetLocalPos(LocalPos)
    duplicator.SetLocalAng(LocalAng)
end

function TOOL:RightClick(trace)
    if not IsValid(trace.Entity) then return false end
    if CLIENT then return true end
    if not self:GetOwner():IsAdmin() then return true end

    Blueprinter_SaveDupe(trace.Entity, trace.HitPos, self:GetClientInfo("filename"), self:GetClientInfo("enablecompression") == "1")

    if not Dupe then return false end
    return true
end