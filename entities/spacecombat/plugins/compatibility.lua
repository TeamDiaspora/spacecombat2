AddCSLuaFile()

local WeaponReplacements = {
    ["Battleship Autocannon"] = {
        LauncherType = "Large Auto Cannon",
        ProjectileRecipe = "Large Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship Antimatter Cannon"] = {
        LauncherType = "Large Particle Blaster",
        ProjectileRecipe = "Large Antihydrogen Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship 1400mm Artillery"] = {
        LauncherType = "Large Cannon",
        ProjectileRecipe = "Large Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship Blaster"] = {
        LauncherType = "Large Particle Blaster",
        ProjectileRecipe = "Large Iron Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship Pulse Cannon"] = {
        LauncherType = "Large Cannon",
        ProjectileRecipe = "Large Pulse Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship MAC"] = {
        LauncherType = "Large Mass Accelerator Cannon",
        ProjectileRecipe = "Large Depleted Uranium MAC Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship Cruise Missile Launcher"] = {
        LauncherType = "Large Missile Launcher",
        ProjectileRecipe = "Large Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship Spartan Torpedo Launcher"] = {
        LauncherType = "Large Spartan Torpedo Launcher",
        ProjectileRecipe = "Large Heavy Kinetic Torpedo",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship Pulse Laser"] = {
        LauncherType = "Large Pulse Beam",
        ProjectileRecipe = "Large Thermal Pulse Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship Beam Laser"] = {
        LauncherType = "Large Beam",
        ProjectileRecipe = "Large Thermal Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship Mining Laser"] = {
        LauncherType = "Large Mining Beam",
        ProjectileRecipe = "Large Uncommon Mining Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Autocannon"] = {
        LauncherType = "Tiny Auto Cannon",
        ProjectileRecipe = "Tiny Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Blaster"] = {
        LauncherType = "Tiny Particle Blaster",
        ProjectileRecipe = "Tiny Iron Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Railgun"] = {
        LauncherType = "Tiny Railgun",
        ProjectileRecipe = "Tiny Trinium Hybrid Charge",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Light Missile Launcher"] = {
        LauncherType = "Tiny Missile Launcher",
        ProjectileRecipe = "Tiny Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Dumbfire Rocket"] = {
        LauncherType = "Tiny Rocket Launcher",
        ProjectileRecipe = "Tiny Explosive Rocket",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Light Torpedo Launcher"] = {
        LauncherType = "Tiny Torpedo Launcher",
        ProjectileRecipe = "Tiny Heavy Kinetic Torpedo",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Mine Layer"] = {
        LauncherType = "Tiny Mine Layer",
        ProjectileRecipe = "Tiny Explosive Mine",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Pulse Laser"] = {
        LauncherType = "Tiny Pulse Beam",
        ProjectileRecipe = "Tiny Thermal Pulse Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Beam Laser"] = {
        LauncherType = "Tiny Beam",
        ProjectileRecipe = "Tiny Thermal Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Plasma Beam"] = {
        LauncherType = "Tiny Plasma Beam",
        ProjectileRecipe = "Tiny Plasma Container",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter Mineral Collection Beam"] = {
        LauncherType = "Tiny Mining Beam",
        ProjectileRecipe = "Tiny Uncommon Mining Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Autocannon"] = {
        LauncherType = "Medium Auto Cannon",
        ProjectileRecipe = "Medium Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Antimatter Cannon"] = {
        LauncherType = "Medium Particle Blaster",
        ProjectileRecipe = "Medium Antihydrogen Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Artillery"] = {
        LauncherType = "Medium Cannon",
        ProjectileRecipe = "Medium Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Blaster"] = {
        LauncherType = "Medium Particle Blaster",
        ProjectileRecipe = "Medium Iron Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser MAC"] = {
        LauncherType = "Medium Mass Accelerator Cannon",
        ProjectileRecipe = "Medium Depleted Uranium MAC Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Pulse Cannon"] = {
        LauncherType = "Medium Cannon",
        ProjectileRecipe = "Medium Pulse Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Railgun"] = {
        LauncherType = "Medium Railgun",
        ProjectileRecipe = "Medium Trinium Hybrid Charge",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Heavy Missile Launcher"] = {
        LauncherType = "Medium Missile Launcher",
        ProjectileRecipe = "Medium Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Heavy Torpedo Launcher"] = {
        LauncherType = "Medium Torpedo Launcher",
        ProjectileRecipe = "Medium Heavy Kinetic Torpedo",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Pulse Laser"] = {
        LauncherType = "Medium Pulse Beam",
        ProjectileRecipe = "Medium Thermal Pulse Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Beam Laser"] = {
        LauncherType = "Medium Beam",
        ProjectileRecipe = "Medium Thermal Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser Mining Laser"] = {
        LauncherType = "Medium Mining Beam",
        ProjectileRecipe = "Medium Uncommon Mining Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Autocannon"] = {
        LauncherType = "Small Auto Cannon",
        ProjectileRecipe = "Small Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Blaster"] = {
        LauncherType = "Small Particle Blaster",
        ProjectileRecipe = "Small Iron Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Railgun"] = {
        LauncherType = "Small Railgun",
        ProjectileRecipe = "Small Trinium Hybrid Charge",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Missile Launcher"] = {
        LauncherType = "Small Missile Launcher",
        ProjectileRecipe = "Small Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Torpedo Launcher"] = {
        LauncherType = "Small Torpedo Launcher",
        ProjectileRecipe = "Small Heavy Kinetic Torpedo",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Swarm Missile"] = {
        LauncherType = "Small Swarm Missile Launcher",
        ProjectileRecipe = "Small Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Pulse Laser"] = {
        LauncherType = "Small Pulse Beam",
        ProjectileRecipe = "Small Thermal Pulse Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Beam Laser"] = {
        LauncherType = "Small Beam",
        ProjectileRecipe = "Small Thermal Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Ship Repair Beam"] = {
        LauncherType = "Small Beam",
        ProjectileRecipe = "Small Shield Recharge Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Salvaging Laser"] = {
        LauncherType = "Small Salvaging Beam",
        ProjectileRecipe = "Small Uncommon Salvaging Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Mining Laser"] = {
        LauncherType = "Small Mining Beam",
        ProjectileRecipe = "Small Uncommon Mining Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate Plasma Beam"] = {
        LauncherType = "Small Plasma Beam",
        ProjectileRecipe = "Small Plasma Container",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Autocannon"] = {
        LauncherType = "X-Large Auto Cannon",
        ProjectileRecipe = "X-Large Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Big Bertha"] = {
        LauncherType = "X-Large Big Bertha",
        ProjectileRecipe = "X-Large Shielded Antianubium Plasma Charge",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Artillery"] = {
        LauncherType = "X-Large Cannon",
        ProjectileRecipe = "X-Large Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Pulse Cannon"] = {
        LauncherType = "X-Large Cannon",
        ProjectileRecipe = "X-Large Pulse Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital MAC"] = {
        LauncherType = "X-Large Mass Accelerator Cannon",
        ProjectileRecipe = "X-Large Depleted Uranium MAC Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Railgun"] = {
        LauncherType = "X-Large Railgun",
        ProjectileRecipe = "X-Large Trinium Hybrid Charge",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Blaster"] = {
        LauncherType = "X-Large Particle Blaster",
        ProjectileRecipe = "X-Large Iron Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Cruise Missile Launcher"] = {
        LauncherType = "X-Large Missile Launcher",
        ProjectileRecipe = "X-Large Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Spartan Torpedo Launcher"] = {
        LauncherType = "X-Large Spartan Torpedo Launcher",
        ProjectileRecipe = "X-Large Heavy Kinetic Torpedo",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Pulse Laser"] = {
        LauncherType = "X-Large Pulse Beam",
        ProjectileRecipe = "X-Large Thermal Pulse Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Beam Laser"] = {
        LauncherType = "X-Large Beam",
        ProjectileRecipe = "X-Large Thermal Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Mining Laser"] = {
        LauncherType = "X-Large Mining Beam",
        ProjectileRecipe = "X-Large Uncommon Mining Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Shipyard Repair Beam"] = {
        LauncherType = "X-Large Beam",
        ProjectileRecipe = "X-Large Shield Recharge Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital Tachyon Beam"] = {
        LauncherType = "X-Large Tachyon Beam",
        ProjectileRecipe = "X-Large Tachyon Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    -- Really ancient style naming below

    ["Battleship_Missiles_Cruise Missile Launcher"] = {
        LauncherType = "Large Missile Launcher",
        ProjectileRecipe = "Large Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship_Missiles_Spartan Torpedo Launcher"] = {
        LauncherType = "Large Spartan Torpedo Launcher",
        ProjectileRecipe = "Large Heavy Kinetic Torpedo",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter_Missiles_Light Missile Launcher"] = {
        LauncherType = "Tiny Missile Launcher",
        ProjectileRecipe = "Tiny Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter_Missiles_Dumbfire Rocket"] = {
        LauncherType = "Tiny Rocket Launcher",
        ProjectileRecipe = "Tiny Explosive Rocket",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter_Missiles_Light Torpedo Launcher"] = {
        LauncherType = "Tiny Torpedo Launcher",
        ProjectileRecipe = "Tiny Heavy Kinetic Torpedo",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Missiles_Heavy Missile Launcher"] = {
        LauncherType = "Medium Missile Launcher",
        ProjectileRecipe = "Medium Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Missiles_Heavy Torpedo Launcher"] = {
        LauncherType = "Medium Torpedo Launcher",
        ProjectileRecipe = "Medium Heavy Kinetic Torpedo",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate_Missiles_Missile Launcher"] = {
        LauncherType = "Small Missile Launcher",
        ProjectileRecipe = "Small Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate_Missiles_Torpedo Launcher"] = {
        LauncherType = "Small Torpedo Launcher",
        ProjectileRecipe = "Small Heavy Kinetic Torpedo",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate_Missiles_Swarm Missile"] = {
        LauncherType = "Small Swarm Missile Launcher",
        ProjectileRecipe = "Small Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Missiles_Cruise Missile Launcher"] = {
        LauncherType = "X-Large Missile Launcher",
        ProjectileRecipe = "X-Large Explosive Missile",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Missiles_Spartan Torpedo Launcher"] = {
        LauncherType = "X-Large Spartan Torpedo Launcher",
        ProjectileRecipe = "X-Large Heavy Kinetic Torpedo",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship_Beams_Pulse Laser"] = {
        LauncherType = "Large Pulse Beam",
        ProjectileRecipe = "Large Thermal Pulse Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship_Beams_Beam Laser"] = {
        LauncherType = "Large Beam",
        ProjectileRecipe = "Large Thermal Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter_Beams_Pulse Laser"] = {
        LauncherType = "Tiny Pulse Beam",
        ProjectileRecipe = "Tiny Thermal Pulse Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter_Beams_Beam Laser"] = {
        LauncherType = "Tiny Beam",
        ProjectileRecipe = "Tiny Thermal Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Beams_Pulse Laser"] = {
        LauncherType = "Medium Pulse Beam",
        ProjectileRecipe = "Medium Thermal Pulse Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Beams_Beam Laser"] = {
        LauncherType = "Medium Beam",
        ProjectileRecipe = "Medium Thermal Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Beams_Mining Laser"] = {
        LauncherType = "Medium Mining Beam",
        ProjectileRecipe = "Medium Uncommon Mining Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate_Beams_Pulse Laser"] = {
        LauncherType = "Small Pulse Beam",
        ProjectileRecipe = "Small Thermal Pulse Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate_Beams_Beam Laser"] = {
        LauncherType = "Small Beam",
        ProjectileRecipe = "Small Thermal Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate_Beams_Salvaging Laser"] = {
        LauncherType = "Small Salvaging Beam",
        ProjectileRecipe = "Small Uncommon Salvaging Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Beams_Pulse Laser"] = {
        LauncherType = "X-Large Pulse Beam",
        ProjectileRecipe = "X-Large Thermal Pulse Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Beams_Beam Laser"] = {
        LauncherType = "X-Large Beam",
        ProjectileRecipe = "X-Large Thermal Laser Crystal",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship_Cannons_Autocannon"] = {
        LauncherType = "Large Auto Cannon",
        ProjectileRecipe = "Large Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship_Cannons_Antimatter Cannon"] = {
        LauncherType = "Large Particle Blaster",
        ProjectileRecipe = "Large Antihydrogen Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship_Cannons_1400mm Artillery"] = {
        LauncherType = "Large Cannon",
        ProjectileRecipe = "Large Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship_Cannons_Blaster"] = {
        LauncherType = "Large Particle Blaster",
        ProjectileRecipe = "Large Iron Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship_Cannons_Pulse Cannon"] = {
        LauncherType = "Large Cannon",
        ProjectileRecipe = "Large Pulse Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Battleship_Cannons_MAC"] = {
        LauncherType = "Large Mass Accelerator Cannon",
        ProjectileRecipe = "Large Depleted Uranium MAC Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter_Cannons_Autocannon"] = {
        LauncherType = "Tiny Auto Cannon",
        ProjectileRecipe = "Tiny Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter_Cannons_Blaster"] = {
        LauncherType = "Tiny Particle Blaster",
        ProjectileRecipe = "Tiny Iron Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Fighter_Cannons_Railgun"] = {
        LauncherType = "Tiny Railgun",
        ProjectileRecipe = "Tiny Trinium Hybrid Charge",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Cannons_Autocannon"] = {
        LauncherType = "Medium Auto Cannon",
        ProjectileRecipe = "Medium Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Cannons_Antimatter Cannon"] = {
        LauncherType = "Medium Particle Blaster",
        ProjectileRecipe = "Medium Antihydrogen Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Cannons_Artilery"] = {
        LauncherType = "Medium Cannon",
        ProjectileRecipe = "Medium Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Cannons_Blaster"] = {
        LauncherType = "Medium Particle Blaster",
        ProjectileRecipe = "Medium Iron Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Cannons_MAC"] = {
        LauncherType = "Medium Mass Accelerator Cannon",
        ProjectileRecipe = "Medium Depleted Uranium MAC Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Cannons_Pulse Cannon"] = {
        LauncherType = "Medium Cannon",
        ProjectileRecipe = "Medium Pulse Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Cruiser_Cannons_Railgun"] = {
        LauncherType = "Medium Railgun",
        ProjectileRecipe = "Medium Trinium Hybrid Charge",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate_Cannons_Autocannon"] = {
        LauncherType = "Small Auto Cannon",
        ProjectileRecipe = "Small Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate_Cannons_Blaster"] = {
        LauncherType = "Small Particle Blaster",
        ProjectileRecipe = "Small Iron Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },

    ["Frigate_Cannons_Railgun"] = {
        LauncherType = "Small Railgun",
        ProjectileRecipe = "Small Trinium Hybrid Charge",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Cannons_Autocannon"] = {
        LauncherType = "X-Large Auto Cannon",
        ProjectileRecipe = "X-Large Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Cannons_Big Bertha"] = {
        LauncherType = "X-Large Big Bertha",
        ProjectileRecipe = "X-Large Shielded Antianubium Plasma Charge",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Cannons_Artilery"] = {
        LauncherType = "X-Large Cannon",
        ProjectileRecipe = "X-Large Solid Trinium Cannon Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Cannons_Pulse Cannon"] = {
        LauncherType = "X-Large Cannon",
        ProjectileRecipe = "X-Large Pulse Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Cannons_MAC"] = {
        LauncherType = "X-Large Mass Accelerator Cannon",
        ProjectileRecipe = "X-Large Depleted Uranium MAC Round",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Cannons_Railgun"] = {
        LauncherType = "X-Large Railgun",
        ProjectileRecipe = "X-Large Trinium Hybrid Charge",
        ProjectileRecipeOwner = "NULL"
    },

    ["Capital_Cannons_Blaster"] = {
        LauncherType = "X-Large Particle Blaster",
        ProjectileRecipe = "X-Large Iron Particle Canister",
        ProjectileRecipeOwner = "NULL"
    },
}

SC.Compatibility = SC.Compatibility or {}
SC.Compatibility.WeaponReplacements = WeaponReplacements

local function MakeSC2Weapon(Ply, Data)
	Data.Class = "sc_module_weapon"

    if Data.EntityMods then
        local WireData = Data.EntityMods.WireDupeInfo
        local OldSC2Data = Data.EntityMods.SC2DupeInfo

        -- If it wasn't there, check for some REALLY OLD SHIT
        if not OldSC2Data and WireData then
            OldSC2Data = WireData.sc_data
        end

        if OldSC2Data then
            local ReplacementData = WeaponReplacements[OldSC2Data.asdf]
            if not ReplacementData then
                if not OldSC2Data.asdf and not OldSC2Data.adsf then
                    SC.Error(string.format("[SC2 Compatibility] - broken dupe data for old (ancient?) weapon, WTF DID YOU JUST SPAWN"), 5)
                elseif not OldSC2Data.asdf or OldSC2Data.asdf:match("%S") == nil then
                    SC.Error(string.format("[SC2 Compatibility] - broken dupe data for old (ancient?) weapon %s, DataFile entry was missing", OldSC2Data.adsf), 5)
                else
                    SC.Error(string.format("[SC2 Compatibility] - could not find replacement data for old (ancient?) weapon %s", OldSC2Data.asdf), 5)
                end
            end

            local NewData = {
                Launcher = ReplacementData,
                Upgrades = {}
            }

            Data.EntityMods.SC2DupeInfo = NewData
        end
    end

	return GAMEMODE.MakeEnt(Ply, Data)
end
duplicator.RegisterEntityClass("sc_weapon_base", MakeSC2Weapon, "Data")
duplicator.RegisterEntityClass("sc_weapon_missile", MakeSC2Weapon, "Data")
duplicator.RegisterEntityClass("sc_weapon_beam", MakeSC2Weapon, "Data")

-- Replace removed entities with props
local function MakePropReplacement(Ply, Data)
	Data.Class = "prop_physics"

	return GAMEMODE.MakeEnt(Ply, Data)
end
duplicator.RegisterEntityClass("sc_module", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("sc_ls_core", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("ls_core", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("storage_cache", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("storage_energy", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("storage_gas", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("storage_gas_steam", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("storage_hot_liquid_nitrogen", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("storage_liquid_nitrogen", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("storage_liquid_hvywater", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("storage_liquid_water", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("generator_liquid_nitrogen", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("generator_ramscoop", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("generator_energy_quantum", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("other_spotlight", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("resource_node", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("base_climate_control", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("livable_module", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("sbep_base_docking_clamp", MakePropReplacement, "Data")
duplicator.RegisterEntityClass("sbep_docking_clamp", MakePropReplacement, "Data")

-- Replace SBEP gyropods
local function MakeSBEPGyro(Ply, Data)
    Data.Class = "sc_gyropod"

    if Data.EntityMods then
        local AdvPodData = Data.EntityMods.SBEPGyroAdv
        if AdvPodData then
            Data.EntityMods.SC2DupeInfo = {
                Pod = AdvPodData.Pod
            }
        end
    end

	return GAMEMODE.MakeEnt(Ply, Data)
end
duplicator.RegisterEntityClass("gyropod_advanced", MakeSBEPGyro, "Data")
duplicator.RegisterEntityClass("wireless_gyropod", MakeSBEPGyro, "Data")

-- Replace SB3 generators
local function ReplaceGenerator(SB3Generator, SC2Generator, ExtraData)
    local function MakeGenerator(Ply, Data)
        local GeneratorInfo = GAMEMODE:GetGeneratorInfo(SC2Generator)
        if GeneratorInfo then
            Data.Class = GeneratorInfo.Class
            Data.EntityMods = Data.EntityMods or {}
            Data.EntityMods.SC2DupeInfo = ExtraData or {}
            Data.EntityMods.SC2DupeInfo.type = SC2Generator
        else
            Data.Class = "prop_physics"
        end

        return GAMEMODE.MakeEnt(Ply, Data)
    end

    duplicator.RegisterEntityClass(SB3Generator, MakeGenerator, "Data")
end

ReplaceGenerator("generator_gas", "Atmospheric Compressor")
ReplaceGenerator("generator_gas_steam", "Water Heater")
ReplaceGenerator("generator_gas_o2h_water", "Water Splitter")
ReplaceGenerator("generator_energy_hydro", "Hydrostatic Generator")
ReplaceGenerator("generator_energy_solar", "Solar Panel")
ReplaceGenerator("generator_energy_steam_turbine", "Steam Turbine")
ReplaceGenerator("generator_liquid_water", "Water Pump")
ReplaceGenerator("generator_liquid_water2", "Hydrogen Fuel Cell")
ReplaceGenerator("other_dispenser", "Suit Dispenser")
ReplaceGenerator("other_probe", "Atmospheric Probe")

-- Reactors have special needs
ReplaceGenerator("generator_energy_fusion", "Fusion Reactor", {
    IsAuxiliary = false,
    ReactorName = "Fusion Reactor",
    ReactorType = 3,
    ReactorReaction = 1,
    ReactorCoolant = 1
})

ReplaceGenerator("pn_reactor", "Fission Reactor", {
    IsAuxiliary = false,
    ReactorName = "Fission Reactor",
    ReactorType = 4,
    ReactorReaction = 5,
    ReactorCoolant = 1
})

-- Class aliases for entities that changed names
hook.Add("InitPostEntity", "SC.Compatibility.LoadAliases", function()
    SC.ClassAlias("sc_generator", "sc_water_pump")
    SC.ClassAlias("ship_core", "ship_core_base")
    SC.ClassAlias("ship_core", "ship_core_test")
    SC.ClassAlias("ship_core", "ship_core_caldari")
    SC.ClassAlias("ship_core", "ship_core_amarr")
    SC.ClassAlias("ship_core", "ship_core_gallente")
    SC.ClassAlias("ship_core", "ship_core_minmatar")
end)