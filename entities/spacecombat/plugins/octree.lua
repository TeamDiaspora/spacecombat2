--[[
Space Combat Octree Builder - Created by Lt.Brandon, based on work by Steeveeo

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

--Global Tables
SC.Octree = {}
SC.Octree.Builder = {}
SC.Octree.Octant = {}

--Octant Functions
function SC.Octree.Octant:New(Origin, Size, IsRoot, Parent)
    if not Origin or not Size or ((not IsRoot) and (not Parent)) then
        SC.Error("[Space Combat 2 - Octree Builder] Unable to create octant, invalid parameters\n")
        return false
    end

    local obj = {}
	setmetatable(obj,self)
    obj.Parent = Parent
    obj.Size = Size
    obj.Position = Origin
    obj.Children = nil
    obj.IsRoot = IsRoot
    obj.IsBlocked = false

    if IsRoot then
        obj.Root = obj
        obj.OctantCount = 1
    else
        obj.Root = Parent.Root
        obj.Root.OctantCount = obj.Root.OctantCount + 1
    end

    return obj
end

function SC.Octree.Octant:IsLeaf()
    return self.Children ~= nil
end

function SC.Octree.Octant:IsPointInOctant(Point)
    local Center = self.Position
    local Size = self.Size * 0.5
    local Min = Center - Vector(Size, Size, Size)
    local Max = Center + Vector(Size, Size, Size)

    return Point:WithinAABox(Min, Max)
end

function SC.Octree.Octant:GetPointOctant(Point)
    if self:IsLeaf() and self:IsPointInOctant(Point) then
        return self
    elseif self.Children then
        local Octants = self.Children
        for I = 1, 8 do
            local SubOctant = Octants[I]
            if SubOctant:IsPointInOctant(Point) then
                return SubOctant:GetPointOctant(Point)
            end
        end
    else
        SC.Error("[Space Combat 2 - Octree Builder] - Unable to find the octant which held the point!\n")
        return false
    end
end

function SC.Octree.Octant:Check(CheckEntities)
    local Size = self.Size * 0.5
    local Trace = util.TraceHull({
	    start = self.Position,
	    endpos = self.Position,
	    filter = player.GetAll(),
	    mins = Vector(Size, Size, Size),
	    maxs = Vector(Size, Size, Size),
        mask = CheckEntities and nil or MASK_NPCWORLDSTATIC
    })

    if Trace.Hit then
        return true
    end

    return false
end

function SC.Octree.Octant:Subdivide()
    local Origin = self.Position
    local NewScanVolume = self.Size * 0.5
    local Dist = NewScanVolume * 0.5

    local Children = {}
    Children[1] = SC.Octree.Octant:New(Origin + Vector(Dist, Dist, Dist), NewScanVolume, false, self)
    Children[2] = SC.Octree.Octant:New(Origin + Vector(Dist, -Dist, Dist), NewScanVolume, false, self)
    Children[3] = SC.Octree.Octant:New(Origin + Vector(-Dist, Dist, Dist), NewScanVolume, false, self)
    Children[4] = SC.Octree.Octant:New(Origin + Vector(-Dist, -Dist, Dist), NewScanVolume, false, self)
    Children[5] = SC.Octree.Octant:New(Origin + Vector(Dist, Dist, -Dist), NewScanVolume, false, self)
    Children[6] = SC.Octree.Octant:New(Origin + Vector(Dist, -Dist, -Dist), NewScanVolume, false, self)
    Children[7] = SC.Octree.Octant:New(Origin + Vector(-Dist, Dist, -Dist), NewScanVolume, false, self)
    Children[8] = SC.Octree.Octant:New(Origin + Vector(-Dist, -Dist, -Dist), NewScanVolume, false, self)

    self.Children = Children
end
SC.Octree.Octant.__index = SC.Octree.Octant

--Builder Functions
function SC.Octree.Builder:New(Position, Size, MaxDepth, CheckEntities, MaxTime, FinishedCallback)
    local obj = {}
	setmetatable(obj,self)
    obj.Position = Position
    obj.Size = Size
    obj.MaxDepth = MaxDepth
    obj.CheckEntities = CheckEntities
    obj.MaxTime = MaxTime
    obj.RootOctant = SC.Octree.Octant:New(Position, Size, true)
    obj.RootOctant:Subdivide()
    obj.Finished = false
    obj.CurDepth = 1
    obj.CurParent = obj.RootOctant
    obj.CurOctant = obj.RootOctant.Children[1]
    obj.OctantIndex = {}
    obj.OctantIndex[1] = 1
    obj.FinishedCallback = FinishedCallback
    obj.StartTime = -1
    obj.StopTime = -1

    return obj
end

function SC.Octree.Builder:Rebuild(running)
    if not running then self.StartTime = SysTime() end
    local Finished = self.Finished
    local finishtime = SysTime() + self.MaxTime
    local time = SysTime()

    while time < finishtime and not Finished do
        time = SysTime()

        --Check if this octant should be a leaf
        if not self.CurOctant:Check(self.CheckEntities) then
            --Iterate back up the Octree
            while self.OctantIndex[self.CurDepth] >= 8 do

                self.CurDepth = self.CurDepth - 1

                --Top of tree, break
                if self.CurDepth < 1 then
                    Finished = true
                    break
                else
                    self.CurOctant = self.CurParent
                    self.CurParent = self.CurOctant.Parent
                end
            end
        --We found something, so subdivide
        else
            --We are at max depth, pop back up a level
            if self.CurDepth >= self.MaxDepth then
                --Mark Invalid
                self.CurOctant.IsBlocked = true

                while self.OctantIndex[self.CurDepth] >= 8 do
                    self.CurDepth = self.CurDepth - 1

                    --Top of tree, break
                    if self.CurDepth < 1 then
                        Finished = true
                        break
                    else
                        self.CurOctant = self.CurParent
                        self.CurParent = self.CurOctant.Parent
                    end
                end
            --Else subdivide and iterate downwards
            else
                self.CurOctant:Subdivide()
                self.CurDepth = self.CurDepth + 1

                self.OctantIndex[self.CurDepth] = 0
                self.CurParent = self.CurOctant
            end
        end

        --Next Octant
        if not Finished then
            self.OctantIndex[self.CurDepth] = self.OctantIndex[self.CurDepth] + 1
            self.CurOctant = self.CurParent.Children[self.OctantIndex[self.CurDepth]]
        end
    end

    if not Finished then
        SC.Error("[Space Combat 2 - Octree Builder] - Built "..self.RootOctant.OctantCount.." Octants\n")
        timer.Simple(0.1, function()
            if self then
                self:Rebuild(true)
            end
        end)
    else
        self.StopTime = SysTime()
        SC.Error("[Space Combat 2 - Octree Builder] - Finished Building.\n")
        self.Finished = true

        if self.FinishedCallback then
            self:FinishedCallback()
        end
    end
end
SC.Octree.Builder.__index = SC.Octree.Builder