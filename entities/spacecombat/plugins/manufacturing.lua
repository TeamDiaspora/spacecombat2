--[[
Space Combat Manufacturing - Created by Lt.Brandon

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

AddCSLuaFile()

--Local table called this, because screw typing SC.Manufacturing.<whatever> constantly.
local this = {}
this.Recipes = {}
this.DefaultRecipes = {}

--Prevents things from erroring out when a recipe doesn't exist, just incase someone didn't check if it was valid.
this.InvalidRecipe = {time=1,capacitor=0,requires={},produces={}}

--Utility function to see if a string is valid
local function CheckString(str)
	str = string.Trim(str)
	return ((str ~= "") and (str ~= nil))
end

--Chekcs if a type is valid
function this.IsValidType(type)
	return CheckString(type) and not (this.Recipes[type] == nil)
end

--Checks if a recipe is valid
function this.IsValidRecipe(name, type)
	if (this.Recipes[type] == nil) or (this.Recipes[type][name] == nil) then return false end

	return true
end

--Gets a recipe's table
function this.GetRecipe(name, type)
	if not this.IsValidRecipe(name, type) then return this.InvalidRecipe end

	return this.Recipes[type][name]
end

--Fetches the tables of all recipes of the specified type
function this.GetRecipes(type)
	return this.Recipes[type] or {}
end

--Returns a list of all the recipe names of a type
function this.GetRecipeList(type)
	return table.GetKeys(this.Recipes[type] or {})
end

--Gets the default recipe for a type
function this.GetDefaultRecipe(type)
	local name = this.DefaultRecipes[type]
	if name == nil then
		return "", this.InvalidRecipe
	end

	return name, this.GetRecipe(name, type)
end

--Adds a new recipe to the table
function this.AddRecipe(name, type, time, capacitor, requires, produces, default)
	if not CheckString(name) or not CheckString(type) or not time or not capacitor or not requires or not produces or this.IsValidRecipe(name, type) then return end

    -- hack because i'm being lazy
    requires.Energy = capacitor

	local tbl = {time=time,capacitor=capacitor,requires=requires,produces=produces}

	if not this.Recipes[type] then this.Recipes[type] = {} end

	this.Recipes[type][name] = tbl

	if default then this.DefaultRecipes[type] = name end
end

--Default refinery recipes
this.AddRecipe("Water", "Refinery", 3, 30000, {["Ice"]=5000}, {["Water"]=75000}, true)
this.AddRecipe("Ship Fuel", "Refinery", 10, 20000, {["Polonium Nitrate"]=160, ["Nubium"]=40, ["Empty Canister"]=20}, {["Ship Fuel Canister"]=20})
this.AddRecipe("Nubium", "Refinery", 20, 180000, {["Solid Nubium"]=600}, {["Nubium"]=60})
this.AddRecipe("Anubium", "Refinery", 30, 120000, {["Polonium Nitrate"]=20, ["Nubium"]=800, ["Titanite"]=60, ["Trinium"]=200}, {["Anubium"]=40})
this.AddRecipe("Tritanium", "Refinery", 15, 30000, {["Veldspar"]=200}, {["Tritanium"]=400, ["Iron"]=200})
this.AddRecipe("Polonium Nitrate", "Refinery", 15, 5000, {["Nitrogen"]=4000, ["Oxygen"]=12000, ["Polonium"]=2000}, {["Polonium Nitrate"]=2000})
this.AddRecipe("Lithium Isotope Separation", "Refinery", 15, 3000, {["Lithium"]=1000}, {["Lithium-6"]=100, ["Lithium-7"]=900})
this.AddRecipe("Steel", "Refinery", 30, 50000, {["Iron"]=5000, ["Carbon"]=2000}, {["Steel"]=5000})
this.AddRecipe("Ground Beef", "Refinery", 30, 50000, {["Beef"]=50}, {["Ground Beef"]=50})
this.AddRecipe("Burger Patty", "Refinery", 300, 150000, {["Ground Beef"]=50}, {["Burger Patty"]=5})
this.AddRecipe("Bread", "Refinery", 300, 50000, {["Wheat"]=500, ["Water"]=5000}, {["Bread"]=10})
this.AddRecipe("Battle-Bread", "Refinery", 600, 50000, {["Bread"]=10, ["Processed Cheese"]=50, ["Dorito Monster Concentrate"]=10}, {["Battle-Bread"]=10})
this.AddRecipe("Cheese", "Refinery", 60, 30000, {["Milk"]=1000}, {["Cheese"]=500})
this.AddRecipe("Processed Cheese", "Refinery", 60, 30000, {["Annatto"]=100, ["Cheese"]=500}, {["Processed Cheese"]=500})
this.AddRecipe("Dorito", "Refinery", 60, 30000, {["Corn"]=1000, ["Cheese"]=500}, {["Dorito"]=100})
this.AddRecipe("Dorito Monster Concentrate", "Refinery", 300, 300000, {["Dorito"]=500, ["Monster Energy"]=5000}, {["Dorito Monster Concentrate"]=5000})

--#region Default factory recipes
this.AddRecipe("Empty Canister", "Factory", 10, 20000, {["Iron"]=500}, {["Empty Canister"]=100}, true)
this.AddRecipe("Tiny Capacitor Charge", "Factory", 10, 5000, {["Empty Canister"]=50, ["Anubium"]=50}, {["Tiny Capacitor Charge"]=50})
this.AddRecipe("Small Capacitor Charge", "Factory", 10, 10000, {["Empty Canister"]=50, ["Anubium"]=500}, {["Small Capacitor Charge"]=50})
this.AddRecipe("Medium Capacitor Charge", "Factory", 15, 15000, {["Empty Canister"]=50, ["Anubium"]=1000}, {["Medium Capacitor Charge"]=50})
this.AddRecipe("Large Capacitor Charge", "Factory", 20, 20000, {["Empty Canister"]=50, ["Anubium"]=2000}, {["Large Capacitor Charge"]=50})
this.AddRecipe("X-Large Capacitor Charge", "Factory", 20, 25000, {["Empty Canister"]=50, ["Anubium"]=5000}, {["X-Large Capacitor Charge"]=50})
this.AddRecipe("Nanite Canister", "Factory", 10, 75000, {["Empty Canister"]=500, ["Anubium"]=5, ["Nubium"]=60, ["Iron"]=100, ["Tritanium"]=500}, {["Nanite Canister"]=500})
this.AddRecipe("Anti-Hydrogen Particle Canister", "Factory", 30, 100000, {["Empty Canister"]=100, ["Antihydrogen"]=100}, {["Anti-Hydrogen Particle Canister"]=100})
this.AddRecipe("Anti-Anubium Particle Canister", "Factory", 30, 100000, {["Empty Canister"]=100, ["Antianubium"]=100}, {["Anti-Anubium Particle Canister"]=100})
this.AddRecipe("Iron Particle Canister", "Factory", 30, 50000, {["Empty Canister"]=100, ["Iron"]=100}, {["Iron Particle Canister"]=100})
this.AddRecipe("Depleted Uranium Particle Canister", "Factory", 30, 75000, {["Empty Canister"]=100, ["Depleted Uranium"]=100}, {["Depleted Uranium Particle Canister"]=100})
this.AddRecipe("Kleiner Particle Canister", "Factory", 300, 750000, {["Anti-Anubium Particle Canister"]=10, ["Kleiner"]=1}, {["Kleiner Particle Canister"]=10})
this.AddRecipe("Rocket Fuel Canister", "Factory", 10, 75000, {["Empty Canister"]=100, ["Methane"]=20000, ["Oxygen"]=20000}, {["Rocket Fuel Canister"]=100})
this.AddRecipe("Lifesupport Canister", "Factory", 10, 75000, {["Empty Canister"]=100, ["Oxygen"]=40000}, {["Lifesupport Canister"]=100})
this.AddRecipe("Battle-Burger", "Factory", 10, 75000, {["Processed Cheese"]=50, ["Bread"]=20, ["Burger Patty"]=40}, {["Battle-Burger"]=10})

-- Wiring
this.AddRecipe("Aluminum Wires", "Factory", 10, 20000, {["Aluminum"]=500}, {["Aluminum Wires"]=100})
this.AddRecipe("Copper Wires", "Factory", 10, 20000, {["Copper"]=500}, {["Copper Wires"]=100})
this.AddRecipe("Gold Wires", "Factory", 10, 20000, {["Gold"]=500}, {["Gold Wires"]=100})
this.AddRecipe("Silver Wires", "Factory", 10, 20000, {["Silver"]=500}, {["Silver Wires"]=100})
this.AddRecipe("Voltarium Wires", "Factory", 10, 20000, {["Voltarium"]=500}, {["Voltarium Wires"]=100})

-- Plates
this.AddRecipe("Iron Plate", "Factory", 10, 20000, {["Iron"]=500}, {["Iron Plate"]=100})
this.AddRecipe("Aluminum Plate", "Factory", 10, 20000, {["Aluminum"]=500}, {["Aluminum Plate"]=100})
this.AddRecipe("Steel Plate", "Factory", 10, 20000, {["Steel"]=500}, {["Steel Plate"]=100})
this.AddRecipe("Tritanium Plate", "Factory", 10, 20000, {["Tritanium"]=500}, {["Tritanium Plate"]=100})
this.AddRecipe("Trinium Plate", "Factory", 10, 20000, {["Trinium"]=500}, {["Trinium Plate"]=100})

-- Gears
this.AddRecipe("Iron Gear", "Factory", 10, 20000, {["Iron"]=500}, {["Iron Gear"]=100})
this.AddRecipe("Aluminum Gear", "Factory", 10, 20000, {["Aluminum"]=500}, {["Aluminum Gear"]=100})
this.AddRecipe("Steel Gear", "Factory", 10, 20000, {["Steel"]=500}, {["Steel Gear"]=100})
this.AddRecipe("Tritanium Gear", "Factory", 10, 20000, {["Tritanium"]=500}, {["Tritanium Gear"]=100})
this.AddRecipe("Trinium Gear", "Factory", 10, 20000, {["Trinium"]=500}, {["Trinium Gear"]=100})

-- Upgrades
this.AddRecipe("Basic Generator", "Factory", 10, 20000, {["Iron Plate"]=20, ["Iron Gear"]=5, ["Aluminum Wires"]=200}, {["Basic Generator"]=1})
this.AddRecipe("Standard Generator", "Factory", 20, 20000, {["Aluminum Plate"]=20, ["Aluminum Gear"]=5, ["Copper Wires"]=200, ["Basic Generator"]=1}, {["Standard Generator"]=1})
this.AddRecipe("Enhanced Generator", "Factory", 30, 20000, {["Steel Plate"]=20, ["Steel Gear"]=5, ["Gold Wires"]=200, ["Standard Generator"]=1}, {["Enhanced Generator"]=1})
this.AddRecipe("Advanced Generator", "Factory", 45, 20000, {["Tritanium Plate"]=20, ["Tritanium Gear"]=5, ["Silver Wires"]=200, ["Enhanced Generator"]=1}, {["Advanced Generator"]=1})
this.AddRecipe("Elite Generator", "Factory", 60, 20000, {["Trinium Plate"]=20, ["Trinium Gear"]=5, ["Voltarium Wires"]=200, ["Advanced Generator"]=1}, {["Elite Generator"]=1})

this.AddRecipe("Basic Turbine", "Factory", 10, 20000, {["Iron Plate"]=200, ["Iron Gear"]=50}, {["Basic Turbine"]=1})
this.AddRecipe("Standard Turbine", "Factory", 20, 20000, {["Aluminum Plate"]=200, ["Aluminum Gear"]=50, ["Basic Turbine"]=1}, {["Standard Turbine"]=1})
this.AddRecipe("Enhanced Turbine", "Factory", 30, 20000, {["Steel Plate"]=200, ["Steel Gear"]=50, ["Standard Turbine"]=1}, {["Enhanced Turbine"]=1})
this.AddRecipe("Advanced Turbine", "Factory", 45, 20000, {["Tritanium Plate"]=200, ["Tritanium Gear"]=50, ["Enhanced Turbine"]=1}, {["Advanced Turbine"]=1})
this.AddRecipe("Elite Turbine", "Factory", 60, 20000, {["Trinium Plate"]=200, ["Trinium Gear"]=50, ["Advanced Turbine"]=1}, {["Elite Turbine"]=1})

this.AddRecipe("Basic Cooler", "Factory", 10, 20000, {["Iron Plate"]=1000}, {["Basic Cooler"]=1})
this.AddRecipe("Standard Cooler", "Factory", 20, 20000, {["Aluminum Plate"]=1000, ["Basic Cooler"]=1}, {["Standard Cooler"]=1})
this.AddRecipe("Enhanced Cooler", "Factory", 30, 20000, {["Steel Plate"]=1000, ["Standard Cooler"]=1}, {["Enhanced Cooler"]=1})
this.AddRecipe("Advanced Cooler", "Factory", 45, 20000, {["Tritanium Plate"]=1000, ["Enhanced Cooler"]=1}, {["Advanced Cooler"]=1})
this.AddRecipe("Elite Cooler", "Factory", 60, 20000, {["Trinium Plate"]=1000, ["Advanced Cooler"]=1}, {["Elite Cooler"]=1})

this.AddRecipe("Basic Compressor", "Factory", 10, 20000, {["Iron Plate"]=50, ["Iron Gear"]=15, ["Aluminum Wires"]=20}, {["Basic Compressor"]=1})
this.AddRecipe("Standard Compressor", "Factory", 20, 20000, {["Aluminum Plate"]=50, ["Aluminum Gear"]=15, ["Copper Wires"]=20, ["Basic Compressor"]=1}, {["Standard Compressor"]=1})
this.AddRecipe("Enhanced Compressor", "Factory", 30, 20000, {["Steel Plate"]=50, ["Steel Gear"]=15, ["Gold Wires"]=20, ["Standard Compressor"]=1}, {["Enhanced Compressor"]=1})
this.AddRecipe("Advanced Compressor", "Factory", 45, 20000, {["Tritanium Plate"]=50, ["Tritanium Gear"]=15, ["Silver Wires"]=20, ["Enhanced Compressor"]=1}, {["Advanced Compressor"]=1})
this.AddRecipe("Elite Compressor", "Factory", 60, 20000, {["Trinium Plate"]=50, ["Trinium Gear"]=15, ["Voltarium Wires"]=20, ["Advanced Compressor"]=1}, {["Elite Compressor"]=1})

this.AddRecipe("Basic Recuperator", "Factory", 10, 20000, {["Iron Plate"]=200, ["Iron Gear"]=50}, {["Basic Recuperator"]=1})
this.AddRecipe("Standard Recuperator", "Factory", 20, 20000, {["Aluminum Plate"]=200, ["Aluminum Gear"]=50, ["Basic Recuperator"]=1}, {["Standard Recuperator"]=1})
this.AddRecipe("Enhanced Recuperator", "Factory", 30, 20000, {["Steel Plate"]=200, ["Steel Gear"]=50, ["Standard Recuperator"]=1}, {["Enhanced Recuperator"]=1})
this.AddRecipe("Advanced Recuperator", "Factory", 45, 20000, {["Tritanium Plate"]=200, ["Tritanium Gear"]=50, ["Enhanced Recuperator"]=1}, {["Advanced Recuperator"]=1})
this.AddRecipe("Elite Recuperator", "Factory", 60, 20000, {["Trinium Plate"]=200, ["Trinium Gear"]=50, ["Advanced Recuperator"]=1}, {["Elite Recuperator"]=1})
--#endregion

--Default reprocessor recipes
this.AddRecipe("Empty Canister", "Reprocessor", 10, 30000, {["Empty Canister"]=50}, {["Iron"]=200}, true)
this.AddRecipe("Scrap Metal", "Reprocessor", 20, 30000, {["Scrap Metal"]=500}, {["Iron"]=200, ["Tritanium"]=50})
this.AddRecipe("Destroyed Consoles", "Reprocessor", 25, 40000, {["Destroyed Consoles"]=50}, {["Scrap Metal"]=100, ["Electronic Circuitry"]=10})
this.AddRecipe("Nubium Generator Core", "Reprocessor", 40, 60000, {["Nubium Generator Core"]=15}, {["Destroyed Consoles"]=60, ["Scrap Metal"]=500, ["Plasma Conduits"]=70, ["Nubium"]=5000, ["Anubium"]=500})
this.AddRecipe("Plasma Conduits", "Reprocessor", 20, 20000, {["Plasma Conduits"]=50}, {["Scrap Metal"]=20, ["Contaminated Coolant"]=5000})
this.AddRecipe("Contaminated Coolant", "Reprocessor", 5, 10000, {["Contaminated Coolant"]=500}, {["Water"]=5000, ["Radioactive Materials"]=500})
this.AddRecipe("Damaged Shield Emitter", "Reprocessor", 30, 55000, {["Damaged Shield Emitter"]=50}, {["Scrap Metal"]=100, ["Destroyed Consoles"]=20, ["Contaminated Coolant"]=750, ["Plasma Conduits"]=20})
this.AddRecipe("Radioactive Materials", "Reprocessor", 10, 15000, {["Radioactive Materials"]=50}, {["Polonium Nitrate"]=200, ["Uranium"]=50, ["Nubium"]=25})
this.AddRecipe("Electronic Circuitry", "Reprocessor", 20, 30000, {["Electronic Circuitry"]=50}, {["Voltarium"]=200, ["Tritanium"]=50})

--Default farm recipes
this.AddRecipe("Corn", "Farm", 300, 30000, {}, {["Corn"]=500})
this.AddRecipe("Annatto", "Farm", 300, 30000, {}, {["Annatto"]=500})
this.AddRecipe("Cow", "Farm", 300, 30000, {["Corn"]=2500, ["Wheat"]=1000, ["Water"]=10000}, {["Cow"]=1})
this.AddRecipe("Wheat", "Farm", 300, 30000, {}, {["Wheat"]=500})
this.AddRecipe("Milk", "Farm", 600, 90000, {["Cow"]=1}, {["Milk"]=5000})
this.AddRecipe("Beef", "Farm", 600, 90000, {["Cow"]=1}, {["Beef"]=50})
this.AddRecipe("Kleiner", "Farm", 900, 1500000, {["Corn"]=2500, ["Cheese"]=1500, ["Dorito Monster Concentrate"]=10000, ["Battle-Burger"]=1}, {["Kleiner"]=1})

SC.Manufacturing = this