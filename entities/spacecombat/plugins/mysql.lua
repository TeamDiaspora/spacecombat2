--[[
Space Combat MySQL - Created by Lt.Brandon

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

--MYSQL BITCH
require("mysqloo")

SC.MYSQLFuncs = {}

hook.Add("InitPostEntity", "LoadMysqlConfiguration", function()
    if file.Exists(SC.DataFolder.."/config/mysql.txt", "DATA") then
        local NewData = lip.load(SC.DataFolder.."/config/mysql.txt")["Server Settings"]
        if NewData and NewData.Address and NewData.Username and NewData.Password and NewData.Database and NewData.Port then
			--Connect to le server
			local SQL = mysqloo.connect(NewData.Address, NewData.Username, NewData.Password, NewData.Database, NewData.Port)

			-- When the database connects, run a test.
			function SQL:onConnected()
                SC.MYSQL = SQL
                hook.Run("SC.OnMySQLConnected", SQL)

				timer.Simple(1, function()
					if SC.MYSQLFuncs then
						for i,k in pairs(SC.MYSQLFuncs) do
							k()
						end
					end
				end)
			end

			-- On the off chance it fails.
			function SQL:onConnectionFailed(err)
				SC.Error("Connection to database failed!", 5)
                SC.Error("Error: "..err, 5)

                hook.Run("SC.OnMySQLConnectionFailed")
			end

			SQL:connect()
        else
            SC.Error("Malformed mysql configuration file detected!", 5)
        end
    else
        local SaveData = {}

        SaveData["Server Settings"] = {
            ["Address"] = "localhost",
			["Port"] = 3306,
            ["Database"] = "spacecombat2",
			["Username"] = "spacecombat2",
			["Password"] = "PleaseChangeMe1234"
        }

        file.CreateDir(SC.DataFolder.."/config/")
        lip.save(SC.DataFolder.."/config/mysql.txt", SaveData)
    end
end)