--[[
    These functions work on any entity derived from base_scentity

    Function List:
        n e:isSC2Entity
        n e:sc2FittingCPUUsage
        n e:sc2FittingPGUsage
        n e:sc2FittingCanRun
        s e:sc2FittingSlotUsage
        s e:sc2FittingStatus
        a e:sc2FittingClasses
]]

e2function number entity:isSC2Entity()
	if IsValid(this) and this.IsSC2Entity then
		return 1
    end

    return 0
end

e2function number entity:sc2FittingCPUUsage()
	if IsValid(this) and this.IsSC2Entity then
		return this:GetFitting().CPU or 0
    end

    return 0
end

e2function number entity:sc2FittingPGUsage()
	if IsValid(this) and this.IsSC2Entity then
		return this:GetFitting().PG or 0
    end

    return 0
end

e2function number entity:sc2FittingCanRun()
	if IsValid(this) and this.IsSC2Entity and this:GetFitting().CanRun then
		return 1
    end

    return 0
end

e2function string entity:sc2FittingSlotUsage()
	if IsValid(this) and this.IsSC2Entity then
		return this:GetFitting().Slot or "None"
    end

    return "None"
end

e2function string entity:sc2FittingStatus()
	if IsValid(this) and this.IsSC2Entity then
		return this:GetFitting().Status or "Offline"
    end

    return "Offline"
end

e2function array entity:sc2FittingClasses()
    if IsValid(this) and this.IsSC2Entity then
        local Classes = {}
        for i,k in pairs(this:GetFitting().Classes or {}) do
            table.insert(Classes, tostring(k))
        end

		return Classes
    end

    return {}
end