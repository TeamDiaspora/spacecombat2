AddCSLuaFile()
ENT.Type = "anim"
ENT.Base = "base_moduleentity"
ENT.PrintName = "Space Combat 2 - Ship Weapon"
ENT.Author = "Lt.Brandon"
ENT.Purpose = "Painting the sky red with clouds of blood mist."
ENT.Instructions = "DO NOT PRESS!"
local Base = scripted_ents.Get("base_moduleentity")

hook.Add("InitPostEntity", "base_sc_module_weapon_post_entity_init", function()
    Base = scripted_ents.Get("base_moduleentity")
end)

ENT.Spawnable = false
ENT.AdminOnly = false
ENT.PlayerToggleable = true
ENT.IsSCWeapon = true
ENT.SC_HasGenericPodLink = true

function ENT:SharedInit()
    Base.SharedInit(self)
    SC.NWAccessors.CreateNWAccessor(self, "ProjectileRecipe", "cachedstring", "FAIL")
    SC.NWAccessors.CreateNWAccessor(self, "ProjectileRecipeOwner", "cachedstring", "NULL")
    SC.NWAccessors.CreateNWAccessor(self, "LauncherType", "cachedstring", "FAIL")
    SC.NWAccessors.CreateNWAccessor(self, "Reloading", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "Overheated", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "Throttled", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "Ammo", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "MaxAmmo", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "LinkedPod", "entity", NULL)
    SC.NWAccessors.CreateNWAccessor(self, "LinkedTargeter", "entity", NULL)
end

function ENT:IsLinkedToPod()
    return IsValid(self:GetLinkedPod())
end

function ENT:IsLinkedToTargeter()
    return IsValid(self:GetLinkedTargeter())
end

if CLIENT then
    function ENT:Think()
        if not self:BeingLookedAtByLocalPlayer() then return end

        if self:GetHideOverlay() then
            self:SetOverlayText("")

            return
        end

        local Text = self:GetModuleName() .. "\n"
        Text = Text .. self:GetProjectileRecipe() .. "\n\n"
        local FittingText = self:GetFittingText()
        Text = Text .. "Status: "

        if self:GetReloading() then
            Text = Text .. "Reloading\n\n"
        elseif self:GetOverheated() then
            Text = Text .. "Overheated\n\n"
        elseif self:GetCycling() then
            if self:GetThrottled() then
                Text = Text .. "Cycling (Throttled)\n\n"
            else
                Text = Text .. "Cycling\n\n"
            end
        else
            Text = Text .. "Off\n\n"
        end

        if self:GetHasCycle() then
            Text = Text .. "Duration: " .. sc_ds(self:GetCycleDuration()) .. "\n"
            Text = Text .. "Cycle: " .. sc_ds(self:GetCyclePercent() * 100) .. "%\n"
        end

        Text = Text .. "Ammo: " .. sc_ds(self:GetAmmo())
        Text = Text .. FittingText

        if self:IsLinkedToPod() then
            Text = Text .. "\nLinked to Pod: " .. tostring(self:GetLinkedPod():EntIndex())
        end

        if self:IsLinkedToTargeter() then
            Text = Text .. "\nLinked to Targeter: " .. tostring(self:GetLinkedTargeter():EntIndex())
        end

        self:SetOverlayText(Text)
    end
else
    function ENT:Initialize()
        -- Super lazy method to fix wires unlinking
        local Inputs = {"Fire"}
        local InputTypes = {"NORMAL"}

        table.insert(Inputs, "Reload")
        table.insert(InputTypes, "NORMAL")
        table.insert(Inputs, "Target")
        table.insert(InputTypes, "ENTITY")

        local Outputs = {"Can Fire"}
        local OutputTypes = {"NORMAL"}

        table.insert(Outputs, "Reloading")
        table.insert(OutputTypes, "NORMAL")
        table.insert(Outputs, "Capacitor")
        table.insert(OutputTypes, "NORMAL")
        table.insert(Outputs, "Max Capacitor")
        table.insert(OutputTypes, "NORMAL")
        table.insert(Outputs, "Temperature")
        table.insert(OutputTypes, "NORMAL")
        table.insert(Outputs, "Max Temperature")
        table.insert(OutputTypes, "NORMAL")
        table.insert(Outputs, "Ammo")
        table.insert(OutputTypes, "NORMAL")
        table.insert(Outputs, "Max Ammo")
        table.insert(OutputTypes, "NORMAL")

        if WireLib ~= nil then
            self.Inputs = WireLib.CreateSpecialInputs(self, Inputs, InputTypes)
            self.Outputs = WireLib.CreateSpecialOutputs(self, Outputs, OutputTypes)
        end

        -- FIXME: This should be moved into a global method, storing it on every weapon/turret/gyro/etc is dumb.
        self.KeysDown = {}

        hook.Add("PlayerButtonDown", "WeaponKeyPress_" .. self:EntIndex(), function(ply, key)
            if not IsValid(self) or not self:IsLinkedToPod() then return end

            if ply:GetVehicle() == self:GetLinkedPod() then
                self.KeysDown[key] = true
            end
        end)

        hook.Add("PlayerButtonUp", "WeaponKeyRelease_" .. self:EntIndex(), function(ply, key)
            if not IsValid(self) or not self:IsLinkedToPod() then return end

            if ply:GetVehicle() == self:GetLinkedPod() then
                self.KeysDown[key] = nil
            end
        end)

        Base.Initialize(self)
    end

    function ENT:Setup()
        Base.Setup(self)
    end

    function ENT:UnlinkPod()
        if self:IsLinkedToPod() then
            self:SetLinkedPod(NULL)
        end
    end

    function ENT:LinkPod(NewPod)
        if self:IsLinkedToPod() then
            self:UnlinkPod()
        end

        if IsValid(NewPod) and NewPod:IsVehicle() then
            self:SetLinkedPod(NewPod)

            return true
        else
            return false
        end
    end

    function ENT:UnlinkTargeter()
        if self:IsLinkedToTargeter() then
            self:SetLinkedTargeter(NULL)
        end
    end

    function ENT:LinkTargeter(NewTargeter)
        if self:IsLinkedToTargeter() then
            self:UnlinkTargeter()
        end

        if IsValid(NewTargeter) and NewTargeter.IsSCTargeter then
            self:SetLinkedTargeter(NewTargeter)

            return true
        else
            return false
        end
    end

    function ENT:CreateLauncher(LauncherType, Recipe, RecipeOwner, Upgrades)
        local LauncherData = GAMEMODE.Launchers.Types.LoadedTypes[LauncherType]

        if IsValid(self.Launcher) then
            self.Launcher:Remove()
            self.Launcher = nil
        end

        self:SetHasCycle(false)
        self:SetCycling(false)
        self:SetCycleDuration(1)
        self:SetReloading(false)
        self:SetOverheated(false)
        self:SetThrottled(false)
        self:SetAmmo(0)
        self:SetMaxAmmo(0)
        self.Invalid = true
        self.SC_Active = false

        if not LauncherData then
            SC.Error(string.format("Invalid launcher data %s owned by %s for player %s", Recipe, RecipeOwner, tostring(self:GetPlayer())), 5)
            self:SetModuleName("INVALID WEAPON, UPDATE ME D:")

            if IsValid(self:GetProtector()) and self:GetProtector().UpdateModifiers then
                self:GetProtector():UpdateModifiers()
            end

            return
        end

        ---@type Launcher
        self.Launcher = LauncherData:CreateLauncher(self:GetPlayer(), self, nil, Upgrades)

        if not self.Launcher then
            SC.Error(string.format("Failed to create launcher %s owned by %s for player %s", Recipe, RecipeOwner, tostring(self:GetPlayer())), 5)
            self:SetModuleName("INVALID WEAPON, UPDATE ME D:")

            if IsValid(self:GetProtector()) and self:GetProtector().UpdateModifiers then
                self:GetProtector():UpdateModifiers()
            end

            return
        end

        self.Launcher:SetRecipeByName(Recipe, RecipeOwner)
        self:SetFitting(self.Launcher.Fitting)
        local FireDelay = 60 / self.Launcher.ShotsPerMinute

        if self.Launcher.HasBurst then
            FireDelay = FireDelay + self.Launcher.ShotsPerBurst * self.Launcher.TimeBetweenBurstShots
        end

        self:SetHasCycle(true)
        self:SetCycleDuration(FireDelay)
        self:SetModuleName(LauncherData:GetName())
        self:SetProjectileRecipe(Recipe)
        self:SetProjectileRecipeOwner(RecipeOwner)
        self.Upgrades = Upgrades or {}
        self.Invalid = false
        --Recreate Wire
        self:CreateInputs()
        self:CreateOutputs()

        self.Launcher.OnSpawnedCallback = function(Launcher)
            if not IsValid(self) then return end
            self:LauncherOnSpawned(Launcher)
        end

        self.Launcher.OnRemovedCallback = function(Launcher)
            if not IsValid(self) then return end
            self:LauncherOnRemoved(Launcher)
        end

        self.Launcher.OnStartedFiringCallback = function(Launcher)
            if not IsValid(self) then return end
            self:LauncherOnStartedFiring(Launcher)
        end

        self.Launcher.OnStoppedFiringCallback = function(Launcher)
            if not IsValid(self) then return end
            self:LauncherOnStoppedFiring(Launcher)
        end

        self.Launcher.OnReloadingStartedCallback = function(Launcher)
            if not IsValid(self) then return end
            self:LauncherOnReloadingStarted(Launcher)
        end

        self.Launcher.OnReloadingFinishedCallback = function(Launcher)
            if not IsValid(self) then return end
            self:LauncherOnReloadingFinished(Launcher)
        end

        self.Launcher.PostProjectileFiredCallback = function(Launcher, Projectile)
            if not IsValid(self) then return end
            self:LauncherPostProjectileFired(Launcher, Projectile)
        end

        self.Launcher.PreProjectileFiredCallback = function(Launcher, Projectile)
            if not IsValid(self) then return end
            self:LauncherPreProjectileFired(Launcher, Projectile)
        end

        self.Launcher.OnOverheatedCallback = function(Launcher)
            if not IsValid(self) then return end
            self:LauncherOnOverheated(Launcher)
        end

        self.Launcher.OnCooledDownCallback = function(Launcher)
            if not IsValid(self) then return end
            self:LauncherOnCooledDown(Launcher)
        end

        if IsValid(self:GetProtector()) and self:GetProtector().UpdateModifiers then
            self:GetProtector():UpdateModifiers()
        end

        return self.Launcher
    end

    function ENT:CanCycle()
        return IsValid(self:GetProtector()) and self:GetFitting().CanRun
    end

    function ENT:OnCycleStarted()
    end

    function ENT:OnCycleFinished()
    end

    function ENT:LauncherOnSpawned()
    end

    function ENT:LauncherOnRemoved()
    end

    function ENT:LauncherOnStartedFiring()
        self:SetCycling(true)
    end

    function ENT:LauncherOnStoppedFiring()
        self:SetCycling(false)
    end

    function ENT:LauncherOnReloadingStarted()
        self:SetReloading(true)
    end

    function ENT:LauncherOnReloadingFinished()
        self:SetReloading(false)
    end

    function ENT:LauncherPostProjectileFired(Projectile)
        if self:GetCycling() then
            self:OnCycleStarted()
        end
    end

    function ENT:LauncherPreProjectileFired(Projectile)
        self:OnCycleFinished()
    end

    function ENT:LauncherOnOverheated()
        self:SetOverheated(true)
    end

    function ENT:LauncherOnCooledDown()
        self:SetOverheated(false)
    end

    function ENT:OnModuleEnabled()
        if IsValid(self.Launcher) then
            self.Launcher:SetCore(self:GetProtector())
        end
    end

    function ENT:OnModuleDisabled()
        if IsValid(self.Launcher) then
            self.Launcher:SetCore(nil)
        end
    end

    function ENT:OnRemove()
        Base.OnRemove(self)

        if IsValid(self.Launcher) then
            self.Launcher:SetCore(nil)
            self.Launcher:StopFiring()
            self.Launcher:Remove()
        end

        -- FIXME: Remove these when the code in Initialize is removed
        hook.Remove("PlayerButtonDown", "WeaponKeyPress_" .. self:EntIndex())
        hook.Remove("PlayerButtonUp", "WeaponKeyRelease_" .. self:EntIndex())
    end

    function ENT:Think()
        if not self:GetProtector() then
            self:GetFitting().Status = "Offline"
        end

        local Protector = self:GetProtector()
        local FireDelay = 3

        if IsValid(self.Launcher) then
            self.Launcher.Fitting = self.SC_Fitting

            if self:GetCycling() and not self:GetReloading() and not self:GetOverheated() and self.Launcher:HasResources() then
                FireDelay = 60 / (self.Launcher:IsThrottled() and self.Launcher.ThrottledShotsPerMinute or self.Launcher.ShotsPerMinute)

                if self.Launcher.HasBurst then
                    FireDelay = FireDelay + self.Launcher.ShotsPerBurst * self.Launcher.TimeBetweenBurstShots
                end

                self:SetCycleDuration(FireDelay)
                self:SetCyclePercent(math.Clamp(1 - (self.Launcher.NextFire - CurTime()) / FireDelay, 0, 1))
                self:SetThrottled(self.Launcher:IsThrottled())
            else
                self:SetCyclePercent(0)
            end

            local WantsToFire = false
            local Target

            if self:IsLinkedToTargeter() then
                local Targeter = self:GetLinkedTargeter()
                Target = Targeter:GetTarget()

                if IsValid(Target) then
                    WantsToFire = Targeter:IsFireGroupFiring(self.FireGroup)
                end
            elseif IsValid(Protector) and Protector.GetTargeter and IsValid(Protector:GetTargeter()) then
                local Targeter = Protector:GetTargeter()
                Target = Targeter:GetTarget()

                if IsValid(Target) then
                    WantsToFire = Targeter:IsFireGroupFiring(self.FireGroup)
                end
            end

            if IsValid(self.WireTarget) then
                Target = self.WireTarget
            end

            self.Launcher:SetTarget(Target)

            -- FIXME: Update this code when the code in Initialize is removed
            if self:IsLinkedToPod() then
                local Pod = self:GetLinkedPod()
                local Driver = Pod:GetDriver()

                if IsValid(Driver) then
                    if self.KeysDown[Driver.WeaponKeys[self.FireGroup]] then
                        WantsToFire = true
                    else
                        WantsToFire = false
                    end
                end
            end

            WantsToFire = WantsToFire or self.WireFire

            if WantsToFire then
                if not self.Launcher:WantsToFire() then
                    self.Launcher:StartFiring()
                end
            else
                if self.Launcher:WantsToFire() then
                    self.Launcher:StopFiring()
                end
            end

            if WireLib ~= nil then
                WireLib.TriggerOutput(self, "Can Fire", self.Launcher:CanFire() and 1 or 0)
            end

            if self.Launcher.HasMagazine then
                self:SetAmmo(self.Launcher.Ammo)
                self:SetMaxAmmo(self.Launcher.MaxAmmo)

                if WireLib ~= nil then
                    if self.Launcher.HasMagazine and self:GetReloading() then
                        local ReloadProgress = (self.Launcher.ReloadingFinishedTime - CurTime()) / self.Launcher.ReloadTime
                        WireLib.TriggerOutput(self, "Reloading", self:GetReloading() and ReloadProgress or 0)
                    end

                    if self.Launcher.HasCapacitor then
                        WireLib.TriggerOutput(self, "Capacitor", self.Launcher.Capacitor)
                        WireLib.TriggerOutput(self, "Max Capacitor", self.Launcher.MaxCapacitor)
                    end

                    if self.Launcher.HasHeat then
                        WireLib.TriggerOutput(self, "Temperature", self.Launcher.Temperature)
                        WireLib.TriggerOutput(self, "Max Temperature", self.Launcher.MaxTemperature)
                    end

                    WireLib.TriggerOutput(self, "Ammo", self:GetAmmo())
                    WireLib.TriggerOutput(self, "Max Ammo", self:GetMaxAmmo())
                end
            end
        end

        self:NextThink(CurTime() + math.min(FireDelay / 3, 0.25))

        return true
    end

    function ENT:CreateInputs()
        if WireLib ~= nil then
            local Inputs = {"Fire"}
            local InputTypes = {"NORMAL"}

            if IsValid(self.Launcher) then
                if self.Launcher.HasMagazine then
                    table.insert(Inputs, "Reload")
                    table.insert(InputTypes, "NORMAL")
                end

                if self.Launcher.HasTargeting then
                    table.insert(Inputs, "Target")
                    table.insert(InputTypes, "ENTITY")
                end
            end

            self.Inputs = WireLib.AdjustSpecialInputs(self, Inputs, InputTypes)
        end
    end

    function ENT:CreateOutputs()
        if WireLib ~= nil then
            local Outputs = {"Can Fire"}
            local OutputTypes = {"NORMAL"}

            if self.Launcher.HasMagazine then
                table.insert(Outputs, "Reloading")
                table.insert(OutputTypes, "NORMAL")
                table.insert(Outputs, "Ammo")
                table.insert(OutputTypes, "NORMAL")
                table.insert(Outputs, "Max Ammo")
                table.insert(OutputTypes, "NORMAL")
            end

            if self.Launcher.HasCapacitor then
                table.insert(Outputs, "Capacitor")
                table.insert(OutputTypes, "NORMAL")
                table.insert(Outputs, "Max Capacitor")
                table.insert(OutputTypes, "NORMAL")
            end

            if self.Launcher.HasHeat then
                table.insert(Outputs, "Temperature")
                table.insert(OutputTypes, "NORMAL")
                table.insert(Outputs, "Max Temperature")
                table.insert(OutputTypes, "NORMAL")
            end

            self.Outputs = WireLib.AdjustSpecialOutputs(self, Outputs, OutputTypes)
        end
    end

    function ENT:GetWirePorts()
        return {}, {}
    end

    function ENT:TriggerInput(Input, Value)
        if self:GetHasCycle() and IsValid(self.Launcher) then
            if Input == "Fire" or Input == "On" then
                if Value ~= 0 then
                    self.WireFire = true
                else
                    self.WireFire = false
                end
            elseif Input == "Reload" and Value ~= 0 then
                self.Launcher:StartReloading()
            elseif Input == "Target" then
                self.WireTarget = Value
            end
        end

        Base.TriggerInput(self, Input, Value)
    end

    duplicator.RegisterEntityClass("sc_module_weapon", GAMEMODE.MakeEnt, "Data")

    function ENT:LoadSCInfo(Info)
        self.SCDupeInfo = Info or {}
        self.FireGroup = Info.FireGroup or "Primary"

        if Info.Launcher and Info.Launcher.LauncherType and Info.Launcher.ProjectileRecipe and Info.Launcher.ProjectileRecipeOwner then
            self:CreateLauncher(Info.Launcher.LauncherType, Info.Launcher.ProjectileRecipe, Info.Launcher.ProjectileRecipeOwner, Info.Upgrades)

            if IsValid(self.Launcher) then
                self.Launcher:DeSerialize(Info.Launcher)
            end
        else
            self.Invalid = true
            self:SetModuleName("INVALID WEAPON, UPDATE ME D:")
        end
    end

    function ENT:ApplySCDupeInfo(Info, GetEntByID)
        if Info.LinkedPod then
            self:LinkPod(GetEntByID(Info.LinkedPod))
        end

        if Info.LinkedTargeter then
            self:LinkTargeter(GetEntByID(Info.LinkedTargeter))
        end
    end

    function ENT:SaveSCInfo()
        local Info = {}

        if IsValid(self.Launcher) then
            Info.Launcher = self.Launcher:Serialize()
        end

        Info.Upgrades = self.Upgrades
        Info.FireGroup = self.FireGroup

        if self:IsLinkedToPod() then
            Info.LinkedPod = self:GetLinkedPod():EntIndex()
        end

        if self:IsLinkedToTargeter() then
            Info.LinkedTargeter = self:GetLinkedTargeter():EntIndex()
        end

        return Info
    end

    function ENT:SavePersistentInfo()
        local Info = {}
        if IsValid(self.Launcher) then
            local Recipe = self.Launcher:GetProjectileRecipe()
            if Recipe then
                Info.ProjectileRecipe = Recipe:GetName()
                Info.ProjectileRecipeOwner = Recipe:GetOwnerID()
            end

            if self.Launcher.HasCapacitor then
                Info.Capacitor = self.Launcher.Capacitor
            end

            if self.Launcher.HasHeat then
                Info.Temperature = self.Launcher.Temperature
                Info.Overheated = self.Launcher.Overheated
            end

            if self.Launcher.HasMagazine then
                Info.Ammo = self.Launcher.Ammo
                Info.Reloading = self.Launcher.Reloading
            end
        end

        Info.Upgrades = self.Upgrades
        Info.FireGroup = self.FireGroup

        return Info
    end

    function ENT:LoadPersistentInfo(Info)
        if IsValid(self.Launcher) then
            if Info.ProjectileRecipe and Info.ProjectileRecipeOwner then
                self.Launcher:SetRecipeByName(Info.ProjectileRecipe, Info.ProjectileRecipeOwner)
            end

            if Info.Capacitor and self.Launcher.HasCapacitor then
                self.Launcher.Capacitor = Info.Capacitor
            end

            if Info.Temperature and self.Launcher.HasHeat then
                self.Launcher.Temperature = Info.Temperature
                self.Launcher.Overheated = Info.Overheated
            end

            if Info.Ammo and self.Launcher.HasMagazine then
                self.Launcher.Ammo = Info.Ammo
                self.Launcher.Reloading = Info.Reloading

                if self.Launcher.Reloading then
                    self.Launcher.ReloadingFinishedTime = CurTime() + self.Launcher.ReloadTime
                end
            end
        end

        self.Upgrades = Info.Upgrades
        self.FireGroup = Info.FireGroup
    end
end