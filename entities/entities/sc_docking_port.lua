AddCSLuaFile()

DEFINE_BASECLASS("base_scentity")

ENT.Type = "anim"
ENT.Base = "base_scentity"
ENT.PrintName	= "Docking Port"
ENT.Author	= "TangentDelta"

ENT.Spawnable	= false
ENT.AdminSpawnable = false
ENT.IsSCModule = true

--Docking port
ENT.Active = false
ENT.Armed = false
ENT.Docked = false	--Is the port currently docked?
ENT.TriggerUndock = false	--Should the docking port undock during the next think?
ENT.Slaved = false	--Is this ship slaved to a different ship?
ENT.FarPort = nil	--The port that this port is docked to, if docked
ENT.PrevPort = nil	--The last port to have docked with this port

ENT.Gyropod = nil	--The gropod of the ship that we're on

ENT.Status = "Holy crap this is a string"	--Status string to communicate to the end user why they're being dumb

--Compatibility with SBEP docking clamps
ENT.PortEntry = {}
ENT.PortType = nil
ENT.CompatiblePorts = nil

--Docking port offsets
ENT.PortOffset = nil
ENT.OtherPortOffset = nil
ENT.PortAngle = nil
ENT.OtherPortAngle = nil

--Gamemode
local base = scripted_ents.Get("base_scentity")
local scbase = scripted_ents.Get("base_lsentity")

--Constants
local ZeroVector = Vector(0,0,0)
local DCDT = list.Get("SBEP_DockingClampModels")	--The SBEP docking clamp list
local FindDist = 200	--The radius to search for docking ports in
local AngSensitivity = 15	--How sensitive the angle snapping is when docking

--[[
======================
	Initialization
======================
]]--
function ENT:SharedInit()
	scbase.SharedInit(self)
end

if CLIENT then
	function ENT:Draw()
		self:DrawModel()

		local C = Color(100, 100, 150, 100)
		local PointM = Material("sprites/light_glow02_add")
		local BeamM = Material("cable/blue_elec")
		for I, Point in ipairs(self.PortEntry.EfPoints) do
			local WorldPoint = self:LocalToWorld(Vector(Point.vec.y,Point.vec.x,Point.vec.z))
			render.SetMaterial(PointM)
			render.DrawSprite(WorldPoint, 20, 20, C)

			local PointIndex = I + 1
			if I == #self.PortEntry.EfPoints then
				PointIndex = 1
			end

			local OtherPoint = self.PortEntry.EfPoints[PointIndex]
			local WorldOtherPoint = self:LocalToWorld(Vector(OtherPoint.vec.y,OtherPoint.vec.x,OtherPoint.vec.z))
			render.SetMaterial(BeamM)
			render.DrawBeam(WorldPoint, WorldOtherPoint, 10, 128, 138, C)
		end
	end
end


function ENT:Initialize()
	base.Initialize(self)

	self.PortEntry = DCDT[string.lower(self:GetModel())]
	if self.PortEntry ~= nil then
		self.CompatiblePorts = self.PortEntry.Compatible
		self.PortType = self.PortEntry.ALType
	end

	if CLIENT then return end

	self:SetDockSound("doors/garage_stop1.wav")
	self:SetUndockSound("doors/doorstop1.wav")

	self.OneSecond = CurTime() + 1

	self.Status = "Standing by"

    if WireLib then
        WireLib.CreateSpecialInputs(self, {"Active", "Undock"}, {"NORMAL", "NORMAL"})
        WireLib.CreateSpecialOutputs(self, {"Docked", "Armed", "Status"}, {"NORMAL", "NORMAL", "STRING"})
    end
end

if not SERVER then return end

util.PrecacheSound("doors/garage_stop1.wav")
util.PrecacheSound("doors/doorstop1.wav")

--[[
==============
	Sounds
==============
]]--
function ENT:SetDockSound(path)
	if self.DockSound then
		self.DockSound:Stop()
	end

	self.DockSound = CreateSound(self, Sound(path))
	self.DockSound_path = path
end

function ENT:SetUndockSound(path)
	if self.UndockSound then
		self.UndockSound:Stop()
	end

	self.UndockSound = CreateSound(self, Sound(path))
	self.UndockSound_path = path
end

function ENT:SoundPlay(which)
	if self[which] then
		self[which]:Stop()
		self[which]:Play()
	elseif self[which.."_path"] ~= nil then
		local setfunc = self["Set" .. which]
		local path = self[which .. "_path"]

		setfunc(self, path)
	end
end
--[[
=======================
	General Utility
=======================
]]--

function DiffAngle(A, B)
	local C = (((B - A) + 180) % 360) - 180
	return C
end

--[[
===============================
	Entity-Specific Utility
===============================
]]--

function ENT:FindGyropod()
	if not IsValid(self:GetProtector()) then return end

	local Core = self:GetProtector()
	local Ents = Core.ConWeldTable

	for _, E in pairs(Ents) do
		if IsValid(E) then
			if E:GetClass() == "sc_gyropod" then
				self.Gyropod = E
				return
			end
		end
	end
end

function ENT:CanDock(E)
	return (not self.Docked) and IsValid(self.Gyropod) and self.Active
end

function ENT:SlaveTo(E)
	self:SoundPlay("DockSound")
	self.Status = "Docked - Passive"

	self.Slaved = true
	self.Docked = true
	self.FarPort = E
	E.Docked = true
	E.FarPort = self

	self.Gyropod:SetDockingMaster(self, E, E.Gyropod, self.OtherPortOffset - self.PortOffset, self.OtherPortAngle)
end

function ENT:Dock(E)
	if self.Gyropod.GetIsSlaved == nil then self.Status = "Gyropod is misbehaving! This really should not happen." return end
	--Don't dock again if we're already docked!
	if self.Gyropod:GetIsSlaved() then self.Status = "Ignoring: My ship is already docked" return end
	--Don't dock with something already docked to us
	if E.FarPort == self then self.Status = "Ignoring: This ship is already docked to my ship" return end

	local MyCore = self:GetProtector()
	local OtherCore = E:GetProtector()
	local MySigRad = MyCore:GetSigRad()
	local OtherSigRad = OtherCore:GetSigRad()

	--TODO: Fix edge cases so that a drone cannot move a titan
	if OtherCore:GetIsStation() and not MyCore:GetIsStation() then	--Always dock to a station if we're not a station
		self:SlaveTo(E)
	else	--Dock ship-to-ship if neither is a station, or we're both stations
		if MySigRad < OtherSigRad then		--Dock to a larger ship
			self:SlaveTo(E)
		elseif MySigRad == OtherSigRad and not E.Gyropod.On then	--Dock to a ship of the same size if its gyropod is off
			self.SlaveTo(E)
		else
			self.Status = "Ignoring: Other ship is too large to dock with"
		end
	end
end

function ENT:Undock()
	self:SoundPlay("UndockSound")
	self.Docked = false

	if self.Slaved then
		self.Gyropod:SetDockingMaster(nil)
		self.Slaved = false
	end

	if self.FarPort.Docked then
		self.FarPort:Undock()
	end

	self.PrevPort = self.FarPort
	self.FarPort = nil
end

--[[
========================
	Game Interaction
========================
]]--

function ENT:OnRemove()

end


function ENT:Think()
	if not SERVER then return end

	if not IsValid(self.Gyropod) then
		self:FindGyropod()
	end

	local RequiredEntsGood = IsValid(self:GetProtector()) and IsValid(self.Gyropod)

	if RequiredEntsGood then
		--Undock if the undock trigger flag is active
		if self.Docked and self.TriggerUndock then
			self:Undock()
			self.TriggerUndock = false	--Clear the undock trigger flag
		end
	end

	--Set our "armed" flag if we are armed and ready to dock!
	self.Armed = RequiredEntsGood and self.Active and not self.Docked and not IsValid(self.FarPort) and not IsValid(self.PrevPort)

	if self.Armed and not self.Gyropod.Slaved then	--If we're armed and the ship is not already docked, look for a port to dock to
		self.Status = "Armed and ready"
		local Ents = ents.FindInSphere(self:GetPos(), FindDist)
		for _,E in ipairs(Ents) do
			if IsValid(E) and (E ~= self) then
				if (E:GetClass() == "sc_docking_port") and IsValid(E:GetProtector()) then
					--Look through the list of port types that the other port is compatible with and see if there is one that will work
					local CompP = nil
					for I,Port in ipairs(E.CompatiblePorts) do
						if self.PortType == Port.Type then
							CompP = Port
							break
						end
					end

					for _, Port in pairs(self.CompatiblePorts) do
						if Port.Type == E.PortType then
							self.PortOffset = Vector(Port.AF or 0, Port.AR or 0, Port.AU or 0)
							self.PortAngle = Angle(Port.RPitch or 0, Port.RYaw or 0, Port.RRoll or 0)
							break
						end
					end

					--APitch, AYaw, ARoll are the relative angles of the docking ports when they are docked together
					--RPitch, RYaw, RRoll are the incremental angles that the docking ports can "snap" to
					if (CompP ~= nil) and E:CanDock(self) then
						local OtherAngle = Angle(CompP.APitch or 0, CompP.AYaw or 0, CompP.ARoll or 0)
						--local AngleDiff = E:GetAngles() - self:GetAngles() - OtherAngle
						local AngleDiff = (OtherAngle + E:WorldToLocalAngles(self:GetAngles()))
						AngleDiff:Normalize()

						--If the port has snappable angles, see if we can snap around those angles to line up with the port better
						if Vector(AngleDiff.p, AngleDiff.y, AngleDiff.r):Distance(ZeroVector) > AngSensitivity then
							if CompP.RPitch ~= nil then
								local P = 0
								while P < 360 do
									local PMod = math.abs(AngleDiff.p - P)
									if PMod < AngSensitivity then
										AngleDiff.p = PMod
										OtherAngle.p = OtherAngle.p + P
										break
									end

									P = P + CompP.RPitch
								end
							end

							if CompP.RYaw ~= nil then
								local Y = 0
								while Y < 360 do
									local YDiff = math.AngleDifference(AngleDiff.y, Y)
									if YDiff < AngSensitivity then
										AngleDiff.y = YDiff
										OtherAngle.y = Y
										break
									end

									Y = Y + CompP.RYaw
								end
							end

							if CompP.RRoll ~= nil then
								local R = 0
								while R < 360 do
									local RMod = math.abs(AngleDiff.r - R)
									if RMod < AngSensitivity then
										AngleDiff.r = RMod
										OtherAngle.r = OtherAngle.r + R
										break
									end

									R = R + CompP.RRoll
								end
							end
						end

						--Dock with the port if we're lined up with it
						if Vector(AngleDiff.p, AngleDiff.y, AngleDiff.r):Distance(ZeroVector) < AngSensitivity then
							self.OtherPortOffset = Vector(CompP.AF or 0, CompP.AR or 0, CompP.AU or 0)	--Get the docking point offset pos
							self.OtherPortAngle = OtherAngle
							self:Dock(E)
						end
					end
				end
			end
		end
	elseif IsValid(self.FarPort) and self.Docked and not self.Slaved then
		self.Status = "Docked - Active"
	elseif IsValid(self.PrevPort) and not self.Docked then
		self.Status = "Waiting for other ship to leave docking range"
		--If the port we were previously docked with is far enough away, forget about it
		if self.PrevPort:GetPos():Distance(self:GetPos()) > (FindDist * 1.10) then
			self.PrevPort = nil
		end
	elseif not self.Docked then
		self.Status = "Standing by"
	end

	self:UpdateOutputs()

	self:NextThink(CurTime() + 1)
	return true
end

duplicator.RegisterEntityClass("sc_docking_port", GAMEMODE.MakeEnt, "Data")


--[[
=============================
	Wiremod "Integration"
=============================
]]--
function ENT:TriggerInput(Name, Value)
	if Name == "Active" then
		self.Active = (Value ~= 0)
	elseif Name == "Undock" and not self.TriggerUndock then		--Edge-triggered detection for undocking
		self.TriggerUndock = (Value ~= 0)
	end
end

function ENT:UpdateOutputs()
	if WireLib then
		WireLib.TriggerOutput(self, "Docked", self.Docked and 1 or 0)
		WireLib.TriggerOutput(self, "Armed", self.Armed and 1 or 0)
		WireLib.TriggerOutput(self, "Status", self.Status)
	end
end

--[[
============
	Hooks
============
]]--