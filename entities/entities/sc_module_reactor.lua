AddCSLuaFile()
--#region config
ENT.Type = "anim"
ENT.Base = "base_moduleentity"
ENT.PrintName = "Ship Reactor"
ENT.Author = "Lt.Brandon"
ENT.Category = "Space Combat"
ENT.Spawnable = false
ENT.AdminSpawnable = false
ENT.IsSCReactor = true
local Base = scripted_ents.Get("base_moduleentity")

local function InitReactors()
    Base = scripted_ents.Get("base_moduleentity")
    local GM = GAMEMODE

    -- Fusion Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Fusion Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Harness the power of a miniature sun inside your tinfoil containment unit to produce power!")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/props_phx/life_support/crylaser_small.mdl")
        GM:RegisterGenerator(Generator)
    end

    -- End Fusion Reactor
    -- Fission Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Fission Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Produces Energy via the power of radioactivity. As a side effect, kind of deadly to humans. And life in general.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/props_phx/life_support/battery_medium.mdl")
        GM:RegisterGenerator(Generator)
    end

    -- End Fission Reactor
    -- Graviton Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Graviton Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Makes use of the seemingly magical properties of this particle that no one knows the properties of to produce Energy without radiation.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/tiberium/small_chemical_storage.mdl")
        GM:RegisterGenerator(Generator)
    end

    -- End Graviton Reactor
    -- Antimatter Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Antimatter Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Collides Hydrogen and Antihydrogen inside of a powerful containment field to produce massive quantities of power.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/mandrac/energy_cell/small_cell.mdl")
        GM:RegisterGenerator(Generator)
    end

    -- End Antimatter Reactor
    -- Auxiliary Fusion Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Auxiliary Fusion Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Harness the power of a miniature sun inside your tinfoil containment unit to produce power!")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/props_phx/life_support/crylaser_small.mdl")
        GM:RegisterGenerator(Generator)
    end

    -- End Fusion Reactor
    -- Fission Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Auxiliary Fission Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Produces Energy via the power of radioactivity. As a side effect, kind of deadly to humans. And life in general.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/props_phx/life_support/battery_medium.mdl")
        GM:RegisterGenerator(Generator)
    end

    -- End Fission Reactor
    -- Graviton Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Auxiliary Graviton Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Makes use of the seemingly magical properties of this particle that no one knows the properties of to produce Energy without radiation.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/tiberium/small_chemical_storage.mdl")
        GM:RegisterGenerator(Generator)
    end

    -- End Graviton Reactor
    -- Antimatter Reactor
    do
        local Generator = GM:NewGeneratorInfo()
        Generator:SetName("Auxiliary Antimatter Reactor")
        Generator:SetClass("sc_module_reactor")
        Generator:SetDescription("Collides Hydrogen and Antihydrogen inside of a powerful containment field to produce massive quantities of power.")
        Generator:SetCategory("Reactors")
        Generator:SetDefaultModel("models/mandrac/energy_cell/small_cell.mdl")
        GM:RegisterGenerator(Generator)
    end
    -- End Antimatter Reactor
end

local function OnReloaded()
    InitReactors()
end

hook.Remove("InitPostEntity", "sc_reactor_post_entity_init")
hook.Add("InitPostEntity", "sc_reactor_post_entity_init", InitReactors)
hook.Remove("OnReloaded", "sc_reactor_post_reloaded")
hook.Add("OnReloaded", "sc_reactor_post_reloaded", OnReloaded)
local MaxUpgradeLevel = 5

local ReactorTypes = {
    "Antimatter";
    "Graviton";
    "Fusion";
    "Fission";
}

local UpgradeResources = {
    Generator = {
        "Basic Generator";
        "Standard Generator";
        "Enhanced Generator";
        "Advanced Generator";
        "Elite Generator";
    };
    Turbine = {
        "Basic Turbine";
        "Standard Turbine";
        "Enhanced Turbine";
        "Advanced Turbine";
        "Elite Turbine";
    };
    Cooler = {
        "Basic Cooler";
        "Standard Cooler";
        "Enhanced Cooler";
        "Advanced Cooler";
        "Elite Cooler";
    };
    Compressor = {
        "Basic Compressor";
        "Standard Compressor";
        "Enhanced Compressor";
        "Advanced Compressor";
        "Elite Compressor";
    };
    Recuperator = {
        "Basic Recuperator";
        "Standard Recuperator";
        "Enhanced Recuperator";
        "Advanced Recuperator";
        "Elite Recuperator";
    };
}

local UpgradeDescriptions = {
    Generator = "Generator upgrades increase the efficiency of the energy conversion, giving you more power grid and energy output";
    Turbine = "Turbine upgrades increase the max efficiency of the reaction, giving you higher energy output";
    Cooler = "Cooler upgrades increase the rate that coolant heat dissipates, allowing you to cool the reactor faster";
    Compressor = "Compressor upgrades increase the coolant flow rate, increasing the amount of coolant that can be used";
    Recuperator = "Recuperator upgrades decrease the amount of coolant that is lost each cycle";
}

local ReactorCasingTemperatures = {
    Antimatter = 36000;
    Graviton = 24000;
    Fusion = 15000;
    Fission = 1700;
}

local ReactorSizes = {
    Drone = 0.5;
    Fighter = 1;
    Frigate = 2;
    Cruiser = 4;
    Battlecruiser = 6;
    Battleship = 10;
    Dreadnaught = 12;
    Titan = 14;
}

local ReactorPGModifiers = {
    Antimatter = 1;
    Graviton = 1.2;
    Fusion = 0.8;
    Fission = 1.4;
}

local ReactorExplosionModifiers = {
    Antimatter = 2.3;
    Graviton = 1.8;
    Fusion = 1.2;
    Fission = 1.0;
}

local ReactorIgnitionModifiers = {
    Antimatter = 0;
    Graviton = 0.15;
    Fusion = 0.75;
    Fission = 0;
}

local RandomSalvageResources = {
    ["Electronic Circuitry"] = 4;
    ["Scrap Metal"] = 10;
    ["Radioactive Materials"] = 2;
    ["Plasma Conduits"] = 4;
    ["Destroyed Consoles"] = 1;
}

local HeaterEnergyUsage = 3000
local PowerVariableHeatMultiplier = 19
local PowerVariableHeatExponent = 1.07
local CoolantTargetDeltaTime = 0.1

local Reactions = {}

local function AddReaction(DisplayName, ReactionType, EfficientTemperature, PowerVariable, CycleTime, Inputs, Outputs)
    local NewReaction = {}
    NewReaction.DisplayName = DisplayName
    NewReaction.ReactionType = ReactionType
    NewReaction.EfficientTemperature = EfficientTemperature
    NewReaction.PowerVariable = PowerVariable
    NewReaction.CycleTime = CycleTime
    NewReaction.Inputs = Inputs
    NewReaction.Outputs = Outputs or {}

    if not Reactions[ReactionType] then
        Reactions[ReactionType] = {}
    end

    table.insert(Reactions[ReactionType], NewReaction)
end

local Coolants = {}

local function AddCoolant(CoolantName, OutResource, HeatDissipation, AmountNeeded)
    local NewCoolant = {}
    NewCoolant.CoolantName = CoolantName
    NewCoolant.OutResource = OutResource
    NewCoolant.HeatDissipation = HeatDissipation
    NewCoolant.AmountNeeded = AmountNeeded
    table.insert(Coolants, NewCoolant)
end

-- Antimatter
AddReaction("AM + H", "Antimatter", 24000, 240, 60, {
    Antihydrogen = 640,
    Hydrogen = 640
})

AddReaction("AM + An", "Antimatter", 32000, 320, 60, {
    Antianubium = 80,
    Anubium = 80
})

-- Graviton
AddReaction("An + E -> -An", "Graviton", 15500, 200, 60, {
    Anubium = 200,
    Energy = 5000
}, {
    Antianubium = 40
})

AddReaction("H + E -> -H", "Graviton", 14000, 120, 30, {
    Hydrogen = 5000,
    Energy = 7500
}, {
    Antihydrogen = 48
})

-- Fusion
AddReaction("H + H -> D", "Fusion", 6000, 12, 60, {
    Hydrogen = 3600
}, {
    Deuterium = 1800
})

AddReaction("H + Li6 -> He4 + T", "Fusion", 7220, 32, 60, {
    Hydrogen = 1800,
    ["Lithium-6"] = 1800
}, {
    Tritium = 1800,
    ["Helium-4"] = 1800
})

AddReaction("H + D -> He3", "Fusion", 6540, 24, 60, {
    Hydrogen = 1800,
    Deuterium = 1800
}, {
    ["Helium-3"] = 1800
})

AddReaction("H + T -> He3", "Fusion", 6540, 24, 60, {
    Hydrogen = 1800,
    Tritium = 1800
}, {
    ["Helium-3"] = 1800
})

AddReaction("D + D -> He3 + T + H", "Fusion", 8550, 42, 60, {
    Deuterium = 3600
}, {
    ["Helium-3"] = 900,
    Tritium = 900,
    Hydrogen = 900
})

AddReaction("T + T -> He4", "Fusion", 8550, 42, 60, {
    Tritium = 3600
}, {
    ["Helium-4"] = 1800
})

AddReaction("D + T -> He4", "Fusion", 9200, 80, 60, {
    Deuterium = 1800,
    Tritium = 1800
}, {
    ["Helium-4"] = 1800
})

AddReaction("He3 + He3 -> He4 + 2H", "Fusion", 8750, 56, 60, {
    ["Helium-3"] = 3600
}, {
    ["Helium-4"] = 1800,
    Hydrogen = 3600
})

AddReaction("He3 + Li6 -> 2He4 + H", "Fusion", 8850, 64, 60, {
    ["Helium-3"] = 1800,
    ["Lithium-6"] = 1800
}, {
    ["Helium-4"] = 3600,
    Hydrogen = 1800
})

AddReaction("H + He3 -> He4", "Fusion", 8550, 42, 60, {
    ["Helium-3"] = 1800,
    Hydrogen = 1800
}, {
    ["Helium-4"] = 1800
})

-- Fission
AddReaction("U - > DU", "Fission", 572, 16, 360, {
    Uranium = 360
}, {
    ["Depleted Uranium"] = 360
})

AddReaction("DU -> Pu", "Fission", 1600, 14, 360, {
    Uranium = 90,
    ["Depleted Uranium"] = 360
}, {
    Plutonium = 36
})

AddReaction("Pu", "Fission", 834, 20, 360, {
    Plutonium = 240
}, {})

AddReaction("P", "Fission", 773, 12, 360, {
    Polonium = 360
})

AddReaction("Pn", "Fission", 750, 48, 240, {
    ["Polonium Nitrate"] = 3600
})

-- Coolant
AddCoolant("Water", "Radioactive Coolant", 520, 350)
AddCoolant("Nitrogen", "Nitrogen", 1400, 1200)
AddCoolant("Helium-4", "Helium-4", 1900, 1000)
AddCoolant("Helium-3", "Helium-3", 2200, 900)
AddCoolant("Nubium", "Nubium", 4700, 600)
SC = SC or {}
SC.Reactors = {}

function SC.Reactors.GetReactorSizeForClass(Class)
    return ReactorSizes[Class]
end

function SC.Reactors.GetCoolantInfo(CoolantID)
    return Coolants[CoolantID]
end

function SC.Reactors.GetCoolantTypes()
    return Coolants
end

function SC.Reactors.GetReactionTypes()
    return Reactions
end

function SC.Reactors.GetReactionInfo(ReactionType, ReactionID)
    return Reactions[ReactionType][ReactionID]
end

function SC.Reactors.GetReactorTypes()
    return ReactorTypes
end

--#endregion config
--#region shared
local BasePowerGeneration = 1000
local e = 2.7182818285

function ENT:CalculatePowerGeneration(Temperature, EfficientTemperature, Size, PowerVariable)
    local MaxEfficiency = math.Remap(self:GetTurbineLevel(), 1, MaxUpgradeLevel, 0.7, 0.93)
    local Efficiency = 7.42 * (e ^ (-Temperature / EfficientTemperature) + math.tanh(Temperature / EfficientTemperature) - 1)
    local GeneratorEfficiency = math.Remap(self:GetGeneratorLevel(), 1, MaxUpgradeLevel, 0.85, 1)
    Efficiency = math.min(MaxEfficiency, Efficiency) * GeneratorEfficiency

    return math.floor(Efficiency * BasePowerGeneration * Size * PowerVariable), Efficiency
end

function ENT:GetBonusText()
    local ReactorType = ReactorTypes[self:GetReactorType()]
    local Reaction = Reactions[ReactorType][self:GetReactorReaction()]
    local Text = string.format("%s\n\n-Output-\nPG: %s", Reaction.DisplayName, sc_ds(self:GetPGBonus()))
    local Energy, Efficiency = self:CalculatePowerGeneration(self:GetTemperature(), Reaction.EfficientTemperature, self:GetReactorSize(), Reaction.PowerVariable)
    local Coolant = Coolants[self:GetReactorCoolant()]
    local CoolantAmount = self:GetCoolantFlowRate()
    local IgnitionTemperature = ReactorIgnitionModifiers[ReactorType] * Reaction.EfficientTemperature

    if self:GetTemperature() > IgnitionTemperature then
        Text = Text .. "\n" .. sc_ds(Energy) .. " Energy/s (" .. (math.floor(Efficiency * 10000) * 0.01) .. "% efficiency)"
        Text = Text .. "\n" .. sc_ds(CoolantAmount) .. " " .. Coolant.OutResource .. "/s"
    end

    return Text .. "\n"
end

function ENT:GetFittingText()
    local Text = Base.GetFittingText(self)

    if CLIENT then
        Text = string.format("%s\n\nPress [%s] to open menu\n", Text, string.upper(input.LookupBinding("+use")))
    end

    return Text
end

function ENT:GetActivationCostText()
    local ReactorType = ReactorTypes[self:GetReactorType()]
    local Cost = self:GetCycleActivationCost()
    local Text = "\n-Input-\n"
    local Reaction = Reactions[ReactorType][self:GetReactorReaction()]
    local Coolant = Coolants[self:GetReactorCoolant()]
    local CoolantAmount = self:GetCoolantFlowRate()
    local IgnitionTemperature = ReactorIgnitionModifiers[ReactorType] * Reaction.EfficientTemperature

    Text = Text .. Coolant.CoolantName .. ": " .. sc_ds(CoolantAmount) .. "/s\n"

    if self:GetTemperature() <= IgnitionTemperature then
        Text = Text .. "Energy: " .. sc_ds(HeaterEnergyUsage * self:GetReactorSize()) .. "/s\n"
    end

    Text = Text .. "\nReactor Temperature: " .. self:GetTemperature() .. "K"
    Text = Text .. "\nMax Reactor Temperature: " .. ReactorCasingTemperatures[ReactorType] .. "K"

    return Text
end

function ENT:GetStatusText()
    if self:GetHasCycle() then
        local Text = "Status: "
        if self:GetCycling() then
            local ReactorType = ReactorTypes[self:GetReactorType()]
            local Reaction = Reactions[ReactorType][self:GetReactorReaction()]
            local IgnitionTemperature = ReactorIgnitionModifiers[ReactorType] * Reaction.EfficientTemperature

            if self:GetTemperature() <= IgnitionTemperature then
                Text = Text .. "Warming Up\n"
            else
                Text = Text .. "Cycling\n"
            end
        else
            Text = Text .. "Off\n"
        end

        return Text
    end
end

function ENT:SharedInit()
    self.PGBonus = 0
    self.MaxTemperature = 0
    self.DisableAlarm = false
    self.CoolantToSet = 1
    self.ReactionToSet = 1
    self.TemperatureToSet = -1
    self.UpdateAfterCycle = false

    Base.SharedInit(self)
    SC.NWAccessors.CreateNWAccessor(self, "IsAuxiliary", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "Temperature", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "CoolantTemperature", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "ReactorSize", "number", 1)
    SC.NWAccessors.CreateNWAccessor(self, "ReactorType", "uint", 3)
    SC.NWAccessors.CreateNWAccessor(self, "ReactorCoolant", "uint", 1)
    SC.NWAccessors.CreateNWAccessor(self, "ReactorReaction", "uint", 1)
    SC.NWAccessors.CreateNWAccessor(self, "PGBonus", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "CoolantFlowRate", "number", 0)

    -- Upgrade levels
    SC.NWAccessors.CreateNWAccessor(self, "GeneratorLevel", "number", 1)
    SC.NWAccessors.CreateNWAccessor(self, "TurbineLevel", "number", 1)
    SC.NWAccessors.CreateNWAccessor(self, "CoolerLevel", "number", 1)
    SC.NWAccessors.CreateNWAccessor(self, "CompressorLevel", "number", 1)
    SC.NWAccessors.CreateNWAccessor(self, "RecuperatorLevel", "number", 1)
end

function ENT:UpdateCycleResources()
    local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
    local Resources = {}

    for i, k in pairs(Reaction.Inputs) do
        Resources[i] = self:GetReactorSize() * k
    end

    self:SetCycleActivationCost(Resources)
end

function ENT:ChangeReaction(NewReaction)
    if self:GetCycling() or not Reactions[ReactorTypes[self:GetReactorType()]][NewReaction] then return end
    self:SetReactorReaction(NewReaction)
    local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
    self:SetCycleDuration(Reaction.CycleTime)
    self:UpdateCycleResources()
    self.MaxTemperature = Reaction.EfficientTemperature

    if SERVER then
        local WIRETABLE = {
            n = {},
            s = {},
            ntypes = {},
            stypes = {},
            size = 0,
            istable = true
        }

        local Production = table.Copy(WIRETABLE)
        local Consumption = table.Copy(WIRETABLE)
        local Mult = self:GetReactorSize()

        for i, k in pairs(Reaction.Outputs) do
            Production.s[i] = k * Mult
            Production.stypes[i] = "n"
            Production.size = Production.size + 1
        end

        for i, k in pairs(Reaction.Inputs) do
            Consumption.s[i] = k * Mult
            Consumption.stypes[i] = "n"
            Consumption.size = Consumption.size + 1
        end

        if WireLib ~= nil then
            WireLib.TriggerOutput(self, "Cycle Duration", Reaction.CycleTime)
            WireLib.TriggerOutput(self, "Reaction Outputs", Production)
            WireLib.TriggerOutput(self, "Reaction Inputs", Consumption)
        end
    end
end

function ENT:SetupGenerator(Generator)
    local Name = Generator:GetName()
    self:SetModuleName(Name)

    for i, k in ipairs(ReactorTypes) do
        if Name:find(k) then
            self:SetReactorType(i)

            if Name:find("Auxiliary") then
                self:SetIsAuxiliary(true)
            else
                self:SetIsAuxiliary(false)
            end

            self:SetSlot(self:GetIsAuxiliary() and "Auxiliary Reactor" or "Primary Reactor")
            self:ChangeReaction(1)

            if IsValid(self:GetProtector()) then
                self:GetProtector():UpdateModifiers()
            end

            break
        end
    end
end

--#endregion shared
if CLIENT then
    function ENT:Think()
        self:UpdateUI()

        return Base.Think(self)
    end

    function ENT:SendUpdate(Active, Recipe, Coolant)
        net.Start("sc_module_reactor_ui")
        net.WriteEntity(self)
        net.WriteInt(Recipe or 1, 8)
        net.WriteInt(Coolant or 1, 8)
        net.WriteBit(Active or false)
        net.SendToServer()
    end

    function ENT:UpdateUI()
        if not self:IsUIOpened() then return end
        self.UIPanel.TemperatureGauge.Value = self:GetTemperature() / ReactorCasingTemperatures[ReactorTypes[self:GetReactorType()]]
        local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self.UIPanel.SelectedReaction]
        local Coolant = Coolants[self.UIPanel.SelectedCoolant]
        local Energy, Efficiency = self:CalculatePowerGeneration(Reaction.EfficientTemperature, Reaction.EfficientTemperature, self:GetReactorSize(), Reaction.PowerVariable)
        local CoolantAmount = math.ceil(self:GetReactorSize() * Coolant.AmountNeeded)
        local GeneratorEfficiency = math.Remap(self:GetGeneratorLevel(), 1, MaxUpgradeLevel, 0.65, 1)
        local RecuperatorEfficiency = math.Remap(self:GetRecuperatorLevel(), 1, MaxUpgradeLevel, 0.9, 1)
        local PGBonus = math.floor(GeneratorEfficiency * 10000 * (self:GetReactorSize() ^ 1.2) * ReactorPGModifiers[ReactorTypes[self:GetReactorType()]])
        local Text = Reaction.DisplayName
        Text = Text .. "\n\nOutputs\n--------------\nPG: " .. sc_ds(PGBonus)
        Text = Text .. "\n" .. Energy .. " Energy/s (" .. sc_ds(math.floor(Efficiency * 10000) * 0.01) .. "% efficiency)"
        Text = Text .. "\n" .. sc_ds(math.floor(CoolantAmount * RecuperatorEfficiency)) .. " " .. Coolant.OutResource .. "/s\n"
        local Outputs = Reaction.Outputs

        if next(Outputs) then
            Text = Text .. "\nPer Cycle:\n"

            for i, k in pairs(Outputs) do
                Text = Text .. i .. ": " .. sc_ds(k * self:GetReactorSize()) .. "\n"
            end
        end

        Text = Text .. "\n\nInputs\n--------------\n"
        Text = Text .. Coolant.CoolantName .. ": " .. sc_ds(CoolantAmount) .. "/s\n"
        local Cost = Reaction.Inputs

        if next(Cost) then
            Text = Text .. "\nPer Cycle:\n"

            for i, k in pairs(Cost) do
                Text = Text .. i .. ": " .. sc_ds(k * self:GetReactorSize()) .. "\n"
            end
        end

        self.UIPanel.RecipeInfoLabel:SetText(Text)
    end

    function ENT:OpenUpgradesUI()
        local ItemInfoPopout

        -- Main DFrame
        local Frame = vgui.Create("DFrame")
        Frame:SetSize(680, 420)
        Frame:Center()
        Frame:SetTitle("Reactor Upgrade Panel")
        Frame:MakePopup()

        Frame.OnClose = function()
            self.UIPanel:SetVisible(true)
            if IsValid(ItemInfoPopout) then
                ItemInfoPopout:Remove()
            end
        end

        -- Button for returning to the main ui
        local CloseButton = vgui.Create("DButton", Frame)
        CloseButton:SetText("Back")
        CloseButton:Dock(BOTTOM)
        CloseButton:DockMargin(10, 10, 10, 10)
        Frame.CloseButton = CloseButton

        CloseButton.DoClick = function()
            Frame:Close()
        end

        local ScrollBox = vgui.Create("DScrollPanel", Frame)
        ScrollBox:DockMargin(10, 10, 10, 10)
        ScrollBox:Dock(FILL)
        Frame.ScrollBox = ScrollBox

        local Canvas = ScrollBox:GetCanvas()

        for Upgrade, Description in pairs(UpgradeDescriptions) do
            local Panel = vgui.Create("DPanel", Canvas)
            Panel:SetHeight(100)
            Panel:Dock(TOP)
            Panel:DockMargin(5, 5, 5, 5)

            local UpgradeButton = vgui.Create("DButton", Panel)
            UpgradeButton:SetText("Upgrade")
            UpgradeButton:Dock(RIGHT)

            -- This is cursed, but whatever.
            local Level = self[string.format("Get%sLevel", Upgrade)](self)

            local Icon = vgui.Create("SpawnIcon", Panel)
            Icon:SetSize(95, 95)
            Icon:Dock(LEFT)
            Icon:DockMargin(5, 5, 5, 5)

            local ResourceName = UpgradeResources[Upgrade][Level]
            local ResourceInfo = GAMEMODE:GetResourceInfoTable(ResourceName)
            if ResourceInfo.Model then
                Icon:SetModel(ResourceInfo.Model)
            end

            local NameLabel = vgui.Create("DLabel", Panel)
            NameLabel:SetText(string.format("%s %d/%d", ResourceName, Level, MaxUpgradeLevel))
            NameLabel:SetFont("SpawnMenuLarge")
            NameLabel:Dock(TOP)
            NameLabel:DockMargin(5, 5, 5, 5)

            local DescriptionLabel = vgui.Create("DLabel", Panel)
            DescriptionLabel:SetText(Description)
            DescriptionLabel:SetFont("SpawnMenuMedium")
            DescriptionLabel:SetWrap(true)
            DescriptionLabel:SetAutoStretchVertical(true)
            DescriptionLabel:Dock(TOP)
            DescriptionLabel:DockMargin(5, 5, 5, 5)

            -- Handle the button state
            local NextResourceName = UpgradeResources[Upgrade][Level + 1]
            local ShowTooltip = true
            if not self:HasResource(NextResourceName, 1) then
                UpgradeButton:SetEnabled(false)
                UpgradeButton:SetTooltip("Missing upgrade resource")
            end

            if Level >= MaxUpgradeLevel then
                UpgradeButton:SetEnabled(false)
                UpgradeButton:SetTooltip("Max upgrade level reached")
                ShowTooltip = false
            end

            UpgradeButton.OnCursorEntered = function()
                if ShowTooltip then
                    ItemInfoPopout = vgui.Create("InventoryInfoPanel", self.ItemInfoPopout)

                    local ResourceObject = {}
                    ResourceObject.Name = NextResourceName
                    ResourceObject.Amount = 1

                    ItemInfoPopout:LoadFromResource(ResourceObject)
                    ItemInfoPopout:SetDrawOnTop(true)
                end
            end

            UpgradeButton.OnCursorExited = function()
                if IsValid(ItemInfoPopout) then
                    ItemInfoPopout:Remove()
                end
            end

            UpgradeButton.DoClick = function()
                UpgradeButton:SetEnabled(false)
                ShowTooltip = false

                net.Start("SC.Reactor.DoUpgrade", false)
                net.WriteEntity(self)
                net.WriteString(Upgrade)
                net.SendToServer()

                -- Refresh UI after 1 second
                timer.Simple(1, function()
                    if IsValid(UpgradeButton) then
                        Frame:Close()
                        self:OpenUpgradesUI()
                    end
                end)
            end
        end
    end

    function ENT:OpenUI()
        -- Main DFrame
        local Frame = vgui.Create("DFrame")
        Frame:SetSize(680, 420)
        Frame:Center()
        Frame:SetTitle("Reactor Control Panel")
        Frame:MakePopup()

        -- Panel that holds the recipe stats
        local StatPan = vgui.Create("DPanel", Frame)
        StatPan:SetSize(220, 0)
        StatPan:Dock(RIGHT)

        -- Header for the recipe info
        local RecipeInfoLabel = vgui.Create("DLabel", StatPan)
        RecipeInfoLabel:SetText("Reaction Information")
        RecipeInfoLabel:SizeToContents()
        RecipeInfoLabel:Dock(TOP)
        RecipeInfoLabel:DockMargin(10, 10, 10, 10)

        -- Label that contains the recipe info text
        local RecipeInfoLabel2 = vgui.Create("DLabel", StatPan)
        RecipeInfoLabel2:SetPos(30, 30)
        RecipeInfoLabel2:Dock(FILL)
        RecipeInfoLabel2:DockMargin(10, 10, 10, 10)
        RecipeInfoLabel2:SetContentAlignment(7)
        Frame.RecipeInfoLabel = RecipeInfoLabel2

        -- Button for accessing the upgrades menu
        local UpgradesButton = vgui.Create("DButton", StatPan)
        UpgradesButton:SetText("Upgrades")
        UpgradesButton:Dock(BOTTOM)
        UpgradesButton:DockMargin(10, 10, 10, 10)
        Frame.UpgradesButton = UpgradesButton

        UpgradesButton.DoClick = function()
            -- Hide the info panel
            Frame:SetVisible(false)

            -- Open the upgrades frame
            self:OpenUpgradesUI()
        end

        -- List of available reactions
        local ReactionList = vgui.Create("DListView", Frame)
        ReactionList:Clear()
        ReactionList:SetMultiSelect(false)
        ReactionList:AddColumn("Reaction")
        ReactionList:AddColumn("Heat Generation")
        ReactionList:AddColumn("Power Output")

        local SelectedReaction = self:GetReactorReaction()
        local SelectedCoolant = self:GetReactorCoolant()
        Frame.SelectedReaction = SelectedReaction
        local ListLines = {}

        for i, k in pairs(Reactions[ReactorTypes[self:GetReactorType()]]) do
            local Energy, Efficiency = self:CalculatePowerGeneration(k.EfficientTemperature, k.EfficientTemperature, self:GetReactorSize(), k.PowerVariable)
            local HeatGeneration = math.pow(k.PowerVariable * PowerVariableHeatMultiplier, PowerVariableHeatExponent) * (1 - Efficiency)
            ListLines[i] = ReactionList:AddLine(k.DisplayName, sc_ds(HeatGeneration), sc_ds(Energy))
        end

        ReactionList:SelectItem(ListLines[self:GetReactorReaction()])

        ReactionList.OnRowSelected = function(panel, line)
            if not IsValid(self) then return end
            SelectedReaction = line
            Frame.SelectedReaction = line
            self:SendUpdate(self:GetCycling(), SelectedReaction, SelectedCoolant)
        end

        ReactionList:Dock(FILL)

        -- List of available coolants
        local CoolantList = vgui.Create("DListView", Frame)
        CoolantList:Clear()
        CoolantList:SetMultiSelect(false)
        CoolantList:SetTall(100)
        CoolantList:AddColumn("Coolant")
        CoolantList:AddColumn("Output")
        CoolantList:AddColumn("Heat Dissipation")
        Frame.SelectedCoolant = SelectedCoolant
        local CoolantListLines = {}

        local CoolerEfficieny = math.Remap(self:GetCoolerLevel(), 1, MaxUpgradeLevel, 0.75, 1)
        for i, k in pairs(Coolants) do
            CoolantListLines[i] = CoolantList:AddLine(k.CoolantName, k.OutResource, math.floor(CoolerEfficieny * k.HeatDissipation) .. "K per " .. k.AmountNeeded)
        end

        CoolantList:SelectItem(CoolantListLines[self:GetReactorCoolant()])

        CoolantList.OnRowSelected = function(panel, line)
            if not IsValid(self) then return end
            SelectedCoolant = line
            Frame.SelectedCoolant = line
            self:SendUpdate(self:GetCycling(), SelectedReaction, SelectedCoolant)
        end

        CoolantList:Dock(TOP)
        local StartProd, StopProd
        StartProd = vgui.Create("DButton", Frame)
        StartProd:SetDisabled(self.SC_Active or self:GetCycling())
        StartProd:SetText("Start Reactor")

        StartProd.DoClick = function()
            if not IsValid(self) then return end
            self:SendUpdate(true, SelectedReaction, SelectedCoolant)
            StopProd:SetDisabled(false)
            StartProd:SetDisabled(true)
        end

        StartProd:Dock(BOTTOM)
        StopProd = vgui.Create("DButton", Frame)
        StopProd:SetDisabled(not self.SC_Active or not self:GetCycling())
        StopProd:SetText("Stop Reactor")

        StopProd.DoClick = function()
            if not IsValid(self) then return end
            self:SendUpdate(false, SelectedReaction, SelectedCoolant)
            StartProd:SetDisabled(false)
            StopProd:SetDisabled(true)
        end

        StopProd:Dock(BOTTOM)
        local CurrentReaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
        local GradientUp = Material("vgui/gradient-u")
        local CasingTemperature = ReactorCasingTemperatures[ReactorTypes[self:GetReactorType()]]
        local OptimalTemperature = 1 - (CurrentReaction.EfficientTemperature / CasingTemperature)

        local TemperatureGauge = vgui.Create("DPanel", Frame)
        TemperatureGauge:Dock(LEFT)
        TemperatureGauge:SetWide(30)
        TemperatureGauge.Value = self:GetTemperature() / CasingTemperature

        TemperatureGauge.Paint = function(Panel, w, h)
            surface.SetDrawColor(Color(0, 150, 0, 255))
            surface.SetMaterial(GradientUp)
            surface.DrawTexturedRect(0, 0, w, h)
            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawTexturedRect(0, 0, w, h * 0.5)
            surface.SetDrawColor(255, 255, 0, 255)
            surface.DrawRect(0, OptimalTemperature * h - 7, w, 14)
            surface.SetDrawColor(0, 0, 0, 250)
            Panel:DrawOutlinedRect()
            surface.DrawRect(0, (1 - Panel.Value) * h - 2, w, 3)
            surface.SetDrawColor(255, 255, 255, 250)
            surface.DrawRect(0, (1 - Panel.Value) * h - 1, w, 1)
        end

        Frame.TemperatureGauge = TemperatureGauge
        self.UIPanel = Frame
    end

    function ENT:CloseUI()
        self.UIPanel:Remove()
    end

    function ENT:IsUIOpened()
        return IsValid(self.UIPanel)
    end

    net.Receive("sc_module_reactor_ui", function()
        local Reactor = net.ReadEntity()
        local SC_Active = net.ReadBool()
        if not IsValid(Reactor) then return end
        Reactor.MaxTemperature = MaxTemp
        Reactor.SC_Active = SC_Active

        if not Reactor:IsUIOpened() then
            Reactor:OpenUI()
        end
    end)
else
    function ENT:Initialize()
        self.IsSupplyingBonuses = false
        self.ReactorIgnitedThisCycle = false
        self.LastTime = 0
        self.CoolantPID = GAMEMODE.class.new("pid", 100, 45, 0.025)
        Base.Initialize(self)
    end

    function ENT:OnReloaded()
        self.CoolantPID = GAMEMODE.class.new("pid", 100, 45, 0.025)
        self.CoolantPID:SetOutputLimits(0, math.Remap(self:GetCompressorLevel(), 1, MaxUpgradeLevel, 0.75, 1))
    end

    function ENT:Explode()
        local Core = self:GetProtector()
        local Size = self:GetReactorSize() * ReactorExplosionModifiers[ReactorTypes[self:GetReactorType()]]
        local DamageMultiplier = Size ^ 2.75

        if IsValid(Core) then
            Core:DamageHull({
                EM = 20000 * DamageMultiplier,
                EXP = 70000 * DamageMultiplier,
                THERM = 50000 * DamageMultiplier,
                ANTIMATTER = ReactorTypes[self:GetReactorType()] == "Antimatter" and 10000 * DamageMultiplier or 0
            }, self:GetPlayer())
        else
            local Damage = {
                EM = 40000 * DamageMultiplier,
                EXP = 140000 * DamageMultiplier,
                THERM = 100000 * DamageMultiplier,
                ANTIMATTER = ReactorTypes[self:GetReactorType()] == "Antimatter" and 100000 * DamageMultiplier or 0
            }

            SC.Explode(self:GetPos(), 250 * Size, 500 * Size, 12000, Damage, self:GetPlayer(), self)
        end

        local EffectData = {}
        EffectData.Origin = self:GetPos()
        EffectData.Normal = self:GetAngles():Forward()
        EffectData.Scale = 0.05 * self:GetReactorSize()
        SC.CreateEffect("impact_bigbertha", EffectData)

        timer.Simple(0, function()
            self:Remove()
        end)
    end

    local function GenerateUpgradeSalvage(Reactor, ReactorSize, Upgrade, OutResources)
        local Level = Reactor[string.format("Get%sLevel", Upgrade)](Reactor)

        if math.random(100) < 15 then
            local ResourceName = UpgradeResources[Upgrade][Level]
            OutResources[ResourceName] = (OutResources[ResourceName] or 0) + 1
        else
            for I = 1, Level * 2 do
                local Amount, ResourceName = table.Random(RandomSalvageResources)
                OutResources[ResourceName] = (OutResources[ResourceName] or 0) + math.ceil(Amount * ReactorSize)
            end
        end
    end
    function ENT:GenerateSalvagingResources()
        local Size = self:GetReactorSize()
        local OutResources = {}

        for Upgrade in pairs(UpgradeResources) do
            GenerateUpgradeSalvage(self, Size, Upgrade, OutResources)
        end

        return OutResources
    end

    duplicator.RegisterEntityClass("sc_module_reactor", GAMEMODE.MakeEnt, "Data")

    function ENT:LoadSCInfo(Info)
        self.SCDupeInfo = Info or {}
        self:SetIsAuxiliary(Info.IsAuxiliary or false)
        self:SetModuleName(Info.ReactorName or "Reactor")
        self:SetReactorType(Info.ReactorType or 3)
        self:ChangeReaction(Info.ReactorReaction or 1)
        self:SetReactorCoolant(Info.ReactorCoolant or 1)
        self:SetSlot(self:GetIsAuxiliary() and "Auxiliary Reactor" or "Primary Reactor")
        self:SetGeneratorLevel(math.Clamp(Info.GeneratorLevel or 1, 1, MaxUpgradeLevel))
        self:SetTurbineLevel(math.Clamp(Info.TurbineLevel or 1, 1, MaxUpgradeLevel))
        self:SetCoolerLevel(math.Clamp(Info.CoolerLevel or 1, 1, MaxUpgradeLevel))
        self:SetCompressorLevel(math.Clamp(Info.CompressorLevel or 1, 1, MaxUpgradeLevel))
        self:SetRecuperatorLevel(math.Clamp(Info.RecuperatorLevel or 1, 1, MaxUpgradeLevel))

        if IsValid(self:GetProtector()) then
            self:GetProtector():UpdateModifiers()
        end
    end

    function ENT:SaveSCInfo()
        local Data = {}
        Data["IsAuxiliary"] = self:GetIsAuxiliary()
        Data["ReactorName"] = self:GetModuleName()
        Data["ReactorType"] = self:GetReactorType()
        Data["ReactorReaction"] = self:GetReactorReaction()
        Data["ReactorCoolant"] = self:GetReactorCoolant()
        Data["GeneratorLevel"] = self:GetGeneratorLevel()
        Data["TurbineLevel"] = self:GetTurbineLevel()
        Data["CoolerLevel"] = self:GetCoolerLevel()
        Data["CompressorLevel"] = self:GetCompressorLevel()
        Data["RecuperatorLevel"] = self:GetRecuperatorLevel()

        return Data
    end

    function ENT:SavePersistentInfo()
        local Info = Base.SavePersistentInfo(self)
        Info["ReactorReaction"] = self:GetReactorReaction()
        Info["ReactorCoolant"] = self:GetReactorCoolant()
        Info["GeneratorLevel"] = self:GetGeneratorLevel()
        Info["TurbineLevel"] = self:GetTurbineLevel()
        Info["CoolerLevel"] = self:GetCoolerLevel()
        Info["CompressorLevel"] = self:GetCompressorLevel()
        Info["RecuperatorLevel"] = self:GetRecuperatorLevel()
        Info["Temperature"] = self:GetTemperature()
        Info["MaxTemperature"] = self.MaxTemperature
        Info["ManualTempControl"] = self.ManualTempControl
        Info["CoolantFlowRate"] = self:GetCoolantFlowRate()
        Info["DisableAlarm"] = self.DisableAlarm

        return Info
    end

    function ENT:LoadPersistentInfo(Info)
        self:ChangeReaction(Info.ReactorReaction or 1)
        self:SetReactorCoolant(Info.ReactorCoolant or 1)
        self:SetGeneratorLevel(math.Clamp(Info.GeneratorLevel or 1, 1, MaxUpgradeLevel))
        self:SetTurbineLevel(math.Clamp(Info.TurbineLevel or 1, 1, MaxUpgradeLevel))
        self:SetCoolerLevel(math.Clamp(Info.CoolerLevel or 1, 1, MaxUpgradeLevel))
        self:SetCompressorLevel(math.Clamp(Info.CompressorLevel or 1, 1, MaxUpgradeLevel))
        self:SetRecuperatorLevel(math.Clamp(Info.RecuperatorLevel or 1, 1, MaxUpgradeLevel))
        self:SetTemperature(Info.Temperature or 0)
        self.MaxTemperature = Info.MaxTemperature or 0
        self.ManualTempControl = Info.ManualTempControl or false
        self:SetCoolantFlowRate(Info.CoolantFlowRate or 0)
        self.DisableAlarm = Info.DisableAlarm or false

        Base.LoadPersistentInfo(self, Info)
    end

    function ENT:StartSupplyingBonuses()
        if not self.IsSupplyingBonuses then
            local Core = self:GetProtector()

            if IsValid(Core) then
                Core.Fitting.PG = Core.Fitting.PG + self:GetPGBonus()
                self.IsSupplyingBonuses = true
            end
        end
    end

    function ENT:StopSupplyingBonuses()
        if self.IsSupplyingBonuses then
            local Core = self:GetProtector()

            if IsValid(Core) then
                Core.Fitting.PG = Core.Fitting.PG - self:GetPGBonus()
            end

            self.IsSupplyingBonuses = false
        end
    end

    function ENT:OnModuleEnabled()
        if self.IsSupplyingBonuses then
            self.IsSupplyingBonuses = false
            self:StartSupplyingBonuses()
        end

        local Mod = 1

        if self:GetIsAuxiliary() then
            Mod = 0.25
        end

        self:SetReactorSize(ReactorSizes[self:GetProtector():GetShipClass()] * Mod)
        local GeneratorEfficiency = math.Remap(self:GetGeneratorLevel(), 1, MaxUpgradeLevel, 0.65, 1)
        self:SetPGBonus(math.floor(GeneratorEfficiency * 10000 * (self:GetReactorSize() ^ 1.2) * ReactorPGModifiers[ReactorTypes[self:GetReactorType()]]))
        self.CoolantPID:SetOutputLimits(0, math.Remap(self:GetCompressorLevel(), 1, MaxUpgradeLevel, 0.75, 1))
        self:ChangeReaction(self:GetReactorReaction())
    end

    function ENT:OnModuleDisabled()
        self:StopSupplyingBonuses()
        self:SetReactorSize(ReactorSizes.Drone)
        self:SetPGBonus(0)
        self:ChangeReaction(self:GetReactorReaction())
    end

    function ENT:OnStartedCycling()
        -- Called when the module starts a cycle
        self:StartSupplyingBonuses()
        self:GetProtector():UpdateModifiers()
    end

    function ENT:OnCycleUpdated(TimeRemaining, DeltaTime)
        -- Called when a module's cycle is updated
        local ReactorType = ReactorTypes[self:GetReactorType()]
        local Reaction = Reactions[ReactorType][self:GetReactorReaction()]
        local Energy, Efficiency = self:CalculatePowerGeneration(self:GetTemperature(), Reaction.EfficientTemperature, self:GetReactorSize(), Reaction.PowerVariable)

        if not self.ManualTempControl then
            self.MaxTemperature = Reaction.EfficientTemperature
        end

        local Steps = math.ceil(DeltaTime / CoolantTargetDeltaTime)
        local TimeStep = DeltaTime / Steps
        local Coolant = Coolants[self:GetReactorCoolant()]
        local CoolantUsed = 0
        local EnergyProduced = 0
        local BaseCoolantAmount = self:GetReactorSize() * Coolant.AmountNeeded * TimeStep
        local CoolantHeatReduction = Coolant.HeatDissipation * math.Remap(self:GetCoolerLevel(), 1, MaxUpgradeLevel, 0.75, 1) * TimeStep
        local TargetTemp = self.MaxTemperature
        local RecuperatorEfficiency = math.Remap(self:GetRecuperatorLevel(), 1, MaxUpgradeLevel, 0.9, 1)
        local IgnitionTemperature = ReactorIgnitionModifiers[ReactorType] * Reaction.EfficientTemperature
        local HeaterEnergy = math.ceil(HeaterEnergyUsage * self:GetReactorSize() * TimeStep)
        local HeaterHeat = math.ceil(200 * TimeStep)

        for I = 1, Steps do
            local HeatGeneration = math.pow(Reaction.PowerVariable * PowerVariableHeatMultiplier, PowerVariableHeatExponent) * (1 - Efficiency) * TimeStep
            local CurrentTemp = self:GetTemperature()

            -- If the reactor is hot enough to ignite, then start flowing coolant and producing energy
            if IgnitionTemperature <= 0 or CurrentTemp > IgnitionTemperature then
                local CoolantFlowRate = self.CoolantPID:Update(CurrentTemp / TargetTemp, 1, TimeStep)

                local CoolantAmount = math.ceil(BaseCoolantAmount * CoolantFlowRate)
                local HadCoolant = self:ConsumeResource(Coolant.CoolantName, CoolantAmount)

                if HadCoolant then
                    CoolantUsed = CoolantUsed + CoolantAmount
                    HeatGeneration = HeatGeneration - (CoolantHeatReduction * CoolantFlowRate)

                    local AmountReturned = math.floor(CoolantAmount * RecuperatorEfficiency)
                    if AmountReturned > 0 then
                        self:SupplyResource(Coolant.OutResource, AmountReturned)
                    end
                end

                EnergyProduced = EnergyProduced + math.floor(Energy * TimeStep)
                self:SetTemperature(math.max(self:GetTemperature() + math.floor(HeatGeneration), 0))
                Energy, Efficiency = self:CalculatePowerGeneration(self:GetTemperature(), Reaction.EfficientTemperature, self:GetReactorSize(), Reaction.PowerVariable)

            -- Otherwise, heat the reactor using energy up to it's target temperature.
            else
                if CurrentTemp < TargetTemp and self:ConsumeResource("Energy", HeaterEnergy) then
                    self:SetTemperature(math.max(CurrentTemp + math.floor(HeaterHeat), 0))

                -- Cool the reactor down if there wasn't enough energy or we reached the target temperature
                else
                    self:SetTemperature(math.max(CurrentTemp - math.ceil(25 * TimeStep), 0))
                end
            end
        end

        -- Track if the reactor ignited at any point during the cycle
        -- If it didn't then the resources for the cycle are returned afterwards to allow
        -- preheating reactors at the cost of increased energy usage
        if EnergyProduced > 0 then
            self.ReactorIgnitedThisCycle = true
        end

        self:SetCoolantFlowRate(CoolantUsed)
        self:SupplyResource("Energy", EnergyProduced)

        if WireLib ~= nil then
            WireLib.TriggerOutput(self, "Efficiency", Efficiency)
            WireLib.TriggerOutput(self, "Energy Output", EnergyProduced)
            WireLib.TriggerOutput(self, "Coolant Usage", CoolantUsed)
        end

        local MaxCasingTemperature = ReactorCasingTemperatures[ReactorType]

        if self:GetTemperature() > MaxCasingTemperature * 0.8 then
            if not self.DisableAlarm then
                self:EmitSound("buttons/button_synth_negative_01.wav", 130, 50)
                self:EmitSound("buttons/button10.wav", 130, 50)
                self:EmitSound("buttons/button8.wav", 130, 75)

                timer.Simple(0.3, function()
                    if not IsValid(self) then return end
                    self:EmitSound("buttons/button10.wav", 130, 100)
                    self:EmitSound("buttons/button8.wav", 130, 125)
                end)
            end

            if self:GetTemperature() > MaxCasingTemperature then
                self:Explode()
            end
        end
    end

    function ENT:OnStoppedCycling()
        -- Called when the module finishes it's cycle.
        if not self.SC_Active or not self:CanCycle() then
            self:StopSupplyingBonuses()
        end
    end

    function ENT:OnCycleFinished()
        local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]

        -- If the reactor ignited this cycle then output the reaction resources
        if self.ReactorIgnitedThisCycle then
            for i, k in pairs(Reaction.Outputs) do
                self:SupplyResource(i, k * self:GetReactorSize())
            end

        -- If it didn't ignite then just refund the resources
        else
            for i, k in pairs(Reaction.Inputs) do
                self:SupplyResource(i, k * self:GetReactorSize())
            end

        end

        -- Reset for the next cycle
        self.ReactorIgnitedThisCycle = false

        if self.UpdateAfterCycle then
            self:ChangeReaction(self.ReactionToSet)
            self:SetReactorCoolant(self.CoolantToSet)

            if math.abs(self.TemperatureToSet - self.MaxTemperature) > 50 then
                self.MaxTemperature = self.TemperatureToSet
                self.ManualTempControl = true
            end

            self.UpdateAfterCycle = false
        end
    end

    function ENT:OnCoreUpdated()
        if not self:IsProtected() then
            SC.Error("sc_module_reactor::OnCoreUpdated Core doesn't exist yet? WTF?", 5)

            return
        end

        -- Called when the core finishes recalculating. Commonly used to apply bonuses to modules.
        local Mod = 1

        if self:GetIsAuxiliary() then
            Mod = 0.25
        end

        self:SetReactorSize(ReactorSizes[self:GetProtector():GetShipClass()] * Mod)
        local GeneratorEfficiency = math.Remap(self:GetGeneratorLevel(), 1, MaxUpgradeLevel, 0.65, 1)
        self:SetPGBonus(math.floor(GeneratorEfficiency * 10000 * (self:GetReactorSize() ^ 1.2) * ReactorPGModifiers[ReactorTypes[self:GetReactorType()]]))
        self:ChangeReaction(self:GetReactorReaction())
        self.CoolantPID:SetOutputLimits(0, math.Remap(self:GetCompressorLevel(), 1, MaxUpgradeLevel, 0.75, 1))
    end

    function ENT:SetSlot(Slot)
        local Fitting = self:GetFitting()
        Fitting.Slot = Slot
        self:SetFitting(Fitting)
    end

    function ENT:GetWirePorts()
        return {"On", "Manual Temperature Control", "Maximum Temperature", "Disable Alarm"}, {"Cycle Duration", "Cycle Percent", "Temperature", "Efficiency", "Energy Output", "Coolant Usage", "Reaction Inputs [TABLE]", "Reaction Outputs [TABLE]"}
    end

    function ENT:TriggerInput(Input, Value)
        if Input == "Manual Temperature Control" then
            self.ManualTempControl = Value == 1

            if not self.ManualTempControl then
                local Reaction = Reactions[ReactorTypes[self:GetReactorType()]][self:GetReactorReaction()]
                self.MaxTemperature = Reaction.EfficientTemperature
            end
        elseif Input == "Maximum Temperature" then
            self.MaxTemperature = Value
        elseif Input == "Disable Alarm" then
            self.DisableAlarm = Value == 1
        end

        Base.TriggerInput(self, Input, Value)
    end

    function ENT:UpdateOutputs()
        if WireLib ~= nil then
            if not self:GetCycling() then
                WireLib.TriggerOutput(self, "Efficiency", 0)
                WireLib.TriggerOutput(self, "Energy Output", 0)
                WireLib.TriggerOutput(self, "Coolant Usage", 0)
            end

            WireLib.TriggerOutput(self, "Temperature", self:GetTemperature())
        end
    end

    function ENT:Think()
        if not self:GetCycling() and self:GetTemperature() > 0 then
            self:SetTemperature(self:GetTemperature() - 25)

            if self:GetTemperature() < 0 then
                self:SetTemperature(0)
            end
        end

        self:UpdateOutputs()

        return Base.Think(self)
    end

    function ENT:Use(Player)
        if not IsValid(Player) or not Player:IsPlayer() or (self.CPPICanTool and not self:CPPICanTool(Player)) then return end
        net.Start("sc_module_reactor_ui")
        net.WriteEntity(self)
        net.WriteFloat(self.MaxTemperature)
        net.WriteBool(self.SC_Active)
        net.Send(Player)
    end

    net.Receive("sc_module_reactor_ui", function(Length, Player)
        if not IsValid(Player) then return end
        local Reactor = net.ReadEntity()
        if not IsValid(Reactor) then return end
        if Reactor.CPPICanTool and not Reactor:CPPICanTool(Player) then return end
        local Reaction = net.ReadInt(8)
        local Coolant = net.ReadInt(8)
        Reactor:TriggerInput("On", net.ReadBit())

        if not Reactor:GetCycling() then
            Reactor:ChangeReaction(Reaction)
            Reactor:SetReactorCoolant(Coolant)
        elseif Coolant ~= Reactor:GetReactorCoolant() or Reaction ~= Reactor:GetReactorReaction() then
            Reactor.CoolantToSet = Coolant
            Reactor.ReactionToSet = Reaction
            Reactor.UpdateAfterCycle = true

            return
        end
    end)

    net.Receive("SC.Reactor.DoUpgrade", function(Length, Player)
        if not IsValid(Player) then return end
        local Reactor = net.ReadEntity()
        if not IsValid(Reactor) then return end
        if Reactor.CPPICanTool and not Reactor:CPPICanTool(Player) then return end

        local UpgradeType = net.ReadString()

        -- This is cursed, but whatever. Gets the current upgrade level.
        local UpgradeLevel = Reactor[string.format("Get%sLevel", UpgradeType)](Reactor)

        -- Only upgrade up to the max level
        if UpgradeLevel >= MaxUpgradeLevel then
            return
        end

        UpgradeLevel = UpgradeLevel + 1

        if not Reactor:ConsumeResource(UpgradeResources[UpgradeType][UpgradeLevel], 1) then
            return
        end

        Reactor[string.format("Set%sLevel", UpgradeType)](Reactor, UpgradeLevel)
    end)
end