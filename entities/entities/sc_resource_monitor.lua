AddCSLuaFile()

DEFINE_BASECLASS("base_lsentity")

ENT.PrintName = "Resource Monitor"
ENT.Author = "Divran"
ENT.Contact = "diaspora-community.com"
ENT.Purpose = "Monitor your resources with fancy progress bars and stuff."
ENT.Instructions = "Monitor your resources with fancy progress bars and stuff."
ENT.RenderGroup		= RENDERGROUP_TRANSLUCENT

ENT.Spawnable = false
ENT.AdminOnly    = false
ENT.SC_HasGenericPodLink = true

local base = scripted_ents.Get("base_lsentity")
hook.Add("InitPostEntity", "sc_resource_monitor_toolinit", function()
    base = scripted_ents.Get("base_lsentity")

    local Generator = GAMEMODE:NewGeneratorInfo()
    Generator:SetName("Resource Monitor")
    Generator:SetClass("sc_resource_monitor")
    Generator:SetDescription("Monitor your resources with fancy progress bars and stuff.")
    Generator:SetCategory("Utility")
    Generator:SetDefaultModel("models/sbep_community/d12shieldemitter.mdl")

    GAMEMODE:RegisterGenerator(Generator)
end)

function ENT:SetupGenerator()
end

function ENT:SharedInit()
    base.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "LinkedPod", "entity", NULL)
end

function ENT:IsLinkedToPod()
    return IsValid(self:GetLinkedPod())
end

if CLIENT then
	local fontname = "GModWorldtip_Smaller_resourcemonitor"

	surface.CreateFont( fontname, {
		font		= "Helvetica",
		size		= SC2ScreenScale(16),
		weight		= 700
	})

	surface.SetFont( fontname )
	local width, char_height = surface.GetTextSize( "M" )

	-- These are some values used by the third display mode
	local columns = {}
	local w1 = surface.GetTextSize( "Resource" )
	local w2 = surface.GetTextSize( "Network (In/Out)" )
	local w3 = surface.GetTextSize( "Stored" )
	local w4 = surface.GetTextSize( "Time" )
	local column_widths = {}
	local consumption = {}
	local production = {}
	local delta_colors = {}
	-- end values

	-- These are some values used by the first display mode
	local bar_height = 8
	local bar_padding = 8
	local column_width = 400
	-- end values

	local pie_column_width = 420

	local displayModesSizes = {
		function( data )
			local h = (#data-1) * (char_height + bar_padding + bar_height) + 9

			local w = math.ceil(h / (ScrH() - 128))

			return w * column_width - 18*2, math.min(h,ScrH()-128)
		end,
		function( data )
			local h = (#data-1) * (char_height) + 9 + 240

			local w = math.ceil(h / (ScrH() - 128))

			return math.max(w * pie_column_width - 18*2,420), math.min(h,ScrH()-128)
		end,
		function( data )
			columns =  {
				name = 			{"Resource"},
				delta =			{"Network (In/Out)"},
				amount = 		{"Stored"},
				time = 			{"Time"}
			}
			column_widths = {
				name = 			w1,
				delta = 		w2,
				amount = 		w3,
				time = 			w4
			}
			delta_colors = {}
			consumption = data.consumption
			production = data.production

			for i=1,#data.res do
				local v = data.res[i]
				local name = v[1]
				local amount = v[2]
				local max = v[3]

				local percent = amount / max

				local display_amount = nicenumber.formatDecimal( amount )
				local display_max = nicenumber.formatDecimal( max )
				local display_percent = math.Round( percent * 100 )

				local display_production = nicenumber.formatDecimal( production[name] or 0 )
				local display_consumption = nicenumber.formatDecimal( consumption[name] or 0 )
				local delta = (production[name] or 0) - (consumption[name] or 0)
				local symbol = delta == 0 and "" or (delta < 0 and "-" or "+")
				local display_delta = symbol .. nicenumber.formatDecimal( math.abs( delta ) )

				-- Time
				local time = 0
				if delta > 0 then
					time = (max - amount) / math.abs( delta )
				elseif delta < 0 then
					time = amount / math.abs( delta )
				end
				time = nicenumber.nicetime( time )

				-- Name
				columns.name[#columns.name+1] = name
				local w = surface.GetTextSize( name )
				column_widths.name = math.max( column_widths.name, w )

				-- Delta
				display_delta = string.format( "%s (%s/%s)", display_delta, display_production, display_consumption )
				columns.delta[#columns.delta+1] = display_delta
				w = surface.GetTextSize( display_delta )
				column_widths.delta = math.max( column_widths.delta, w )
				delta_colors[#columns.delta] = (delta == 0 and Color(255,255,255,255) or (delta < 0 and Color(200,75,75) or Color(75,200,75)))

				-- Amount
				amount = string.format( "%s/%s (%s%%)", display_amount, display_max, display_percent )
				columns.amount[#columns.amount+1] = amount
				w = surface.GetTextSize( amount )
				column_widths.amount = math.max( column_widths.amount, w )

				-- Time
				columns.time[#columns.time+1] = time
				w = surface.GetTextSize( time )
				column_widths.time = math.max( column_widths.time, w )
			end

			local total_width = 0
			for k,v in pairs( column_widths ) do
				total_width = total_width + v + 18
			end

			return total_width, (#data.res+1) * char_height + 18
		end
	}

	function ENT:GetWorldTipBodySize()
		local data = self:GetOverlayData()
		if not data or data[1] == -1 or data[1] == -2 then return 40, 32 end

		return displayModesSizes[data[1]+1]( data )
	end

	-------------------------
	-- colors
	-------------------------
	local colors = {
		Energy = 		Color(255,0,0,255), -- red
		Oxygen = 		Color(0,255,0,255), -- green
		Water = 		Color(0,0,255,255), -- blue
		["Carbon Dioxide"] = 			Color(255,165,0,255), -- orange
		Hydrogen = 		Color(255,255,0,255), -- yellow
		Nitrogen = 		Color(255,0,255,255), -- purple
		Steam = 		Color(0,255,255,255), -- teal
		Antimatter =	Color(255,255,255,255), -- white
		["Lifesupport Canister"] = 			Color(0,100,0,255), -- dark green
		["Empty Canister"] =	Color(100,100,100,255), -- gray
	}
	local white = Color(255,255,255,255)
	local black = Color(0,0,0,255)

	-------------------------
	-- DisplayModes
	-------------------------
	local deg2rad = math.pi / 180

	local function circleOutline( posx, posy, radius )
		surface.SetDrawColor( black )
		local lastx, lasty, firstx, firsty = 0,0,0,0
		local vertices = 360
		for i=1,vertices do
			local cos = math.cos(i/vertices*360*deg2rad)
			local sin = math.sin(i/vertices*360*deg2rad)
			local x = cos*radius + posx
			local y = sin*radius + posy

			if i > 1 then
				surface.DrawLine( lastx, lasty, x, y )
			else
				firstx, firsty = x, y
			end

			lastx, lasty = x, y
		end
		surface.DrawLine( lastx, lasty, firstx, firsty )
	end

	local function partialCircle( posx, posy, radius, start, stop )
		local poly = {x=posx,y=posy}
		local poly2 = {x=posx,y=posy}
		for i=start,stop do
			if i - start <= 180 then
				poly[#poly+1] = {
					x = math.cos( i * deg2rad ) * radius + posx,
					y = math.sin( i * deg2rad ) * radius + posy,
				}

				if i - start == 180 then
					poly2[#poly2+1] = {
						x = math.cos( i * deg2rad ) * radius + posx,
						y = math.sin( i * deg2rad ) * radius + posy,
					}
				end
			else
				poly2[#poly2+1] = {
					x = math.cos( i * deg2rad ) * radius + posx,
					y = math.sin( i * deg2rad ) * radius + posy,
				}
			end
		end
		poly[#poly+1] = {x=posx,y=posy}
		poly2[#poly2+1] = {x=posx,y=posy}
		surface.SetTexture(0)
		surface.DrawPoly( poly )
		if #poly2 > 2 then surface.DrawPoly( poly2 ) end
	end

	local function drawAllPartialCircles(data,pos,posx,posy,currentmax_idx)
		local currentmax = 0
		for i=2,#data do currentmax = currentmax + data[i][currentmax_idx] end

		local angoffset = 0
		--if LocalPlayer():SteamID() == "STEAM_0:1:26603751" then angoffset = (RealTime() * 90) % 360 end
		if pos.spin then angoffset = (RealTime() * 90) % 360 end
		for i=2,#data do
			local v = data[i]
			local name = v[1]
			local amount = v[2]

			if amount > 0 then
				local start = angoffset
				local stop = start + (amount / currentmax) * 360

				surface.SetDrawColor( colors[name] or white )
				partialCircle( posx, posy, 100, start, stop )

				surface.SetDrawColor( black )
				local x, y = posx + math.cos( start * deg2rad ) * 100, posy + math.sin( start * deg2rad ) * 100
				surface.DrawLine( x, y, posx, posy )

				angoffset = angoffset + (stop - start)
			end
		end
	end

	local displayModes = {
		function(data, pos) -- bars
			local xpos = 0
			local counter = 0
			for i=2,#data do
				local ypos = counter * (char_height + bar_padding + bar_height)
				counter = counter + 1

				if ypos >= (ScrH() - 128 - pos.edgesize * 2) then
					xpos = xpos + column_width
					ypos = 0
					counter = 1
				end

				local v = data[i]
				local name = v[1]
				local amount = v[2]
				local max = v[3]
				local delta = v[4]

				local percent = amount / max

				draw.DrawText( name, fontname, pos.min.x + pos.edgesize + xpos, pos.min.y + pos.edgesize + ypos, white, TEXT_ALIGN_LEFT )

				local display_amount = nicenumber.formatDecimal( amount )
				local display_max = nicenumber.formatDecimal( max )
				local display_percent = math.Round( percent * 100 )
				local display_delta = nicenumber.formatDecimal( math.abs(delta) )

				-- Symbol
				local symbol = delta == 0 and "" or (delta < 0 and "-" or "+")

				-- Time
				local time = 0
				if delta > 0 then
					time = (max - amount) / math.abs( delta )
				elseif delta < 0 then
					time = amount / math.abs( delta )
				end
				time = nicenumber.nicetime( time )

				local str = string.format(
											"%s/%s (%s%%), %s%s, %s",
											display_amount,
											display_max,
											display_percent,
											symbol,
											display_delta,
											time
										)

				-- draw text
				draw.DrawText( str, fontname, pos.min.x + xpos + column_width - pos.edgesize, pos.min.y + pos.edgesize + ypos, white, TEXT_ALIGN_RIGHT )

				local width = column_width - pos.edgesize * 2

				-- draw bar
				surface.SetDrawColor( colors[name] or white )
				surface.DrawRect( pos.min.x + pos.edgesize + xpos, pos.min.y + pos.edgesize + ypos + 20, width * percent, bar_height )

				-- Draw delta progress bar
				local c = colors[name] or white
				if delta > 0 then
					c = Color(c.r,c.g,c.b,50)
				else
					c = Color(0,0,0,200)
				end
				surface.SetDrawColor( c )
				local w = (math.abs(delta)/max) * width
				local x = pos.min.x + pos.edgesize + width * percent
				if delta < 0 then
					x = math.max(math.ceil(x - w),pos.min.x+pos.edgesize)
					w = math.min(w,width*percent)
				else
					w = math.min(w,width - width*percent)
				end
				surface.DrawRect( x + xpos, pos.min.y + pos.edgesize + ypos + 20, w, bar_height )

				-- draw outline
				surface.SetDrawColor( black )
				surface.DrawOutlinedRect( pos.min.x + pos.edgesize + xpos, pos.min.y + pos.edgesize + ypos + 20, (column_width - pos.edgesize * 2), bar_height )
			end
		end,
		function(data, pos) -- pie chart
			local center = pos.min.x + pos.size.w / 2
			local left = center - 110
			local right = center + 110

			do
				draw.DrawText( "% of current", fontname, left, pos.min.y + pos.edgesize, white, TEXT_ALIGN_CENTER )
				local posx = left
				local posy = pos.min.y + pos.edgesize + 130
				drawAllPartialCircles( data, pos, posx, posy, 2 )
				circleOutline( left, pos.min.y + pos.edgesize + 130, 100 )
			end

			do
				draw.DrawText( "% of total", fontname, right, pos.min.y + pos.edgesize, white, TEXT_ALIGN_CENTER )
				local posx = right
				local posy = pos.min.y + pos.edgesize + 130
				drawAllPartialCircles( data, pos, posx, posy, 3 )
				circleOutline( right, pos.min.y + pos.edgesize + 130, 100 )
			end

			local xpos = 0
			local counter = 0
			for i=2,#data do
				local ypos = 240 + counter * (char_height)
				counter = counter + 1

				if ypos >= (ScrH() - 128 - pos.edgesize * 2) then
					xpos = xpos + pie_column_width
					ypos = 240
					counter = 1
				end

				local v = data[i]
				local name = v[1]
				local amount = v[2]
				local max = v[3]
				local delta = v[4]

				local percent = amount / max

				local color = colors[name] or white
				surface.SetDrawColor( color )
				surface.DrawRect( pos.min.x + pos.edgesize + xpos, pos.min.y + pos.edgesize + ypos + 5, 10, 10 )
				surface.SetDrawColor( black )
				surface.DrawOutlinedRect( pos.min.x + pos.edgesize + xpos, pos.min.y + pos.edgesize + ypos + 5, 10, 10 )

				draw.DrawText( name, fontname, pos.min.x + pos.edgesize + 20 + xpos, pos.min.y + pos.edgesize + ypos, white, TEXT_ALIGN_LEFT )

				local display_amount = nicenumber.formatDecimal( amount )
				local display_max = nicenumber.formatDecimal( max )
				local display_percent = math.Round( percent * 100 )
				local display_delta = nicenumber.formatDecimal( math.abs(delta) )

				-- Symbol
				local symbol = delta == 0 and "" or (delta < 0 and "-" or "+")

				-- Time
				local time = 0
				if delta > 0 then
					time = (max - amount) / math.abs( delta )
				elseif delta < 0 then
					time = amount / math.abs( delta )
				end
				time = nicenumber.nicetime( time )

				local str = string.format(
											"%s/%s (%s%%), %s%s, %s",
											display_amount,
											display_max,
											display_percent,
											symbol,
											display_delta,
											time
										)

				draw.DrawText( str, fontname, pos.min.x + xpos + pie_column_width - pos.edgesize, pos.min.y + pos.edgesize + ypos, white, TEXT_ALIGN_RIGHT )
			end
		end,
		function( data, pos ) -- total net income
			-- this table ensures we iterate in the correct order
			local columns_order = { "name", "delta", "amount", "time" }
			local xpos = pos.edgesize
			for i=1,#columns_order do
				local index = columns_order[i]

				local column = columns[index]
				if index == "delta" then
					for i=1,#column do
						local color = delta_colors[i] or white
						draw.DrawText( column[i], fontname, pos.min.x + pos.edgesize + xpos, pos.min.y + pos.edgesize + (i-1) * char_height, color, TEXT_ALIGN_LEFT )
					end
				elseif index == "name" then
					for i=1,#column do
						if i > 1 then
							local color = colors[data.res[i-1][1]] or white
							surface.SetDrawColor( color )
							surface.DrawRect( pos.min.x + pos.edgesize, pos.min.y + pos.edgesize + (i-1) * char_height + 5, 10, 10 )
							surface.SetDrawColor( black )
							surface.DrawOutlinedRect( pos.min.x + pos.edgesize, pos.min.y + pos.edgesize + (i-1) * char_height + 5, 10, 10 )
						end

						draw.DrawText( column[i], fontname, pos.min.x + pos.edgesize + xpos, pos.min.y + pos.edgesize + (i-1) * char_height, white, TEXT_ALIGN_LEFT )
					end
				else
					local str = table.concat( column, "\n" )
					draw.DrawText( str, fontname, pos.min.x + pos.edgesize + xpos, pos.min.y + pos.edgesize, white, TEXT_ALIGN_LEFT )
				end

				local width = column_widths[index]
				xpos = xpos + width + 18
			end
		end,
	}

	-------------------------
	-- Draw the stuff
	-------------------------
	function ENT:DrawWorldTipBody( WorldTip, pos )
		local data = self:GetOverlayData()
		if not data or data[1] == -1 then

			local str = "Not linked"
			draw.DrawText( str, fontname, pos.min.x + pos.size.w/2, pos.min.y + pos.edgesize, white, TEXT_ALIGN_CENTER )

			return
		end

		if not data or data[1] == -2 then

			local str = "No resources to display"
			draw.DrawText( str, fontname, pos.min.x + pos.size.w/2, pos.min.y + pos.edgesize, white, TEXT_ALIGN_CENTER )

			return
		end

		-- secrit hobnob spin
		if LocalPlayer():KeyPressed( IN_USE ) then
			self.start_use = RealTime()
		elseif LocalPlayer():KeyReleased( IN_USE ) then
			if RealTime() - (self.start_use or RealTime()) > 2 then
				if not self.spin then
					self.spin = true
				else
					self.spin = nil
				end
			end

			self.start_use = nil
		end

		if self.spin then pos.spin = true end

		local displayMode = data[1] + 1
		displayModes[displayMode]( data, pos )
	end


	local base = scripted_ents.Get("base_lsentity")
	hook.Add( "InitPostEntity", "resource_monitor_post_entity_init", function()
		base = scripted_ents.Get("base_lsentity")
	end)
    function ENT:Draw()
        if self:GetOverlayData() ~= nil and next(self:GetOverlayData()) ~= nil and self:GetNWBool("emit", false) and GPULib then
            if not self.GPU then self.GPU = GPULib.WireGPU(self, true) end
            self:DrawModel()

            local max = self:OBBMaxs()
            local min = self:OBBMins()
            local sizex = (max.x - min.x)
            local sizey = (max.y - min.y)

            sizex = 512 * (1 + (sizex + 18) / 512)
            sizey = 512 * (1 + (sizey + 200) / 512)

            local size = math.max(sizex, sizey)

            self.GPU:RenderToWorld(size, size, function()
                GAMEMODE:PaintWorldTip(self)
            end, 1, true)
        else
            base.Draw(self)
            if self.GPU then
                self.GPU:Finalize()
                self.GPU = nil
            end

            if self:GetOverlayData() ~= nil and next(self:GetOverlayData()) ~= nil then
                if self:BeingLookedAtByLocalPlayer() then
                    AddWorldTip(self)
                end
            end
        end
    end

    local ResourceMonitorToDraw
    function ENT:Think()
        base.Think(self)

        if self:IsLinkedToPod() and self:GetLinkedPod():GetDriver() == LocalPlayer() then
            ResourceMonitorToDraw = self
        else
            ResourceMonitorToDraw = nil
        end
    end

    hook.Remove("HUDPaint", "resource_monitor_hudpaint")
    hook.Add("HUDPaint", "resource_monitor_hudpaint", function()
        if ResourceMonitorToDraw then
            local sizeW, sizeH = ResourceMonitorToDraw:GetWorldTipBodySize()
            GAMEMODE:PaintWorldTip(ResourceMonitorToDraw, {x=100, y=ScrH() * 0.5 - sizeH * 0.5})
        end
    end)

	local displayModesSorting = {
		function( a,b )
			return a[1] < b[1] -- sort by name
		end,
		function( a,b )
			if a[2] == b[2] then return a[3] > b[3]	end -- amount = 0, sort by max size
			return a[2] > b[2] -- sort by amount
		end,
		function( a,b )
			return a[1] < b[1] -- sort by name
		end,
	}

	function ENT:SetOverlayData( t )
		base.SetOverlayData( self, t )

		if self:GetOverlayData() == t then
			local displayMode = t[1] + 1
			if displayModesSorting[displayMode] then
				if displayMode == 3 then
					local rt = t.res
					table.sort(rt, function( a, b )
						if type(a) == "number" then return true end -- this is just the display type, always put this in front
						if type(b) == "number" then return false end -- this is just the display type, always put this in front

						return displayModesSorting[displayMode]( a, b )
					end)
				end
			end
		end
	end

	return
end

function ENT:Setup()
	self.delta = {}
	self.prev_amounts = {}
	self.DisplayMode = 0
	self:SetUseType( SIMPLE_USE )

	self.DisplayResources = {}

    if WireLib ~= nil then
        WireLib.CreateInputs( self, { "Emit", "Display Resources [ARRAY]" } )
    end
end

function ENT:UnlinkPod()
    if self:IsLinkedToPod() then
        self:SetLinkedPod(NULL)
    end
end

function ENT:LinkPod(NewPod)
    if self:IsLinkedToPod() then
        self:UnlinkPod()
    end

    if IsValid(NewPod) and NewPod:IsVehicle() then
        self:SetLinkedPod(NewPod)
        return true
    else
        return false
    end
end

function ENT:TriggerInput( name, val )
	if name == "Emit" then
		self:SetNWBool( "emit", val ~= 0 )
	elseif name == "Display Resources" then
		self.DisplayResources = {}

		for i=1,#val do
			if type(val[i]) == "string" then
				self.DisplayResources[string.lower(val[i])] = true
			end
		end
	end
end

function ENT:GetNodeStorage()
	return self:GetNode():GetStorage()
end

function ENT:Use()
	self.DisplayMode = self.DisplayMode + 1
	if self.DisplayMode == 3 then self.DisplayMode = 0 end
	self:UpdateOutputs()
end

-------------------------
-- Calculate rates, averages, etc, and sync to client
-------------------------
function ENT:UpdateOutputs()
    if IsValid(self:GetNode()) then
        --Get Players in Range if Emitting
        if self:GetNWBool("emit", false) then
            for _, ply in pairs(player.GetAll()) do
                local range = ply:GetInfoNum("sc_resourcemonitor_emitrange", 512)
                local distSqr = ply:GetPos():DistToSqr(self:GetPos())

                if distSqr < range * range then
                    self:UpdateOverlayData(ply)
                end
            end
        end

        local storages = self:GetNodeStorage()

        local res = {}
        if self.DisplayMode ~= 2 then res[1] = self.DisplayMode end

        local displayResourcesSet = table.Count(self.DisplayResources) > 0

        for type, storage in pairs(storages) do
            local resources = storage:GetStored()
            for name, v in pairs(resources) do
                if not displayResourcesSet or self.DisplayResources[string.lower(name)] then
                    local amount = self:GetAmount(name)
                    local max = self:GetMaxAmount(name)

                    if max > 0 then
                        -- we could use node:GetDelta( name ) here, but then the atmospheric probe wouldn't work properly
                        -- so we're leaving this as it is
                        local prev_amount = self.prev_amounts[name] or amount
                        local delta = amount - prev_amount
                        self.delta[name] = math.Round((self.delta[name] or 0) * 0.1 + delta * 0.9, 3)

                        res[#res + 1] = {
                            name,
                            amount,
                            max,
                            self.delta[name],
                        }

                        self.prev_amounts[name] = amount
                    end
                end
            end
        end

        if self.DisplayMode == 2 then -- for displaymode nr 3, we're going to do some extra stuff here
            local node = self:GetNode()
            local links = node:GetLinks()

            local consumption = {}
            local production = {}

            for _, ent in pairs(links) do
                if IsValid(ent) and ent.IsLSEnt and ent.IsOn and ent:IsOn() then
                    if ent.Consumption then
                        for name, v in pairs(ent.Consumption) do
                            if not displayResourcesSet or (displayResourcesSet and self.DisplayResources[string.lower(name)]) then
                                local amount = ent:GetBaseMultiplier() * ent:GetMultiplier() * v.mul
                                consumption[name] = (consumption[name] or 0) + amount
                            end
                        end
                    end

                    if ent.Production then
                        for name, v in pairs(ent.Production) do
                            if not displayResourcesSet or (displayResourcesSet and self.DisplayResources[string.lower(name)]) then
                                local amount = ent:GetBaseMultiplier() * ent:GetMultiplier() * v.mul
                                production[name] = (production[name] or 0) + amount
                            end
                        end
                    end
                end
            end

            local oldres = res
            res = {
                self.DisplayMode,
                res = oldres,
                consumption = consumption,
                production = production,
            }

            if #oldres == 1 then
                self:SetOverlayData({ -2 })
            else
                self:SetOverlayData(res)
            end
        else
            if #res == 1 then
                self:SetOverlayData({ -2 })
            else
                self:SetOverlayData(res)
            end
        end
    else
        self:SetOverlayData({ -1 })
    end
end

function ENT:SaveSCInfo()
    local Info = {}

    if self:IsLinkedToPod() then
        Info.LinkedPod = self:GetLinkedPod():EntIndex()
    end

    return Info
end

function ENT:LoadSCInfo(Info)
end

function ENT:ApplySCDupeInfo(Info, GetEntByID)
    if Info.LinkedPod then
        self:LinkPod(GetEntByID(Info.LinkedPod))
    end
end

duplicator.RegisterEntityClass("sc_resource_monitor", GAMEMODE.MakeEnt, "Data")
