--[[
	Drill Rig for SB3 Mining

	Author: F�hrball + Steeveeo
]]--
AddCSLuaFile()

hook.Add("InitPostEntity", "sc_drill_toolinit", function()
    local Generator = GAMEMODE:NewGeneratorInfo()
    Generator:SetName("Mining Drill")
    Generator:SetClass("mining_drill_rig")
    Generator:SetDescription("Stationary mining platform that spins around in the planet until resources pop out. Unfortunately cannot dig through concrete and some types of dirt.")
    Generator:SetForceModel(true)
    Generator:SetCategory("Mining")
    Generator:SetDefaultModel("models/slyfo/drillplatform.mdl")

    GAMEMODE:RegisterGenerator(Generator)
end)

function ENT:SetupGenerator() end

ENT.Type = "anim"
ENT.Base = "base_lsentity"

ENT.PrintName = "Mining Drill Rig"
ENT.Author = "F�hrball + Steeveeo"

ENT.Purpose = "Drills into the ground to dig for resources."
ENT.Instructions = "Place on terrain, and drill, baby, drill!"

if CLIENT then
    local SoundPathActive = Sound("ambient/machines/refinery_loop_1.wav")
    local SoundPathStop = Sound("doors/door_metal_thin_close2.wav")

	--Display Vars
	ENT.CurSlide = 0
	ENT.MaxSlide = 160
	ENT.CurSpin = 0

	--Sounds
	ENT.SoundState = 0
	ENT.SoundActive = nil
	ENT.SoundStop = nil

	--Particles
	local Emitter = nil
	ENT.NextSprayEmit = CurTime()
	ENT.NextDustEmit = CurTime()

    function ENT:Initialize()
		self:SetRenderMode(RENDERMODE_WORLDGLOW)
    end

    function ENT:OnRemove()
	    if self.SoundActive then
			self.SoundActive:Stop()
		end

	    if self.SoundStop then
			self.SoundStop:Stop()
		end

		if IsValid(self.Emitter) then self.Emitter:Finish() end
    end

	function ENT:Think()
		if Emitter == nil then Emitter = ParticleEmitter(Vector(0, 0, 0)) end
	end

    function ENT:Draw()
		--If active (or in ground), animate
		if self:GetNetworkedBool("Drill_Active") then
			--Start Sound
			if self.SoundState ~= 1 then
				if not self.SoundActive then
					self.SoundActive = CreateSound(self, SoundPathActive)
				end

				self.SoundActive:Play()
				self.SoundActive:ChangePitch(85)
				self.SoundActive:ChangePitch(100, 1)
				self.SoundState = 1
			end

			--Rotate Drill
			self.CurSpin = self.CurSpin + (135 * FrameTime())
			if self.CurSpin >= 360 then
				self.CurSpin = self.CurSpin - 360
			end

			--Drop Drill
			if self.CurSlide < self.MaxSlide then
				self.CurSlide = self.CurSlide + (5 * FrameTime())
			end

		--Bring Drill Up
		else
			if self.CurSlide > 0 then
				self.CurSlide = self.CurSlide - (10 * FrameTime())

				--Speed up Sound
				if self.SoundState ~= 2 then
					if not self.SoundActive then
						self.SoundActive = CreateSound(self, SoundPathActive)
						self.SoundActive:Play()
					end

					self.SoundActive:ChangePitch(150, 1)
					self.SoundState = 2
				end

				--Reverse Drill
				self.CurSpin = self.CurSpin - (170 * FrameTime())
				if self.CurSpin <= 0 then
					self.CurSpin = self.CurSpin + 360
				end

				--Stop Sound
				if self.CurSlide <= 0 and self.SoundState ~= 0 then
					self.SoundState = 0
					self.SoundActive:Stop()

					--Play Clunk Sound
					if not self.SoundStop then
						self.SoundStop = CreateSound(self, SoundPathStop)
					end

					self.SoundStop:Play()
					self.SoundStop:ChangePitch(75)
				end
			end
		end

		--Do Particles
		local dist = LocalPlayer():GetPos():DistToSqr(self:GetPos())
		local maxdist = GetConVarNumber("mining_drill_rig_particle_dist")
		if self.CurSlide > 15 and dist < maxdist * maxdist then
			--Check Ground
			local tracedata = {}
			tracedata.start = self:GetPos() + self:GetUp()*50
			tracedata.endpos = self:GetPos() + self:GetUp()*-100
			tracedata.filter = self
			local tr = util.TraceLine(tracedata)

			--Ambient Dust Plume
			if CurTime() > self.NextDustEmit then
				local p = Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), tr.HitPos + (tr.HitNormal * 2))
				p:SetDieTime(math.Rand(4, 16))
				p:SetAirResistance(75)
				p:SetVelocity(VectorRand() * math.Rand(0, 50))
				p:SetGravity(Vector(20, 20, 10))
				p:SetStartAlpha(128)
				p:SetEndAlpha(0)
				p:SetStartSize(math.random(0, 2))
				p:SetEndSize(math.random(135, 150))
				p:SetRoll(math.Rand(0, 360))
				p:SetRollDelta(math.Rand(-0.5, 0.5))
				p:SetColor(58, 49, 45)

				self.NextDustEmit = CurTime() + 0.25
			end
		end

	    --Get Model Info
	    local model = self:GetModel()
	    local mat = self:GetMaterial()
	    local pos = self:GetPos()
	    local ang = self:GetAngles()
	    local spinAng = self:LocalToWorldAngles(Angle(0, self.CurSpin, 0))
	    local up = self:GetUp()
	    local col = self:GetColor()


	    --Drill Shaft
	    self:SetAngles(spinAng)
	    self:SetPos(pos + (up * (160-self.CurSlide)))
	    self:SetModel("models/Slyfo/rover_drillshaft.mdl")
	    self:SetModelScale(1, 0)
	    self:SetColor(col)
	    self:SetMaterial(mat)
	    self:DrawModel()

	    --Drill Bit
	    self:SetAngles(spinAng)
	    self:SetPos(pos + (up * (30-self.CurSlide)))
	    self:SetModel("models/Slyfo/rover_drillbit.mdl")
	    self:SetModelScale(1, 0)
	    self:SetColor(col)
	    self:SetMaterial(mat)
	    self:DrawModel()

	    --Platform Rigging
	    self:SetAngles(ang)
	    self:SetPos(pos + (up * 160))
	    self:SetModel("models/Slyfo/drillbase_basic.mdl")
	    self:SetModelScale(0.9, 0)
	    self:SetColor(col)
	    self:SetMaterial(mat)
	    self:DrawModel()

	    --Main Model
	    self:SetAngles(ang)
	    self:SetPos(pos)
	    self:SetModel(model)
	    self:SetModelScale(1, 0)
	    self:SetColor(col)
	    self:SetMaterial(mat)
	    self:DrawModel()
    end


elseif SERVER then
    local OreMin = 15
    local OreMax = 150
    local OreCrystalFieldMult = 0.25
    local PNMin = 0
    local PNMax = 5
    local IceMin = 0
    local IceMax = 40
    local PNCrystalFieldMult = 1
    local EnergyIncrement = 5000
    local CrystalFindRange = 500

    function ENT:Initialize()
	    self.BaseClass.Initialize(self)
	    self.Active = 0
	    self.lastused = 0
	    self.CurSlide = 0
	    self.MaxSlide = 150
	    self.InCrystalField = false
		self.Mult = 1
	    self:SetName("Mining Drill Rig")

	    if WireLib ~= nil then
		    self.WireDebugName = self.PrintName
		    self.Inputs = WireLib.CreateInputs(self, {"On", "Multiplier"})
		    self.Outputs = WireLib.CreateSpecialOutputs(self, {"On", "Energy Usage", "Mined"}, {"NORMAL","NORMAL","TABLE"})
	    end
    end

    function ENT:OnRemove()
	    if self.SoundActive then self.SoundActive:Stop() end
    end

    function ENT:TurnOn()
	    if self.Active == 0 and IsValid(self:GetNode()) then
		    --Check Resources
		    if self:GetNode():GetCapAmount() > EnergyIncrement then
			    --Check Ground
			    local tracedata = {}
			    tracedata.start = self:GetPos() + self:GetUp()*50
			    tracedata.endpos = self:GetPos() + self:GetUp()*-100
			    tracedata.filter = self
			    local Trace = util.TraceLine(tracedata)
			    if string.find(Trace.HitTexture,"displacement") ~= nil then
				    self.Active = 1
				    self:SetNetworkedBool("Drill_Active",true)
				    if WireLib ~= nil then WireLib.TriggerOutput(self, "On", self.Active) end

				    --Prevent movement while drilling
				    local Phys = self:GetPhysicsObject()
				    Phys:EnableMotion(false)
				    Phys:Sleep()

				    --Check for Nearby Crystals
				    local Crystals = ents.FindInSphere(self:GetPos(),CrystalFindRange)
				    for k,v in pairs(Crystals) do
					    if string.find(v:GetClass(), "mining_mineral") then
						    self.InCrystalField = true
						    break
					    else
						    self.InCrystalField = false
					    end
				    end
			    end
		    end
	    end
    end

    function ENT:TurnOff()
	    if self.Active == 1 then
		    self.Active = 0
		    self:SetNetworkedBool("Drill_Active",false)
		    if WireLib ~= nil then WireLib.TriggerOutput(self, "On", self.Active) end
	    end
    end

    function ENT:SetActive(value)
	    if value == nil then return end

	    if value then
		    self.lastused = CurTime()
		    self:TurnOn()
	    elseif not value and (CurTime() - self.lastused) > 2 then
		    self:TurnOff()
	    end
    end

    function ENT:Use()
	    self:SetActive(self.Active == 0)
    end

    function ENT:TriggerInput(input, value)
	    if input == "On" then
		    self:SetActive(value ~= 0)
	    end

		if input == "Multiplier" then
			self.Mult = math.Clamp(value, 1, 10)
		end
    end

    --Do all your tracing and resource returns here.
    function ENT:Drill()
        if not self:ConsumeResource("Energy", EnergyIncrement * self.Mult) then
            self:TurnOff()
            return
        end

        -- Base Mults
        local BaseMult = 1
        local VeldsparMult = 1
        local PNMult = PNCrystalFieldMult
        local IceMult = 1
        local CarbonMult = 0.1
        local UraniumMult = 0.2
        local PoloniumMult = 0.2
        local IronMult = 0.35
        local SNubiumMult = 0.01

        local Environment = 0           --So resources that only spawn in atmospheres can be nullified
        local EnvironmentMult = 1       --Bonus to apply based on atmospheric conditions

        if self.GetEnvironment then
            Environment = 1
            local env = self:GetEnvironment()
            local Temp = env:GetTemperature(self)
            local Oxy = env:GetResource("Oxygen")
            local O2Percent = Oxy and (Oxy:GetAmount() / Oxy:GetMaxAmount()) or 0
            local Water = env:GetResource("Water")
            local WaterPercent = Water and (Water:GetAmount() / Water:GetMaxAmount()) or 0
            local CO2 = env:GetResource("Carbon Dioxide")
            local CO2Percent = CO2 and (CO2:GetAmount() / CO2:GetMaxAmount()) or 0

            EnvironmentMult = math.Clamp(math.max((Temp - 308) / 164, 0) + math.max((283 - Temp) / 94, 0) + math.max((5 - O2Percent*100) / 1.6), 1, 3)
            VeldsparMult = VeldsparMult * ((O2Percent < 0.01 and WaterPercent < 0.25) and 1 or 0)
            IceMult = IceMult * WaterPercent * (Temp < 283 and 1 or 0)
            CarbonMult = CarbonMult * ((Temp > 283 and Temp < 308 and O2Percent > 0.05) and 1 or 0)
            PoloniumMult = PoloniumMult * ((Temp > 308 and CO2Percent > 0.5) and 1 or 0)
            SNubiumMult = SNubiumMult * ((Temp < 100 and O2Percent < 0.05) and 1 or 0)
        end

        OreMult = BaseMult * (self.InCrystalField and OreCrystalFieldMult or 1) * EnvironmentMult * self.Mult * math.Rand(OreMin, OreMax)
        PNMult = PNMult * (self.InCrystalField and PNCrystalFieldMult or 0) * EnvironmentMult * self.Mult * (PNMult and math.Rand(PNMin, PNMax) or 1) --idk how expensive Rand is
        IceMult = IceMult * EnvironmentMult * self.Mult * (IceMult and math.Rand(IceMin, IceMax) or 1)
        UraniumMult = UraniumMult * OreMult
        IronMult = IronMult * OreMult
        VeldsparMult = VeldsparMult * OreMult * Environment
        CarbonMult = CarbonMult * OreMult * Environment
        PoloniumMult = PoloniumMult * OreMult * Environment
        SNubiumMult = SNubiumMult * OreMult * Environment

        if VeldsparMult > 1 then self:SupplyResource("Veldspar", math.floor(VeldsparMult)) end
        if PNMult > 1 then self:SupplyResource("Polonium Nitrate", math.floor(PNMult)) end
        if IceMult > 1 then self:SupplyResource("Ice", math.floor(IceMult)) end
        if UraniumMult > 1 then self:SupplyResource("Uranium", math.floor(UraniumMult)) end
        if IronMult > 1 then self:SupplyResource("Iron", math.floor(IronMult)) end
        if CarbonMult > 1 then self:SupplyResource("Carbon", math.floor(CarbonMult)) end
        if PoloniumMult > 1 then self:SupplyResource("Polonium", math.floor(PoloniumMult)) end
        if SNubiumMult > 1 then self:SupplyResource("Solid Nubium", math.floor(SNubiumMult)) end

		-- FIXME: This needs to use the correct wire table format...
        --WireLib.TriggerOutput(self, "Mined", {Ore=OreGive, PN=PNGive, Ice=IceGive})
        if WireLib then
            WireLib.TriggerOutput(self, "Energy Usage", EnergyIncrement * self.Mult)
        end
    end

    function ENT:Think()
	    if self.Active == 1 and IsValid(self:GetNode()) then
            local Core = self:GetNode()
            local Gyropod = Core:GetGyropod()
            if IsValid(Gyropod) and Gyropod.On then
                self:TurnOff()
                return
            end

		    self:Drill()

		    --Prevent movement while drilling
		    local Phys = self:GetPhysicsObject()
		    Phys:EnableMotion(false)
		    Phys:Sleep()
	    end

	    self:NextThink(CurTime() + 1)
	    return true
    end

    duplicator.RegisterEntityClass("mining_drill_rig", GAMEMODE.MakeEnt, "Data")
end