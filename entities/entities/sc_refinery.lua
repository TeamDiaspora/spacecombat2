AddCSLuaFile()
ENT.Type = "anim"
ENT.Base = "base_moduleentity"
ENT.PrintName = "Refinery"
ENT.Author = "Lt.Brandon"
ENT.Purpose = "Melts down your soul and refines it into diamonds."
ENT.Instructions = "Throw rocks at door and have a rave."
ENT.Category = "Space Combat 2"
ENT.Spawnable = false
ENT.AdminSpawnable = false

local base = scripted_ents.Get("base_moduleentity")
local function InitGenerators()
    base = scripted_ents.Get("base_moduleentity")
    do
        local Generator = GAMEMODE:NewGeneratorInfo()
        Generator:SetName("Refinery")
        Generator:SetClass("sc_refinery")
        Generator:SetDescription("A giant, noisy rock tumbler that refines resources into more useful versions of what you already have.")
        Generator:SetCategory("Manufacturing")
        Generator:SetDefaultModel("models/slyfo/refinery_small.mdl")
        GAMEMODE:RegisterGenerator(Generator)
    end

    do
        local Generator = GAMEMODE:NewGeneratorInfo()
        Generator:SetName("Small Refinery")
        Generator:SetClass("sc_refinery")
        Generator:SetDescription("A giant, noisy rock tumbler that refines resources into more useful versions of what you already have.")
        Generator:SetCategory("Manufacturing")
        Generator:SetDefaultModel("models/smallbridge/life support/sbclimatereg.mdl")
        GAMEMODE:RegisterGenerator(Generator)
    end

    do
        local Generator = GAMEMODE:NewGeneratorInfo()
        Generator:SetName("Factory")
        Generator:SetClass("sc_refinery")
        Generator:SetDescription("A station sized industrial laser cutter complete with robot arms and tools for building everything you could ever want.")
        Generator:SetCategory("Manufacturing")
        Generator:SetDefaultModel("models/slyfo/refinery_large.mdl")
        GAMEMODE:RegisterGenerator(Generator)
    end

    do
        local Generator = GAMEMODE:NewGeneratorInfo()
        Generator:SetName("Small Factory")
        Generator:SetClass("sc_refinery")
        Generator:SetDescription(
        "A ship sized industrial laser cutter that works incredibly slowly, but hey, at least it's small!")
        Generator:SetCategory("Manufacturing")
        Generator:SetDefaultModel("models/cerus/modbridge/misc/accessories/acc_furnace1.mdl")
        GAMEMODE:RegisterGenerator(Generator)
    end


    do
        local Generator = GAMEMODE:NewGeneratorInfo()
        Generator:SetName("Reprocessor")
        Generator:SetClass("sc_refinery")
        Generator:SetDescription("The biggest car crusher you've ever seen, it even has a furnace to melt down parts into raw resources!")
        Generator:SetCategory("Manufacturing")
        Generator:SetDefaultModel("models/slyfo/crate_resource_large.mdl")
        GAMEMODE:RegisterGenerator(Generator)
    end

    do
        local Generator = GAMEMODE:NewGeneratorInfo()
        Generator:SetName("Small Reprocessor")
        Generator:SetClass("sc_refinery")
        Generator:SetDescription("The biggest car crusher you've ever seen, it even has a furnace to melt down parts into raw resources!")
        Generator:SetCategory("Manufacturing")
        Generator:SetDefaultModel("models/spacebuild/nova/toesmall.mdl")
        GAMEMODE:RegisterGenerator(Generator)
    end

    do
        local Generator = GAMEMODE:NewGeneratorInfo()
        Generator:SetName("Farm")
        Generator:SetClass("sc_refinery")
        Generator:SetDescription("It's a farm!")
        Generator:SetCategory("Manufacturing")
        Generator:SetDefaultModel("models/props/cs_office/plant01.mdl")
        GAMEMODE:RegisterGenerator(Generator)
    end
end

local function OnReloaded()
    InitGenerators()
end

hook.Remove("InitPostEntity", "sc_refinery_post_entity_init")
hook.Add("InitPostEntity", "sc_refinery_post_entity_init", InitGenerators)
hook.Remove("OnReloaded", "sc_refinery_post_reloaded")
hook.Add("OnReloaded", "sc_refinery_post_reloaded", OnReloaded)

function ENT:SetupGenerator(Generator)
    self:SetupType(Generator:GetName())
end

ENT.Types = {
    ["Refinery"] = {
        PG = 16000,
        CPU = 6000,
        Slot = "Industrial",
        Status = "Offline",
        CanRun = false,
        Classes = {}
    },
    ["Reprocessor"] = {
        PG = 16000,
        CPU = 8000,
        Slot = "Industrial",
        Status = "Offline",
        CanRun = false,
        Classes = {}
    },
    ["Factory"] = {
        PG = 30000,
        CPU = 20000,
        Slot = "Industrial",
        Status = "Offline",
        CanRun = false,
        Classes = {}
    },
    ["Small Refinery"] = {
        PG = 8000,
        CPU = 800,
        Slot = "Industrial",
        Status = "Offline",
        CanRun = false,
        Classes = {
            "Frigate",
            "Cruiser",
            "Battlecruiser"
        }
    },
    ["Small Reprocessor"] = {
        PG = 8000,
        CPU = 500,
        Slot = "Industrial",
        Status = "Offline",
        CanRun = false,
        Classes = {
            "Frigate",
            "Cruiser",
            "Battlecruiser"
        }
    },
    ["Small Factory"] = {
        PG = 20000,
        CPU = 1500,
        Slot = "Industrial",
        Status = "Offline",
        CanRun = false,
        Classes = {
            "Frigate",
            "Cruiser",
            "Battlecruiser"
        }
    },
    ["Farm"] = {
        PG = 6000,
        CPU = 500,
        Slot = "Industrial",
        Status = "Offline",
        CanRun = false,
        Classes = {}
    }
}

function ENT:SharedInit()
    base.SharedInit(self)
    SC.NWAccessors.CreateNWAccessor(self, "Type", "cachedstring", "")
    SC.NWAccessors.CreateNWAccessor(self, "Recipe", "cachedstring", "")
    SC.NWAccessors.CreateNWAccessor(self, "NextRecipe", "cachedstring", "")
    SC.NWAccessors.CreateNWAccessor(self, "TimeMultiplier", "number", 1)
end

if CLIENT then
    function ENT:GetActivationCostText()
        local type = self:GetType()
        local recipe = self:GetRecipe()
        local tbl = SC.Manufacturing.GetRecipe(recipe, type)

        local text = "\nRecipe: " .. recipe
        return text .. "\n"
    end

    function ENT:SendUpdate(active, recipe)
        if not IsValid(self) or (active == nil) or (recipe == nil) then return end
        net.Start("sc_refinery_ui")
        net.WriteEntity(self)
        net.WriteString(recipe)
        net.WriteBit(active)
        net.SendToServer()
    end

    local function UpdateText(ent, panel, name, recipe)
        if not IsValid(panel) or (recipe == nil) or (name == nil) then return end
        local text = "Recipe: " .. name .. "\nTime Required: " .. sc_ds(recipe.time * ent:GetTimeMultiplier()) .. "\n\nResources Needed: "

        for i, k in pairs(recipe.requires) do
            text = text .. "\n    " .. sc_ds(k) .. " units of " .. i
        end

        text = text .. "\n\nResources Produced:"

        for i, k in pairs(recipe.produces) do
            text = text .. "\n    " .. sc_ds(k) .. " units of " .. i
        end

        panel:SetText(text)
        panel:SizeToContents()
        panel:SetPos(30, 30)
    end

    net.Receive("sc_refinery_ui", function()
        local self = net.ReadEntity()
        if not IsValid(self) then return end
        local RealType = string.Replace(self:GetType(), "Small ", "")
        local recipes = SC.Manufacturing.GetRecipes(RealType)
        local current = SC.Manufacturing.GetRecipe(self:GetNextRecipe(), RealType)
        local currentname = self:GetNextRecipe()
        local InfoPanel = vgui.Create("DFrame")
        InfoPanel:SetPos(100, 100)
        InfoPanel:SetSize(680, 420)
        InfoPanel:SetTitle("Manufacturing Control Panel")
        InfoPanel:MakePopup()

        local StatPan = vgui.Create("DPanel", InfoPanel)
        StatPan:SetSize(300, 0)
        StatPan:Dock(RIGHT)

        local RecipeInfoLabel = vgui.Create("DLabel", StatPan)
        RecipeInfoLabel:SetPos(5, 5)
        RecipeInfoLabel:SetText("Recipe Information")
        RecipeInfoLabel:SizeToContents()

        local RecipeInfoLabel2 = vgui.Create("DLabel", StatPan)
        RecipeInfoLabel2:SetPos(30, 30)
        UpdateText(self, RecipeInfoLabel2, currentname, current)

        local RecipeList = vgui.Create("DListView", InfoPanel)
        RecipeList:Clear()
        RecipeList:SetMultiSelect(false)
        RecipeList:AddColumn("Recipe Name")
        RecipeList:AddColumn("Time Required")

        local listLines = {}
        for i, k in pairs(recipes) do
            listLines[i] = RecipeList:AddLine(i, sc_ds(k.time * self:GetTimeMultiplier()))
        end

        RecipeList:SelectItem(listLines[self:GetNextRecipe()])

        RecipeList.OnRowSelected = function(panel, line)
            local row = panel:GetLine(line)
            local recipe = string.Trim(row:GetValue(1))
            if not IsValid(self) or not SC.Manufacturing.IsValidRecipe(recipe, RealType) then return end
            current = SC.Manufacturing.GetRecipe(recipe, RealType)
            currentname = recipe
            UpdateText(self, RecipeInfoLabel2, currentname, current)
        end

        RecipeList:Dock(FILL)
        local StartProd = vgui.Create("DButton", InfoPanel)
        StartProd:SetText("Start Production")

        StartProd.DoClick = function()
            self:SendUpdate(true, currentname)
        end

        StartProd:Dock(BOTTOM)
        local StopProd = vgui.Create("DButton", InfoPanel)
        StopProd:SetText("Stop Production")

        StopProd.DoClick = function()
            self:SendUpdate(false, "")
        end

        StopProd:Dock(BOTTOM)
    end)
elseif SERVER then
    function ENT:SetupType(type)
        if self.Types[type] == nil then return end

        self:SetType(type)

        local DefaultRecipe
        if string.StartsWith(type, "Small ") then
            self:SetTimeMultiplier(3)

            local RealType = string.Replace(self:GetType(), "Small ", "")
            DefaultRecipe = SC.Manufacturing.GetDefaultRecipe(RealType)
        else
            self:SetTimeMultiplier(1)

            DefaultRecipe = SC.Manufacturing.GetDefaultRecipe(type)
        end

        self:ChangeRecipe(DefaultRecipe)
        self:SetFitting(table.Copy(self.Types[type]))

        if IsValid(self:GetNode()) then
            self:GetNode():UpdateModifiers()
        end

        self:SetModuleName(type)

        timer.Simple(0.1, function()
            if IsValid(self) then
                SC.NWAccessors.SyncAccessors(self)
            end
        end)
    end

    function ENT:SaveSCInfo()
        return {
            type = self:GetType(),
            recipe = self:GetNextRecipe()
        }
    end

    function ENT:LoadSCInfo(Info)
        if not Info.type then
            self:SetupType("Refinery")

            return
        end

        self:SetupType(Info.type)
        if not Info.recipe then return end
        self:ChangeRecipe(Info.recipe)
    end

    function ENT:Use(ply)
        net.Start("sc_refinery_ui")
        net.WriteEntity(self)
        net.Send(ply)
    end

    net.Receive("sc_refinery_ui", function()
        local self = net.ReadEntity()
        if not IsValid(self) then return end
        self:ChangeRecipe(net.ReadString())
        self:TriggerInput("On", net.ReadBit())
    end)

    function ENT:ChangeRecipe(RecipeName)
        local RealType = string.Replace(self:GetType(), "Small ", "")
        if not SC.Manufacturing.IsValidRecipe(RecipeName, RealType) then return false end
        self:SetNextRecipe(RecipeName)

        return true
    end

    function ENT:UpdateRecipeForCycle()
        if self:GetRecipe() ~= self:GetNextRecipe() then
            self:SetRecipe(self:GetNextRecipe())

            local RealType = string.Replace(self:GetType(), "Small ", "")
            local RecipeInfo = SC.Manufacturing.GetRecipe(self:GetRecipe(), RealType)
            self:SetCycleActivationCost(RecipeInfo.requires)
            self:SetCycleDuration(RecipeInfo.time * self:GetTimeMultiplier())
        end
    end

    function ENT:PreStartCycle()
        self:UpdateRecipeForCycle()
    end

    function ENT:OnCycleFinished()
        local RealType = string.Replace(self:GetType(), "Small ", "")
        local RecipeInfo = SC.Manufacturing.GetRecipe(self:GetRecipe(), RealType)
        self:SupplyResources(RecipeInfo.produces)
        self:UpdateRecipeForCycle()
    end

    duplicator.RegisterEntityClass("sc_refinery", GAMEMODE.MakeEnt, "Data")
end