AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_scentity"
ENT.PrintName	= "Vehicle Suspension"
ENT.Author	= "Lt.Brandon"

ENT.Spawnable	= true
ENT.AdminSpawnable = false
ENT.IsDriveLineComponent = true
ENT.IsMechanical = true
ENT.IsSuspension = true
ENT.SC_HasGenericPodLink = true

-- Suspension
ENT.SuspensionLength = 17.5
ENT.SpringWeightRating = 2200
ENT.Stiffness = 10--8.85
ENT.Damping = 1.8--2.45
ENT.Multiplier = 1
ENT.SpringPosition = 0
ENT.PreviousSpringPosition = 0
ENT.PreviousCompresison = 0
ENT.SuspensionForward = Vector(1, 0, 0)
ENT.SuspensionRight = Vector(0, 1, 0)
ENT.Compression = 0

-- Wheel
ENT.WheelModel = "models/sprops/trans/wheel_c/wheel45.mdl"--"models/sprops/trans/wheel_c/wheel45.mdl"
ENT.WheelScale = Vector(1, 1, 1)
ENT.WheelOffset = Vector(0, 0, 0)
ENT.WheelRadius = 22.5
ENT.WheelWidth = 8
ENT.WheelWeight = 25
ENT.WheelSlideFrictionCoefficient = 0.5
ENT.WheelRollingResistanceCoefficient = 0.375
ENT.WheelRPM = 0
ENT.MaxSidewaysForce = 2.5
ENT.MinCompressionGrip = 0.95
ENT.MaxCompressionGrip = 1.15
ENT.VehicleWeight = 1500
ENT.NumberOfWheels = 4
ENT.WheelInertia = 0
ENT.PeakTraction = 1.05
ENT.PeakSlipRatio = 6
ENT.SlipFalloff = 0.6
ENT.SlipFalloffRatio = 20

-- Steering
ENT.HasSteering = true
ENT.SteeringAngle = 35
ENT.SteeringFrictionMultiplier = 1.2
ENT.SteeringSensitivity = 1
ENT.CurrentSteeringAngle = 0
ENT.SteeringInput = 0

-- Brakes
ENT.HasBrakes = true
ENT.BrakeTorque = 1600 / 0.14
ENT.BrakePressure = 0

ENT.DriveTorque = 0
ENT.DriveRPM = 0
ENT.DriveEnergy = 0
ENT.AppliedDriveEnergy = 0

function ENT:CreateHologram(model, pos, ang, col, par, scale, relativetoself)
    if CLIENT then
        local h = ClientsideModel(model, (col and col.a or 255) == 255 and RENDERMODE_NORMAL or RENDERMODE_TRANSALPHA)
        h:SetPos((relativetoself and self or par):LocalToWorld(pos))
        h:SetAngles((relativetoself and self or par):LocalToWorldAngles(ang))
        h:SetModelScale(scale or Vector(1, 1, 1))
        h:SetColor(col or Color(255, 255, 255, 255))
        --h:SetParent(par)
        local Min, Max = h:GetModelRenderBounds()
        h:SetRenderBounds(Min, Max, Vector(100, 100, 100))

        table.insert(self.Holograms, h)

        return h
    else
        local h = ents.Create("info_target")
        h:SetPos((relativetoself and self or par):LocalToWorld(pos))
        h:SetAngles((relativetoself and self or par):LocalToWorldAngles(ang))
        h:SetParent(par)

        function h:UpdateTransmitState()
	        return TRANSMIT_NEVER
        end

        h:Spawn()

        table.insert(self.Holograms, h)

        return h
    end
end

function ENT:SetupWheel()
    self:CleanupHolograms()

    self.WheelInertia = self.WheelWeight * math.pow(self.WheelRadius, 2) / 2
    self.Wheel = self:CreateHologram(self.WheelModel, Vector(0, 0, 0), Angle(0, 0, 0), self.HoloColor, self, self.WheelScale, true)
end

function ENT:CleanupHolograms()
    for i,k in pairs(self.Holograms) do
        if IsValid(k) then
            k:Remove()
        end
    end

    self.Holograms = {}
end

function ENT:Initialize()
    self:SetModel("models/maxofs2d/cube_tool.mdl")

    self.BaseClass.Initialize(self)

    self.Holograms = {}
    self.NextMatCheck = 0
    self.HoloColor = self:GetColor()
    self.HoloMaterial = self:GetMaterial()

    self:SetupWheel()

    self:CallOnRemove("Cleanup Holograms", function()
        hook.Remove("PlayerButtonDown", "SuspensionKeyPress_"..self:EntIndex())
        hook.Remove("PlayerButtonUp", "SuspensionKeyPress_"..self:EntIndex())
        self:CleanupHolograms()
    end)

    if CLIENT then
        net.Start("SC.SendSuspensionSetup")
        net.WriteEntity(self)
        net.SendToServer()
        return
    end

    if WireLib then
        self.Inputs = WireLib.CreateSpecialInputs(self, {"Steering", "Brake"}, {"NORMAL", "NORMAL"})
        self.Outputs = WireLib.CreateOutputs(self, {"DEBUG [STRING]"})
    end

    -- Input hooks
    self.KeysDown = {}
    hook.Add("PlayerButtonDown", "SuspensionKeyPress_"..self:EntIndex(), function(ply, key)
        if not IsValid(self) or not self:IsLinkedToPod() then return end
        if ply:GetVehicle() == self:GetLinkedPod() then
            self.KeysDown[key] = true

            self:OnKeyDown(ply, key)
        end
    end)

    hook.Add("PlayerButtonUp", "SuspensionKeyPress_"..self:EntIndex(), function(ply, key)
        if not IsValid(self) or not self:IsLinkedToPod() then return end
        if ply:GetVehicle() == self:GetLinkedPod() then
            self.KeysDown[key] = nil

            self:OnKeyUp(ply, key)
        end
    end)
end

function ENT:OnRemove()
    self.BaseClass.OnRemove(self)
    hook.Remove("PlayerButtonDown", "SuspensionKeyPress_"..self:EntIndex())
    hook.Remove("PlayerButtonUp", "SuspensionKeyPress_"..self:EntIndex())

    self:CleanupHolograms()
end

function ENT:IsLinkedToPod()
    return IsValid(self:GetLinkedPod())
end

if SERVER then
    function ENT:OnKeyDown(Ply, Key)
        if Key == Ply.MechanicalKeys["Brakes"] then
            self.BrakePressure = 1
        elseif Key == Ply.MechanicalKeys["TurnLeft"] then
            if self.KeysDown[Ply.MechanicalKeys["TurnRight"]] then
                self.SteeringInput = 0
            else
                self.SteeringInput = 1
            end
        elseif Key == Ply.MechanicalKeys["TurnRight"] then
            if self.KeysDown[Ply.MechanicalKeys["TurnLeft"]] then
                self.SteeringInput = 0
            else
                self.SteeringInput = -1
            end
        end
    end

    function ENT:OnKeyUp(Ply, Key)
        if Key == Ply.MechanicalKeys["Brakes"] then
            self.BrakePressure = 0
        elseif Key == Ply.MechanicalKeys["TurnLeft"] then
            if self.KeysDown[Ply.MechanicalKeys["TurnRight"]] then
                self.SteeringInput = -1
            else
                self.SteeringInput = 0
            end
        elseif Key == Ply.MechanicalKeys["TurnRight"] then
            if self.KeysDown[Ply.MechanicalKeys["TurnLeft"]] then
                self.SteeringInput = 1
            else
                self.SteeringInput = 0
            end
        end
    end

    util.AddNetworkString("SC.SendSuspensionSetup")
    function ENT:WriteCreationPacket(Len, Ply)
        net.Start("SC.SendSuspensionSetup")
        net.WriteEntity(self)
        net.WriteBool(self.HasSteering)
        net.WriteFloat(self.SuspensionLength)
        net.WriteFloat(self.WheelRadius)
        net.WriteFloat(self.WheelWeight)
        net.WriteVector(self.WheelScale)
        net.WriteString(self.WheelModel)

        if IsValid(Ply) then
            net.Send(Ply)
        else
            net.SendPVS(self:GetPos())
        end
    end

    function ENT:UnlinkPod()
        if self:IsLinkedToPod() then
            self:SetLinkedPod(NULL)
        end
    end

    function ENT:LinkPod(NewPod)
        if self:IsLinkedToPod() then
            self:UnlinkPod()
        end

        if IsValid(NewPod) and NewPod:IsVehicle() then
            self:SetLinkedPod(NewPod)
            return true
        else
            return false
        end
    end
end

function ENT:ReadCreationPacket()
    if SERVER then return end
    self.HasSteering = net.ReadBool()
    self.SuspensionLength = net.ReadFloat()
    self.WheelRadius = net.ReadFloat()
    self.WheelWeight = net.ReadFloat()
    self.WheelScale = net.ReadVector()
    self.WheelModel = net.ReadString()

    self:SetupWheel()
end

net.Receive("SC.SendSuspensionSetup", function(Ply)
    local self = net.ReadEntity()
    if IsValid(self) then
        if SERVER then
            self:WriteCreationPacket(Ply)
        else
            self:ReadCreationPacket()
        end
    end
end)

if CLIENT then
    hook.Add("NotifyShouldTransmit", "sc_suspension_notifyshouldtransmit", function(Ent, ShouldTransmit)
        if not IsValid(Ent) or not Ent.IsSuspension then return end
        if ShouldTransmit then
            net.Start("SC.SendSuspensionSetup")
            net.WriteEntity(Ent)
            net.SendToServer()

            Ent.HoloColor = Ent:GetColor()
            Ent:SetupWheel()
        else
            Ent:CleanupHolograms()
        end
    end)

    function ENT:CheckMaterials()
        if self.NextMatCheck < CurTime() then
            if self:GetMaterial() ~= self.HoloMaterial then
                self.HoloMaterial = self:GetMaterial()
                for _,v in pairs(self.Holograms) do
                    if IsValid(v) then
                        local al = v:GetColor().a
                        if al == 255 then v:SetMaterial(self.HoloMaterial) end
                    end
                end
            end
            local CC, CH = self:GetColor(), self.HoloColor
            if CH and (CC.r ~= CH.r or CC.g ~= CH.g or CC.b ~= CH.b) then
                self.HoloColor = Color(CC.r,CC.g,CC.b)
                local AColor = Color(CC.r,CC.g,CC.b,255)
                for _,v in pairs(self.Holograms) do
                    if IsValid(v) then
                        local al = v:GetColor().a
                        if al == 255 then v:SetColor(AColor) end
                    end
                end
            end

            self.NextMatCheck = CurTime() + 1
        end
    end
end

function ENT:SharedInit()
    self.BaseClass.SharedInit(self)

    self.WheelRotation = 0

    SC.NWAccessors.CreateNWAccessor(self, "WheelRPM", "int", 0)
    SC.NWAccessors.CreateNWAccessor(self, "CurrentSteeringAngle", "number", 0)
    SC.NWAccessors.CreateNWAccessor(self, "LinkedPod", "entity", NULL)
end

function ENT:OnReloaded()
    self:SetupWheel()
end

function ENT:ApplySuspensionForce(Parent, TraceStart, TraceEnd, TraceResult)
    local Phys = Parent:GetPhysicsObject()
    if TraceResult.Hit then
        local Compression = 1 - TraceResult.Fraction
        self.Compression = Compression

        self.SpringPosition = (TraceStart - TraceResult.HitPos):Length()
        local DamperVelocity = (self.SpringPosition - self.PreviousSpringPosition) / FrameTime()
        self.PreviousSpringPosition = self.SpringPosition

        if SERVER and IsValid(Phys) then
            local SpringForce = (Compression * self.Stiffness * 100) - (self.Damping * DamperVelocity)
            Phys:ApplyForceOffset(TraceResult.HitNormal * SpringForce * self.SpringWeightRating * self.Multiplier * FrameTime(), self:GetPos())
        end
    else
        self.SpringPosition = self.SuspensionLength + self.WheelRadius
		self.PreviousPosition = self.SpringPosition
		self.Compression = 0.0
    end

    -- Apply gravity forces on the wheel
    if SERVER and IsValid(Phys) then
        Phys:ApplyForceOffset(-vector_up * (9.81 * self.WheelWeight) * Parent:GetGravity(), self.Wheel:GetPos())
    end
end

function ENT:UpdateSteering(Parent, TraceStart, TraceEnd, TraceResult)
    if self.HasSteering then
        if SERVER then
            local OldSteeringAngle = self:GetCurrentSteeringAngle()
            self:SetCurrentSteeringAngle(math.Clamp(Lerp(self.SteeringSensitivity * FrameTime(), OldSteeringAngle, self.SteeringInput * self.SteeringAngle), -self.SteeringAngle, self.SteeringAngle))
        end

        local ForwardAngle = self:GetForward():AngleEx(self:GetUp())
        ForwardAngle:RotateAroundAxis(self:GetUp(), self:GetCurrentSteeringAngle())

        self.SuspensionForward = ForwardAngle:Forward()
        self.SuspensionRight = ForwardAngle:Right()
    else
        self.SuspensionForward = self:GetForward()
        self.SuspensionRight = self:GetRight()
    end
end

function ENT:CalculateSidewaysForce(Parent, SurfaceFriction, CompressionGripRatio)
    local Phys = Parent:GetPhysicsObject()
    local Velocity = Phys:GetVelocity() * 2.54

    -- Velocity in cm/s
    local VelocityAtSuspension = self:GetPhysicsObject():GetVelocityAtPoint(self:LocalToWorld(-Vector(0, 0, self.SpringPosition + self.WheelRadius))) * 2.54
    local MaxSidewaysForce = self.MaxSidewaysForce * 1000
    local Dot = VelocityAtSuspension:Dot(self.SuspensionRight)
    local SidewaysForce = Dot * self.WheelSlideFrictionCoefficient * CompressionGripRatio * SurfaceFriction * (self.VehicleWeight / self.NumberOfWheels / self.WheelRadius)
    SidewaysForce = math.Clamp(SidewaysForce, -MaxSidewaysForce, MaxSidewaysForce)

    if self.HasSteering then
        SidewaysForce = SidewaysForce * Lerp(math.abs(self:GetCurrentSteeringAngle() / self.SteeringAngle), 1, self.SteeringFrictionMultiplier)
    end

    return SidewaysForce
end

function ENT:CalculateBrakeTorque(CurrentWheelInertia)
    if not self.HasBrakes then
        return 0
    end

    if self.BrakePressure > 0 then
        local BrakeDirection = self.WheelRPM > 0 and -1 or 1
        return math.Clamp(BrakeDirection * self.BrakeTorque * self.BrakePressure, -CurrentWheelInertia, CurrentWheelInertia)
    end

    return 0
end

function ENT:CalculateForwardForce(Parent, RollingFriction, SurfaceFriction, CompressionGripRatio)
    local ParentPhys = self:GetPhysicsObject()

    local TotalTorque = 0
    if math.abs(self.DriveRPM) > 0 then
        local RPMDiff = math.Clamp((self.DriveRPM - self.WheelRPM) / 2.5, -1, 1)
        TotalTorque = self.DriveTorque * RPMDiff
    end

    if self.Compression > 0.0001 then
        local CarVelocity = ParentPhys:WorldToLocalVector(ParentPhys:GetVelocityAtPoint(self:LocalToWorld(-Vector(0, 0, self.SpringPosition + self.WheelRadius))))
        if math.abs(TotalTorque) > 0.5 then
            self.WheelRPM = ((self.WheelRPM * 5) + ((CarVelocity.x / self.WheelRadius) * 9.55)) / 6
        else
            self.WheelRPM = (self.WheelRPM + ((CarVelocity.x / self.WheelRadius) * 9.55)) / 2
        end
    elseif math.abs(TotalTorque) < 0.5 then
        -- lazy af
        self.WheelRPM = Lerp(0.01, self.WheelRPM, 0)
    end

    local CurrentWheelInertia = math.abs(self.WheelInertia * self.WheelRPM)
    local BrakeTorque = self:CalculateBrakeTorque(CurrentWheelInertia)
    TotalTorque = TotalTorque + BrakeTorque

    local WheelLoad = TotalTorque / self.WheelRadius
    local WheelAngularVelocity = self.WheelRPM / 9.55

    WheelAngularVelocity = WheelAngularVelocity + (TotalTorque / self.WheelInertia)

    local ForwardForce
    if self.Compression > 0.0001 then
        if math.abs(self.DriveRPM) > 0 then
            local CarVelocity = ParentPhys:WorldToLocalVector(ParentPhys:GetVelocity())
            local SlipRatio = math.Clamp(((WheelAngularVelocity * self.WheelRadius) - CarVelocity.x) / CarVelocity.x, -self.SlipFalloffRatio, self.SlipFalloffRatio)
            ForwardForce = (math.Clamp(SlipRatio, -self.PeakSlipRatio, self.PeakSlipRatio) / self.PeakSlipRatio) * self.PeakTraction

            if math.abs(SlipRatio) > self.PeakSlipRatio then
                ForwardForce = ForwardForce * (1 - (math.abs(SlipRatio / self.SlipFalloffRatio) * (1 - self.SlipFalloff)))
            end
        else
            ForwardForce = self.WheelRPM > 0 and 1 or -1
        end

    else
        ForwardForce = 0
    end

    local FrictionForce = RollingFriction * SurfaceFriction * CompressionGripRatio
    local FrictionTorque = FrictionForce * self.WheelRadius * self.WheelRPM > 0 and 1 or -1

    ForwardForce = ForwardForce * (FrictionForce + math.abs(WheelLoad))

    WheelAngularVelocity = WheelAngularVelocity + (((ForwardForce / self.WheelRadius)) / self.WheelInertia)

    self.WheelRPM = WheelAngularVelocity * 9.55

    return WheelLoad
end

function ENT:ApplyFrictionForces(Parent, TraceStart, TraceEnd, TraceResult)
    local Phys = Parent:GetPhysicsObject()
    if IsValid(Phys) then
        -- Get info about the current surface
        local SurfaceFriction
        if self.Compression > 0.0001 then
            local SurfaceData = util.GetSurfaceData(TraceResult.SurfaceProps)
            if SurfaceData then
                SurfaceFriction = math.Clamp(SurfaceData.friction, 0.9, 1.2)
            else
                SurfaceFriction = 1.0
            end
        else
            SurfaceFriction = 0
        end

        -- Calculate the rolling friction
        local RollingFriction = self.WheelRollingResistanceCoefficient * self.VehicleWeight / self.NumberOfWheels / self.WheelRadius
        local CompressionGripRatio = Lerp(1 - TraceResult.Fraction, self.MinCompressionGrip, self.MaxCompressionGrip)

        -- Calculate wheel forces
        local SidewaysForce = self:CalculateSidewaysForce(Parent, SurfaceFriction, CompressionGripRatio)
        local ForwardForce = self:CalculateForwardForce(Parent, RollingFriction, SurfaceFriction, CompressionGripRatio)

        if self.Compression < 0.0001 then
            return
        end

        -- Apply the wheel forces
        local SurfaceForward, SurfaceRight = self.SuspensionForward, -self.SuspensionRight--MakeFromXZ(self.SuspensionForward, TraceResult.HitNormal) --self.SuspensionForward, self.SuspensionRight
        local RightForce = SurfaceRight * SidewaysForce
        local ForwardForce = SurfaceForward * ForwardForce
        Phys:ApplyForceOffset((ForwardForce + RightForce) * (self.VehicleWeight / self.NumberOfWheels) * FrameTime(), TraceResult.HitPos)
    end
end

function ENT:UpdateWheelPositionAndRotation(Parent, TraceStart, TraceEnd, TraceResult)
    if not IsValid(self.Wheel) then return end
    local NewPos = self:LocalToWorld(-Vector(0, 0, self.SpringPosition - self.WheelRadius))
    self.Wheel:SetPos(NewPos)

    self.WheelRotation = (self.WheelRotation + self.WheelRPM * 6 * FrameTime()) % 360
    local NewAngle = self.SuspensionForward:AngleEx(self:GetUp())
    NewAngle:RotateAroundAxis(self.SuspensionRight, 360 - self.WheelRotation)
    self.Wheel:SetAngles(NewAngle)

    if SERVER then
        self:SetWheelRPM(math.floor(self.WheelRPM))
    end
end

function ENT:Think()
    local Parent = self:GetParent()
    if not IsValid(Parent) then
        Parent = self
    end

    -- Find out where the ground is
    local TraceStart = self:GetPos()
    local TraceEnd = TraceStart + (-self:GetUp() * (self.SuspensionLength + self.WheelRadius))
    local TraceData = {
        start = TraceStart,
        endpos = TraceEnd,
        filter = {self, Parent}
    }

    local TraceResult = util.TraceLine(TraceData)

    -- This shouldn't normally happen, but the wiki says it can
    if not TraceResult then
        return
    end

    -- Update the steering
    self:UpdateSteering(Parent, TraceStart, TraceEnd, TraceResult)

    if CLIENT then
        self.WheelRPM = self:GetWheelRPM()

        -- Update suspension position for client
        self:ApplySuspensionForce(Parent, TraceStart, TraceEnd, TraceResult)

        self:CheckMaterials()
    else
        if self:GetPhysicsObject():IsAsleep() then
            -- Hack to force updates even if physics object is sleeping
            self:PhysicsUpdate()
        end
    end

    -- Update wheel visuals
    self:UpdateWheelPositionAndRotation(Parent, TraceStart, TraceEnd, TraceResult)

    self:NextThink(CurTime())
    return true
end

function ENT:PhysicsUpdate()
    local Parent = self:GetParent()
    if not IsValid(Parent) then
        return
    end

    -- Find out where the ground is
    local TraceStart = self:GetPos()
    local TraceEnd = TraceStart + (-self:GetUp() * (self.SuspensionLength + self.WheelRadius))
    local TraceData = {
        start = TraceStart,
        endpos = TraceEnd,
        filter = {self, Parent}
    }

    local TraceResult = util.TraceLine(TraceData)

    -- This shouldn't normally happen, but the wiki says it can
    if not TraceResult then
        return
    end

    -- Apply the suspension forces
    self:ApplySuspensionForce(Parent, TraceStart, TraceEnd, TraceResult)

    -- Apply the friction forces (static friction, grip, etc.)
    self:ApplyFrictionForces(Parent, TraceStart, TraceEnd, TraceResult)
end

function ENT:OnEngineUpdate(UpdateTime, InputRPM, InputEnergy, InputTorque)
    local Parent = self:GetParent()
    if not IsValid(Parent) then
        return
    end

    self.DriveTorque = InputTorque / 0.14
    self.DriveRPM = InputRPM
    self.DriveEnergy = InputEnergy

    local RPMDiff = self.DriveRPM - self.WheelRPM
    local AngularVelocity = RPMDiff / 9.55
    local EnergyDifference = ((0.5 * self.WheelInertia * math.pow(AngularVelocity, 2)) / self.DriveTorque)

    if InputRPM > 0 then
        EnergyDifference = EnergyDifference * (RPMDiff > 0 and 1 or -1)
    else
        EnergyDifference = EnergyDifference * (RPMDiff > 0 and -1 or 1)
    end

    if WireLib then
        WireLib.TriggerOutput(self, "DEBUG", Format("%d", EnergyDifference))
    end

    return EnergyDifference
end

function ENT:TriggerInput(Name, Value)
    if Name == "Steering" then
        self.SteeringInput = math.Clamp(-Value, -1, 1)
    elseif Name == "Brake" then
        self.BrakePressure = math.Clamp(Value, 0, 1)
    end
end

function ENT:UpdateOutputs()
    if WireLib then

    end
end

function ENT:SaveSCInfo()
    return {
        LinkedPod = self:IsLinkedToPod() and self:GetLinkedPod():EntIndex() or nil,
        SuspensionLength = self.SuspensionLength,
        SpringWeightRating = self.SpringWeightRating,
        Stiffness = self.Stiffness,
        Damping = self.Damping,
        Multiplier = self.Multiplier,
        WheelModel = self.WheelModel,
        WheelScale = self.WheelScale,
        WheelOffset = self.WheelOffset,
        WheelRadius = self.WheelRadius,
        WheelWidth = self.WheelWidth,
        WheelWeight = self.WheelWeight,
        WheelSlideFrictionCoefficient = self.WheelSlideFrictionCoefficient,
        WheelRollingResistanceCoefficient = self.WheelRollingResistanceCoefficient,
        MaxSidewaysForce = self.MaxSidewaysForce,
        MinCompressionGrip = self.MinCompressionGrip,
        MaxCompressionGrip = self.MaxCompressionGrip,
        VehicleWeight = self.VehicleWeight,
        NumberOfWheels = self.NumberOfWheels,
        PeakTraction = self.PeakTraction,
        PeakSlipRatio = self.PeakSlipRatio,
        SlipFalloff = self.SlipFalloff,
        SlipFalloffRatio = self.SlipFalloffRatio,
        HasSteering = self.HasSteering,
        SteeringAngle = self.SteeringAngle,
        SteeringFrictionMultiplier = self.SteeringFrictionMultiplier,
        SteeringSensitivity = self.SteeringSensitivity,
        HasBrakes = self.HasBrakes,
        BrakeTorque = self.BrakeTorque
    }
end

function ENT:ApplySCDupeInfo(Info, GetEntByID)
    if Info then
        if Info.LinkedPod then
            self:SetLinkedPod(GetEntByID(Info.LinkedPod))
        end

        -- Apply Settings
        self.SuspensionLength = Info.SuspensionLength or 22.5
        self.SpringWeightRating = Info.SpringWeightRating or 2200
        self.Stiffness = Info.Stiffness or 10
        self.Damping = Info.Damping or 1.8
        self.Multiplier = Info.Multiplier or 1

        -- Wheel
        self.WheelModel = Info.WheelModel or "models/sprops/trans/wheel_c/wheel45.mdl"
        self.WheelScale = Info.WheelScale or Vector(1, 1, 1)
        self.WheelOffset = Info.WheelOffset or Vector(0, 0, 0)
        self.WheelRadius = Info.WheelRadius or 22.5
        self.WheelWidth = Info.WheelWidth or 8
        self.WheelWeight = Info.WheelWeight or 25
        self.WheelSlideFrictionCoefficient = Info.WheelSlideFrictionCoefficient or 0.5
        self.WheelRollingResistanceCoefficient = Info.WheelRollingResistanceCoefficient or 0.375
        self.MaxSidewaysForce = Info.MaxSidewaysForce or 2.5
        self.MinCompressionGrip = Info.MinCompressionGrip or 0.95
        self.MaxCompressionGrip = Info.MaxCompressionGrip or 1.15
        self.VehicleWeight = Info.VehicleWeight or 1500
        self.NumberOfWheels = Info.NumberOfWheels or 4
        self.PeakTraction = Info.PeakTraction or 1.05
        self.PeakSlipRatio = Info.PeakSlipRatio or 6
        self.SlipFalloff = Info.SlipFalloff or 0.6
        self.SlipFalloffRatio = Info.SlipFalloffRatio or 20

        -- Steering
        self.HasSteering = Info.HasSteering or false
        self.SteeringAngle = Info.SteeringAngle or 35
        self.SteeringFrictionMultiplier = Info.SteeringFrictionMultiplier or 1.2
        self.SteeringSensitivity = Info.SteeringSensitivity or 1

        -- Brakes
        self.HasBrakes = Info.HasBrakes or false
        self.BrakeTorque = Info.BrakeTorque or (1600 / 0.14)

        self:SetupWheel()
        self:WriteCreationPacket()
    end
end

duplicator.RegisterEntityClass("sc_engine", GAMEMODE.MakeEnt, "Data")