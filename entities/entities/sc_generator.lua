AddCSLuaFile()

DEFINE_BASECLASS("base_generatorentity")

ENT.PrintName = "Generator"
ENT.Author = "Lt.Brandon"
ENT.Contact = "diaspora-community.com"
ENT.Purpose = "Does generator things, mainly it generates stuff"
ENT.Instructions = "Throw batteries into mouth, flip switch."

ENT.Spawnable = false
ENT.AdminOnly = false
ENT.DelayAfterCyclingStopped = 0

local base = scripted_ents.Get("base_generatorentity")
hook.Add("InitPostEntity", "sc_generator_InitPostEntity", function()
	base = scripted_ents.Get("base_generatorentity")
end)

function ENT:SharedInit()
    base.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "Broken", "bool", false)
    SC.NWAccessors.CreateNWAccessor(self, "MaxMultiplier", "number", 20)
    SC.NWAccessors.CreateNWAccessor(self, "DamagingMultiplier", "number", 10)
    SC.NWAccessors.CreateNWAccessor(self, "HeatPerCycle", "number", 1)
    SC.NWAccessors.CreateNWAccessor(self, "MaxHeat", "number", 100)
    SC.NWAccessors.CreateNWAccessor(self, "Heat", "number", 0)
end

function ENT:GetHeatPercent()
    return self:GetHeat() / self:GetMaxHeat()
end

function ENT:CanRepair()
    return self:GetHeatPercent() <= 0.01
end

if CLIENT then
	return
end

function ENT:UpdateMultiplier()
    self:SetMultiplier(math.Clamp(self:GetWantedMultiplier(), 0, self:GetMaxMultiplier()) * (1 - self:GetHeatPercent()))
    return self:GetMultiplier()
end

function ENT:GetWantedMultiplier()
    return self.WantedMultiplier
end

function ENT:SetWantedMultiplier(Mult)
    self.WantedMultiplier = math.Round(math.Clamp(Mult, 0, self:GetMaxMultiplier()))
    self:SetMultiplier(self.WantedMultiplier)
end

function ENT:ApplyHeat(amount)
    self:SetHeat(self:GetHeat() + amount)

    if self:GetHeat() >= self:GetMaxHeat() then
        self:SetBroken(true)
    end
end

function ENT:DissipateHeat(amount)
    self:SetHeat(math.max(self:GetHeat() - amount, 0))
end

function ENT:CanGenerate()
    return (not self:GetBroken()) and IsValid(self:GetProtector()) and self:GetFitting().CanRun and self:CheckResources()
end

function ENT:OnCycleFinished()
    if self:GetWantedMultiplier() > self:GetDamagingMultiplier() then -- We need to use the actual multiplier here instead of the heated one
        self:ApplyHeat(self:GetHeatPerCycle() * (1 + ((self:GetWantedMultiplier() - self:GetDamagingMultiplier()) / self:GetMaxMultiplier())))
    end

    base.OnCycleFinished(self)
end

function ENT:OnCycleStarted()
    if self.GeneratorInfo then
        self:DissipateHeat(self:GetHeatPerCycle())
    end

    return base.OnCycleStarted(self)
end

function ENT:CanCycle()
    -- Need to update the multiplier before the cycle is actually started for some generators
    if self.GeneratorInfo then
        self:UpdateMultiplier()

        local MultFunc = self.GeneratorInfo:GetCalculateMultiplierFunction()
        MultFunc(self)
    end

	return self:CanGenerate() and self:GetHasCycle() and CurTime() >= self.DelayAfterCyclingStopped
end

function ENT:Think()
    -- If we don't have a "Cycle" then just call ConsumeResources and GenerateResources once a second
    -- This will make most generators act similarly to how they did before the base_scmoduleentity was created
    if not self:GetHasCycle() then
        -- Need to update the multiplier before the cycle is actually started for some generators
        if self.GeneratorInfo then
            self:UpdateMultiplier()

            local MultFunc = self.GeneratorInfo:GetCalculateMultiplierFunction()
            MultFunc(self)
        end

        if self:CanGenerate() then
            self:OnCycleStarted()
            self:ConsumeResources()
            self:GenerateResources()
            self:OnCycleFinished()
        end

        self:UpdateOutputs()
        self:NextThink(CurTime() + 1)
        return true
    end

    return base.Think(self)
end

function ENT:Setup()
    local volume = self:GetPhysicsObject():GetVolume()
	self:SetMultiplier(1)
    self:SetCycleDuration(1)
	self:SetBaseMultiplier(1)
	self:SetUseType(SIMPLE_USE)
    self:SetEnableSound("buttons/button16.wav")
    self.WantedMultiplier = 1
end

function ENT:GetWirePorts()
    local inputs, outputs = {}, {}
    if self.GeneratorInfo:HasCustomWireIO() then
        local newin, newout = self.GeneratorInfo:GetWireIO()
        for i,k in pairs(newin) do
            table.insert(inputs, k)
        end

        for i,k in pairs(newout) do
            table.insert(outputs, k)
        end
    end

    if self.GeneratorInfo:ShouldUseDefaultWireIO() then
        local newin, newout = base.GetWirePorts(self)
        for i,k in pairs(newin) do
            table.insert(inputs, k)
        end

        -- Heat Outputs, not on default generators so add them here.
        table.insert(outputs, "Overheating")
        table.insert(outputs, "Overheat Percent")

        for i,k in pairs(newout) do
            table.insert(outputs, k)
        end
    end

    return inputs, outputs
end

function ENT:UpdateOutputs()
    if not self.GeneratorInfo then return end

    if self.GeneratorInfo:HasCustomWireIO() then
        local UpdateOutputs = self.GeneratorInfo:GetUpdateOutputsFunction()
        UpdateOutputs(self)
    end

    if self.GeneratorInfo:ShouldUseDefaultWireIO() then
        if WireLib ~= nil then
            WireLib.TriggerOutput(self, "Overheating", (self:GetHeatPercent() > 0 or self:GetWantedMultiplier() > self:GetDamagingMultiplier()) and 1 or 0)
            WireLib.TriggerOutput(self, "Overheat Percent", self:GetHeatPercent())
        end
        base.UpdateOutputs(self)
    end
end

function ENT:TriggerInput(iname, value)
    if not self.GeneratorInfo then return end

    if self.GeneratorInfo:HasCustomWireIO() then
        local TriggerInput = self.GeneratorInfo:GetTriggerInputFunction()
        TriggerInput(self, iname, value)
    end

    if self.GeneratorInfo:ShouldUseDefaultWireIO() then
        if iname == "Multiplier" then
            local Mult = math.Clamp(value, 1, self:GetMaxMultiplier())

            if Mult ~= Mult then
                Mult = 1
            end

            self:SetWantedMultiplier(Mult)
            return
        end

        base.TriggerInput(self, iname, value)
    end
end

function ENT:OnStoppedCycling()
    if not self.GeneratorInfo then return end
    local Func = self.GeneratorInfo:GetTurnedOffFunction()
    Func(self)

    Func = self.GeneratorInfo:GetToggledFunction()
    Func(self)

    self.DelayAfterCyclingStopped = CurTime() + 2
end

function ENT:OnStartedCycling()
    if not self.GeneratorInfo then return end
    local Func = self.GeneratorInfo:GetTurnedOnFunction()
    Func(self)

    Func = self.GeneratorInfo:GetToggledFunction()
    Func(self)
end

function ENT:SetupGenerator(GeneratorInfo)
    if not GeneratorInfo then SC.Error("sc_generator: Someone tried to make an invalid generator!?", 5) return end
	self:SetModuleName(GeneratorInfo:GetName())
    self:SetProduction(table.Copy(GeneratorInfo:GetProduction()))
    self.Production_num = table.Count(self:GetProduction())
    self:SetCycleActivationCost(table.Copy(GeneratorInfo:GetConsumption()))
    self.Consumption_num = table.Count(self:GetCycleActivationCost())
    self.NoMultiplierInput = not GeneratorInfo:GetHasMultiplier()
    self:SetHasCycle(GeneratorInfo:GetHasCycle())
    self:SetMaxMultiplier(GeneratorInfo:GetMaxMultiplier())
    self:SetDamagingMultiplier(GeneratorInfo:GetDamagingMultiplier())
    self:SetHeatPerCycle(GeneratorInfo:GetHeatPerCycle())
    self:SetMaxHeat(GeneratorInfo:GetMaxHeat())
    self:SetTurnOnSound(GeneratorInfo:GetActivationSound())
    self:SetTurnOffSound(GeneratorInfo:GetDeactivationSound())
    self:SetLoopSound(GeneratorInfo:GetLoopSound())
    local cpu, pg, slot, classes = GeneratorInfo:GetFitting()
    self:SetupFitting(cpu, pg, slot, classes)
    self.CanGenerate = GeneratorInfo:GetCanGenerateFunction() or self.CanGenerate
    self.GeneratorInfo = GeneratorInfo
    self:SetMuted(not GeneratorInfo:GetHasSounds())

    local MultFunc = GeneratorInfo:GetCalculateBaseMultiplierFunction()
    local BaseMult = MultFunc(self)
    if BaseMult > 0 then
        self:SetBaseMultiplier(BaseMult)
    else
        if self:GetGeneratorClass() == "Unusably Large" then
            self:SetBaseMultiplier(0)
        elseif GAMEMODE.Config:Get("generators", "General", "EnableVolumeBasedMultipliers", false) then
            if IsValid(self:GetPhysicsObject()) then
                local Volume = math.Round(self:GetPhysicsObject():GetVolume())
                local BaseVolume = math.Round(GeneratorInfo:GetBaseVolume())
                local Negative = 1
                if Volume < BaseVolume then Negative = -1 end

                self:SetBaseMultiplier((((math.abs(Volume - BaseVolume) ^ 0.85) / BaseVolume) * Negative) + 1)
            end
        else
            local Multiplier = GAMEMODE.Config:Get("generators", "ClassMultipliers", self:GetGeneratorClass(), 0)
            self:SetBaseMultiplier(Multiplier)
        end
    end

    self:SetupWirePorts()

    local SetupFunc = GeneratorInfo:GetSetupFunction()
    SetupFunc(self)
end

function ENT:SaveSCInfo()
    return {type=self:GetModuleName()}
end

function ENT:LoadSCInfo(Info)
    if not Info.type then return end
    self:SetupGenerator(GAMEMODE:GetGeneratorInfo(Info.type))
end

local function MakeGenerator(Ply, Data)
    -- Replace invalid generators
    if Data and Data.EntityMods then
        local SC2Data = Data.EntityMods.SC2DupeInfo
        if SC2Data then
            if not SC2Data.type then
                Data.Class = "prop_physics"
            else
                local GeneratorInfo = GAMEMODE:GetGeneratorInfo(SC2Data.type)
                if not GeneratorInfo then
                    Data.Class = "prop_physics"
                else
                    Data.Class = GeneratorInfo.Class
                end
            end
        end
    end

    return GAMEMODE.MakeEnt(Ply, Data)
end
duplicator.RegisterEntityClass("sc_generator", MakeGenerator, "Data")