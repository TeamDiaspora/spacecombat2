
local meta = FindMetaTable( "Player" )

-- Return if there's nothing to add on to
if not meta then return end

g_SBoxObjects = {}

function meta:CheckLimit( str )

	return true

end

function meta:GetCount( str, minus )

	if ( CLIENT ) then
		return self:GetNWInt( "Count." .. str, 0 )
	end

	minus = minus or 0

	if not self:IsValid() then return end

	local key = self:SteamID64()
	local tab = g_SBoxObjects[ key ]

	if ( not tab or not tab[ str ] ) then

		self:SetNWInt( "Count."..str, 0 )
		return 0

	end

	local c = 0

	for k, v in pairs ( tab[ str ] ) do

		if ( IsValid( v ) and not v:IsMarkedForDeletion() ) then
			c = c + 1
		else
			tab[ str ][ k ] = nil
		end

	end

	self:SetNWInt( "Count." .. str, math.max(c - minus, 0) )

	return c

end

function meta:AddCount( str, ent )

end

function meta:LimitHit( str )

	self:SendLua( "hook.Run('LimitHit', '".. str .."' )" )

end

function meta:AddCleanup( type, ent )

	cleanup.Add( self, type, ent )

end

if ( SERVER ) then

	function meta:GetTool( mode )

		local wep = self:GetWeapon( "gmod_tool" )
		if ( not IsValid( wep ) ) then return nil end

		local tool = wep:GetToolObject( mode )
		if not tool then return nil end

		return tool

	end

	function meta:SendHint( str, delay )

		self.Hints = self.Hints or {}
		if ( self.Hints[ str ] ) then return end

		self:SendLua( 'hook.Run("AddHint","' .. str .. '","' .. delay .. '")' )
		self.Hints[ str ] = true

	end

	function meta:SuppressHint( str )

		self.Hints = self.Hints or {}
		if ( self.Hints[ str ] ) then return end

		self:SendLua( 'hook.Run("SuppressHint","' .. str .. '")' )
		self.Hints[ str ] = true

	end

else

	function meta:GetTool( mode )

		local wep
		for _, ent in pairs( ents.FindByClass( "gmod_tool" ) ) do
			if ( ent:GetOwner() == self ) then wep = ent break end
		end
		if (not IsValid( wep )) then return nil end

		local tool = wep:GetToolObject( mode )
		if not tool then return nil end

		return tool

	end

end
