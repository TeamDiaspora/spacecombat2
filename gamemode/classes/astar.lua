-- A-Star pathfinding algorithm
-- Author: TangentDelta

local GM = GM

---@class AStarSolver
---@field Start integer #The index of the node that the solver starts from
---@field Goal integer #The index of the node that the solver will try to navigate to
---@field Nodes table #A table of nodes in the graph
---@field Step integer #How many steps the solver has taken in solving the path
---@field ClosedSet table #The set of nodes that have already been processed and discarded
---@field OpenSet table #The set of nodes that need to be processed
---@field CameFrom table #The current path of nodes taken
---@field GScore table #The set path costs to nodes
---@field FScore table #The set of how "fruitful" the path to a node is
---@field Path table #The final path, unwound
---@field GetHeuristic function #A function that returns the heuristic value between two nodes
local C = GM.LCS.class({
    Start = 1,
    Goal = 1,
    Nodes = nil,

    Step = 0,
    ClosedSet = nil,
    OpenSet = nil,
    CameFrom = nil,
    GScore = nil,
    FScore = nil,
    Path = nil,

    GetHeuristic = function(AStar, A, B) return 1 end
})


function C:init()

end

---Prepare the solver to search for a path
---@param Start integer #The starting node index
---@param Goal integer #The target node index
---@param Nodes table #The graph to solve
function C:Setup(Start, Goal, Nodes)
    self.Start = Start
    self.Goal = Goal
    self.Nodes = Nodes

    -- Initialize the solver's tables
    self.ClosedSet = {}
    self.OpenSet = { self.Start }
    self.CameFrom = {}

    self.GScore, self.FScore = {}, {}
    self.GScore[self.Start] = 0
    self.FScore[self.Start] = 0

    self.Path = {}

    self.Step = 0
end

---Recursively unwind the path taken to reach the current node and insert the path into the Path field
---@param CurrentNode integer #The current node to unwind
---@return table CurrentPath #The current path taken to unwind. Used for recursion
function C:UnwindPath(CurrentNode)
    if self.CameFrom[CurrentNode] then
        table.insert(self.Path, 1, self.CameFrom[CurrentNode])
        return self:UnwindPath(self.CameFrom[CurrentNode])
    end
end

---Return the node in the open set with the lowest F score
---@return integer BestNode #The ID of the node with the lowest F score
function C:GetLowestFScore()
    local LowestScore = 0
    local BestNode = nil

    for _, Node in ipairs(self.OpenSet) do
        local Score = self.FScore[Node]
        if (Score < LowestScore) or (BestNode == nil) then
            LowestScore = Score
            BestNode = Node
        end
    end

    return BestNode
end

---Remove a node from the open set and move the last node in the set to its position
---@param NodeToRemove integer #The ID of the node to remove from the open set
function C:RemoveNode(NodeToRemove)
    for NodeIndex, Node in ipairs(self.OpenSet) do
        if Node == NodeToRemove then
            self.OpenSet[NodeIndex] = self.OpenSet[#self.OpenSet]
            self.OpenSet[#self.OpenSet] = nil
            return
        end
    end
end

---Solve one step
---@return boolean SolverComplete
function C:SolveOnce()
    if #self.OpenSet > 0 then
        self.Step = self.Step + 1
        local Current = self:GetLowestFScore()

        if Current == self.Goal then
            self:UnwindPath(Current)
            table.insert(self.Path, self.Goal)
            return true
        end

        self:RemoveNode(Current)
        table.insert(self.ClosedSet, Current)

        for NeighborIndex, NeighborNode in ipairs(self.Nodes[Current].ConnectedNodes) do
            if not table.HasValue(self.ClosedSet, NeighborNode) then
                local NewGScore = self.GScore[Current] + self:GetHeuristic(Current, NeighborNode)  -- Add heuristic
                if (not table.HasValue(self.OpenSet, NeighborNode)) or (NewGScore < self.GScore[NeighborNode]) then
                    self.CameFrom[NeighborNode] = Current
                    self.GScore[NeighborNode] = NewGScore
                    self.FScore[NeighborNode] = NewGScore
                    if not table.HasValue(self.OpenSet, NeighborNode) then
                        table.insert(self.OpenSet, NeighborNode)
                    end
                end
            end
        end

        --Open set is still processing, return false (still solving)
        return false
    end

    --Open set is empty, return true for solution found (no solution)
    return true
end

---Solve completely
function C:Solve()
    for I = 1, 1000 do
        if self:SolveOnce() then return end
    end
end

---Get the solved path
---@return table SolvedPath
function C:GetPath()
    return self.Path
end

function C:GetGScore()
    return self.GScore
end

GM.class.registerClass("AStarSolver", C)