local GM = GM

--#endregion

---The base class for NPC ship activities
---@class NPCShip_Activity_Base
---@param NPCShip NPCShip #The NPC ship that this activity belongs to
local C = GM.LCS.class({
    NPCShip = nil
})

--#region General Class Stuff

---Class initialization
---@param NPCShip NPCShip #The NPC ship to assign this activity to
function C:init(NPCShip)
    self.NPCShip = NPCShip
end

--#endregion
--#region General Callbacks
function C:OnArrivedAtTarget()
end

function C:OnDepartedFromTarget()
end

function C:OnArrivedAtLastWaypoint()
end

---Called when the NPC Ship AI switches to this activty
---@param OldActivity string #The name of the activity that we just switched from
function C:OnActivityStarted(OldActivity)
end

---Called when the NPC Ship AI switches away from this activity
---@param NewActivity string #The name of the activity that we are switching to
function C:OnActivityFinished(NewActivity)
end
--#endregion
--#region Combat Callbacks

---comment
---@param Attacker Player #The entity that is attacking us
---@param Inflictor Entity #The entity that is applying damage to us. If a ship weapon, the launcher object.
function C:OnTakeDamage(Attacker, Inflictor)
    if IsValid(Inflictor) and IsValid(Attacker) then
        self:OnUnderAttackByShip(Inflictor:GetCore())
    end
end

---Called when our core has sustained damage within the last second
---@param AttackingShips table #The ships, and how much damage they've done within the last second
function C:OnReceivedDamageFromShips(AttackingShips)
    local Ship = self.NPCShip
    local MyFaction = Ship:GetFaction()
    local MostSeriousShip = nil

    for _, Attacker in pairs(AttackingShips) do
        if not self:GetCoreInEnvironment(Attacker.Core) and (self:GetCoreFaction(Attacker.Core) ~= MyFaction) then
            MostSeriousShip = Attacker.Core
            break
        end
    end

    --TODO: Make the NPC ships not go on holy crusades upon receiving damage from an undead ship
    --Nobody seems to have attacked us, but we were told we were being attacked!
    --Lash out at the nearest ship

    if not IsValid(MostSeriousShip) then
        --MostSeriousShip = self:GetNearestShipInSpace()
    end

    if IsValid(MostSeriousShip) then
        if Ship:GetHasWeapons() then
            if Ship:GetCurrentActivityName() ~= "Combat" then
                Ship:SetActivity("Combat")
            end

            Ship:GetActivity("Combat"):AddNewAttacker(MostSeriousShip)
        else
            --Flee!
        end
    end
end

--#endregion
--#region Attack Management

---Gets the faction ID of the faction that a core belongs to
---@param Core entity #The core to get the faction from
---@return number FactionID
function C:GetCoreFaction(Core)
    if not IsValid(Core) then return end

    local NPCShip = GAMEMODE:GetNPCShipManager():GetShipByCore(Core)
    if NPCShip then
        return NPCShip:GetFaction()
    elseif Core.CPPIGetOwner then
        local Owner = Core:CPPIGetOwner()
        if IsValid(Owner) then
            return Owner:Team()
        end
    end
end

---Returns if a core is inside of an environment
---@param Core entity #The ship core to check
---@return boolean InEnvironment #If the ship core is in an environment
function C:GetCoreInEnvironment(Core)
    local Space = GAMEMODE:GetSpace()
    return not (Core.GetEnvironment and Core:GetEnvironment() == Space)
end

---Returns the nearest ship in space that might have attacked us
---Called when a ship attacks us from within an environment
---@return entity NearestCore #The core that is the closest to us
function C:GetNearestShipInSpace()
    local Ship = self.NPCShip
    local Targeter = Ship:GetTargeter()
    local MyPos = Ship:GetGyropod():GetPos()
    local MyFaction = Ship:GetFaction()
    local NearestCore = nil
    local ShortestDistance = 99999999

    for _, Target in ipairs(Targeter:GetTargetList()) do
        if IsValid(Target) and (Target ~= Ship:GetCore()) and (not self:GetCoreInEnvironment(Target)) and (self:GetCoreFaction(Target) ~= MyFaction) then
            local Distance = MyPos:Distance(Target:GetPos())
            if Distance < ShortestDistance then
                ShortestDistance = Distance
                NearestCore = Target
            end
        end
    end

    return NearestCore
end
--#endregion

--#region Thinking
function C:Think()

end

function C:SlowThink()

end

--#endregion

GM.class.registerClass("NPCShip_Activity_Base", C)