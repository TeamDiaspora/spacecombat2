local GM = GM

---@class NPCShip_Wander : NPCShip_Activity_Base #A transport NPC ship
local C = GM.class.getClass("NPCShip_Activity_Base"):extends({

})

local BaseClass = C:getClass()

--#region General Callbacks
---Called when the NPC Ship AI switches to this activty
---@param OldActivity string #The name of the activity that we just switched from
function C:OnActivityStarted(OldActivity)
    --Delete any waypoints that might be left over from the previous activity
    self.NPCShip:ClearWaypoints()
    --And clear the current position that we are moving to
    self.NPCShip:SetTargetMovePos(nil)
end

---Called when the NPC Ship AI switches away from this activity
---@param NewActivity string #The name of the activity that we are switching to
function C:OnActivityFinished(NewActivity)
    --Delete all of the waypoints that we created
    self.NPCShip:ClearWaypoints()
end
--#endregion

--#region Thinking

function C:SlowThink()
    if #self.NPCShip:GetWaypoints() == 0 then
        self.NPCShip:AddRandomWaypoints(5)
    end
end
--#endregion

GM.class.registerClass("NPCShip_Activity_Wander", C)