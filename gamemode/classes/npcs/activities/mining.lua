local GM = GM

local TARGETER_FILTERID_MINING = 1
local TARGETER_FILTERID_SALVAGE = 2
local TARGETER_FILTERID_COMBAT = 3

---@class NPCShip_Activity_Mining : NPCShip_Activity_Base #A mining NPC ship
---@field TargetMineable entity #The entity that the ship is currently trying to mine
---@field Mining boolean #Is the ship currently mining the asteroid with its lasers
local C = GM.class.getClass("NPCShip_Activity_Base"):extends({
    TargetMineable = nil,
    Mining = false
})

local BaseClass = C:getClass()

--#region Member variables

--#endregion
--#region Callback Overrides

---Called when the ship arrives at the asteroid
function C:OnArrivedAtTarget()
    if not self.Mining then
        self.NPCShip:GetTargeter():SetFireGroupFiring("Secondary", true)
        self.Mining = true
    end
end

---Called when the NPC Ship AI switches to this activty
---@param OldActivity string #The name of the activity that we just switched from
function C:OnActivityStarted(OldActivity)
    --If we were in the middle of mining an asteroid, go back to mining it
    self.TargetMineable = nil
end

---Called when the NPC Ship AI switches away from this activity
---@param NewActivity string #The name of the activity that we are switching to
function C:OnActivityFinished(NewActivity)
    --If we were mining an asteroid, switch off our laser
    if self.Mining then
        self.NPCShip:GetTargeter():SetFireGroupFiring("Secondary", false)
        self.Mining = false
    end
end

--#endregion
--#region Mining Management

---Set an asteroid as our mining target
---@param TargetIndex integer #The index of the target
function C:SetMiningTarget(TargetIndex)
    local Ship = self.NPCShip
    local Targeter = Ship:GetTargeter()

    Targeter:TriggerInput("Target Override", TargetIndex)
    self.TargetMineable = Targeter:GetTargetList()[TargetIndex]

    -- Set up ship navigation
    Ship:ClearWaypoints()
    Ship:SetTargetAimPos(self.TargetMineable:GetPos())
    Ship:SetTargetMovePos(self.TargetMineable:GetPos())
    Ship:SetTargetAimDot(1.98)
end

---Find a new random salvage target and set it as our next target
---@return boolean TargetFound #Was a target found and assigned to the targeter?
function C:FindSalvageTarget()
    local Targeter = self.NPCShip:GetTargeter()
    if Targeter.FilterID ~= TARGETER_FILTERID_SALVAGE then
        Targeter:SetFilter(TARGETER_FILTERID_SALVAGE)
    end

    return self:FindTarget(true)
end

---Find a new random mining asteroid and set it as our next target
---@return boolean TargetFound #Was a target found and assigned to the targeter?
function C:FindMiningTarget()
    local Targeter = self.NPCShip:GetTargeter()
    if Targeter.FilterID ~= TARGETER_FILTERID_MINING then
        Targeter:SetFilter(TARGETER_FILTERID_MINING)
    end

    return self:FindTarget(false)
end

---Find a target in the current list of targets from the targeter
function C:FindTarget(FindNearestTarget)
    local Targeter = self.NPCShip:GetTargeter()
    local Targets = Targeter:GetTargetList()
    if #Targets > 0 then
        local BestTargetIndex = nil
        if FindNearestTarget then
            local MyPos = self.NPCShip:GetGyropod():GetPos()
            local ClosestDistance = 32000
            for TargetIndex, Entity in ipairs(Targets) do
                if IsValid(Entity) then
                    local Distance = Entity:GetPos():Distance(MyPos)
                    if Distance < ClosestDistance then
                        BestTargetIndex = TargetIndex
                        ClosestDistance = Distance
                    end
                end
            end
        else
            -- Just find a random asteroid to mine
            _, BestTargetIndex = table.Random(Targets)
        end

        if IsValid(Targets[BestTargetIndex]) then
            self:SetMiningTarget(BestTargetIndex)
            return true
        end
    end

    --No more targets to mine? Go poof
    return false
end
--#endregion
--#region Thinking

---
function C:SlowThink()
    local Ship = self.NPCShip
    local Targeter = Ship:GetTargeter()
    --Go away if the cargo hold is full

    --TODO: Make the miner sit around and refine resources before leaving
    if Ship:GetCore():GetStorage()["Cargo"]:GetRemainingSize() < 1000 or not IsValid(Targeter) then
        Ship:SetActivity("Leave")
        return
    end

    --Refine resources if we have refineries on board
    if #Ship:GetRefineries() > 0 then
        Ship:GetActivity("Refining"):SlowThink()
    end

    if not IsValid(self.TargetMineable) then
        if self.Mining then
            Targeter:SetFireGroupFiring("Secondary", false)
            Ship:SetTargetMovePos(nil)
            self.Mining = false
        end

        local TargetFound = self:FindSalvageTarget()    -- Try to find a salvage target

        if not TargetFound then
            TargetFound = self:FindMiningTarget() --Try to find a mining target
        end

        if not TargetFound then
            Ship:GetActivity("Wander"):SlowThink()
        end
    end
end
--#endregion

GM.class.registerClass("NPCShip_Activity_Mining", C)