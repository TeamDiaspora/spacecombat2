local GM = GM

---What the ship will try to produce, with highest priority first
--The default is to produce DU particle canisters from mined resources
local DefaultProductions = {
    {
        Name = "Iron",
        Quantity = 0
    },
    {
        Name = "Depleted Uranium Particle Canister",
        Quantity = 0
    },
    {
        Name = "Empty Canister",
        Quantity = 100
    }
}

local RefineryTypes = {
    "Refinery",
    "Factory"
}

---@class NPCShip_Activity_Refining : NPCShip_Activity_Base
---@field Productions table #The resources that the ship will try to produce
local C = GM.class.getClass("NPCShip_Activity_Base"):extends({
    Productions = nil
})

local BaseClass = C:getClass()

--#region General Callbacks

--#endregion
--#region Refinery Management

---Return the name of the recipe, and the refinery type, for producing a resource
---@param ResourceName string #The resource to produce
---@return string RefineryType, string RecipeName
function C:FindRecipeForResource(ResourceName)
    for _, RefineryType in pairs(RefineryTypes) do
        for RecipeName, Recipe in pairs(SC.Manufacturing.GetRecipes(RefineryType)) do
            if Recipe.produces then
                if Recipe.produces[ResourceName] then
                    return RefineryType, RecipeName
                end
            end
        end
    end
end

---Return a table of refineries on the ship that match the requested type
---@param RefineryType string
---@return table Refineries #The refineries matching the type
function C:GetShipRefineriesOfType(RefineryType)
    local Refineries = {}

    for _, Refinery in ipairs(self.NPCShip:GetRefineries()) do
        if Refinery:GetType() == RefineryType then
            table.insert(Refineries, Refinery)
        end
    end

    return Refineries
end

---Populate the productions table
function C:PopulateProductions()
    self.Productions = {}

    for _, Production in ipairs(DefaultProductions) do
        local RefineryType, RecipeName = self:FindRecipeForResource(Production.Name)
        if RefineryType and RecipeName then
            local NewProductionEntry = {
                ResourceName = Production.Name,
                Quantity = Production.Quantity,
                InProduction = 0,
                RecipeName = RecipeName,
                RefineryType = RefineryType,
                ValidRefineries = self:GetShipRefineriesOfType(RefineryType)
            }

            --Don't insert a production that we don't have a refinery for
            if #NewProductionEntry.ValidRefineries > 0 then
                table.insert(self.Productions, NewProductionEntry)
            end
        end
    end
end

function C:ManageProductions()
    local Ship = self.NPCShip
    local Core = Ship:GetCore()
    for _, Production in ipairs(self.Productions) do
        local ResourceName = Production.ResourceName
        local DesiredQuantity = (Production.Quantity == 0) and Core:GetMaxAmount(ResourceName) or Production.Quantity
        local QuantityNeeded = DesiredQuantity - Core:GetAmount(Production.ResourceName)

        if QuantityNeeded > 0 then
            local ProductionStarted = false
            for RefineryIndex, Refinery in pairs(Production.ValidRefineries) do
                if IsValid(Refinery)  then
                    if not (Refinery:GetCycling() or ProductionStarted) then
                        Refinery:ChangeRecipe(Production.RecipeName)
                        Refinery:StartCycle()
                        ProductionStarted = true
                    end
                else
                    Production.ValidRefineries[RefineryIndex] = nil
                end
            end
        end
    end
end

--#endregion
--#region Thinking

function C:SlowThink()
    if not self.Productions then self:PopulateProductions() end
    self:ManageProductions()
end
--#endregion

GM.class.registerClass("NPCShip_Activity_Refining", C)