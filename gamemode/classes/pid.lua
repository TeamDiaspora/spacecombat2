local GM = GM

local C = GM.LCS.class({
    p = 0,
    i = 0,
    d = 0,
    ITerm = 0,
    MinOut = 0,
    MaxOut = 0,
    DCut = 0,
    MinI = 0,
    MaxI = 0,
    LastThink = 0,
    LastError = 0
})

---Init for PID class
---@param p number
---@param i number
---@param d number
function C:init(p, i, d)
    self.p = p
    self.i = i
    self.d = d
    self.LastThink = CurTime()
end

---Set the output limits for the pid
---@param Min number
---@param Max number
function C:SetOutputLimits(Min, Max)
    self.OutMin = Min
    self.OutMax = Max
end

---Set the i term limits for the pid
---@param Min number
---@param Max number
function C:SetILimits(Min, Max)
    self.IMin = Min
    self.IMax = Max
end

---Set the d term value where the i term no longer gets evaluated
---@param DCut number
function C:SetDCutoff(DCut)
    self.DCut = DCut
end

---Update the pid loop and return the output
---@param Input number
---@param Target number
---@return number
function C:Update(Input, Target, DeltaTimeOverride)
    local Error = Input - Target
    local DeltaTime = isnumber(DeltaTimeOverride) and DeltaTimeOverride or (CurTime() - self.LastThink)
    local DTerm = 0

    if DeltaTime > 0 then
        DTerm = (Error - self.LastError) / DeltaTime
    end

    DTerm = DTerm * self.d

    if self.DCut ~= 0 and math.abs(DTerm) < self.DCut then
        self.ITerm = self.ITerm + self.i * Error * DeltaTime

        if self.IMin ~= self.IMax then
            self.ITerm = math.Clamp(self.ITerm, self.IMin, self.IMax)
        end
    end

    self.LastThink = CurTime()
    self.LastError = Error
    local Out = (self.p * Error) + self.ITerm + DTerm

    if self.OutMin == self.OutMax then
        return Out
    else
        return math.Clamp(Out, self.OutMin, self.OutMax)
    end
end

GM.class.registerClass("pid", C)