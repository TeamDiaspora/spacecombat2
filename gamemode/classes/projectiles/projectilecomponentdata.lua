local GM = GM
local ProjectileComponentData = {}
GM.Projectiles = GM.Projectiles or {}
GM.Projectiles.Components = {}
GM.Projectiles.Components.LoadedComponents = ProjectileComponentData

require("lip")
require("luaxp")

if SERVER then
    util.AddNetworkString("SC.ProjectileComponentDataLoad")

    net.Receive("SC.ProjectileComponentDataLoad", function(Length, Client)
        net.Start("SC.ProjectileComponentDataLoad")
        net.WriteUInt(table.Count(ProjectileComponentData), 16)

        for _, Data in pairs(ProjectileComponentData) do
            Data:WriteCreationPacket()
        end

        net.Send(Client)
    end)
else
    net.Receive("SC.ProjectileComponentDataLoad", function()
        local NumberOfComponents = net.ReadUInt(16)
        SC.Print("Got " .. NumberOfComponents .. " serialized Projectile Components from the server!", 4)

        for I = 1, NumberOfComponents do
            local Data = GM.class.new("ProjectileComponentData")
            Data:ReadCreationPacket()
            ProjectileComponentData[Data:GetName()] = Data
            SC.Print("Loaded Projectile Component " .. Data:GetName(), 4)
        end

        SC.Print("Finished loading Projetile Components", 4)
        hook.Run("SC.ProjectileComponentDataLoaded")
    end)
end

local function CreateResourceSection(Args)
    local Section = "[ResourceCost]"
    local ResourceCost = Args[1]
    local Multiplier = Args[2]
    for Resource, Amount in pairs(ResourceCost) do
        Section = string.format("%s\n%s=%d", Section, Resource, math.ceil(Amount * Multiplier))
    end
    return Section
end

local function TableToRows(Args)
    local Table, Prefix, Multiplier = unpack(Args)

    if not isstring(Prefix) then
        Prefix = ''
    end

    if not isnumber(Multiplier) then
        Multiplier = 1
    end

    local AsString = ""
    for Key, Value in pairs(Table) do
        local RealValue
        if isnumber(Value) then
            RealValue = tostring(Value * Multiplier)
        else
            RealValue = tostring(Value)
        end

        AsString = string.format("%s%s%s=%s\n", AsString, Prefix, Key, RealValue)
    end

    return AsString
end

function GM.Projectiles.Components.ReloadProjectileComponentData()
    -- Server loads all files from disk for projectile Components
    if SERVER then
        local ComponentFolder = string.format("%s/projectiles/components", SC.DataFolder)
        local FactoriesFolder = string.format("%s/factories", ComponentFolder)
        local TemplatesFolder = string.format("%s/templates", ComponentFolder)

        -- Get all the normal component files
        local ComponentFiles = file.FindRecursive("*", ComponentFolder, "DATA", {
            [FactoriesFolder] = true;
            [TemplatesFolder] = true;
        })

        -- For each file load the data into a new projectile Component class
        for _, File in pairs(ComponentFiles) do
            local NewProjectileComponentData = GM.class.new("ProjectileComponentData")

            if NewProjectileComponentData:LoadFromINI(File, false) then
                ProjectileComponentData[NewProjectileComponentData:GetName()] = NewProjectileComponentData
            else
                SC.Error("Failed to load projectile component from file " .. File, 5)
            end
        end

        -- Get all the factory files
        local FactoryFiles = file.FindRecursive("*", FactoriesFolder)

        -- Load the data for each factory and start creating new components
        for _, File in pairs(FactoryFiles) do
            local Success, FileData = GM.util.LoadINIFile(File, false)

            if not Success then
                SC.Error("Failed to load component factory from file " .. File, 5)
                continue
            end

            local FactoryData = FileData.FactoryData
            local BaseData = FileData.BaseData
            local Sizes = FileData.Sizes
            local Variations = FileData.Variations
            local TemplateExpressions = FileData.TemplateExpressions or {}
            local Template

            SC.Print(string.format("Creating components using component factory %s", FactoryData.FactoryName), 5)

            -- Load the template if it was set
            if not SC.IsEmptyString(FactoryData.TemplatePath) then
                ---@cast Template string
                Template = file.Read(string.format("%s/%s", TemplatesFolder, FactoryData.TemplatePath), "DATA")

                if SC.IsEmptyString(Template) then
                    SC.Error("Component factory had an invalid template path, skipping", 5)
                    continue
                end
            else
                SC.Error("Component factory had an invalid template path, skipping", 5)
                continue
            end

            local function GenerateComponentData(ExpressionContext)
                local CompiledExpressions = {
                    Size = ExpressionContext.Size;
                    Variation = ExpressionContext.Variation;
                }

                for ID, Expression in pairs(TemplateExpressions) do
                    local ParsedExpression, Error = luaxp.compile(Expression)
                    if ParsedExpression == nil then
                        local ErrorText
                        if istable(Error) then
                            ErrorText = string.format("(%s error at %s) %s", Error.type, Error.location or "unknown", Error.message)
                        else
                            ErrorText = tostring(Error)
                        end

                        SC.Error(ErrorText, 5)
                        continue
                    end

                    local Result, EvalError = luaxp.run(ParsedExpression, ExpressionContext)
                    if Result == nil then
                        local ErrorText
                        if istable(EvalError) then
                            ErrorText = string.format("(%s error at %s) %s", EvalError.type, EvalError.location or "unknown", EvalError.message)
                        else
                            ErrorText = tostring(EvalError)
                        end

                        SC.Error(tostring(ErrorText), 5)
                        continue
                    end

                    CompiledExpressions[ID] = Result
                end

                local ComponentData = Template
                for ID, Value in pairs(CompiledExpressions) do
                    ComponentData = ComponentData:gsub("$" .. ID, Value)
                end

                local NewProjectileComponentData = GM.class.new("ProjectileComponentData")
                if NewProjectileComponentData:LoadFromINI(ComponentData, true) then
                    SC.Print("Registered new factory component data: " .. NewProjectileComponentData:GetName(), 4)
                    ProjectileComponentData[NewProjectileComponentData:GetName()] = NewProjectileComponentData
                else
                    SC.Error("Failed to register new component data from factory\n\n" .. ComponentData, 5)
                end
            end

            if FactoryData.GenerateSizes and FactoryData.GenerateVariations then
                for Size, SizeData in pairs(Sizes) do
                    for Variation, VariationData in pairs(Variations) do
                        local ExpressionContext = {
                            Size = Size;
                            Variation = Variation;
                            SizeData = SizeData;
                            VariationData = VariationData;
                            BaseData = BaseData;
                            CreateResourceSection = CreateResourceSection;
                            TableToRows = TableToRows;
                        }

                        GenerateComponentData(ExpressionContext)
                    end
                end
            elseif FactoryData.GenerateSizes then
                for Size, SizeData in pairs(Sizes) do
                    local ExpressionContext = {
                        Size = Size;
                        SizeData = SizeData;
                        BaseData = BaseData;
                        CreateResourceSection = CreateResourceSection;
                        TableToRows = TableToRows;
                    }

                    GenerateComponentData(ExpressionContext)
                end
            elseif FactoryData.GenerateVariations then
                for Variation, VariationData in pairs(Variations) do
                    local ExpressionContext = {
                        Variation = Variation;
                        VariationData = VariationData;
                        BaseData = BaseData;
                        CreateResourceSection = CreateResourceSection;
                        TableToRows = TableToRows;
                    }

                    GenerateComponentData(ExpressionContext)
                end
            else
                SC.Error("Component factory didn't generate anything", 5)
            end
        end

        hook.Run("SC.ProjectileComponentDataLoaded")
        -- Clients request data from the server for projectile Components
    else
        -- Reset the existing data until we get the new stuff
        ProjectileComponentData = {}
        GM.Projectiles.Components.LoadedComponents = ProjectileComponentData
        -- Request new data from the server
        net.Start("SC.ProjectileComponentDataLoad")
        net.SendToServer()
    end
end

if SERVER then
    hook.Remove("SC.Content.PostConfig", "SC.LoadProjectileComponentData")

    hook.Add("SC.Content.PostConfig", "SC.LoadProjectileComponentData", function()
        GM.Projectiles.Components.ReloadProjectileComponentData()
    end)

    concommand.Add("sc_reloadweapons", function()
        GM.Projectiles.Components.ReloadProjectileComponentData()
    end, nil, nil, FCVAR_CHEAT)
else
    hook.Remove("SC.Content.PostContentMounted", "SC.LoadProjectileComponentData")

    hook.Add("SC.Content.PostContentMounted", "SC.LoadProjectileComponentData", function()
        GM.Projectiles.Components.ReloadProjectileComponentData()
    end)

    concommand.Add("sc_reloadweapons_cl", function()
        GM.Projectiles.Components.ReloadProjectileComponentData()
    end, nil, nil, FCVAR_CHEAT)
end

---@class ProjectileComponentData : LCSClass
---@field Name string Name of the component
---@field Description string Description of the component displayed in the projectile creation interface
---@field Class string What class should be created when spawning the projectile
---@field Family string What family of components does this component belong to -- Used to determine what components can be placed in a slot
---@field Factions string[] A list of factions that can use the component in projectiles
---@field ResourceCost table A list of resources required to produce a projectile with this component
---@field Hidden boolean If the component should be hidden from the projectile creation tool
---@field Mass number The amount of mass added to the projectile by the component
---@field ClassData table A table of data passed to the component class when it is created
---@field ToolOptions table A list of options available for this component in the projectile creation tool
local C = GM.LCS.class({
    Name = "Component",
    Description = "",
    Class = "ProjectileComponent",
    Family = "Component",
    Factions = nil,
    ResourceCost = nil,
    Hidden = false,
    Mass = 0,
    ClassData = nil,
    ToolOptions = nil,
})

function C:init()
    self.Factions = {}
    self.ResourceCost = {}
    self.ClassData = {}
    self.ToolOptions = {}
end

-- Does nothing by default, add functionality here
function C:GetName()
    return self.Name
end

function C:GetDescription()
    return self.Description
end

function C:GetClass()
    return self.Class
end

function C:GetFamily()
    return self.Family
end

function C:IsHidden()
    return self.Hidden
end

function C:GetMass()
    return self.Mass
end

function C:GetFactions()
    return table.Copy(self.Factions)
end

function C:GetResourceCost()
    return table.Copy(self.ResourceCost)
end

function C:LoadFromINI(FileName, IsData)
    local Success, Data = GM.util.LoadINIFile(FileName, IsData)

    -- Load all the data into the class if we got any data back
    if Success then
        if self:ValidateINIData(Data) then
            self:DeSerialize(Data)

            return true
        else
            SC.Error(Format("Projectile component file %s has invalid data, it will not be loaded!", FileName), 5)

            return false
        end
    end

    return false
end

function C:ValidateINIData(Data)
    -- Check if it has a configuration table
    if not istable(Data.Configuration) then
        SC.Error("No configuration table provided!", 5)

        return false
    end

    -- Make sure everything is actually in that table
    if not isstring(Data.Configuration.Name) then
        SC.Error("No name provided!", 5)

        return false
    end

    if not isstring(Data.Configuration.Description) then
        SC.Error("No description provided!", 5)

        return false
    end

    if not isstring(Data.Configuration.Class) then
        SC.Error("No class provided!", 5)

        return false
    end

    if not isstring(Data.Configuration.Family) then
        SC.Error("No family provided!", 5)

        return false
    end

    -- Check that all factions exist
    if istable(Data.Configuration.Factions) then
        local FactionList = team.GetAllTeams()

        for _, FactionName in ipairs(Data.Configuration.Factions) do
            local FoundFaction = false

            for _, FactionData in ipairs(FactionList) do
                if FactionName == FactionData.Name then
                    FoundFaction = true
                    break
                end
            end

            if not FoundFaction then
                SC.Error(Format("Faction %s does not exist!", FactionName), 5)

                return false
            end
        end
    end

    -- Make sure it has a valid class
    do
        local TestComponent = GM.class.new(Data.Configuration.Class)

        if not TestComponent then
            SC.Error("Component class does not exist!", 5)
        end
    end

    -- Make sure any resources exist
    if istable(Data.ResourceCost) then
        for Name, Amount in pairs(Data.ResourceCost) do
            if not GM:GetResourceFromName(Name) then
                SC.Error(Format("Resource %s does not exist!", Name), 5)

                return false
            end

            if Amount < 1 then
                SC.Error(Format("Resource cost for %s is less than 1!", Name), 5)

                return false
            end
        end
    end

    -- Check default tool options
    if istable(Data.ToolOptions) then
        -- Temporarily set the tool options table for verification
        self.ToolOptions = Data.ToolOptions
        local DefaultOptions = {}

        for Name, Info in pairs(Data.ToolOptions) do
            DefaultOptions[Name] = Info.Default
        end

        if not self:ValidateToolOptions(DefaultOptions) then
            SC.Error("Invalid default tool options found!", 5)

            return false
        end

        self.ToolOptions = {}
    end

    return true
end

function C:ValidateToolOptions(Options)
    -- Make sure the options are a table
    if not Options or type(Options) ~= "table" then
        SC.Error("Tool Options table was invalid!", 5)

        return false
    end

    -- Loop over all the options and make sure everything matches the tool options set in the data
    for Name, Value in pairs(Options) do
        -- If the option isn't supposed to be here then the data isn't valid
        local ToolOptions = self.ToolOptions[Name]

        if not ToolOptions then
            SC.Error(Format("No data for tool option %s!", Name), 5)

            return false
        end

        -- Check to make sure the data is of the right type
        if ToolOptions.Type == "Number" then
            -- Make sure it's a number
            if type(Value) ~= "number" then
                SC.Error(Format("Bad data for tool option %s!", Name), 5)

                return false
            end

            -- Make sure it's not too low
            if ToolOptions.Min and Value < ToolOptions.Min then
                SC.Error(Format("Value below minimum for option %s!", Name), 5)

                return false
            end

            -- Make sure it's not too high
            if ToolOptions.Max and Value > ToolOptions.Max then
                SC.Error(Format("Value above maximum for option %s!", Name), 5)

                return false
            end
        elseif ToolOptions.Type == "Color" then
            -- Make sure it's a color
            if type(Value) ~= "table" or not Value.r or not Value.g or not Value.b then
                SC.Error(Format("Bad data for tool option %s!", Name), 5)

                return false
            end
        elseif ToolOptions.Type == "String" then
            -- Make sure it's a string
            if type(Value) ~= "string" then
                SC.Error(Format("Bad data for tool option %s!", Name), 5)

                return false
            end
        elseif ToolOptions.Type == "Vector" then
            -- Make sure it's a vector
            if type(Value) ~= "Vector" then
                SC.Error(Format("Bad data for tool option %s!", Name), 5)

                return false
            end
        elseif ToolOptions.Type == "Angle" then
            -- Make sure it's a angle
            if type(Value) ~= "Angle" then
                SC.Error(Format("Bad data for tool option %s!", Name), 5)

                return false
            end
        end
    end

    return true
end

function C:GetComponentData(Options, Modifiers)
    -- If the options we got from the tool aren't valid them don't return anything
    if not self:ValidateToolOptions(Options) then
        SC.Error("Unable to create component from data, invalid tool options provided", 5)

        return false, {}
    end

    -- This table has the basic data needed to create a component
    local Data = {
        ComponentClass = self.Class,
        Modifiers = table.Copy(Modifiers or {}),
        Mass = self.Mass
    }

    -- Add any special data needed by our component class
    for Name, Value in pairs(self.ClassData) do
        Data[Name] = Value
    end

    -- Set the values of any configured data
    for Name, Info in pairs(self.ToolOptions) do
        Data[Name] = Options[Name] or Info.Default
    end

    return true, Data
end

function C:WriteCreationPacket()
    if not SERVER then return end
    net.WriteString(self.Name)
    net.WriteString(self.Description)
    net.WriteString(self.Class)
    net.WriteString(self.Family)
    net.WriteTable(self.Factions)
    net.WriteBool(self.Hidden)
    net.WriteFloat(self.Mass)
    net.WriteTable(self.ClassData)
    net.WriteTable(self.ToolOptions)
    net.WriteTable(self.ResourceCost)
end

function C:ReadCreationPacket()
    if not CLIENT then return end
    self.Name = net.ReadString()
    self.Description = net.ReadString()
    self.Class = net.ReadString()
    self.Family = net.ReadString()
    self.Factions = net.ReadTable()
    self.Hidden = net.ReadBool()
    self.Mass = net.ReadFloat()
    self.ClassData = net.ReadTable()
    self.ToolOptions = net.ReadTable()
    self.ResourceCost = net.ReadTable()
end

function C:Serialize()
    return {
        Configuration = {
            Name = self.Name,
            Description = self.Description,
            Class = self.Class,
            Family = self.Family,
            Factions = self.Factions,
            Hidden = self.Hidden,
            Mass = self.Mass,
        },
        ClassData = self.ClassData,
        ToolOptions = self.ToolOptions,
        ResourceCost = self.ResourceCost,
    }
end

function C:DeSerialize(Data)
    if Data.Configuration then
        self.Name = Data.Configuration.Name or "Component"
        self.Description = Data.Configuration.Description or ""
        self.Class = Data.Configuration.Class or "ProjectileComponent"
        self.Family = Data.Configuration.Family or "Component"
        self.Factions = Data.Configuration.Factions or {}
        self.Hidden = Data.Configuration.Hidden or false
        self.Mass = Data.Configuration.Mass or 0
    end

    self.ClassData = Data.ClassData or {}
    self.ToolOptions = Data.ToolOptions or {}
    self.ResourceCost = Data.ResourceCost or {}
end

GM.class.registerClass("ProjectileComponentData", C)