local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({

})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("MjolnirEffectComponent")

local glow = {}

if CLIENT then
    glow[1] = GM.MaterialFromVMT(
        "ThunderBallCore1",
        [["UnLitGeneric"
        {
            "$basetexture"		"sprites/orangecore1"
            "$nocull" 1
            "$additive" 1
            "$vertexalpha" 1
            "$vertexcolor" 1
        }]]
    )
    glow[2] = GM.MaterialFromVMT(
        "ThunderBallCore2",
        [["UnLitGeneric"
        {
            "$basetexture"		"sprites/orangecore2"
            "$nocull" 1
            "$additive" 1
            "$vertexalpha" 1
            "$vertexcolor" 1
        }]]
    )
end

function C:init(Parent, NewColor)
	BaseClass.init(self, Parent)

    self:SetColor(NewColor or Color(255, 255, 80))
end

function C:Draw()
    local pos = self:GetOffsetPos()
    local color = self:GetColor()
	local r, g, b = color.r, color.g, color.b

	for i=1,8 do
		local size = math.random(50,75) * self:GetScale()
		render.SetMaterial(glow[math.random(1,2)])
		render.DrawSprite(pos, size, size, Color(r, g, b, 150+math.random(75)))
	end

	for i=1,4 do
		local size = math.random(150,400) * self:GetScale()
		render.SetMaterial(glow[math.random(1,2)])
		render.DrawSprite(pos, size, size, Color(r, g, b, 10+math.random(25)))
	end
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "MjolnirEffectComponent"
end

GM.class.registerClass("MjolnirEffectComponent", C)