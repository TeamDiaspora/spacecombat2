--[[*********************************************
	Tachyon Beam Charge Effect

	Big laz0r

	Author: Lt.Brandon
**********************************************]]--

local function ParentedParticle(p)
	if p.OffsetSpeed then
		local offsetSpeed = p.OffsetSpeed * FrameTime()
		p.Offset = p.Offset + offsetSpeed
	end

	p:SetPos(p.Parent:LocalToWorld(p.Offset))
	p:SetNextThink(CurTime())
end

function EFFECT:Init(Data)
    self.Scale = 0.01
	self.MaxScale = Data:GetScale()
	self.Parent = Data:GetEntity()
	self.Pos = Data:GetOrigin()
	self.Normal = Data:GetNormal()
    self.LocalAngle	= self.Parent:WorldToLocalAngles(Data:GetNormal():Angle()):Forward()
	self.Emitter = ParticleEmitter(self.Pos)
    self.StartTime = CurTime()

    self.NextShockEmit = CurTime()
	self.ShockEmitDelay = 0.1

    self.NextFlareEmit = CurTime()
	self.FlareEmitDelay = 0.1
end

function EFFECT:Think()
	--Cancelled/Failed?
    if not IsValid(self.Parent) or self.Parent:GetChargeLevel() <= 0 then
        self.Emitter:Finish()
		return false
	end

    --Shakey flare
	if CurTime() > self.NextFlareEmit then
		local size = 400 * self.Parent:GetChargeLevel()

		for i=1, 3 do
			local p = self.Emitter:Add("sprites/light_ignorez", self.Parent:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(0.1, 0.15))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(size + math.Rand(-size * 0.5, 100) * self.MaxScale)
			p:SetColor(255, 150, 50)
			p.Offset = self.Pos
			p.Parent = self.Parent
			p:SetThinkFunction(ParentedParticle)
			p:SetNextThink(CurTime())
		end

		for i=1, 4 do
			local p = self.Emitter:Add("effects/flares/light-rays_001", self.Parent:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(0.1, 0.25))
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(size * 2 + math.Rand(-size * 0.5, 100) * self.MaxScale)
			p:SetColor(255, 100, 0)
			p.Offset = self.Pos
			p.Parent = self.Parent
			p:SetThinkFunction(ParentedParticle)
			p:SetNextThink(CurTime())
		end

		self.NextFlareEmit = CurTime() + self.FlareEmitDelay
	end

    --Sparky shaft stuff
	if CurTime() > self.NextShockEmit then
		for i=1, math.ceil(15 * self.Parent:GetChargeLevel()) do
			local mins = self.Parent:OBBMins()
			local maxs = self.Parent:OBBMaxs()
			local pos = Vector(math.Rand(mins.x, maxs.x), math.Rand(mins.y, maxs.y), math.Rand(mins.z, maxs.z))
			local p = self.Emitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), self.Parent:LocalToWorld(pos))
			p:SetDieTime(math.Rand(0.2, 0.5))
			p:SetStartAlpha(0)
			p:SetEndAlpha(255)
			p:SetStartSize(math.random(300, 400) * self.MaxScale * self.Parent:GetChargeLevel())
			p:SetEndSize(0)
			p:SetRoll(math.random(0, 360))
			p:SetRollDelta(math.Rand(-1, 1))
			p:SetColor(255, 150, 0)
			p.Offset = pos
			p.Parent = self.Parent
			p:SetThinkFunction(ParentedParticle)
			p:SetNextThink(CurTime())
		end

		self.NextShockEmit = CurTime() + self.ShockEmitDelay
	end

    local TimeLive = CurTime() - self.StartTime
    if TimeLive > 5 then
        local Scale = self.Scale
        local Normal = self.LocalAngle
        local Emitter = self.Emitter
        --Flare
        for i=1, 30 do
            local velocity = (Normal + (VectorRand() * 0.25)):GetNormalized() * math.Rand(50, 2500)
            local Pos = self.Pos + (VectorRand() * 60 * Scale)
            local p = Emitter:Add("effects/spark", self.Parent:LocalToWorld(Pos))
            p:SetDieTime(math.Rand(0.35, 0.75))
            p:SetStartAlpha(255)
            p:SetEndAlpha(0)
            p:SetStartSize(math.random(5, 15) * Scale)
            p:SetEndSize(0)
            p:SetRoll(math.Rand(0, 360))
            p:SetVelocity(velocity)
            p:SetColor(255, 120, 75)

            p.Offset = Pos
            p.Parent = self.Parent
            p.OffsetSpeed = velocity
            p:SetThinkFunction(ParentedParticle)
            p:SetNextThink(CurTime())
        end

        --Flare
        for i=1, 15 do
            local velocity = (Normal + (VectorRand() * 0.015)):GetNormalized() * math.random(1300,3500)
            local Pos = self.Pos
            local p = Emitter:Add("effects/spark", self.Parent:LocalToWorld(Pos))
            p:SetDieTime(math.Rand(1.00, 1.75))
            p:SetStartAlpha(math.random(100,255))
            p:SetEndAlpha(0)
            p:SetStartSize(math.random(75, 100) * Scale)
            p:SetEndSize(0)
            p:SetRoll(math.Rand(0, 360))
            p:SetColor(255, 70, 0)

            p.Offset = Pos
            p.Parent = self.Parent
            p.OffsetSpeed = velocity
            p:SetThinkFunction(ParentedParticle)
            p:SetNextThink(CurTime())
        end

        --Sparks Long
        for i=1, 15 do
            local velocity = (Normal + (VectorRand() * 0.013)):GetNormalized() * math.Rand(1000, 3000)
            local Pos = self.Pos + (VectorRand() * 35 * Scale)
            local p = Emitter:Add("effects/shipsplosion/sparks_002", self.Parent:LocalToWorld(Pos))
            p:SetDieTime(math.Rand(0.3, 1.65))
            p:SetVelocity(velocity)
            p:SetStartAlpha(255)
            p:SetEndAlpha(0)
            p:SetStartLength(math.Rand(150, 300) * Scale)
            p:SetEndLength(p:GetStartLength() + (math.Rand(15, 17.5) * Scale))
            p:SetStartSize(math.Rand(50, 100) * Scale)
            p:SetEndSize(p:GetStartSize() + (math.Rand(200, 350) * Scale))
            --p:SetRoll(math.Rand(0, 360))
            p:SetColor(255, 120, math.random(0,100))

            p.Offset = Pos
            p.Parent = self.Parent
            p.OffsetSpeed = velocity
            p:SetThinkFunction(ParentedParticle)
            p:SetNextThink(CurTime())
        end

        --Sparks Long
        for i=1, 35 do
            local velocity = (Normal + (VectorRand() * 0.015)):GetNormalized() * math.random(1300,2400)
            local Pos = self.Pos + (VectorRand() * 35 * Scale)
            local p = Emitter:Add("effects/shipsplosion/sparks_002", self.Parent:LocalToWorld(Pos))
            p:SetDieTime(math.Rand(2, 4))
            p:SetStartAlpha(255)
            p:SetEndAlpha(0)
            p:SetStartLength(math.Rand(150, 500) * Scale)
            p:SetEndLength(p:GetStartLength() + (math.Rand(150, 300) * Scale))
            p:SetStartSize(math.Rand(60, 120) * Scale)
            p:SetEndSize(p:GetStartSize() + (math.Rand(250, 350) * Scale))
            --p:SetRoll(math.Rand(0, 360))
            p:SetColor(255, 255, math.random(0,100))

            p.Offset = Pos
            p.Parent = self.Parent
            p.OffsetSpeed = velocity
            p:SetThinkFunction(ParentedParticle)
            p:SetNextThink(CurTime())
        end

        self.Scale = math.min(Scale + (Scale * 0.0100 + 0.015 * FrameTime()), self.MaxScale)
    end

    return true
end

function EFFECT:Render( )
	return false
end