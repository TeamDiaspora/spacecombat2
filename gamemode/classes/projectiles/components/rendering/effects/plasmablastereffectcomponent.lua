local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("PlasmaBlasterEffectComponent")

local TimeScale = 0.25
function C:Think()
    local Parent = self:GetParent()
    local Normal = Parent:GetAngles():Forward()
    local Pos = self:GetOffsetPos()
    local MyVelocity = vector_origin

    local MovementComponent = Parent:GetMovementComponent()
    if IsValid(MovementComponent) then
        MyVelocity = MovementComponent:GetVelocity()
    end

    local Emitter = self:GetEmitter()
    local Scale = self:GetScale()

 	--Flare
    for i=1, 8 do
        local velocity = (-Normal + (VectorRand() * 0.01)):GetNormalized() * math.Rand(200, 1800)
    	local p = Emitter:Add("effects/spark", Pos + (VectorRand() * 25 * Scale))
    	p:SetDieTime(math.Rand(0.35, 0.45) * Scale)
    	p:SetStartAlpha(255)
    	p:SetEndAlpha(0)
    	p:SetStartSize(math.random(10, 50) * Scale)
    	p:SetEndSize(0)
    	p:SetRoll(math.Rand(0, 360))
    	p:SetVelocity(MyVelocity + velocity * Scale)
    	p:SetColor(self:GetColor().r, self:GetColor().g, self:GetColor().b)
    end

    --Flare
    for i=1, 4 do
    	local p = Emitter:Add("effects/spark", Pos)
    	p:SetDieTime(math.Rand(0.35, 0.45) * Scale)
    	p:SetStartAlpha(math.random(100,255))
    	p:SetEndAlpha(0)
    	p:SetStartSize(math.random(75, 100) * Scale)
    	p:SetEndSize(0)
    	p:SetRoll(math.Rand(0, 360))
    	p:SetVelocity(MyVelocity + -Normal * 500 * Scale)
    	p:SetColor(self:GetColor().r, self:GetColor().g, self:GetColor().b)
    end

    --Sparks Long
    for i=1, 4 do
    	local velocity = (-Normal + (VectorRand() * 0.03)):GetNormalized() * math.Rand(200, 1800)

    	local p = Emitter:Add("effects/shipsplosion/sparks_002", Pos + (VectorRand() * 35 * Scale))
    	p:SetDieTime(math.Rand(0.3, 0.85) * Scale)
    	p:SetVelocity(MyVelocity + velocity * Scale)
    	p:SetStartAlpha(255)
    	p:SetEndAlpha(0)
    	p:SetStartLength(math.Rand(7.5, 15) * Scale)
    	p:SetEndLength(p:GetStartLength() + (math.Rand(15, 17.5) * Scale))
    	p:SetStartSize(math.Rand(25, 50) * Scale)
    	p:SetEndSize(p:GetStartSize() + (math.Rand(20, 35) * Scale))
    	p:SetRoll(math.Rand(0, 360))
    	p:SetColor(math.Rand(self:GetColor().r, self:GetColor().g), math.Rand(self:GetColor().g, self:GetColor().b), math.Rand(self:GetColor().b, self:GetColor().r))
    end
end

-- This effect is only particles, don't need to draw anything
function C:ShouldDraw()
    return false
end

function C:GetComponentClass()
    return "PlasmaBlasterEffectComponent"
end

GM.class.registerClass("PlasmaBlasterEffectComponent", C)