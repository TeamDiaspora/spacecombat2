-- This is an example ProjectileComponent that prints "Hello World!" to both the server and client console.
-- It also networks a simple variable.

local GM = GM
---@type ProjectileComponent
local BaseClass = GM.class.getClass("ProjectileComponent")

---@class ExampleComponent : ProjectileComponent
local C = BaseClass:extends({
    -- Add default variables here
    ThinkEnabled = true,
    MySerializedNumber = 0
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("ExampleComponent")

function C:init()
    -- We only want the server to decide what our number is.
    if SERVER then
        -- Choose a random number between 1 and 100
        self.MySerializedNumber = math.Rand(1, 100)
    end
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.'
function C:ShouldThink()
    -- Check if self.MySerializedNumber is 0, if it is then we don't want to run yet!
    return self.ThinkEnabled and self.MySerializedNumber ~= 0
end

function C:Think()
    -- Print Hello World! and tell them what our number is.
    print("Hello World! My number is: "..tostring(self.MySerializedNumber))

    -- Disable thinking so we don't do this again.
    self.ThinkEnabled = false
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return true
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "ExampleComponent"
end

-- This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end

    net.WriteFloat(self.MySerializedNumber)
end

-- This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end

    self.MySerializedNumber = net.ReadFloat()
end

-- This function is used to save information about a projectile.
function C:Serialize()
    -- Call the Serialize function on the base class first!
    local Out = self:super("Serialize")

    -- Now add our data to it!
    Out.MySerializedNumber = self.MySerializedNumber

    -- Return the table so it gets networked
    return Out
end

-- This function takes a data table and applies it to the object
function C:DeSerialize(Data)
    -- Call the DeSerialize function from the base class first!
    self:super("DeSerialize", Data)

    -- Now let's set our number!
    self.MySerializedNumber = Data.MySerializedNumber
end

-- Make sure you register your class so it can be created!
GM.class.registerClass("ExampleComponent", C)