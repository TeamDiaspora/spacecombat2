local GM = GM
---@type ProjectileComponent
local BaseClass = GM.class.getClass("ProjectileComponent")

---@class BeamCollisionComponent : ProjectileComponent
local C = BaseClass:extends({
    Length = 0,
    LastHitLength = 0,
    UpdateRate = 0.1,
    NextThink = 0,
    Pulse = false,
    HasChecked = false
})

function C:init(Length, UpdateRate, Pulse)
    self.Length = Length or 0
    self.UpdateRate = UpdateRate or 0.1
    self.Pulse = Pulse or false
end

function C:SetParent(NewParent)
    BaseClass.SetParent(self, NewParent)

    if IsValid(self:GetParent()) then
        self:GetParent():SetCollisionComponent(self)
    end
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return true
end

-- If this returns true then the C:Think function will be called later than normal
-- Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return true
end

function C:Think()
    if CurTime() < self.NextThink or (self.Pulse and self.HasChecked) then
        return
    end

    local Parent = self:GetParent()
    local Data = {}
    Data.start = Parent:GetPos()
    Data.endpos = Parent:GetPos() + (Parent:GetAngles():Forward() * (self.Length > 0 and self.Length or 40000))
    Data.filter = {Parent:GetLauncherEntity()}
    local Trace = util.TraceLine(Data)

    if Trace.Hit then
        self.LastHitLength = Trace.Fraction * self.Length
        Parent:FireEvent("Collision", Trace)
    else
        self.LastHitLength = self.Length
    end

    self.HasChecked = true
    self.NextThink = CurTime() + self.UpdateRate

    if self.Pulse then
        timer.Simple(self.UpdateRate, function()
            if IsValid(Parent) then
                Parent:Remove()
            end
        end)
    end
end

function C:Serialize()
    local Data = BaseClass.Serialize(self) or {}

    Data.Length = self.Length
    Data.UpdateRate = self.UpdateRate
    Data.Pulse = self.Pulse

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)

    self.Length = Data.Length or 0
    self.UpdateRate = Data.UpdateRate or 0.1
    self.Pulse = Data.Pulse or false
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return false
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "BeamCollisionComponent"
end

-- Make sure you register your class so it can be created!
GM.class.registerClass("BeamCollisionComponent", C)