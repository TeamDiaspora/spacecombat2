local GM = GM

---@type DamageComponent
local BaseClass = GM.class.getClass("DamageComponent")

---@class KineticDamageComponent : DamageComponent
local C = BaseClass:extends({

})

local KineticDamageMultiplier = 0.025
hook.Remove("SC.Config.Register", "SC.LoadSC2Config")
hook.Add("SC.Config.Register", "SC.LoadSC2Config", function()
    local Config = GM.Config

    Config:Register({
        File = "spacecombat",
        Section = "Combat",
        Key = "KineticDamageMultiplier",
        Default = 0.025,
        ConVar = "sc_kineticdamagemultiplier",
        OnChanged = function(File, Section, Key, NewValue)
            KineticDamageMultiplier = NewValue
        end
    })
end)

function C:GetDamage()
    local Parent = self:GetParent()
    if not IsValid(Parent) then
        SC.Error("Warning: KineticDamageComponent has no parent!", 5)
        return {KIN=0}
    end

    local MovementComponent = self:GetParent():GetMovementComponent()
    if not MovementComponent then
        SC.Error("Warning: KineticDamageComponent has no movement component!", 5)
        return {KIN=0}
    end

    return {KIN = Parent:GetMass() * MovementComponent:GetVelocity():Length() * KineticDamageMultiplier}
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "KineticDamageComponent"
end

GM.class.registerClass("KineticDamageComponent", C)