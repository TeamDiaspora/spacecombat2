local GM = GM
local ClearedIDs = {}
---@type Projectile[]
local Projectiles = {}
local ComponentNetworkIDs = {}
local ComponentReverseLookup = {}

GM.Projectiles = GM.Projectiles or {}

--- Get a table of all projectiles
---@return Projectile[]
---@nodiscard
function GM.Projectiles.GetAllProjectiles()
    return Projectiles
end

--- Get a specific projectile
---@return Projectile # A weak reference to the projectile
---@nodiscard
function GM.Projectiles.GetProjectile(ID)
    return SC.MakeWeakRef(Projectiles[ID])
end

-- Find all projectiles near a location
---@return Projectile[]
---@nodiscard
function GM.Projectiles.FindProjectilesNearPosition(Position, Distance)
    local Found = {}
    local SqrDist = Distance * Distance
    for i,k in pairs(Projectiles) do
        if k:GetPos():DistToSqr(Position) <= SqrDist then
            table.insert(Found, k)
        end
    end

    return Found
end

function GM.Projectiles.RegisterNetworkedComponent(Name)
    if type(Name) ~= "string" then error("Error! Name must be a string!") end

    local NewID = table.Count(ComponentNetworkIDs) + 1
    ComponentNetworkIDs[Name] = NewID
    ComponentReverseLookup[NewID] = Name
    return NewID
end

function GM.Projectiles.GetComponentNetworkID(Name)
    if not ComponentNetworkIDs[Name] then error("Unable to find component Network ID: "..Name) end
    return ComponentNetworkIDs[Name]
end

function GM.Projectiles.GetComponentClassFromID(ID)
    if not ComponentReverseLookup[ID] then error("Unable to find component with Network ID: "..ID) end
    return ComponentReverseLookup[ID]
end

local ProjectileCount = 0
local function GetNetUpdateDelay()
    return 1 / math.min(33, 1 / (ProjectileCount / 300))
end

if SERVER then
    util.AddNetworkString("SC.ProjectileNetworkedEvent")
    util.AddNetworkString("SC.ProjectileCreated")
    util.AddNetworkString("SC.ProjectileNetworkUpdate")
else
    net.Receive("SC.ProjectileNetworkedEvent", function()
        local Projectile = GM.Projectiles.GetProjectile(net.ReadUInt(16))

        local Data
        local Event = SC.ReadNetworkedString("", function(ID, String)
            if SC.IsValidWeakRef(Projectile) then
                Projectile:FireEvent(String, Data)
            end
        end)

        Data = net.ReadTable()

        if SC.IsValidWeakRef(Projectile) and Event ~= "" then
            Projectile:FireEvent(Event, Data)
        end
    end)

    net.Receive("SC.ProjectileCreated", function()
        local Projectile = GM.class.new("Projectile")
        Projectile:ReadCreationPacket()
    end)

    net.Receive("SC.ProjectileNetworkUpdate", function()
        local id = net.ReadUInt(16)
        local Projectile = GM.Projectiles.GetProjectile(id)
        if not SC.IsValidWeakRef(Projectile) then return end
        Projectile:ReadNetworkUpdate()
    end)
end

---@class Projectile : LCSClass
---@field MovementComponent MovementComponent
---@field TargetingComponent TargetingComponent
---@field CollisionComponent CollisionComponent
---@field LauncherEntity entity
---@field Components ProjectileComponent[]
---@field ThinkingComponents ProjectileComponent[]
---@field LateThinkingComponents ProjectileComponent[]
---@field Launcher Launcher
---@field NetworkedPlayers player[]
local C = GM.LCS.class({
    ID = 0,
    Position = Vector(),
    Angles = Angle(),
    Components = nil,
    Owner = NULL,
    NetworkedPlayers = nil,
    NextNetUpdate = 0,
    Removed = false,
    ShouldSendNetUpdates = false,
    MovementComponent = nil,
    TargetingComponent = nil,
    CollisionComponent = nil,
    Launcher = nil,
    LauncherEntity = NULL,
    Mass = 0,
    LastNetworkedPos = vector_origin,
    LastNetworkedAng = angle_zero,
    ThinkingComponents = nil,
    LateThinkingComponents = nil
})

function C:init(Position, Angles, Components, ID)
    self:SetPos(Position or Vector(0, 0, 0))
    self:SetAngles(Angles or Angle(0, 0, 0))
    self:SetComponents(Components or {})
    self.ThinkingComponents = {}
    self.LateThinkingComponents = {}

    ProjectileCount = ProjectileCount + 1

    if CLIENT then
        if not ID then return end
        Projectiles[ID] = self
        self.ID = ID
    else
        -- If we hit this then init already ran once and we shouldn't do this again
        if self.ID ~= 0 then return end

        -- Only the server can decide our ID, projectiles should never be created on the client directly
        self.ID = #ClearedIDs > 0 and table.remove(ClearedIDs) or (#Projectiles + 1)

        Projectiles[self.ID] = self
    end
end

function C:describe()
    if IsValid(self) then
        return string.format("Projectile: %s", tostring(self:GetID()))
    else
        return "Projectile: Invalid"
    end
end

function C:SendToClient()
    if not SERVER then return end
    local SendToPlayers = {}
    for _, Player in ipairs(player.GetHumans()) do
        if Player:TestPVS(self:GetPos()) then
            table.insert(SendToPlayers, Player)
        end
    end

    self.NetworkedPlayers = SendToPlayers

    net.Start("SC.ProjectileCreated", false)
    self:WriteCreationPacket()
    net.Send(SendToPlayers)
end

function C:WriteCreationPacket()
    if not SERVER then return end
    net.WriteVector(self:GetPos(), false)
    net.WriteAngle(self:GetAngles())
    net.WriteUInt(self:GetID(), 16)
    net.WriteFloat(self.Mass)
    net.WriteEntity(self:GetLauncherEntity())

    -- Write all of the component's to the packet
    for i,k in ipairs(self:GetComponents()) do
        if k:IsClientside() then
            net.WriteUInt(GM.Projectiles.GetComponentNetworkID(k:GetComponentClass()), 12)
            k:WriteCreationPacket()
        end
    end

    -- Signifies that all components were read
    net.WriteUInt(0, 12)
end

function C:ReadCreationPacket()
    if not CLIENT then return end

    self:init(net.ReadVector(), net.ReadAngle(), {}, net.ReadUInt(16))
    self.Mass = net.ReadFloat()
    self.LauncherEntity = net.ReadEntity()

    -- Create components until we hit an invalid ID
    while true do
        local ComponentNetworkID = net.ReadUInt(12)
        if ComponentNetworkID == 0 then break end

        local ComponentClass = GM.Projectiles.GetComponentClassFromID(ComponentNetworkID)
        local Component = GM.class.new(ComponentClass)
        self:AddComponent(Component)
        Component:ReadCreationPacket()
        Component:PostComponentInit()

        if Component:ShouldThink() then
            self:RegisterComponentThink(Component)
        end
    end

    self:Spawn()
end

function C:FireEvent(Event, Info)
    for i,k in ipairs(self:GetComponents()) do
        k:OnProjectileEvent(Event, Info)
    end

    if CLIENT and Event == "Removed" then
        self:Remove()
    end
end

function C:FireNetworkedEvent(Event, Info, Reliable)
    if not SERVER then return end

    self:FireEvent(Event, Info)

    net.Start("SC.ProjectileNetworkedEvent", Reliable or false)
    net.WriteUInt(self:GetID(), 16)
    SC.WriteNetworkedString(Event)
    net.WriteTable(Info)
    net.Send(self.NetworkedPlayers)
end

function C:ReadNetworkUpdate()
    if not CLIENT then return end

    if net.ReadBool() then
        self:SetPos(net.ReadVector())
    end

    if net.ReadBool() then
        self:SetAngles(net.ReadAngle())
    end

    for i,k in ipairs(self:GetComponents()) do
        k:ReadNetworkUpdate()
    end
end

local NetPosUpdateDelta = 500 * 500
function C:SendNetworkUpdate()
    if not SERVER then return end

    if self.ShouldSendNetUpdates and self.NextNetUpdate <= CurTime() then
        net.Start("SC.ProjectileNetworkUpdate", false)
        net.WriteUInt(self:GetID(), 16)

        if self:GetPos():DistToSqr(self.LastNetworkedPos) > NetPosUpdateDelta then
            net.WriteBool(true)
            net.WriteVector(self:GetPos(), false)
        else
            net.WriteBool(false)
        end

        if self:GetAngles() ~= self.LastNetworkedAng then
            net.WriteBool(true)
            net.WriteAngle(self:GetAngles())
        else
            net.WriteBool(false)
        end

        for i,k in ipairs(self:GetComponents()) do
            k:SendNetworkUpdate()
        end

        net.Send(self.NetworkedPlayers)

        self.NextNetUpdate = CurTime() + GetNetUpdateDelay()
    end
end

function C:GetLauncherEntity()
    if SERVER and IsValid(self.Launcher) then
        return self.Launcher:GetAttachedEntity()
    else
        return self.LauncherEntity
    end
end

function C:SetLauncherEntity(NewLauncherEntity)
    if IsValid(self.LauncherEntity) then
        SC.Error("Warning! Overriding LauncherEntity for projectile! (Are you sure this is right?)", 5)
    end

    self.LauncherEntity = NewLauncherEntity
end

function C:GetLauncher()
    return self.Launcher
end

function C:SetLauncher(NewLauncher)
    if not IsValid(NewLauncher) then
        return
    end

    if IsValid(self.Launcher) then
        SC.Error("Warning! Overriding Launcher for projectile! (Are you sure this is right?)", 5)
    end

    self.Launcher = SC.MakeWeakRef(NewLauncher)
    self:SetLauncherEntity(self.Launcher:GetAttachedEntity())
end

function C:GetMovementComponent()
    return self.MovementComponent
end

function C:SetMovementComponent(NewMovementComponent)
    if self.MovementComponent then
        SC.Error("Warning! Overriding movement component for projectile! (Are you sure this is right?)", 5)
    end

    self.MovementComponent = SC.MakeWeakRef(NewMovementComponent)
end

function C:GetTargetingComponent()
    return self.TargetingComponent
end

function C:SetTargetingComponent(NewTargetingComponent)
    if self.TargetingComponent then
        SC.Error("Warning! Overriding targeting component for projectile! (Are you sure this is right?)", 5)
    end

    self.TargetingComponent = SC.MakeWeakRef(NewTargetingComponent)
end

function C:GetCollisionComponent()
    return self.CollisionComponent
end

function C:SetCollisionComponent(NewCollisionComponent)
    if self.CollisionComponent then
        SC.Error("Warning! Overriding collision component for projectile! (Are you sure this is right?)", 5)
    end

    self.CollisionComponent = SC.MakeWeakRef(NewCollisionComponent)
end

function C:CalculateMass()
    if CLIENT then return end
    local Mass = 0
    for i,k in ipairs(self:GetComponents()) do
        Mass = Mass + k:GetMass()
    end

    self.Mass = Mass
end

function C:GetMass()
    return self.Mass
end

function C:SetPos(NewPos)
    self.Position = NewPos
end

function C:GetPos()
    return self.Position
end

function C:SetAngles(NewAngles)
    self.Angles = NewAngles
end

function C:GetAngles()
    return self.Angles
end

function C:AddComponent(Component)
    table.insert(self.Components, Component)
    Component:SetParent(self)
end

function C:RemoveComponent(Component)
    table.remove(self.Components, Component)
    Component:SetParent(nil)
end

function C:SetComponents(Components)
    self.Components = Components

    for i,k in ipairs(self.Components) do
        k:SetParent(self)
    end
end

function C:GetComponents()
    return self.Components
end

function C:GetID()
    return self.ID
end

function C:GetOwner()
    return self.Owner
end

function C:SetOwner(NewOwner)
    self.Owner = NewOwner
end

function C:Remove()
    if not IsValid(self) then return end

    self:OnRemoved()

    Projectiles[self:GetID()] = nil
    table.insert(ClearedIDs, self:GetID())

    self:FireNetworkedEvent("Removed", {})
    self.ID = 0
    self.Removed = true

    ProjectileCount = ProjectileCount - 1

    for i,k in pairs(self.Components) do
        k:SetParent(nil)
    end

    self.Components = {}
    self.ThinkingComponents = {}
    self.LateThinkingComponents = {}
    self.NetworkedPlayers = {}
end

function C:OnRemoved()

end

function C:Spawn()
    if self.Initialized then return end

    self:CalculateMass()
    self.Initialized = true
    self:FireEvent("Initialized", {})

    if SERVER then
        self:SendToClient()
    end
end

function C:IsValid()
    return self:GetID() ~= 0 and not self.Removed and self.Initialized
end

function C:RegisterComponentThink(Component)
    if Component:UsesLateThink() then
        return table.insert(self.LateThinkingComponents, SC.MakeWeakRef(Component))
    else
        return table.insert(self.ThinkingComponents, SC.MakeWeakRef(Component))
    end
end

function C:UnregisterComponentThink(Component)
    local RemoveFrom
    if Component:UsesLateThink() then
        RemoveFrom = self.LateThinkingComponents
    else
        RemoveFrom = self.ThinkingComponents
    end

    local ToRemove
    for i, k in ipairs(RemoveFrom) do
        if k == Component then
            ToRemove = i
            break
        end
    end

    if ToRemove ~= nil then
        table.remove(RemoveFrom, ToRemove)
    end
end

local MaxDistance = 32000 * 32000
function C:Think()
    for i,k in ipairs(self.ThinkingComponents) do
        k:Think()

        if not IsValid(self) then
            return
        end
    end

    for i,k in ipairs(self.LateThinkingComponents) do
        k:Think()

        if not IsValid(self) then
            return
        end
    end

    if SERVER then
        self:SendNetworkUpdate()

        if not util.IsInWorld(self:GetPos()) then
            self:Remove()
        end
    else
        -- IsInWorld doesn't exist on the client, and sometimes during extreme lag they can escape the map
        if self:GetPos():LengthSqr() > MaxDistance then
            self:Remove()
        end
    end
end


-- FIXME: In theory this code can result in the Projectiles table being modified while it's being iterated, which might cause weird issues
local ThinkCleanup = {}
hook.Add("Think", "SC2ProjectileThink", function()
    local CleanupNeeded = false
    for i,k in pairs(Projectiles) do
        if IsValid(k) then
            k:Think()
        elseif k ~= nil and k.Removed then
            CleanupNeeded = true
            table.insert(ThinkCleanup, i)
        end
    end

    if CleanupNeeded then
        for i,k in ipairs(ThinkCleanup) do
            Projectiles[i] = nil
        end
        ThinkCleanup = {}
    end
end)

function C:ShouldDraw()
    return CLIENT
end

function C:Draw()
    if not CLIENT then return end
    for i,k in ipairs(self:GetComponents()) do
        k:Draw()
    end
end

hook.Add("PreDrawEffects", "SC2ProjectileDraw", function()
    for i,k in pairs(Projectiles) do
        if IsValid(k) and k:ShouldDraw() then
            k:Draw()
        end
    end
end)

function C:Serialize()
    local SerializedComponents = {}
    for i,k in ipairs(self.Components) do
        table.insert(SerializedComponents, k:Serialize())
    end

    return {Position=self.Position, Angles=self.Angles, Components=SerializedComponents, ID=self.ID, Mass=self.Mass, Owner=self.Owner, LauncherEntity=self:GetLauncherEntity()}
end

function C:DeSerialize(Data)
    self:init(Data.Position, Data.Angles, {}, Data.ID)
    self:SetOwner(Data.Owner)
    self.Mass = Data.Mass or 0
    self.LauncherEntity = Data.LauncherEntity or NULL

    local WantsNetworkUpdate = false
    for i,k in ipairs(Data.Components) do
        local Component = GM.class.new(k.ComponentClass)
        self:AddComponent(Component)
        Component:DeSerialize(k)
        Component:PostComponentInit()

        if Component:ShouldThink() then
            self:RegisterComponentThink(Component)
        end

        if Component:WantsNetworkUpdate() then
            WantsNetworkUpdate = true
        end
    end

    if WantsNetworkUpdate then
        self.ShouldSendNetUpdates = true
    end
end

GM.class.registerClass("Projectile", C)