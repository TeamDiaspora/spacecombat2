local GM = GM

local C = GM.class.getClass("Launcher"):extends({
    -- Our current projectile (we can only have 1)
    BeamProjectile = nil,

    -- Cycle Time
    HasCycleTime = false,
    CycleLength = 30
})

local BaseClass = C:getClass()

function C:init()
    BaseClass.init(self)
end

function C:IsProjectileRecipeCompatible(Recipe)
    return BaseClass.IsProjectileRecipeCompatible(self, Recipe)
end

function C:SetLauncherType(Type)
    local LauncherData = BaseClass.SetLauncherType(self, Type)
    if LauncherData then

    end
end

function C:ApplyProjectileVelocity(Projectile)
    -- All beams must use a localized movement component
    local MovementComponent = Projectile:GetMovementComponent()
    if not MovementComponent then
        MovementComponent = GAMEMODE.class.new("LocalizedMovementComponent")
        Projectile:AddComponent(MovementComponent)
    end
end

function C:RemoveBeam()
    if IsValid(self.BeamProjectile) then
        self.BeamProjectile:Remove()
        self.BeamProjectile = nil
    end
end

function C:FireBurst()
    -- Fire the first shot of the burst immediately
    self:Fire()

    -- Then queue up the rest of the shots
    for I=1, self.ShotsPerBurst - 1 do
        timer.Simple(I * self.TimeBetweenBurstShots, function()
            if IsValid(self) then
                self:Fire()
            end
        end)

        timer.Simple(I * self.TimeBetweenBurstShots + (self.TimeBetweenBurstShots * 0.9), function()
            if IsValid(self) then
                self:RemoveBeam()
            end
        end)
    end
end

function C:Fire()
    if IsValid(self.BeamProjectile) then
        if not self:ConsumeResources() then
            self:RemoveBeam()
        end

        return
    end

    self.BeamProjectile = BaseClass.Fire(self)
    return self.BeamProjectile
end

function C:OnReloadingStarted()
    self:RemoveBeam()
end

function C:OnStoppedFiring()
    self:RemoveBeam()
end

function C:OnOverheated()
    self:RemoveBeam()
end

function C:OnRemoved()
    self:RemoveBeam()
end

GM.class.registerClass("Launcher_Beam", C)