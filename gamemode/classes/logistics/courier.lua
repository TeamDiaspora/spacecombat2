local GM = GM

---@class LogisticsCourier
---@field ID integer #The unique ID of this courier in the global Courier table
---@field Available boolean #A flag indicating that the courier is available to have tasks assigned to it
---@field Owner entity #The player that owns this courier
---@field Parent entity|table #Whatever this courier is attached to
---@field TaskQueue table #A table of tasks for the courier to complete
---@field MaxTasks integer #The maximum number of tasks that can be added to the courier's task queue
---@field StorageSize integer #How much storage capacity the courier has
---@field OnTaskAddedCallback function #Called when a task is added to the courier
---@field OnRequestAvailableNode function #Called when the courier is asked for a list of nodes that it can service
---@field OnGetIsAvailable function #Called to check if the courier is available to receive tasks
---@field OnGetPos function #Called to get the position of the courier
local C = GM.LCS.class({
    ID = 0,
    Available = false,
    Owner = nil,
    Parent = nil,
    TaskQueue = nil,
    MaxTasks = 1,

    StorageSize = 1000,

    OnTaskAddedCallback = function(Courier, Task) end,
    OnRequestAvailableNodes = function(Courier) end,
    OnGetIsAvailable = function(Courier) return true end,
    OnGetPos = function(Courier) return Vector(0,0,0) end
})

function C:init()
    self.TaskQueue = {}

    GAMEMODE:GetLogisticsManager():RegisterCourier(self)
end

function C:IsValid()
    return IsValid(self.Parent) and self.ID ~= 0
end

function C:Remove()
    self:DiscardTasks()
    GAMEMODE:GetLogisticsManager():UnregisterCourier(self)
end

--#region Member Field Accessors
function C:SetID(NewID)
    self.ID = NewID
end

function C:GetID()
    return self.ID
end

function C:SetIsAvailable(Available)
    self.Available = Available
end

function C:GetIsAvailable()
    local Available = self.Available and (#self.TaskQueue < self.MaxTasks) and self:OnGetIsAvailable()

    return Available
end

function C:SetOwner(Owner)
    self.Owner = Owner
end

function C:GetOwner()
    return self.Owner
end

function C:SetParent(Parent)
    self.Parent = Parent
end

function C:GetParent()
    return self.Parent or self
end

function C:SetMaxTasks(MaxTasks)
    self.MaxTasks = MaxTasks
end

function C:SetStorageSize(Size)
    self.StorageSize = Size
end

function C:GetTasks()
    return self.TaskQueue
end

function C:SetAvailabilityCheck(Func)
    self.AvailabilityCheck = Func
end

--#endregion
--#region Node management

---Get a table of nodes that are servicable by this courier
---@return table
function C:GetAvailableNodes()
    return self:OnRequestAvailableNodes()
end

--#endregion
--#region Task Management

---The logisitcs task is created in the courier from data provided by the logistics manager
---@class LogisticsTask : table
---@field SupplyNode LogisticsNode #The node to visit to pull the resource from
---@field DemandNode LogisticsNode #The node to visit to push the resources to
---@field ResourceName string #The name of the resource for this transaction
---@field Amount integer #How much of the resource ot transfer

---Add a new task to the courier
---@param SupplyNode LogisticsNode #The node to visit to pull the resources from
---@param DemandNode LogisticsNode #The node to visit to push the resources to
---@param ResourceName string #The name of the resource to transfer
---@param ResourceAmount integer #The amount of a resource to transfer
function C:AddTask(SupplyNode, DemandNode, ResourceName, ResourceAmount)
    ---@type LogisticsTask
    local NewTask = {
        SupplyNode = SupplyNode,
        DemandNode = DemandNode,
        ResourceName = ResourceName,
        Amount = ResourceAmount
    }

    table.insert(self.TaskQueue, NewTask)

    self:OnTaskAddedCallback(NewTask)
end

---Pull a task out of the task queue, FIFO
---@return LogisticsTask
function C:PullTask()
    return table.remove(self.TaskQueue, 1)
end

---Remove all tasks from the task queue and reset all incoming/outgoing resource amounts for the nodes
function C:DiscardTasks()
    while #self.TaskQueue > 0 do
        local Task = self:PullTask()
        local ResName = Task.ResourceName
        local ResAmount = Task.Amount
        Task.SupplyNode:SubtractOutbound(ResName, ResAmount, self.ID)
        Task.DemandNode:SubtractInbound(ResName, ResAmount, self.ID)
    end
end

--#endregion
--#region Resource Management

function C:GetMaxCarryableAmount(ResourceName)
    local Resource = GAMEMODE:GetResourceFromName(ResourceName)
    if not Resource then return 0 end
    local ResourceSize = Resource:GetSize()
    return math.floor(self.StorageSize / ResourceSize)
end

--#endregion
--#region Entity-Like Member Methods

---Get the position of the courier
---@return vector CourierPosition
function C:GetPos()
    return self:OnGetPos()
end
--#endregion

GM.class.registerClass("LogisticsCourier", C)