--[[

	Developed By Dubby

	Copyright (c) Dubby [tiniuc@hotmail.com] 2010

]]--

-- This addresses a few problems with some of the util functions.

module( "util", package.seeall )

local IsValidModelOld = util.IsValidModel
local IsValidPropOld = util.IsValidProp

--door table
local Door_Models = { "models/smallbridge/sents/sbadoors1a.mdl", "models/smallbridge/sents/sbadoors1.mdl", "models/smallbridge/sents/sbadoors2a.mdl", "models/smallbridge/sents/sbadoors2.mdl",
"models/smallbridge/sents/sbadoort.mdl", "models/smallbridge/sents/sbadoorsirisa.mdl", "models/smallbridge/sents/sbadoorsiris.mdl", "models/smallbridge/sents/sbadoorwb.mdl",
"models/smallbridge/sents/sbadoorwa.mdl", "models/smallbridge/sents/sbadoorla.mdl", "models/smallbridge/sents/sbadoorl.mdl", "models/slyfo/slYadoor1.mdl", "models/smallbridge/sents/sbadoordbs.mdl",
"models/smallbridge/sents/sbahulldse.mdl", "models/smallbridge/sents/sbahatchelevs.mdl", "models/smallbridge/sents/sbahatchelevl.mdl", "models/cerus/modbridge/misc/doors/door11a_anim.mdl",
"models/cerus/modbridge/misc/doors/door11b_anim.mdl", "models/cerus/modbridge/misc/doors/door12b_anim.mdl", "models/cerus/modbridge/misc/doors/door12a_anim.mdl",
"models/cerus/modbridge/misc/doors/door13a_anim.mdl", "models/cerus/modbridge/misc/doors/door23a_anim.mdl", "models/cerus/modbridge/misc/doors/door33a_anim.mdl",
"models/cerus/modbridge/misc/accessories/acc_furnace1_anim.mdl" }

function IsValidModel( mdl )
	if table.HasValue( Door_Models, mdl ) then return true end
	return IsValidModelOld( mdl )
end

function IsValidProp( mdl )
	if table.HasValue( Door_Models, mdl ) then return true end
	return IsValidPropOld( mdl )
end