local GM = GM

hook.Remove("SC.Config.Register", "SC.LoadSC2Config")
hook.Add("SC.Config.Register", "SC.LoadSC2Config", function()
    local Config = GM.Config

    Config:Register({
        File = "spacecombat",
        Section = "Combat",
        Key = "EnableDamageProtection",
        Default = false
    })

    Config:Register({
        File = "spacecombat",
        Section = "Combat",
        Key = "LimitShipVolumeToClass",
        Default = true,
        ConVar = "sc_limitshipvolumetoclass"
    })

    Config:Register({
        File = "spacecombat",
        Section = "Combat",
        Key = "LightClassesUseVolume",
        Default = true,
        ConVar = "sc_lightclassesusevolume"
    })

    Config:Register({
        File = "spacecombat",
        Section = "Combat",
        Key = "GlobalDamageMultiplier",
        Default = 0.05,
        ConVar = "sc_globaldamagemultiplier",
        OnChanged = function(File, Section, Key, NewValue)
            SC.GlobalDamageMultiplier = NewValue
        end
    })

    Config:Register({
        File = "spacecombat",
        Section = "General",
        Key = "EnableSandboxMode",
        Default = true
    })

    Config:Register({
        File = "spacecombat",
        Section = "General",
        Key = "EnableUnlimitedResources",
        Default = false
    })

    Config:Register({
        File = "spacecombat",
        Section = "Compatibility",
        Key = "EnableSpacebuildMapCompatibility",
        Default = true
    })

    Config:Register({
        File = "spacecombat",
        Section = "Compatibility",
        Key = "ForceHabitableEnvironmentIfNoPlanets",
        Default = true
    })

    Config:Register({
        File = "spacecombat",
        Section = "Network",
        Key = "LightDistance",
        Default = 8192
    })

    Config:Register({
        File = "spacecombat",
        Section = "Debug",
        Key = "EnableDebugMode",
        Default = false,
        ConVar = "sc_debug",
        OnChanged = function(File, Section, Key, NewValue)
            SC.Debug = NewValue
        end
    })

    Config:Register({
        File = "spacecombat",
        Section = "Debug",
        Key = "DebugPrintLevel",
        Default = 3,
        ConVar = "sc_debuglevel",
        OnChanged = function(File, Section, Key, NewValue)
            SC.DebugLevel = NewValue
        end
    })
end)