-----------------------------------------
-- CLIENTSIDE CONVARS
-----------------------------------------

-- Mineral Field Settings
CreateConVar("mining_mineralNodeViewDist", 5000, FCVAR_ARCHIVE, "Maximum distance at which mineral field parents are drawn.")
CreateConVar("mining_mineralNodeFadeDist", 4500, FCVAR_ARCHIVE, "Distance at which mineral field parents start to fade out.")
CreateConVar("mining_mineralChildViewDist", 3000, FCVAR_ARCHIVE, "Maximum distance at which mineral patches are drawn.")
CreateConVar("mining_mineralChildFadeDist", 2500, FCVAR_ARCHIVE, "Distance at which mineral patches start to fade out.")
CreateConVar("mining_mineralParticleViewDist", 5000, FCVAR_ARCHIVE, "Maximum distance at which mineral field particle effects can be emitted.")
CreateConVar("mining_mineralParticleDelay", 0.05, FCVAR_ARCHIVE, "Delay in seconds between particle emissions across a field. Lower time means more particles.")


-- Ship Explosion Settings
CreateConVar("sc_doScreenShakeFX", 1, FCVAR_ARCHIVE, "Toggle Screen Shake effects on big explosions.")
CreateConVar("sc_shipDeathSoundRadius_Max", 30000, FCVAR_ARCHIVE, "Maximum distance at which to play explosion sounds across the map.")
CreateConVar("sc_shipDeathSoundRadius_NearFactor", 0.3, FCVAR_ARCHIVE, "Fraction of maximum distance at which \"Near\" versions of explosion sound effects are played.")
CreateConVar("sc_shipDeathSoundRadius_MidFactor", 0.65, FCVAR_ARCHIVE, "Fraction of maximum distance at which \"Medium\" versions of explosion sound effects are played.")

--Various Device Settings
CreateConVar("mining_drill_rig_particle_dist", 7500, FCVAR_ARCHIVE, "Maximum distance at which Drill Rigs can emit dust particles.")
CreateConVar("sc_resourcemonitor_emitrange", 512, {FCVAR_ARCHIVE, FCVAR_USERINFO}, "Range in units at which emitted Resource Monitors will sync to your client.")
