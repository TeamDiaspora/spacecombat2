AddCSLuaFile()

local GM = GM
local storagetypes = {}
local storagemodels = {}
local LastStorageID = 0
local resourcetypes = {}
local resourcestoragetypes = {}
local resourceinfo = {}
local resourceInfoPageBuilders = {}
local LastResourceID = 0

local itemRarityColors = {}
local itemRarityNames = {}
local itemRarityLevels = 0

local ResourceIDs = {}
local ResourceReverseLookup = {}

local StorageTypeIDs = {}
local StorageTypeReverseLookup = {}

function GM:GetStorageTypeID(String)
    if not StorageTypeIDs[String] then SC.Error("Unable to find storage type ID: "..tostring(String), 5) end
    return StorageTypeIDs[String]
end

function GM:GetStorageTypeFromID(ID)
    if not StorageTypeReverseLookup[ID] then SC.Error("Unable to find storage type with ID: "..tostring(ID).."\n\n"..debug.traceback().."\n", 5) end
    return StorageTypeReverseLookup[ID]
end

function GM:GetStorageTypeLookupTableByID()
    return table.Copy(StorageTypeReverseLookup)
end

function GM:GetStorageTypeLookupTable()
    return table.Copy(StorageTypeIDs)
end

function GM:GetResourceID(String)
    if not ResourceIDs[String] then SC.Error("Unable to find resource ID: "..tostring(String), 5) end
    return ResourceIDs[String]
end

function GM:GetResourceFromID(ID, ReturnResourceData)
    if not ResourceReverseLookup[ID] then SC.Error("Unable to find resource with ID: "..tostring(ID).."\n\n"..debug.traceback().."\n", 5) end
    if ReturnResourceData and ResourceReverseLookup[ID] then
        return resourcetypes[ResourceReverseLookup[ID]]
    else
        return ResourceReverseLookup[ID]
    end
end

function GM:GetResourceFromName(Name)
    if not resourcetypes[Name] then SC.Error("Unable to find resource with Name: "..tostring(Name).."\n\n"..debug.traceback().."\n", 5) end
    return resourcetypes[Name]
end

function GM:GetResourceLookupTableByID()
    return table.Copy(ResourceReverseLookup)
end

function GM:GetResourceLookupTable()
    return table.Copy(ResourceIDs)
end

function GM:GetStorageTypes()
	return table.Copy(storagetypes)
end

function GM:GetStorageModels()
	return table.Copy(storagemodels)
end

function GM:GetDefaultStorageModel(name)
    return storagemodels[name]
end

function GM:AddDefaultStorageModel(name, model)
    if not name or name == "" or not model or model == "" then return end
    storagemodels[name] = model
end

function GM:AddStorageType(name)
	if not storagetypes[name] then
		storagetypes[name] = true
		resourcestoragetypes[name] = {}
        LastStorageID = LastStorageID + 1

        StorageTypeIDs[name] = LastStorageID
        StorageTypeReverseLookup[LastStorageID] = name
	end
end

function GM:NewStorage(type, max, StorageMode)
	if type and storagetypes[type] then
		return GM.class.getClass("ResourceContainer"):new(type, max, {}, StorageMode)
	end
end

function GM:StorageTypeCount()
	return LastStorageID
end

function GM:GetResourceTypes()
	return table.Copy(resourcetypes)
end

function GM:GetResourcesOfType(type)
    return resourcestoragetypes[type]
end

function GM:AddResourceType(name, volumePerUnit, type, massPerUnit, itemIconModel, itemWorldModel, itemBaseRarity, itemArchetype, descriptionText)
    if not resourcetypes[name] and storagetypes[type] then
        LastResourceID = LastResourceID + 1

		resourcetypes[name] = GM.class.getClass("Resource"):new(name, volumePerUnit, type, nil, nil, LastResourceID, true, itemBaseRarity)
		resourcestoragetypes[type][name] = resourcetypes[name]

        ResourceIDs[name] = LastResourceID
        ResourceReverseLookup[LastResourceID] = name

		local IconModelPath, IconModelSkin = GM:ReadModelPathSkin(itemIconModel or itemWorldModel or "models/props_junk/wood_crate001a.mdl")
		local WorldModelPath, WorldModelSkin = GM:ReadModelPathSkin(itemWorldModel or itemIconModel or "models/props_junk/wood_crate001a.mdl")

		resourceinfo[name] = {}
		resourceinfo[name].Volume = volumePerUnit or 1
		resourceinfo[name].Mass = massPerUnit or 1
		resourceinfo[name].Desc = descriptionText or "[PLACEHOLDER DESCRIPTION TEXT - FIXME]"
		resourceinfo[name].Icon = IconModelPath
		resourceinfo[name].IconSkin = tonumber(IconModelSkin) or 0
		resourceinfo[name].Model = WorldModelPath
		resourceinfo[name].ModelSkin = tonumber(WorldModelSkin) or 0
		resourceinfo[name].Rarity = itemBaseRarity or 1
		resourceinfo[name].Archetype = itemArchetype or "Item"
	end
end


function GM:GetResourceStorageType(name)
  if not resourcetypes[name] then return "" end

  return resourcetypes[name]:GetType()
end

function GM:NewResource(name, amount, max)
	if resourcetypes[name] then
    local res = resourcetypes[name]
		return GM.class.getClass("Resource"):new(res:GetName(), res:GetSize(), res:GetType(), amount, max, res:GetID())
	end
end

function GM:ResourceTypesCount()
	return LastResourceID
end

function GM:ReadModelPathSkin(Path)
	local Matches = {}
	for Match in string.gmatch(Path .. ":", "(.-):") do
		table.insert(Matches, Match)
	end

	return unpack(Matches)
end


--Sets the factory function for building an item's information page to a named archetype.
--Note 1: If a builder function is not provided, the panel will use its internal builder (basically, just print description and amounts).
--Note 2: This is in Shared for now because this is where the other resource crap is; move to clientside-only if we decouple resourcing between client/server.
--	BuilderFunc = function( InventoryPanel dermaObject )
--EX:
--	name =			"Core Mod"
--	builderFunc =	function that generates a RichText element displaying bonuses in green,
--					detriments in red, and a Fitting Cost panel.
function GM:SetResourceInfoPageBuilder(name, builderFunc)
	if not isfunction(builderFunc) then
		error("[SC2 SHARED] - InfoPanel builder for " .. name .. " was not function type!")
		return
	end

	resourceInfoPageBuilders[name] = builderFunc
end

function GM:GetResourceInfoPageBuilder(name)
	return resourceInfoPageBuilders[name]
end

function GM:GetResourceInfoTable(name)
	if not resourceinfo[name] then return {} end

	return table.Copy(resourceinfo[name])
end


--Item Rarity Levels
function GM:DefineItemRarityLevel(level, name, color)
	if not itemRarityColors[level] or not itemRarityNames[level] then
		itemRarityNames[level] = name
		itemRarityColors[level] = color

		itemRarityLevels = itemRarityLevels + 1
	end
end

function GM:GetItemRarityColor(level)
	if itemRarityColors[level] then
		return itemRarityColors[level]
	else
		return Color(255, 255, 255, 255)
	end
end

function GM:GetItemRarityName(level)
	if itemRarityNames[level] then
		return itemRarityNames[level]
	else
		return "[UNDEFINED RARITY]"
	end
end



----------------------------------------------------------------------------------
--						RESOURCE DEFINITIONS START HERE							--
----------------------------------------------------------------------------------

--Rarity Settings
ITEM_RARITY_COMMON = 1
ITEM_RARITY_UNCOMMON = 2
ITEM_RARITY_RARE = 3
ITEM_RARITY_VERYRARE = 4
ITEM_RARITY_LEGENDARY = 5

GM:DefineItemRarityLevel(ITEM_RARITY_COMMON,	"Common",		Color(128, 128, 128, 255))
GM:DefineItemRarityLevel(ITEM_RARITY_UNCOMMON,	"Uncommon",		Color(0, 255, 0, 255))
GM:DefineItemRarityLevel(ITEM_RARITY_RARE,		"Rare",			Color(0, 0, 255, 255))
GM:DefineItemRarityLevel(ITEM_RARITY_VERYRARE,	"Very Rare",	Color(196, 0, 196, 255))
GM:DefineItemRarityLevel(ITEM_RARITY_LEGENDARY,	"Legendary",	Color(255, 128, 0, 255))


--Storages
GM:AddStorageType("Energy")
GM:AddStorageType("Cargo")
GM:AddStorageType("Liquid")
GM:AddStorageType("Gas")
GM:AddStorageType("Ammo")

-- Effectively, doesn't exist
GM:AddResourceType("Energy", 1, "Energy", 0, "models/items/car_battery01.mdl", "models/items/car_battery01.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"Energy, voltage, amperage, watts, NRG, zapitude; a resource with many names, all of which determine if your guns are firing and your lifesupport will turn on.")

-- Cargo
GM:AddResourceType("Lifesupport Canister", 1, "Cargo", 1, "models/slyfo_2/acc_oxygenpaste.mdl", "models/slyfo_2/acc_oxygenpaste.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"Lifesupport Canisters make it so you do not die when exposed to inhospitable climates, like the vacuum of space or the heat of a lava planet.\n\nDISCLAIMER: May not protect the user from actual lava.")
GM:AddResourceType("Empty Canister", 1, "Cargo", 0.25, "models/slyfo_2/acc_oxygenpaste.mdl", "models/slyfo_2/acc_oxygenpaste.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"It's light, almost as if there's nothing inside it...")
GM:AddResourceType("Carbon", 1, "Cargo", 2.25, "models/maxofs2d/hover_classic.mdl", "models/maxofs2d/hover_classic.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"A black, dusty material; base material for organics. Can be burned for energy.")
GM:AddResourceType("Rocket Fuel Canister", 1, "Cargo", 1.06, "models/props_junk/propane_tank001a.mdl", "models/props_junk/propane_tank001a.mdl", ITEM_RARITY_UNCOMMON,	--Note: Basing this off of an empty canister (0.25 kg) being filled with a liter of kerosene (0.81kg/liter)
					"Resource",
					"A highly energetic fuel in a bottle. Can be attached to explosives to make ammunition.")

-- Gasses
--[[
    These values are based on Covalent Radius
    Uranium 2.58
    Carbon Dioxide 1.52
    Carbon Monoxide 1.47
    Methane 1.42
    Water 1.26
    Carbon 1.00
    Nitrogen 0.93
    Oxygen 0.86
    Hydrogen 0.40
]]--

--FIXME: I have no idea how to convert these covalent radii into meaningful mass numbers. Luckily mass is not important to gameplay, so these will all be 0.1kg for now. -Steeveeo

GM:AddResourceType("Oxygen", 0.86, "Gas", 0.1, "models/chipstiks_ls3_models/smalloxygentank/smalloxygentank.mdl", "models/chipstiks_ls3_models/smalloxygentank/smalloxygentank.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"Tasty, life-giving oxygen. Most organics need this to function properly, with the exception of plants (plants are weird).")
GM:AddResourceType("Hydrogen", 0.40, "Gas", 0.1, "models/chipstiks_ls3_models/smallhydrogentank/smallhydrogentank.mdl", "models/chipstiks_ls3_models/smallhydrogentank/smallhydrogentank.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"The most abundant gas in the universe. Flammable.")
GM:AddResourceType("Nitrogen", 0.93, "Gas", 0.1, "models/chipstiks_ls3_models/smallnitrogentank/smallnitrogentank.mdl", "models/chipstiks_ls3_models/smallnitrogentank/smallnitrogentank.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"A highly stable gas. Nitrogen has many uses, including coolant when liquified.")
GM:AddResourceType("Carbon Dioxide", 1.52, "Gas", 0.1, "models/chipstiks_ls3_models/smallco2tank/smallco2tank.mdl", "models/chipstiks_ls3_models/smallco2tank/smallco2tank.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"Most organics produce this as a byproduct of their function. Has a heating effect when applied liberally to atmospheres.")
GM:AddResourceType("Carbon Monoxide", 1.47, "Gas", 0.1, "models/props_wasteland/kitchen_stove002a.mdl", "models/props_wasteland/kitchen_stove002a.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"A highly poisonous gas. This description would not recommend ingesting or inhaling this resource.")
GM:AddResourceType("Methane", 1.42, "Gas", 0.1, "models/props_c17/canister01a.mdl", "models/props_c17/canister01a.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"A flammable gas most commonly associated with Old Earth carbon mines and bovine-type organics.")
GM:AddResourceType("Steam", 1.26, "Gas", 0.1, "models/chipstiks_ls3_models/smallsteamtank/smallsteamtank.mdl", "models/chipstiks_ls3_models/smallsteamtank/smallsteamtank.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"Vaporized water. Byproduct of most power generation assemblies.")

GM:AddResourceType("Helium-3", 0.5, "Gas", 0.1, "models/props_junk/PropaneCanister001a.mdl", "models/props_junk/PropaneCanister001a.mdl")
GM:AddResourceType("Helium-4", 0.5, "Gas", 0.1, "models/props_junk/PropaneCanister001a.mdl:1", "models/props_junk/PropaneCanister001a.mdl:1")
GM:AddResourceType("Tritium", 0.40, "Gas", 0.1, "models/props_lab/jar01a.mdl", "models/props_lab/jar01a.mdl")
GM:AddResourceType("Deuterium", 0.40, "Gas", 0.1, "models/ce_ls3additional/resource_tanks/resource_tank_medium.mdl:1", "models/ce_ls3additional/resource_tanks/resource_tank_medium.mdl:1")

-- Liquids
GM:AddResourceType("Water", 1.26, "Liquid", 1, "models/ce_ls3additional/resource_tanks/resource_tank_medium.mdl", "models/ce_ls3additional/resource_tanks/resource_tank_medium.mdl", ITEM_RARITY_COMMON,
					"Resource",
					"It's somewhat moist.")
GM:AddResourceType("Methanol", 1.7, "Liquid", 1.7, "models/props_junk/gascan001a.mdl", "models/props_junk/gascan001a.mdl", ITEM_RARITY_UNCOMMON, -- The size of this is total guesswork, my brief search found no information about its radius
					"Resource",
					"A methane-based fuel. Toxic to humanoid organics.")
GM:AddResourceType("Liquid Nitrogen", 0.93, "Liquid", 0.75, "models/ce_ls3additional/canisters/canister_small.mdl:5", "models/ce_ls3additional/canisters/canister_small.mdl:5", ITEM_RARITY_UNCOMMON,
					"Resource",
					"A common industrial refrigerant. Used to cool high-temperature devices.")

-- Volatile
-- TODO: Add a way to mark these so they can explode in the cargo bay
GM:AddResourceType("Radioactive Coolant", 1.26, "Cargo", 1.5, "models/slyfo/barrel_unrefined.mdl", "models/slyfo/barrel_unrefined.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"Contaminated waste products of nuclear reactions. Can be purified to extract useful water.")
GM:AddResourceType("Uranium", 2.58, "Cargo", 19.05, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"A radioactive actinide used as fissile material for Fission Reactors.")
GM:AddResourceType("Polonium", 1.84, "Cargo", 9.4, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"A highly radioactive metal used in generating neutrons and alpha particles. Commonly used in Radioisotope Thermoelectric Generators (RTG).")
GM:AddResourceType("Antihydrogen", 0.40, "Cargo", 0.1, "models/slyfo_2/mortarsys_cryo.mdl", "models/slyfo_2/mortarsys_cryo.mdl", ITEM_RARITY_RARE,
					"Resource",
					"Instantaneously annihilates when coming in contant with Hydrogen, emitting gamma radiation which can be harnessed for Energy generation.")
GM:AddResourceType("Antianubium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_incen.mdl", "models/slyfo_2/mortarsys_incen.mdl", ITEM_RARITY_LEGENDARY,
					"Resource",
					"Instantaneously annihilates when coming in contant with Anubium, emitting gamma radiation which can be harnessed for Energy generation.")
GM:AddResourceType("Anubium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_VERYRARE,
					"Material",
					nil) --Not sure on the differences between Nubium, Solid Nubium, and Anubium, fill this in with actual stuff later
GM:AddResourceType("Polonium Nitrate", 1, "Cargo", 1, "models/spacecombat2/ltbrandon/crystals/crystal_01.mdl", "models/spacecombat2/ltbrandon/crystals/crystal_01.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					"An inexplicable freak of nature. A seed crystal, under the right conditions, can explosively precipitate outwards. Constantly emits highly corrosive polonium nitroxide gas.")
GM:AddResourceType("Plutonium", 2.58, "Cargo", 19.05, "models/props/de_train/barrel.mdl:7", "models/props/de_train/barrel.mdl:7", ITEM_RARITY_UNCOMMON,
					"Resource",
					"A radioactive actinide used as fissile material for Fission Reactors.")


-- Mining Materials
GM:AddResourceType("Veldspar", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_COMMON,
					"Material",
					"A naturally occuring alloy of Tritanium and Iron.")
GM:AddResourceType("Iron", 1, "Cargo", 7.87, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_COMMON,
					"Material",
					"One of the most commonly used metals.")
GM:AddResourceType("Copper", 1, "Cargo", 7.87, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_COMMON,
					"Material",
					"One of the most commonly used metals.")
GM:AddResourceType("Aluminum", 1, "Cargo", 7.87, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_COMMON,
					"Material",
                    "One of the most commonly used metals.")
GM:AddResourceType("Steel", 1, "Cargo", 7.87, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
                    "Material",
                    "One of the most commonly used metals.")
GM:AddResourceType("Silver", 1, "Cargo", 7.87, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					"One of the most commonly used metals.")
GM:AddResourceType("Gold", 1, "Cargo", 7.87, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					"One of the most commonly used metals.")
GM:AddResourceType("Ice", 1, "Cargo", 1, "models/props/cs_office/snowman_face.mdl", "models/props/cs_office/snowman_face.mdl", ITEM_RARITY_COMMON,
					"Material",
					"Solid state of water. It's somewhat cold.")
GM:AddResourceType("Tritanium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_COMMON,
					"Material",
					"A common metal used in ship construction.") --Perhaps we can just rename this Titanium and get it over with?
GM:AddResourceType("Titanite", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --Nil for placeholder autoreplace test
GM:AddResourceType("Triidium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --Nil because I actually cannot find any lore/science behind this
GM:AddResourceType("Trinium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_RARE,
					"Material",
					nil) --Are we just making up names now?
GM:AddResourceType("Tronium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_RARE,
					"Material",
					nil) --This one's the name of a power supply chip...
GM:AddResourceType("Vibranium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_VERYRARE,
					"Material",
					nil) --Perhaps we should remove this in case Marvel wants to sue...
GM:AddResourceType("Tylium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_VERYRARE,
					"Material",
					nil) --This one's not even used in any recipes!
GM:AddResourceType("Voltarium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_RARE,
					"Material",
					nil) --I actually like this one, but it doesn't have any use currently, so I'm not sure what to put for the "lore"
GM:AddResourceType("Nubium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --Not sure on the differences between Nubium, Solid Nubium, and Anubium, fill this in with actual stuff later
GM:AddResourceType("Solid Nubium", 1, "Cargo", 1, "models/slyfo_2/mortarsys_carpetbomb.mdl", "models/slyfo_2/mortarsys_carpetbomb.mdl", ITEM_RARITY_UNCOMMON,
					"Material",
					nil) --Not sure on the differences between Nubium, Solid Nubium, and Anubium, fill this in with actual stuff later
GM:AddResourceType("Lithium-6", 1, "Cargo", 1, "models/props_lab/jar01a.mdl", "models/props_lab/jar01a.mdl")
GM:AddResourceType("Depleted Uranium", 2.58, "Cargo", 19.05, "models/slyfo/barrel_refined.mdl", "models/slyfo/barrel_refined.mdl", ITEM_RARITY_UNCOMMON,
					"Resource",
					"Depleted Uranium.")

--Wreckage Drops
GM:AddResourceType("Electronic Circuitry", 3, "Cargo", 0.1, "models/cheeze/wires/ram.mdl", "models/cheeze/wires/ram.mdl", ITEM_RARITY_UNCOMMON,
					"Salvage",
					"Salvaged electronics. Can be broken down into useful components.")
GM:AddResourceType("Scrap Metal", 1, "Cargo", 2, "models/props_c17/oildrumchunk01d.mdl", "models/props_c17/oildrumchunk01d.mdl", ITEM_RARITY_COMMON,
					"Salvage",
					"Charred metal salvaged from a ship wreckage. Can be melted down and recycled.")
GM:AddResourceType("Contaminated Coolant", 0.1, "Cargo", 0.2, "models/props/de_train/barrel.mdl", "models/props/de_train/barrel.mdl", ITEM_RARITY_COMMON,
					"Salvage",
					"Liquid coolant contaminated by the meltdown of a ship's core.") --We should probably get rid of this one since Radioactive Coolant's already a thing in the normal resource tree
GM:AddResourceType("Radioactive Materials", 0.5, "Cargo", 1.5, "models/props/de_nuke/nuclearcontainerboxclosed.mdl", "models/props/de_nuke/nuclearcontainerboxclosed.mdl", ITEM_RARITY_COMMON,
					"Salvage",
					"Various contaminated materials. Not much of value unless reprocessed.")
GM:AddResourceType("Nubium Generator Core", 15, "Cargo", 30, "models/tiberium/small_tiberium_storage.mdl", "models/tiberium/small_tiberium_storage.mdl", ITEM_RARITY_VERYRARE,
					"Salvage",
					"A salvaged component used in ship cores. Should have some rather valuable materials inside.")
GM:AddResourceType("Damaged Shield Emitter", 7, "Cargo", 15, "models/slyfo/powercrystal.mdl", "models/slyfo/powercrystal.mdl", ITEM_RARITY_RARE,
					"Salvage",
					"A salvaged component used to protect a ship from incoming enemy fire. This one is irreparably damaged.")
GM:AddResourceType("Plasma Conduits", 2, "Cargo", 3, "models/slyfo_2/acc_sci_tube1sml.mdl", "models/slyfo_2/acc_sci_tube1sml.mdl", ITEM_RARITY_UNCOMMON,
					"Salvage",
					"Ship operations require a lot of superheated plasma, this damaged conduit ferries that plasma to where its needed.")
GM:AddResourceType("Destroyed Consoles", 5, "Cargo", 7, "models/props_lab/reciever01c.mdl", "models/props_lab/reciever01c.mdl", ITEM_RARITY_RARE,
					"Salvage",
					"Why are these consoles always exploding and killing the cadets? You'd think the Safety Commission would do something about that...")

-- Other
GM:AddResourceType("Money", 0.01, "Cargo", 0.01, "models/props/cs_assault/money.mdl", "models/props_c17/SuitCase001a.mdl", ITEM_RARITY_COMMON, "Money", "Currency commonly used to purcahse things in space.")
GM:AddResourceType("Cow", 5, "Cargo", 2, "models/props/cs_militia/fishriver01.mdl", "models/props/cs_militia/fishriver01.mdl", ITEM_RARITY_COMMON, "Food", "It's a cow.")
GM:AddResourceType("Corn", 0.5, "Cargo", 0.2, "models/props/de_dust/grainbasket01b.mdl", "models/props/de_dust/grainbasket01b.mdl", ITEM_RARITY_COMMON, "Food", "It's corn.")
GM:AddResourceType("Annatto", 0.5, "Cargo", 0.2, "models/props/cs_italy/it_mkt_container3a.mdl", "models/props/cs_italy/it_mkt_container3a.mdl", ITEM_RARITY_COMMON, "Food", "Smells of processed cheese.")
GM:AddResourceType("Wheat", 0.5, "Cargo", 0.2, "models/props/de_dust/grainbasket01b.mdl", "models/props/de_dust/grainbasket01b.mdl", ITEM_RARITY_COMMON, "Food", "It's wheat.")
GM:AddResourceType("Beef", 0.5, "Cargo", 0.2, "models/slyfo_2/acc_food_meat.mdl", "models/slyfo_2/acc_food_meat.mdl", ITEM_RARITY_COMMON, "Food", "It's beef.")
GM:AddResourceType("Milk", 0.5, "Cargo", 0.2, "models/props_junk/garbage_milkcarton001a.mdl", "models/props_junk/garbage_milkcarton001a.mdl", ITEM_RARITY_COMMON, "Food", "It's milk.")
GM:AddResourceType("Ground Beef", 0.5, "Cargo", 0.2, "models/props_junk/garbage_metalcan001a.mdl", "models/props_junk/garbage_metalcan001a.mdl", ITEM_RARITY_COMMON, "Food", "It's also beef.")
GM:AddResourceType("Burger Patty", 0.5, "Cargo", 0.2, "models/props_phx/construct/metal_angle360.mdl", "models/props_phx/construct/metal_angle360.mdl", ITEM_RARITY_COMMON, "Food", "It's more beef.")
GM:AddResourceType("Bread", 0.5, "Cargo", 0.2, "models/props_junk/garbage_glassbottle001a.mdl", "models/props_junk/garbage_glassbottle001a.mdl", ITEM_RARITY_COMMON, "Food", "It's bread.")
GM:AddResourceType("Battle-Bread", 0.5, "Cargo", 0.2, "models/slyfo_2/acc_food_meatsandwichhalf.mdl", "models/slyfo_2/acc_food_meatsandwichhalf.mdl", ITEM_RARITY_VERYRARE, "Food", "It's fancy bread.")
GM:AddResourceType("Battle-Burger", 0.5, "Cargo", 0.2, "models/food/burger.mdl", "models/food/burger.mdl", ITEM_RARITY_LEGENDARY, "Food", "It's a god-damned cheeseburger.")
GM:AddResourceType("Cheese", 0.5, "Cargo", 0.2, "models/props/cs_militia/militiarock05.mdl", "models/props/cs_militia/militiarock05.mdl", ITEM_RARITY_COMMON, "Food", "It's cheese.")
GM:AddResourceType("Processed Cheese", 0.5, "Cargo", 0.2, "models/props/de_inferno/crate_fruit_break_gib2.mdl", "models/props/de_inferno/crate_fruit_break_gib2.mdl", ITEM_RARITY_COMMON, "Food", "It's cheese?")
GM:AddResourceType("Dorito", 0.5, "Cargo", 0.2, "models/slyfo_2/acc_food_snckstridernugs.mdl", "models/slyfo_2/acc_food_snckstridernugs.mdl", ITEM_RARITY_UNCOMMON, "Food", "It's a dorito.")
GM:AddResourceType("Monster Energy", 0.5, "Liquid", 0.2, "models/props/cs_office/trash_can_p8.mdl", "models/props/cs_office/trash_can_p8.mdl", ITEM_RARITY_UNCOMMON, "Food", "It's a monster.")
GM:AddResourceType("Dorito Monster Concentrate", 0.5, "Liquid", 0.2, "models/props_junk/PopCan01a.mdl:2", "models/props_junk/PopCan01a.mdl:2", ITEM_RARITY_LEGENDARY, "Food", "It's cursed.")
GM:AddResourceType("Kleiner", 0.5, "Cargo", 0.2, "models/Kleiner.mdl", "models/Kleiner.mdl", ITEM_RARITY_LEGENDARY, "Food", "It's extremely cursed.")

--#region Crafting
-- Plates
GM:AddResourceType("Iron Plate", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_COMMON, "Plating", "A plate made of flattened Iron")
GM:AddResourceType("Aluminum Plate", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_COMMON, "Plating", "A plate made of flattened Iron")
GM:AddResourceType("Steel Plate", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_UNCOMMON, "Plating", "A plate made of flattened Iron")
GM:AddResourceType("Tritanium Plate", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_UNCOMMON, "Plating", "A plate made of flattened Iron")
GM:AddResourceType("Trinium Plate", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_RARE, "Plating", "A plate made of flattened Iron")

-- Gears
GM:AddResourceType("Iron Gear", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_COMMON, "Gears", "A gear made of Iron")
GM:AddResourceType("Aluminum Gear", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_COMMON, "Gears", "A gear made of Iron")
GM:AddResourceType("Steel Gear", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_UNCOMMON, "Gears", "A gear made of Iron")
GM:AddResourceType("Tritanium Gear", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_UNCOMMON, "Gears", "A gear made of Iron")
GM:AddResourceType("Trinium Gear", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_RARE, "Gears", "A gear made of Iron")

-- Wiring
GM:AddResourceType("Aluminum Wires", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_COMMON, "Wiring", "A spool of wire made of Iron")
GM:AddResourceType("Copper Wires", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_COMMON, "Wiring", "A spool of wire made of Iron")
GM:AddResourceType("Gold Wires", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_UNCOMMON, "Wiring", "A spool of wire made of Iron")
GM:AddResourceType("Silver Wires", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_UNCOMMON, "Wiring", "A spool of wire made of Iron")
GM:AddResourceType("Voltarium Wires", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_RARE, "Wiring", "A spool of wire made of Iron")
--#endregion

-- Upgrades
GM:AddResourceType("Basic Generator", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_COMMON, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Standard Generator", 10, "Cargo", 100, "models/slyfo_2/mortarsys_carpetbomb.mdl", nil, ITEM_RARITY_UNCOMMON, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Enhanced Generator", 10, "Cargo", 100, "models/slyfo_2/mortarsys_cryo.mdl", nil, ITEM_RARITY_RARE, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Advanced Generator", 10, "Cargo", 100, "models/slyfo_2/mortarsys_incen.mdl", nil, ITEM_RARITY_VERYRARE, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Elite Generator", 10, "Cargo", 100, "models/slyfo_2/mortarsys_scrambler.mdl", nil, ITEM_RARITY_LEGENDARY, "Upgrades", "A reactor upgrade")

GM:AddResourceType("Basic Turbine", 10, "Cargo", 100, "models/props/de_inferno/ceiling_fan_blade.mdl", nil, ITEM_RARITY_COMMON, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Standard Turbine", 10, "Cargo", 100, "models/props/de_inferno/ceiling_fan_blade.mdl", nil, ITEM_RARITY_UNCOMMON, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Enhanced Turbine", 10, "Cargo", 100, "models/props/de_prodigy/fanoff.mdl", nil, ITEM_RARITY_RARE, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Advanced Turbine", 10, "Cargo", 100, "models/props_bts/fan01_small.mdl", nil, ITEM_RARITY_VERYRARE, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Elite Turbine", 10, "Cargo", 100, "models/props_residue/exhaust_fan.mdl", nil, ITEM_RARITY_LEGENDARY, "Upgrades", "A reactor upgrade")

GM:AddResourceType("Basic Cooler", 10, "Cargo", 100, "models/slyfo_2/weap_prover_itisagunofsomesort.mdl", nil, ITEM_RARITY_COMMON, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Standard Cooler", 10, "Cargo", 100, "models/slyfo_2/weap_prover_itisagunofsomesort.mdl", nil, ITEM_RARITY_UNCOMMON, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Enhanced Cooler", 10, "Cargo", 100, "models/slyfo_2/weap_prover_industrialspiker.mdl", nil, ITEM_RARITY_RARE, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Advanced Cooler", 10, "Cargo", 100, "models/slyfo_2/weap_prover_energyblastersml.mdl", nil, ITEM_RARITY_VERYRARE, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Elite Cooler", 10, "Cargo", 100, "models/slyfo_2/weap_prover_energyblasterlrg.mdl", nil, ITEM_RARITY_LEGENDARY, "Upgrades", "A reactor upgrade")

GM:AddResourceType("Basic Compressor", 10, "Cargo", 100, "models/slyfo_2/acc_sci_tube1sml.mdl", nil, ITEM_RARITY_COMMON, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Standard Compressor", 10, "Cargo", 100, "models/slyfo_2/acc_sci_tube1sml.mdl", nil, ITEM_RARITY_UNCOMMON, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Enhanced Compressor", 10, "Cargo", 100, "models/slyfo_2/acc_sci_tube2.mdl", nil, ITEM_RARITY_RARE, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Advanced Compressor", 10, "Cargo", 100, "models/slyfo_2/acc_sci_hoterator.mdl", nil, ITEM_RARITY_VERYRARE, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Elite Compressor", 10, "Cargo", 100, "models/slyfo_2/acc_sci_coolerator.mdl", nil, ITEM_RARITY_LEGENDARY, "Upgrades", "A reactor upgrade")

GM:AddResourceType("Basic Recuperator", 10, "Cargo", 100, "models/slyfo_2/smltorpedotubedoublebase.mdl", nil, ITEM_RARITY_COMMON, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Standard Recuperator", 10, "Cargo", 100, "models/slyfo_2/smltorpedotubedoublebase.mdl", nil, ITEM_RARITY_UNCOMMON, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Enhanced Recuperator", 10, "Cargo", 100, "models/slyfo_2/smltorpedotubeminimal.mdl", nil, ITEM_RARITY_RARE, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Advanced Recuperator", 10, "Cargo", 100, "models/slyfo_2/smltorpedotube.mdl", nil, ITEM_RARITY_VERYRARE, "Upgrades", "A reactor upgrade")
GM:AddResourceType("Elite Recuperator", 10, "Cargo", 100, "models/slyfo_2/smltorpedotubedouble.mdl", nil, ITEM_RARITY_LEGENDARY, "Upgrades", "A reactor upgrade")

--Ammo
GM:AddResourceType("Tiny Capacitor Charge", 20, "Ammo", 2, "models/ce_ls3additional/energy_cells/energy_cell_mini.mdl", "models/ce_ls3additional/energy_cells/energy_cell_mini.mdl")
GM:AddResourceType("Small Capacitor Charge", 40, "Ammo", 4, "models/ce_ls3additional/energy_cells/energy_cell_medium.mdl", "models/ce_ls3additional/energy_cells/energy_cell_medium.mdl")
GM:AddResourceType("Medium Capacitor Charge", 80, "Ammo", 8, "models/ce_ls3additional/energy_cells/energy_cell_large.mdl", "models/ce_ls3additional/energy_cells/energy_cell_large.mdl")
GM:AddResourceType("Large Capacitor Charge", 160, "Ammo", 16, "models/ce_ls3additional/energy_cells/energy_cell_huge.mdl", "models/ce_ls3additional/energy_cells/energy_cell_huge.mdl")
GM:AddResourceType("X-Large Capacitor Charge", 320, "Ammo", 32, "models/ce_ls3additional/energy_cells/energy_cell_giant.mdl", "models/ce_ls3additional/energy_cells/energy_cell_giant.mdl")
GM:AddResourceType("Nanite Canister", 1, "Ammo", 2, "models/slyfo_2/gunball.mdl", "models/slyfo_2/gunball.mdl")
GM:AddResourceType("Anti-Hydrogen Particle Canister", 5, "Ammo", 2, "models/slyfo_2/mini_turret_pulselaser.mdl", "models/slyfo_2/mini_turret_pulselaser.mdl")
GM:AddResourceType("Anti-Anubium Particle Canister", 5, "Ammo", 4, "models/slyfo_2/mini_turret_flamer.mdl", "models/slyfo_2/mini_turret_flamer.mdl")
GM:AddResourceType("Iron Particle Canister", 5, "Ammo", 3, "models/slyfo_2/mini_turret_shotgun.mdl", "models/slyfo_2/mini_turret_shotgun.mdl")
GM:AddResourceType("Depleted Uranium Particle Canister", 5, "Ammo", 5, "models/slyfo_2/mini_turret_shotgun.mdl", "models/slyfo_2/mini_turret_shotgun.mdl")
GM:AddResourceType("Kleiner Particle Canister", 5, "Ammo", 5, "models/Police.mdl", "models/Police.mdl")


include("sh_generators.lua")