--[[
	Space Combat Inventory Control Panel
	Author: Steve "Steeveeo" Green

	For use in any player-accessible inventory, like
	own pockets and global storage.
]]--

local PANEL = {}

PANEL.CurSelectedItem = nil

PANEL.StorageMode = CONTAINERMODE_DEFAULT	--What type of container is this
PANEL.Capacity = 400.0
PANEL.CapacityUsed = 0.0
PANEL.Mass = 0.0
PANEL.InfoPanel = nil
PANEL.InfoLabel = nil
PANEL.InfoLabelRight = nil
PANEL.StorageModeSelectionDropdown = nil

PANEL.ResourcePanel = nil
PANEL.InventoryList = nil   --The list of inventory items within the resource panel
PANEL.ResourceList = nil    --The list of valid resources for the container type, if the container type is CUSTOMSPLIT
PANEL.RightClickMenu = nil  --The right-click menu you get when right-clicking in the inventory list
PANEL.AddStorageMenu = nil  --The menu for adding more storage to the container
PANEL.ResizeStorageMenu = nil   --The menu for resizing storage

PANEL.DropItemButton = nil
PANEL.DestroyItemButton = nil

PANEL.UseSmallDisplayMode = false
PANEL.AllowCustomSplitting = false

--[[
CONTAINERMODE_DEFAULT = 1
CONTAINERMODE_AUTOENLARGE = 2
CONTAINERMODE_AUTOSPLIT = 3
CONTAINERMODE_EQUALSPLIT = 4
CONTAINERMODE_CUSTOMSPLIT = 5
]]

local StorageModeStrings = {
    "Default",
    "Auto-Enlarge",
    "Auto-Split",
    "Equal Split",
    "Custom Split"
}

local StorageModesChoosable = {
    CONTAINERMODE_AUTOENLARGE,
    CONTAINERMODE_AUTOSPLIT,
    CONTAINERMODE_EQUALSPLIT,
    CONTAINERMODE_CUSTOMSPLIT
}

local StorageModeDropdownIndicies = {
    [CONTAINERMODE_AUTOENLARGE] = 1,
    [CONTAINERMODE_AUTOSPLIT] = 2,
    [CONTAINERMODE_EQUALSPLIT] = 3,
    [CONTAINERMODE_CUSTOMSPLIT] = 4
}

local StorageTypesChangeable = {
    ["Energy"] = false,
    ["Liquid"] = true,
    ["Gas"] = true,
    ["Ammo"] = true,
    ["Cargo"] = true
}

function PANEL:GetInfoLabelString()
    return string.format("Capacity: %.2f / %d\nMass: %.2f", self.CapacityUsed, self.Capacity, self.Mass)
end

--[[---------------------------------------------------------
   Initialize Inventory Panel
-----------------------------------------------------------]]
function PANEL:Init()
    --The panel holding the top information
    self.InfoPanel = vgui.Create("DPanel", self)
    self.InfoPanel:Dock(TOP)
    self.InfoPanel:DockMargin(0, 5, 0, 5)

    --Inventory Information
    self.InfoLabel = vgui.Create("DLabel", self.InfoPanel)
    self.InfoLabel:SetText(self:GetInfoLabelString())
    --self.InfoLabel:AlignLeft()
    self.InfoLabel:Dock(LEFT)

    --self.InfoLabel:DockMargin(0, 5, 5, 5)

    self.InfoLabel:SizeToContents()
    self.InfoPanel:SizeToContents()

    --Displays the current storage mode and lets you change the mode for some storage types
    self.StorageModeSelectionDropdown = vgui.Create("DComboBox", self.InfoPanel)
    self.StorageModeSelectionDropdown:Dock(RIGHT)

    --Add the choices to the combo box
    for I, StorageModeIndex in ipairs(StorageModesChoosable) do
        self.StorageModeSelectionDropdown:AddChoice(StorageModeStrings[StorageModeIndex], StorageModeIndex)
    end

    --Add the some functions to the dropdown
    self.StorageModeSelectionDropdown.OnSelect = function(Panel, Index, Value)
        local NewStorageMode = StorageModesChoosable[Index]
        --Update the storage mode of our local copy of the resource container if the storage mode was changed
        if self.ResourceContainer.StorageMode ~= NewStorageMode then
            self.ResourceContainer.StorageMode = NewStorageMode
            self.StorageMode = self.ResourceContainer.StorageMode
            self.StorageModeSelectionDropdown:SizeToContentsX(20)

            --Call the callback function
            if self.StorageModeChangedCallback then self.StorageModeChangedCallback(self.ResourceContainer.Type, NewStorageMode) end
        end
    end

    self.StorageModeSelectionDropdown:SetEnabled(false)

    --Displays all of the items in the inventory in a big list
    self.InventoryList = vgui.Create("DScrollPanel", self)
    self.InventoryList:SetBackgroundColor(Color(255, 255, 255, 255))
    self.InventoryList:SetPaintBackground(true)
    self.InventoryList:Dock(FILL)

    --Add some functions to the list
    self.InventoryList.OnMousePressed = function(Panel, KeyCode)
        if KeyCode == MOUSE_RIGHT then
            self:OpenRightClickMenu()
        elseif KeyCode == MOUSE_LEFT then
            --Deselect any selected inventory items
            self:SetSelected(nil)
        end
    end

    self:InvalidateLayout()

    --The list of entries in the inventory list
    self.InventoryItems = {}
    --Our local copy of the resource container
    self.ResourceContainer = nil
    --Callback functions
    PANEL.StorageModeChangedCallback = nil      --Called when a new storage mode is selected from the drop-down
    PANEL.InventoryListChangedCallback = nil  --Called when the inventory list is modified by the user
end


--[[---------------------------------------------------------
   Change Settings
-----------------------------------------------------------]]
function PANEL:SetSmallDisplayMode(Enabled)
    self.UseSmallDisplayMode = Enabled or false
end

function PANEL:SetAllowCustomSplitting(Enabled)
    self.AllowCustomSplitting = Enabled or false

    if not self.AllowCustomSplitting then
        self.StorageModeSelectionDropdown:Remove()
    end
end

function PANEL:SetStorageModeChangedCallback(FN)
    self.StorageModeChangedCallback = FN
end

function PANEL:SetInventoryListChangedCallback(FN)
    self.InventoryListChangedCallback = FN
end

function PANEL:HideInfoLabel()
    if IsValid(self.InfoLabel) then
        self.InfoLabel:Remove()
    end
end

--Update the contents of the inventory list
function PANEL:UpdateInventoryList()
    local StoredResources = self.ResourceContainer:GetStored()

    --Create any new resources
    for ResourceName, Resource in pairs(StoredResources) do
        if not self.InventoryItems[ResourceName] then
            local InventoryItem = vgui.Create("InventoryItem", self.InventoryList)
            InventoryItem:Dock(TOP)
            InventoryItem:DockMargin(2, 1, 2, 1)
            InventoryItem:SetSmallDisplayMode(self.UseSmallDisplayMode)
            InventoryItem.InventoryPanel = self
            self.InventoryItems[ResourceName] = InventoryItem
        end
    end

    local Mass = 0

    --Update the existing list of resources
    for ResourceName,InventoryItem in pairs(self.InventoryItems) do
        local Resource = StoredResources[ResourceName]
        --Remove any inventory item elements that no longer exist in the storage
        if Resource == nil then
            InventoryItem:Remove()
            self.InventoryItems[ResourceName] = nil
        else    --Otherwise update it with the new data
            InventoryItem:LoadFromResource(Resource)
            Mass = Mass + Resource:GetAmount()
        end
    end

    self.Mass = Mass
end

--Resize the storage tank in the resource container
--Also deletes containers with a quantity of nil, and creates containers that do not exist
function PANEL:ResizeStorageItem(ChangedResName, ResQuantity)
    --Update our local copy of the resource container
    if not ResQuantity then
        self.ResourceContainer:RemoveResourceType(ChangedResName)
    else
        if not self.ResourceContainer:GetStored()[ChangedResName] then
            self.ResourceContainer:AddResourceType(ChangedResName, ResQuantity)
        else
            self.ResourceContainer:SetMaxAmount(ChangedResName, ResQuantity)
        end
    end

    --Put together a table of the resources and their maximum stored amount
    local Resources = {}
    for ResName, Resource in pairs(self.ResourceContainer:GetStored()) do
        Resources[ResName] = self.ResourceContainer:GetMaxAmount(ResName)
    end

    --Call the callback if it exists
    if self.InventoryListChangedCallback then self.InventoryListChangedCallback(self.ResourceContainer.Type, Resources) end
    --Update our local display of resources
    self:UpdateInventoryList()
end

--Add a new storage tank to the resource container
function PANEL:AddStorageItem(NewResName)
    self:ResizeStorageItem(NewResName, 0)
end

--Delete a storage tank from the resource container
function PANEL:DeleteStorageItem(DeleteResName)
    self:ResizeStorageItem(DeleteResName, nil)
end

function PANEL:UpdateContainer(Container)
    self.ResourceContainer = Container
    self.StorageMode = self.ResourceContainer.StorageMode

    self:UpdateInventoryList()

    self.CapacityUsed = self.ResourceContainer:GetUsed()
    self.Capacity = self.ResourceContainer:GetSize()

    if IsValid(self.InfoLabel) then
        self.InfoLabel:SetText(self:GetInfoLabelString())
        self.InfoLabel:SizeToContents()
        self.InfoPanel:SizeToContents()
    end

    if IsValid(self.StorageModeSelectionDropdown) then
        local OptionText, OptionData = self.StorageModeSelectionDropdown:GetSelected()
        if OptionData ~= self.StorageMode then
            local Changeable = StorageTypesChangeable[self.ResourceContainer.Type]
            if Changeable then
                self.StorageModeSelectionDropdown:ChooseOptionID(StorageModeDropdownIndicies[self.StorageMode])
                self.StorageModeSelectionDropdown:SetEnabled(true)
                self.StorageModeSelectionDropdown:SizeToContentsX(20)
            else
                self.StorageModeSelectionDropdown:Remove()
            end
        end
    end

    self:InvalidateLayout()
end


function PANEL:LoadFromContainer(Container)
    self:UpdateContainer(Container)
end

--[[---------------------------------------------------------
    Right-click Menu
-----------------------------------------------------------]]
function PANEL:OpenRightClickMenu()
    self.RightClickMenu = vgui.Create( "DMenu" )

    local RefreshOption = self.RightClickMenu:AddOption( "Refresh", function()
        self:UpdateInventoryList()
    end)
    RefreshOption:SetIcon( "icon16/arrow_refresh.png" )

    if (self.StorageMode == CONTAINERMODE_CUSTOMSPLIT) and self.AllowCustomSplitting then
        --Open the "Add Storage" menu
        local AddStorageOption = self.RightClickMenu:AddOption( "Add Item", function()
            self:OpenAddStorageMenu()
        end)
        AddStorageOption:SetIcon( "icon16/database_add.png" )
    end

    self.RightClickMenu:Open()
end

function PANEL:OpenAddStorageMenu()
    if not (IsValid(self.AddStorageMenu) or IsValid(self.ResizeStorageMenu)) then
        self.AddStorageMenu = vgui.Create( "DFrame" )
        self.AddStorageMenu:SetDeleteOnClose( true )
        self.AddStorageMenu:SetScreenLock( true ) --Keep within boundaries of screen
        self.AddStorageMenu:SetPos( gui.MousePos() )
        self.AddStorageMenu:DockMargin(0, 5, 0, 5)
        self.AddStorageMenu:SetSize(300,300)
        self.AddStorageMenu:SetTitle( "Add Storage Container" )

        local ResourceList = vgui.Create("DScrollPanel", self.AddStorageMenu)
        ResourceList:SetBackgroundColor(Color(255, 255, 255, 255))
        ResourceList:SetPaintBackground(true)
        ResourceList:Dock(FILL)

        for ResourceName, Resource in pairs(GAMEMODE:GetResourcesOfType(self.ResourceContainer.Type)) do
            if not self.InventoryItems[ResourceName] then
                local ResourceItem = vgui.Create("DButton", ResourceList)
                ResourceItem:Dock(TOP)
                ResourceItem:SetText(ResourceName)
                ResourceItem:SizeToContents()

                ResourceItem.DoClick = function()
                    self:AddStorageItem(ResourceName)
                    self.AddStorageMenu:Remove()
                end
            end
        end

        local CancelItem = vgui.Create("DButton", ResourceList)
        CancelItem:Dock(TOP)
        CancelItem:SetText("Cancel")
        CancelItem:SizeToContents()
        CancelItem.DoClick = function()
            self.AddStorageMenu:Remove()
        end

        ResourceList:SizeToContents()
        self.AddStorageMenu:SizeToContents()
        self.AddStorageMenu:InvalidateLayout()

        self.AddStorageMenu:MakePopup()
    end
end

--[[---------------------------------------------------------
   Inventory Item Resize Menu
-----------------------------------------------------------]]

function PANEL:ResizeInventoryItem(ResName)
    if not (IsValid(self.AddStorageMenu) or IsValid(self.ResizeStorageMenu)) then
        self.ResizeStorageMenu = vgui.Create( "DFrame" )
        self.ResizeStorageMenu:SetDeleteOnClose( true )
        self.ResizeStorageMenu:SetScreenLock( true ) --Keep within boundaries of screen
        self.ResizeStorageMenu:SetPos( gui.MousePos() )
        self.ResizeStorageMenu:SetSize(300,200)
        self.ResizeStorageMenu:SetTitle( "Resize Storage Container" )

        local Resource = self.ResourceContainer:GetStored()[ResName]       --The resource entry in the container
        local ResourceSize = Resource:GetSize()     --How many units of container storage one unit of this resource consumes

        local ContainerTotalSize = math.floor(self.ResourceContainer:GetSize() / ResourceSize)     --The over all total capacity of the resource container to hold this resource
        local ContainerResourceSize = self.ResourceContainer:GetMaxAmount(ResName)   --How much space in the container is allocated to this resource
        local UsableSize = (self.ResourceContainer:GetRemainingSize() / ResourceSize) + ContainerResourceSize --How much of the capacity of the container is free

        --Create the information shown at the top of the panel
        local InfoPanel = vgui.Create("DPanel", self.ResizeStorageMenu)
        InfoPanel:Dock(TOP)
        InfoPanel:DockMargin(0,5,0,5)
        InfoPanel:SetPaintBackground(false)

        local InfoLabel = vgui.Create("DLabel", InfoPanel)
        InfoLabel:SetText(string.format("Remaining Size: %d\n How much would you like to allocate?", UsableSize))
        InfoLabel:SizeToContents()
        InfoLabel:Dock(LEFT)

        InfoPanel:SizeToContents()

        --Create the percentage used slider
        --The percentage of the free space to allocate to this resource
        local PercentageSlider = vgui.Create("DNumSlider", self.ResizeStorageMenu)
        PercentageSlider:SetText("Percentage")
        PercentageSlider:SetMax(100)
        PercentageSlider:SetMin(0)
        PercentageSlider:SetValue((ContainerResourceSize / UsableSize) * 100)
        PercentageSlider:Dock(TOP)
        --PercentageSlider:DockMargin(50,5,50,5)

        --Create the amount used slider
        --The total amount of the free space to allocate to this resource
        local AmountSlider = vgui.Create("DNumSlider", self.ResizeStorageMenu)
        AmountSlider:SetText("Amount")
        AmountSlider:SetMax(UsableSize)
        AmountSlider:SetMin(0)
        AmountSlider:SetValue(ContainerResourceSize)
        AmountSlider:SetDecimals(0)
        AmountSlider:Dock(TOP)
        --AmountSlider:DockMargin(50,5,50,5)
        --AmountSlider:DockPadding(5,5,50,5)

        --Create the total percentage slider
        --The percentage of the total amount of space in the storage to allocate to this resource
        local TotalPercentageSlider = vgui.Create("DNumSlider", self.ResizeStorageMenu)
        TotalPercentageSlider:SetText("Total Percentage")
        TotalPercentageSlider:SetMax(100 * (UsableSize / ContainerTotalSize))
        TotalPercentageSlider:SetMin(0)
        TotalPercentageSlider:SetValue((ContainerResourceSize / ContainerTotalSize) * 100)
        TotalPercentageSlider:Dock(TOP)
        --TotalPercentageSlider:DockMargin(50,5,50,5)

        --Create the apply button
        local ApplyButton = vgui.Create("DButton", self.ResizeStorageMenu)
        ApplyButton:Dock(TOP)
        ApplyButton:SetText("Apply")
        ApplyButton:SizeToContents()

        local function ResizeSliders()
            PercentageSlider:SizeToContents()
            AmountSlider:SizeToContents()
            TotalPercentageSlider:SizeToContents()
            self.ResizeStorageMenu:InvalidateLayout()
        end

        --Used to prevent infinite loops of the sliders updating eachother
        local UpdateSliderValues = true
        --Set up the slider and button functions
        PercentageSlider.OnValueChanged = function(Slider, Value)
            if UpdateSliderValues then
                UpdateSliderValues = false
                local Percentage = Value / 100
                AmountSlider:SetValue(math.floor(UsableSize * Percentage))
                TotalPercentageSlider:SetValue(((UsableSize * Percentage) / ContainerTotalSize) * 100)
                --ResizeSliders()
                UpdateSliderValues = true
            end
        end

        AmountSlider.OnValueChanged = function(Slider, Value)
            if UpdateSliderValues then
                UpdateSliderValues = false
                PercentageSlider:SetValue((Value / UsableSize) * 100)
                TotalPercentageSlider:SetValue((Value / ContainerTotalSize) * 100)
                --ResizeSliders()
                UpdateSliderValues = true
            end
        end

        TotalPercentageSlider.OnValueChanged = function(Slider, Value)
            if UpdateSliderValues then
                UpdateSliderValues = false
                local Percentage = Value / 100
                AmountSlider:SetValue(math.floor(ContainerTotalSize * Percentage))
                PercentageSlider:SetValue(((ContainerTotalSize * Percentage) / UsableSize) * 100)
                --ResizeSliders()
                UpdateSliderValues = true
            end
        end

        ApplyButton.DoClick = function()
            self:ResizeStorageItem(ResName, AmountSlider:GetValue())
            self.ResizeStorageMenu:Remove()
        end

        self.ResizeStorageMenu:InvalidateLayout()
        --Pop the menu into existance
        self.ResizeStorageMenu:MakePopup()
    end
end

--[[---------------------------------------------------------
   InventoryItem Selection
-----------------------------------------------------------]]
function PANEL:SetSelected(item)
    --Deselect if another item is already selected
    if self.CurSelectedItem and self.CurSelectedItem ~= item then
        self.CurSelectedItem:Deselect()
    end

    --Select this one (since we do the selection event on InventoryItem, we only need to update the tracker)
    self.CurSelectedItem = item
end

function PANEL:SetResourceEnabled(ResName, DoEnable)
    if self.StorageMode ~= CONTAINERMODE_CUSTOMSPLIT then return end
    if DoEnable then
        if not self.ResourceContainer:GetResource(ResName) then
            self.ResourceContainer:AddResourceType(ResName, 0)
        end
    else
        if self.ResourceContainer:GetResource(ResName) then
            self.ResourceContainer:RemoveResourceType(ResName)
        end
    end

    self:UpdateInventoryList()
end


vgui.Register("InventoryPanel", PANEL, "Panel")