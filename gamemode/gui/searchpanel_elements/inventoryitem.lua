local PANEL = {}
-- Panel Settings/Materials
local GradientUpMat = Material("gui/gradient_up")
local GradientDownMat = Material("gui/gradient_down")

local SmallRarityFlag = {
    {
        x = 0,
        y = 0
    },
    {
        x = 10,
        y = 0
    },
    {
        x = 0,
        y = 10
    },
}

local NormalRarityFlag = {
    {
        x = 0,
        y = 0
    },
    {
        x = 25,
        y = 0
    },
    {
        x = 0,
        y = 25
    },
}

PANEL.PanelHeight = 85
PANEL.SubPanelHeight = 20
PANEL.IsSelected = false
-- Element Pointers
PANEL.RightClickMenu = nil
PANEL.ItemIcon = nil
PANEL.ItemLabel = nil
PANEL.ItemVolumeLabel = nil
PANEL.ItemMassLabel = nil
PANEL.ItemRarityFlag = nil
PANEL.ItemInfoPage = nil
PANEL.ItemInfoPopout = nil
-- Default Element Values
PANEL.Index = 0
PANEL.IconPath = "models/props_interiors/pot01a.mdl"
PANEL.ItemName = "[PLACEHOLDER ITEM NAME]"
PANEL.ItemAmount = 1337 -- Hurr Durr
PANEL.ItemMaxAmount = 9001 -- Herp a derp
PANEL.ItemMassPerUnit = 1
PANEL.ItemVolumePerUnit = 1
PANEL.ItemRarityColor = nil

--#region Initialization
function PANEL:Init()
    -- Panel variables
    self.Data = nil
    self.Resource = nil

    -- Keys that are searchable in `self.Data` by the search bar filter
    self.SearchableKeys = {"ResourceName"}

    -- Initial panel properties
    self:SetMouseInputEnabled(true)
    -- Fill horizontal, clip vertical
    self:SetSize(0, self.PanelHeight)
    -- Top Panel Section (doing this in two DPanels so we can use Docks without issue)
    local PanelTop = vgui.Create("DPanel", self)
    PanelTop:SetSize(0, self.PanelHeight - self.SubPanelHeight)
    PanelTop:Dock(TOP)
    PanelTop:SetPaintBackground(false)
    self.TopPanel = PanelTop
    -- Bottom Panel Section
    local PanelBottom = vgui.Create("DPanel", self)
    PanelBottom:SetSize(0, self.SubPanelHeight)
    PanelBottom:Dock(BOTTOM)
    PanelBottom:SetPaintBackground(false)
    self.BottomPanel = PanelBottom
    -- Item Icon Backing
    local BackPanel = vgui.Create("DPanel", PanelTop)
    local Size = self.PanelHeight - self.SubPanelHeight - 2
    BackPanel:SetSize(Size, Size)
    BackPanel:Dock(LEFT)
    BackPanel:DockMargin(1, 1, 1, 1)
    BackPanel:SetPaintBackground(true)
    BackPanel:SetBackgroundColor(Color(0, 0, 0, 255))
    self.BackPanel = BackPanel
    -- Item Icon (SpawnIcon for now, should probably be switched when I can figure out how to make a Material panel read from spawnicons/models/)
    --	Can also be switched for Material if someone feels like coming up with icons for each resource.
    self.ItemIcon = vgui.Create("SpawnIcon", BackPanel)
    self.ItemIcon:SetModel(self.IconPath)
    self.ItemIcon:Dock(FILL)
    self.ItemIcon:DockMargin(5, 5, 5, 5)
    -- Resource Title
    self.ItemLabel = vgui.Create("DLabel", PanelTop)
    self.ItemLabel:SetFont("SpawnMenuLarge")
    self.ItemLabel:SetText(self.ItemName)
    self.ItemLabel:SizeToContents()
    self.ItemLabel:Dock(LEFT)
    self.ItemLabel:DockMargin(5, 0, 0, 0)
    -- Resource Amount Label
    self.ItemAmountLabel = vgui.Create("DLabel", PanelTop)
    self.ItemAmountLabel:SetFont("SpawnMenuLarge")
    self.ItemAmountLabel:SetText(self.ItemAmount)
    self.ItemAmountLabel:SizeToContents()
    self.ItemAmountLabel:Dock(RIGHT)
    self.ItemAmountLabel:DockMargin(0, 0, 5, 0)
    -- Resource Volume Label
    self.ItemVolumeLabel = vgui.Create("DLabel", PanelBottom)
    self.ItemVolumeLabel:SetFont("SpawnMenuSmall")
    self.ItemVolumeLabel:SetText("Volume: 1000000000.00L")
    self.ItemVolumeLabel:SizeToContents()
    self.ItemVolumeLabel:Dock(LEFT)
    self.ItemVolumeLabel:DockMargin(2, 0, 0, 0)
    -- Resource Mass Label
    self.ItemMassLabel = vgui.Create("DLabel", PanelBottom)
    self.ItemMassLabel:SetFont("SpawnMenuSmall")
    self.ItemMassLabel:SetText("Mass: 1000000000.00kg")
    self.ItemMassLabel:SizeToContents()
    self.ItemMassLabel:Dock(LEFT)
    self.ItemMassLabel:DockMargin(10, 0, 0, 0)
    -- The below, oddly enough, lets us turn mouse input on for the entire panel, so yay for that
    PanelTop:SetMouseInputEnabled(false)
    PanelBottom:SetMouseInputEnabled(false)
    BackPanel:SetMouseInputEnabled(false)
    self.ItemIcon:SetMouseInputEnabled(false)
    self.ItemLabel:SetMouseInputEnabled(false)
    self.ItemAmountLabel:SetMouseInputEnabled(false)
    self.ItemVolumeLabel:SetMouseInputEnabled(false)
    self.ItemMassLabel:SetMouseInputEnabled(false)

    -- Custom painting for the icon backing panel
    self.BackPanel.Paint = function(Panel, W, H)
        --surface.SetMaterial(GradientUpMat)
        --surface.SetDrawColor(self.ItemRarityColor or Color(64, 64, 64, 255))
        --surface.DrawTexturedRect(0, 0, W, H)
        draw.RoundedBox(5, 0, 0, W, H, Color(0, 0, 0, 255))

        --Rarity flag
        if self.ItemRarityColor then
            surface.SetDrawColor(self.ItemRarityColor)
            draw.NoTexture()
            surface.DrawPoly(self.UseSmallDisplayMode and SmallRarityFlag or NormalRarityFlag)
        end
    end

    self:DockMargin(2, 1, 2, 1)
    self.Initialized = true
end

--#endregion
--#region Accessors/Settings
function PANEL:SetItemIcon(modelPath)
    self.IconPath = modelPath
    self.ItemIcon:SetModel(self.IconPath)
end

function PANEL:GetItemIcon()
    return self.IconPath
end

function PANEL:SetItemName(name)
    self.ItemName = name
    self.ItemLabel:SetText(self.ItemName)
    self.ItemLabel:SizeToContents()
end

function PANEL:GetItemName()
    return self.ItemName
end

function PANEL:SetAmount(amount)
    self.ItemAmount = amount
    self.ItemAmountLabel:SetText(string.format("%.2f/%.2f", self.ItemAmount, self.ItemMaxAmount))
    self.ItemAmountLabel:SizeToContents()
    self.ItemVolumeLabel:SetText(string.format("Volume: %.2fL", self.ItemAmount * self.ItemVolumePerUnit))
    self.ItemVolumeLabel:SizeToContents()
    self.ItemMassLabel:SetText(string.format("Mass: %.2fkg", self.ItemAmount * self.ItemMassPerUnit))
    self.ItemMassLabel:SizeToContents()
end

function PANEL:SetMaxAmount(amount)
    self.ItemMaxAmount = amount
end

function PANEL:GetAmount()
    return self.ItemAmount
end

function PANEL:SetVolumePerUnit(volume)
    self.ItemVolumePerUnit = volume
    self.ItemVolumeLabel:SetText(string.format("Volume: %.2fL", self.ItemAmount * self.ItemVolumePerUnit))
    self.ItemVolumeLabel:SizeToContents()
end

function PANEL:GetItemVolumePerUnit()
    return self.ItemVolumePerUnit
end

function PANEL:SetMassPerUnit(mass)
    self.ItemMassPerUnit = mass
    self.ItemMassLabel:SetText(string.format("Mass: %.2fkg", self.ItemAmount * self.ItemMassPerUnit))
    self.ItemMassLabel:SizeToContents()
end

function PANEL:GetMassPerUnit()
    return self.ItemMassPerUnit
end

function PANEL:SetResourceRarityColor(RarityColor)
    self.ItemRarityColor = RarityColor
end

function PANEL:LoadFromResource(Resource)
    local ResourceName = Resource:GetName()
    self.Resource = Resource
    local ResourceInfo = GAMEMODE:GetResourceInfoTable(ResourceName)
    self:SetItemName(ResourceName)
    self:SetMaxAmount(Resource:GetMaxAmount())
    self:SetAmount(Resource:GetAmount())
    self:SetVolumePerUnit(ResourceInfo.Volume)
    self:SetMassPerUnit(ResourceInfo.Mass)
    self:SetItemIcon(ResourceInfo.Icon)
    self:SetResourceRarityColor(GAMEMODE:GetItemRarityColor(ResourceInfo.Rarity))

    self.Data = {
        Amount = Resource:GetAmount(),
        MaxAmount = Resource:GetMaxAmount(),
        ResourceName = ResourceName,
        ResourceVolume = ResourceInfo.Volume,
        ResourceMass = ResourceInfo.Mass,
        ResourceRarity = ResourceInfo.Rarity,
        ResourceType = GAMEMODE:GetResourceStorageType(ResourceName)
    }
end

--#endregion
--#region Painting
function PANEL:Paint(w, h)
    --Draw Backing Rect
    surface.SetDrawColor(Color(64, 64, 64, 255))
    surface.DrawRect(0, 0, w, h)

    --Selection Glow
    if self.IsSelected then
        surface.SetDrawColor(Color(84, 144, 96, 255))
    else
        surface.SetDrawColor(Color(96, 96, 96, 255))
    end

    --Top Bevel Background
    surface.SetMaterial(GradientUpMat)
    surface.DrawTexturedRect(0, 0, w, h - self.SubPanelHeight)

    --Use a brighter color for the resource percentage bar
    if self.IsSelected then
        surface.SetDrawColor(Color(23, 216, 61))
    else
        surface.SetDrawColor(Color(124, 124, 124))
    end

    --Top Bevel Foreground (Percentage Bar)
    surface.SetMaterial(GradientUpMat)
    surface.DrawTexturedRect(0, 0, w * (self.ItemAmount / self.ItemMaxAmount), h - self.SubPanelHeight)

    --Go back to the dull color
    if self.IsSelected then
        surface.SetDrawColor(Color(84, 144, 96, 255))
    else
        surface.SetDrawColor(Color(96, 96, 96, 255))
    end

    --Bottom Bevel
    surface.SetMaterial(GradientDownMat)
    surface.DrawTexturedRect(0, h - self.SubPanelHeight, w, self.SubPanelHeight)
    --Separator
    surface.SetDrawColor(Color(0, 0, 0, 255))
    surface.DrawLine(0, h - self.SubPanelHeight, w, h - self.SubPanelHeight)
end

--#endregion
--#region Panel Hooks
---Called after the panel is resized, with the new sizes passed in
---@param W number #The new width of the panel
---@param H number #The new height of the panel
function PANEL:OnSizeChanged(W, H)
    if not self.Initialized then return end
    local IsSmall = W < 256
    local SmallToLarge = self.UseSmallDisplayMode and not IsSmall
    local LargeToSmall = IsSmall and not self.UseSmallDisplayMode

    -- Update sizes
    if LargeToSmall then
        self.PanelHeight = 40
        self.SubPanelHeight = 18
        self.ItemLabel:SetFont("SpawnMenuMedium")
        self.ItemAmountLabel:SetFont("SpawnMenuSmall")
    elseif SmallToLarge then
        self.PanelHeight = 85
        self.SubPanelHeight = 20
        self.ItemLabel:SetFont("SpawnMenuLarge")
        self.ItemAmountLabel:SetFont("SpawnMenuLarge")
    end

    if LargeToSmall or SmallToLarge then
        self:SetHeight(self.PanelHeight)
        self.TopPanel:SetHeight(self.PanelHeight - self.SubPanelHeight)
        self.BottomPanel:SetHeight(self.SubPanelHeight)
        local BackPanelSize = self.PanelHeight - self.SubPanelHeight - 2
        self.BackPanel:SetSize(BackPanelSize, BackPanelSize)
        self:InvalidateLayout()
    end

    self.UseSmallDisplayMode = IsSmall
end

--#endregion
--#region Element Interaction
---Set the index of the element representing its position in the parent searchpanel
---@param Index integer #The index of the element
function PANEL:SetIndex(Index)
    self.Index = Index
end

---Get the index of the element in the parent searchpanel's data set
---@return integer Index #The index of the element
function PANEL:GetIndex()
    return self.Index
end

---Set/Reset the state of the element's selection. Used to tell the panel how to draw itself.
---@param Selected boolean #The selection state
function PANEL:SetSelected(Selected)
    self.IsSelected = Selected
end

---Load the element panel with its initial data from the searchpanel's data set
---@param Data table #The data to load
function PANEL:SetData(Data)
    self:LoadFromResource(Data)
    self.Data.SearchableKeys = self.SearchableKeys -- Put the searchable keys table into the element's data
end

---Returns the element panel's data table
---@return table Data
function PANEL:GetData()
    return self.Data
end

--#endregion
vgui.Register("SearchElement_InventoryItem", PANEL, "Panel")