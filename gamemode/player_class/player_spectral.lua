
AddCSLuaFile()
DEFINE_BASECLASS( "player_spacecombat" )

local PLAYER = {}

PLAYER.DuckSpeed			= 0.1		-- How fast to go from not ducking, to ducking
PLAYER.UnDuckSpeed			= 0.1		-- How fast to go from ducking, to not ducking

--
-- Creates a Taunt Camera
--
PLAYER.TauntCam = TauntCamera()

--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.WalkSpeed 			= 200
PLAYER.RunSpeed				= 400


PLAYER.BaseResistances = {

}

-- A list of resource percentages that needs to be met for the player to breath in an atmosphere
PLAYER.BreathableAtmosphere = {
    ["Dorito Monster Concentrate"] = 0.05
}

PLAYER.CanBreatheUnderWater = false

PLAYER.MaxBreath = 15

-- The temperature that the player's body will try to maintain
PLAYER.IdealTemperature = 310

-- The maximum temperature of the player's body before they take damage
PLAYER.MaxTemperature = 325

-- The minimum temperature of the player's body before they take damage
PLAYER.MinTemperature = 295

-- The highest environment temperature before life support starts to kick in
PLAYER.MaxSafeEnvironmentTemperature = 315

-- The lowest environment temperature before life support starts to kick in
PLAYER.MinSafeEnvironmentTemperature = 270

-- The highest atmosphere pressure the player can withstand
PLAYER.MaxPressure = 2

-- The lowest atmosphere pressure the player can withstand
PLAYER.MinPressure = 0.2

PLAYER.EnergyStorage = 10000

PLAYER.CargoStorage = 3500

PLAYER.AmmoStorage = 2000

PLAYER.AirStorage = 1500

PLAYER.CoolantStorage = 3000

PLAYER.CoolantName = "Methanol"

PLAYER.CoolantRegenRate = 1

PLAYER.CoolantConsumeRate = 5

PLAYER.HeatingEnergyConsumeRate = 15

PLAYER.AirName = "Dorito Monster Concentrate"

PLAYER.AirRegenRate = 1

PLAYER.AirConsumeRate = 2

function PLAYER:Loadout()
	self.Player:RemoveAllAmmo()

	self.Player:Give( "gmod_tool" )
	self.Player:Give( "weapon_physgun" )
	self.Player:Give( "personal_miner" )

	self.Player:SwitchToDefaultWeapon()
end

player_manager.RegisterClass( "player_spectral", PLAYER, "player_spacecombat" )
